
// TestUU.h : main header file for the TestUU application
//
#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"       // main symbols

#include <uu.h>
#pragma comment(lib, "uu")

// CTestUUApp:
// See TestUU.cpp for the implementation of this class
//

class CTestUUApp : public CWinApp
{
public:
	CTestUUApp();


// Overrides
public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	virtual int Run();

// Implementation

public:
	afx_msg void OnAppAbout();
	DECLARE_MESSAGE_MAP()

	uu::QtApp *qtApp;
};

extern CTestUUApp theApp;
