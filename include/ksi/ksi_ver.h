#define KSI_VERSION "3.9.1"
#define KSI_LIB_VERSION "3.9"
#define KSI_MAJOR_VERSION 3
#define KSI_MINOR_VERSION 9
#define KSI_PATCH_LEVEL 1
#define KSI_CPU "x86"
#define KSI_OS "windows"
#define KSI_SCM_LIB_DIR "c:/ksi/share/ksi"
#define KSI_INSTAL_INCLUDE_DIR "c:/ksi/include"
#define KSI_INSTAL_LIB_DIR "c:/ksi/lib"
#define KSI_INSTAL_BIN_DIR "c:/ksi/bin"
#define KSI_INSTAL_INFO_DIR "c:/ksi/share/info"
#define KSI_INSTAL_MAN_DIR "c:/ksi/share/man"
#define KSI_BUILD_CFLAGS ""
#define KSI_BUILD_LIBS ""
#define KSI_DLLNAME "ksi-" KSI_VERSION ".dll"