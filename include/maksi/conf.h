/* -*- mode:C++; tab-width:8 -*- */
/*
 *
 * Copyright (C) 2012-2018, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 *
 */

/**
 * @file    conf.h
 * @author  ivan demakov <ksion@users.sourceforge.net>
 * @date    Tue Dec  4 20:43:26 2012
 *
 * @brief   conf class
 *
 */

#ifndef MAKSI_CONF_H
#define MAKSI_CONF_H


#include <QUrl>

#include "iface.h"
#include "xml.h"
#include "type.h"


//! maksi namespace
namespace maksi
{

class MAKSI_EXPORT Conf
{
public:
    Conf(Maksi *maksi, const QUrl &url);
    ~Conf();

    const QUrl &url() const { return m_url; }
    Maksi *maksi() const { return m_maksi; }

    bool load();

protected:
    bool load(xml::item_t conf);
    bool imported(xml::item_t conf);
    bool initType(xml::item_t conf, const QString &targetNamespace);
    bool initEngine(xml::item_t conf);
    bool initScript(xml::item_t conf);
    bool initPackage(xml::item_t conf);

    xml::item_t m_import;
    QString m_defaultNamespace;

private:
    Maksi *m_maksi;
    QUrl m_url;

    struct Type {
        Type() : m_type(0), m_targetName(0), m_conf(0) {}
        Type(maksi::Type *type, name_t name, xml::item_t conf) : m_type(type), m_targetName(name), m_conf(conf) {}
        Type(const Type &t)  : m_type(t.m_type), m_targetName(t.m_targetName), m_conf(t.m_conf) {}
        Type &operator=(const Type &t) { m_type=t.m_type; m_targetName=t.m_targetName; m_conf=t.m_conf; return *this; }

        maksi::Type *m_type;
        name_t m_targetName;
        xml::item_t m_conf;
    };

    QList<Type> m_types;
};

};

#endif

/* End of file */
