/* -*- mode:C++; tab-width:8 -*- */
/*
 *
 * Copyright (C) 2011-2018, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 *
 */

#ifndef MAKSI_FUNCTOR_H
#define MAKSI_FUNCTOR_H

#include "iface.h"


//! maksi namespace
namespace maksi
{

class MAKSI_EXPORT Functor
{
public:
    Functor() : m_permanent(false) {}
    virtual ~Functor();

    virtual int argCount() const = 0;
    virtual QString argName(int n) const = 0;
    virtual QVariant arg(int n) const = 0;
    virtual QVariant arg(const QString &name) const = 0;
    virtual void setArg(int n, const QVariant &val) = 0;
    virtual void setArg(const QString &name, const QVariant &val) = 0;
    virtual QVariant eval() = 0;

    bool isPermanent() const { return m_permanent; }
    void setPermanent() { m_permanent = true; }

private:
    bool m_permanent;
};

};


#endif
