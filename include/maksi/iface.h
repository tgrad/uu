/* -*- mode:C++; tab-width:8 -*- */
/*
 *
 * Copyright (C) 2013-2015, 2017, 2018, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 *
 */

/**
 * @file    iface.h
 * @author  ivan demakov <ivan.demakov@gmail.com>
 * @date    Thu Aug 15 04:37:23 2013
 *
 * @brief   Public intarface to maksi library
 *
 */

#ifndef MAKSI_IFACE_H
#define MAKSI_IFACE_H

#include <stdint.h>

#include <QUrl>
#include <QString>
#include <QVariant>
#include <QSharedPointer>


#ifdef MAKSI_LIB
# define MAKSI_EXPORT Q_DECL_EXPORT
#else
# define MAKSI_EXPORT Q_DECL_IMPORT
#endif


class Maksi;
typedef class Maksi *maksi_t;

Q_DECLARE_OPAQUE_POINTER(maksi_t);
Q_DECLARE_METATYPE(maksi_t);


//! maksi namespace
namespace maksi
{

//! maksi message types
enum message_t {
    MESSAGE_QUIET,
    MESSAGE_ALARM = MESSAGE_QUIET,
    MESSAGE_ERROR,
    MESSAGE_WARN,
    MESSAGE_NOTE,
    MESSAGE_INFO,
    MESSAGE_DEBUG,
    MESSAGE_TRACE,
    MESSAGE_PROGRESS,
    MESSAGE_ALL
};

class Name;
typedef class Name *name_t;

class Functor;
typedef class Functor *functor_t;

class Item;
class Type;

};

Q_DECLARE_OPAQUE_POINTER(maksi::name_t);
Q_DECLARE_METATYPE(maksi::name_t);

Q_DECLARE_OPAQUE_POINTER(maksi::functor_t);
Q_DECLARE_METATYPE(maksi::functor_t);


//! maksi namespace
namespace maksi
{

/** Get maksi instance
 *
 * The function returns the maksi instance with \a name, if it exist.
 * If the instance with such a name is not exist, the function creates the new one.
 *
 * @param name name of instance
 *
 * @return maksi instance
 */
MAKSI_EXPORT
maksi_t
newMaksi(const QString &name);


/** Free \a maksi instance
 *
 * @param maksi maksi instance
 */
MAKSI_EXPORT
void
free(maksi_t maksi);


/** Set message handler for \a maksi instance
 *
 *
 * @param maksi maksi instance
 * @param message new message handler
 */
MAKSI_EXPORT
void
setMessageHandler(maksi_t maksi, void (*message)(int, const QString &));


/** Load config
 *
 *
 * @param maksi maksi instance
 * @param confURL config url
 *
 * @return \a true if success, \a false otherwice
 */
MAKSI_EXPORT
bool
loadConfig(maksi_t maksi, const QString &confURL);


/** Get xml tag name
 *
 *
 * @param localName
 * @param namespaceURI
 *
 * @return tag name
 */
MAKSI_EXPORT
name_t
qname(const QString &localName, const QString &namespaceURI=QString());


/** Get script context
 *
 *
 * @param maksi
 * @param scriptName
 *
 * @return
 */
MAKSI_EXPORT
maksi::functor_t
newFunc(maksi_t maksi, const QString &scriptName);


/** Free context
 *
 *
 * @param context
 */
MAKSI_EXPORT
void
free(maksi::functor_t context);


/** Get argument value from context
 *
 *
 * @param context
 * @param name
 *
 * @return
 */
MAKSI_EXPORT
QVariant
arg(maksi::functor_t context, const QString &name);


/** Set argument value to context
 *
 *
 * @param context
 * @param name
 * @param value
 */
MAKSI_EXPORT
void
setArg(maksi::functor_t context, const QString &name, const QVariant &value);


/** Evaluate context
 *
 *
 * @param context
 */
MAKSI_EXPORT
QVariant
eval(maksi::functor_t context);

}

#endif

/* End of file */
