/* -*- mode:C++; tab-width:8 -*- */
/*
 *
 * Copyright (C) 2011-2018, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 *
 */

/**
 * @file    item.h
 * @author  ivan demakov <ivan@demakov.net>
 * @date    Fri May 13 17:12:44 2011
 *
 * @brief   maksi item interface
 *
 */

#ifndef MAKSI_ITEM_H
#define MAKSI_ITEM_H

#include "iface.h"
#include "xml.h"
#include "functor.h"

#include <QMap>
#include <QUrl>
#include <QVariant>
#include <QDateTime>


class QXmlStreamWriter;
class QJsonValue;


//! maksi namespace
namespace maksi
{

class Type;


//! Maski Item interface
class MAKSI_EXPORT Item
{
protected:
    friend class Type;
    friend class TypeFactory;
    class Private;
    Private *d;

public:
    Item(); /**< Default Constructor */
    Item(name_t name, const Type &type); /**< Constructor */
    Item(Private *x);  /**< Constructor */

    virtual ~Item();    /**< Destructor */

    Item(const Item &x); /**< Constructor */
    Item &operator=(const Item &x); /**< Constructor */


    /** Clear item.
     *
     */
    void clear();


    /** Clone item.
     *
     * @return item copy
     */
    Item clone() const;


    /** Get item name.
     *
     * @return item name
     */
    name_t name() const;


    /** Set item name.
     *
     * @param name
     */
    void setName(name_t name);


    /** Get item type
     *
     * @return item type name
     */
    const Type &type() const;


    /** Test if the item is valid.
     *
     * @return is item valid?
     */
    bool isValid() const;


    /** Compare items
     *
     * @param elem item to compare with
     *
     * @return true if item is equal to \a value, false otherwise
     */
    bool isEqual(const Item &value) const;


    /** Get attribute name
     *
     * Convert string to attribute name.
     *
     * @param name string with attribute name
     *
     * @return attribute name
     */
    name_t attributeName(const QString &name) const;


    /** Get attribute names
     *
     * Default implementation returns list of attributes defined in the type of item.
     *
     * @return list of attribute names
     */
    QVector<name_t> attributeNames() const;


    /** Test if the attribute exists
     *
     * @param name attribute name
     *
     * @return is the attribute exists in the item?
     */
    bool hasAttribute(name_t name) const;
    bool hasAttribute(const QString &name) const;


    /** Test if the attribute has fixed value (i.e. read-only)
     *
     * @param name attribute name
     *
     * @return is the attribute read-only?
     */
    bool attributeFixed(name_t name) const;
    bool attributeFixed(const QString &name) const;


    /** Get attribute value
     *
     * @param name attribute name
     *
     * @return attribute value
     */
    Item attribute(name_t name) const;
    Item attribute(const QString &name) const;


    /** Set attribute value
     *
     * @param name attribute name
     * @param value new value
     */
    bool setAttribute(name_t name, const Item &value);
    bool setAttribute(const QString &name, const Item &value);


    /** Get the number of child items
     *
     * @return the number of items
     */
    int itemCount() const;


    /** Get the child item
     *
     * @param index the order number of item
     *
     * @return the item value
     */
    Item item(int index) const;


    /** Append new child item
     *
     * @param value value for item initialization
     *
     * @return is append successful
     */
    bool append(const Item &value);


    /// conversion to/from QJson
    QJsonValue toJson() const;


    /// conversion to/from QVariant
    QVariant toVariant() const;
    static Item newVariant(const QVariant &value);

    /// conversion to/from QObject
    QObject *toQObject() const;
    static Item newQObject(QObject *value);

    /// conversion to/from string
    QString toString() const;
    bool toString(QString *val) const;
    static Item newString(name_t name, const Type &type, const QString &val);
    static Item newString(const QString &val);
    static Item newToken(const QString &val);
    static Item newName(const QString &val);

    /// conversion to/from QName
    bool toQName(name_t *val) const;
    static Item newQName(name_t name, const Type &type, name_t val);
    static Item newQName(name_t val);

    /// conversion to/from byte array
    bool toByteArray(QByteArray *val) const;
    static Item newByteArray(name_t name, const Type &type, const QByteArray &val);
    static Item newByteArray(const QByteArray &val);

    /// conversion to/from bool
    bool toBool() const;
    bool toBool(bool *val) const;
    static Item newBool(name_t name, const Type &type, bool val);
    static Item newBool(bool val);

    /// conversion to/from int
    int32_t toInt() const;
    bool toInt(int32_t *val) const;
    static Item newInt(name_t name, const Type &type, int32_t val);
    static Item newInt(int32_t val);

    /// conversion to/from uint
    uint32_t toUInt() const;
    bool toUInt(uint32_t *val) const;
    static Item newUInt(name_t name, const Type &type, uint32_t val);
    static Item newUInt(uint32_t val);

    /// conversion to/from long
    int64_t toLong() const;
    bool toLong(int64_t *val) const;
    static Item newLong(name_t name, const Type &type, int64_t val);
    static Item newLong(int64_t val);

    /// conversion to/from ulong
    uint64_t toULong() const;
    bool toULong(uint64_t *val) const;
    static Item newULong(name_t name, const Type &type, uint64_t val);
    static Item newULong(uint64_t val);

    /// conversion to/from float
    float toFloat() const;
    bool toFloat(float *val) const;
    static Item newFloat(name_t name, const Type &type, float val);
    static Item newFloat(float val);

    /// conversion to/from double
    double toDouble() const;
    bool toDouble(double *val) const;
    static Item newDouble(name_t name, const Type &type, double val);
    static Item newDouble(double val);

    /// conversion to/from Url
    bool toUrl(QUrl *val) const;
    static Item newUrl(name_t name, const Type &type, QUrl val);
    static Item newUrl(QUrl val);

    /// conversion to/from DateTime
    bool toDate(QDateTime *val) const;
    static Item newDate(name_t name, const Type &type, const QDateTime &val);
    static Item newDate(const QDateTime &val);

    /// conversion to/from Date
    bool toDate(QDate *val) const;
    static Item newDate(name_t name, const Type &type, const QDate &val);
    static Item newDate(const QDate &val);

    /// conversion to/from Time
    bool toTime(QTime *val) const;
    static Item newTime(name_t name, const Type &type, const QTime &val);
    static Item newTime(const QTime &val);

    /// list
    static Item newList(name_t name, const Type &type);

    /// struct
    static Item newStruct(name_t name, const Type &type);


    class MAKSI_EXPORT Method : public maksi::Functor {
    public:
	Method(const QString &name) : m_name(name) {}
	~Method() override;

	QString argName(int n) const override;
	QVariant arg(int n) const override;
	QVariant arg(const QString &name) const override;
	void setArg(int n, const QVariant &val) override;
	void setArg(const QString &name, const QVariant &val) override;

	int argCount() const override = 0;
	QVariant eval() override = 0;

	const QString name() const { return m_name; }

    protected:
	QString m_name;
    };

    class MAKSI_EXPORT Method0 : public Method {
    public:
	typedef QVariant (Item::Private::*fun_t)();

	Method0(const QString &name, Item::Private *d, fun_t fun) : Method(name), m_d(d), m_fun(fun) {}
	~Method0() override;

	int argCount() const override;
	QVariant eval() override;

    protected:
	Item::Private *m_d;
	fun_t m_fun;
    };

    class MAKSI_EXPORT Method1 : public Method {
    public:
	typedef QVariant (Item::Private::*fun_t)(QVariant);

	Method1(const QString &name, Item::Private *d, fun_t fun) : Method(name), m_d(d), m_fun(fun) {}
	~Method1() override;

	int argCount() const override;
	QVariant arg(int n) const override;
	void setArg(int n, const QVariant &val) override;
	QVariant eval() override;

    protected:
	Item::Private *m_d;
	fun_t m_fun;
	QVariant m_val;
    };

    class MAKSI_EXPORT Method2 : public Method {
    public:
	typedef QVariant (Item::Private::*fun_t)(QVariant, QVariant);

	Method2(const QString &name, Item::Private *d, fun_t fun) : Method(name), m_d(d), m_fun(fun) {}
	~Method2() override;

	int argCount() const override;
	QVariant arg(int n) const override;
	void setArg(int n, const QVariant &val) override;
	QVariant eval() override;

    protected:
	Item::Private *m_d;
	fun_t m_fun;
	QVariant m_val0, m_val1;
    };

    class MAKSI_EXPORT Method3 : public Method {
    public:
	typedef QVariant (Item::Private::*fun_t)(QVariant, QVariant, QVariant);

	Method3(const QString &name, Item::Private *d, fun_t fun) : Method(name), m_d(d), m_fun(fun) {}
	~Method3() override;

	int argCount() const override;
	QVariant arg(int n) const override;
	void setArg(int n, const QVariant &val) override;
	QVariant eval() override;

    protected:
	Item::Private *m_d;
	fun_t m_fun;
	QVariant m_val0, m_val1, m_val2;
    };

    class MAKSI_EXPORT MethodN : Method {
    public:
	typedef QVariant (Item::Private::*fun_t)(QVector<QVariant>);

	MethodN(const QString &name, Item::Private *d, fun_t fun) : Method(name), m_d(d), m_fun(fun) {}
	~MethodN() override;

	int argCount() const override;
	QVariant arg(int n) const override;
	void setArg(int n, const QVariant &val) override;
	QVariant eval() override;

    protected:
	Item::Private *m_d;
	fun_t m_fun;
	QVector<QVariant> m_vals;
    };

    /** Get methods
     *
     * @return list of methods
     */
    QVector<QSharedPointer<Method>> methods() const;


    /** Call method
     *
     * @param name method name
     * @param args method arguments
     *
     * @return result
     */
    QVariant call(const QString &name, QVector<QVariant> args);


    /** Convert item to xml
     *
     * @return
     */
    QString itemXmlText() const;


    /** Write item to xml stream
     *
     */
    void itemWriteXml(QXmlStreamWriter &stream, name_t name, QList<QString> ns=QList<QString>()) const;
};

};


Q_DECLARE_METATYPE(maksi::Item);


inline bool operator==(const maksi::Item &x, const maksi::Item &y) { return x.isEqual(y); }
inline bool operator!=(const maksi::Item &x, const maksi::Item &y) { return !x.isEqual(y); }


#endif

/* End of file */
