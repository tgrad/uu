/* -*- mode:C++; tab-width:8 -*- */
/*
 *
 * Copyright (C) 2011-2018, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 *
 */

/**
 * @file    item_pri.h
 * @author  ivan demakov <ivan@demakov.net>
 *
 * @brief   maksi item private
 *
 */

#ifndef ITEM_PRI_H
#define ITEM_PRI_H

#include "item.h"
#include "type.h"

#include <QSharedData>
#include <QPointer>


//! maksi namespace
namespace maksi
{

class MAKSI_EXPORT Item::Private : public QSharedData
{
protected:
    name_t m_name;
    Type m_type;

public:
    Private() : m_name(0) {}
    Private(name_t name, const Type &type) : m_name(name), m_type(type) {}

    virtual ~Private();

    virtual Private *clone() const;

    virtual QVector<name_t> attributeNames() const;
    virtual bool hasAttribute(name_t name) const;
    virtual bool attributeFixed(name_t name) const;
    virtual Item attribute(name_t name) const;
    virtual bool setAttribute(name_t name, const Item &value);

    virtual int itemCount() const;
    virtual Item item(int index) const;
    virtual bool itemAppend(const Item &value);

    virtual bool isItemValid() const;
    virtual bool isItemEqual(const Item &value) const;

    virtual QVariant toVariant() const;
    virtual QObject *toQObject() const;

    virtual bool toString(QString *val) const;
    virtual bool toQName(name_t *val) const;
    virtual bool toByteArray(QByteArray *val) const;
    virtual bool toBool(bool *val) const;
    virtual bool toInt(int32_t *val) const;
    virtual bool toUInt(uint32_t *val) const;
    virtual bool toLong(int64_t *val) const;
    virtual bool toULong(uint64_t *val) const;
    virtual bool toFloat(float *val) const;
    virtual bool toDouble(double *val) const;
    virtual bool toUrl(QUrl *val) const;
    virtual bool toDate(QDateTime *val) const;
    virtual bool toDate(QDate *val) const;
    virtual bool toTime(QTime *val) const;

    virtual QVector<QSharedPointer<Item::Method>> itemMethods();
    virtual QVariant call(const QString &name, QVector<QVariant> args);

    name_t name() const { return m_name; }
    void setName(name_t x) { m_name = x; }

    const Type &type() const { return m_type; }
    void setType(const Type &x) { m_type = x; }
};


class MAKSI_EXPORT Item_Variant : public Item
{
public:
    class MAKSI_EXPORT Private : public Item::Private
    {
    public:
	Private(name_t name, const Type &type, const QVariant &val) : Item::Private(name, type), m_val(val) {}
	~Private() override;
	Private *clone() const override;

	bool isItemValid() const override;
	bool isItemEqual(const Item &value) const override;

	QVariant toVariant() const override;
	bool toString(QString *val) const override;

    protected:
	QVariant m_val;
    };
};

class MAKSI_EXPORT Item_QObject : public Item
{
public:
    class MAKSI_EXPORT Private : public Item::Private
    {
    public:
	Private(name_t name, const Type &type, QObject *val);
	~Private() override;
	Private *clone() const override;

	QVector<name_t> attributeNames() const override;
	bool hasAttribute(name_t name) const override;
	Item attribute(name_t name) const override;
	bool setAttribute(name_t name, const Item &value) override;

	bool isItemValid() const override;
	bool isItemEqual(const Item &value) const override;

	QVariant toVariant() const override;
	QObject *toQObject() const override;
	bool toString(QString *val) const override;

	QVector<QSharedPointer<Item::Method>> itemMethods() override;
	QVariant call(const QString &name, QVector<QVariant> args) override;

    protected:
	QPointer<QObject> m_val;
	QVector<QSharedPointer<Item::Method>> m_methods;
    };

    class MAKSI_EXPORT Method : public Item::Method {
    public:
	Method(const QString &name, QObject *val, int index) : Item::Method(name), m_val(val), m_index(index) {}
	~Method() override;

	int argCount() const override;
	QVariant eval() override;

    protected:
	QPointer<QObject> m_val;
	int m_index;
    };
};

class MAKSI_EXPORT Item_String : public Item
{
public:
    class MAKSI_EXPORT Private : public Item::Private
    {
    public:
	Private(name_t name, const Type &type, const QString &val) : Item::Private(name, type), m_val(val) {}
	~Private() override;
	Private *clone() const override;

	bool isItemValid() const override;
	bool isItemEqual(const Item &value) const override;

	QVariant toVariant() const override;
	bool toString(QString *val) const override;

    protected:
	QString m_val;
    };
};


#define MAKSI_ITEM_ATOMIC_PRIVATE(C, T, F)			\
    class Private : public Item::Private			\
    {								\
    public:							\
	Private(name_t name, const Type &type, const T &val) :	\
            Item::Private(name, type), m_val(val) {}		\
	~Private() override;					\
	Private *clone() const override;			\
	bool isItemValid() const override;			\
	bool isItemEqual(const Item &val) const override;	\
	QVariant toVariant() const override;		\
	bool toString(QString *val) const override;		\
        virtual bool F(T *val) const override;			\
								\
    protected:							\
	T m_val;						\
    };

#define MAKSI_ITEM_ATOMIC_PRIVATE_DEF0(C,T,F)		\
    C::Private::~Private() {}				\
    C::Private *C::Private::clone() const {		\
        return new Private(m_name, m_type, m_val);	\
    }							\
    bool C::Private::isItemEqual(const Item &x) const {	\
	T val; if (x.F(&val)) { return m_val == val; }	\
	return false;					\
    }							\
    QVariant C::Private::toVariant() const {	\
	return QVariant::fromValue(m_val);		\
    }							\
    bool C::Private::F(T *val) const {			\
	if (val) *val = m_val;				\
	return true;					\
    }

#define MAKSI_ITEM_ATOMIC_PRIVATE_DEF(C,T,F,S)			\
    MAKSI_ITEM_ATOMIC_PRIVATE_DEF0(C,T,F)			\
    bool C::Private::isItemValid() const { return true; }	\
    bool C::Private::toString(QString *val) const {		\
        if (val) *val = S;					\
        return true;						\
    }

#define MAKSI_ITEM_SIMPLE_PRIVATE(C,T,F)	\
    MAKSI_ITEM_ATOMIC_PRIVATE(C,T,F)

#define MAKSI_ITEM_SIMPLE_PRIVATE_DEF0(C,T,F)	\
    MAKSI_ITEM_ATOMIC_PRIVATE_DEF0(C,T,F)

#define MAKSI_ITEM_SIMPLE_PRIVATE_DEF(C,T,F,S)		\
    MAKSI_ITEM_SIMPLE_PRIVATE_DEF0(C,T,F)		\
    bool C::Private::isItemValid() const {		\
        return m_val.isValid();				\
    }							\
    bool C::Private::toString(QString *val) const {	\
        if (val) *val = S;				\
        return true;					\
    }

#define MAKSI_ITEM_NUMERIC_PRIVATE(C,T)				\
    class Private : public Item::Private			\
    {								\
    public:							\
	Private(name_t name, const Type &type, const T &val) :	\
            Item::Private(name, type), m_val(val) {}		\
	~Private() override;					\
	Private *clone() const override;			\
	bool isItemValid() const override;			\
	bool isItemEqual(const Item &val) const override;	\
	QVariant toVariant() const override;			\
	bool toString(QString *val) const override;		\
	bool toInt(int32_t *val) const override;		\
	bool toUInt(uint32_t *val) const override;		\
	bool toLong(int64_t *val) const override;		\
	bool toULong(uint64_t *val) const override;		\
	bool toFloat(float *val) const override;		\
	bool toDouble(double *val) const override;		\
								\
    protected:							\
        T m_val;						\
    };


#define MAKSI_ITEM_NUMERIC_PRIVATE_DEF(C,T,F)			\
    C::Private::~Private() {}					\
    C::Private *C::Private::clone() const {			\
        return new Private(m_name, m_type, m_val);		\
    }								\
    bool C::Private::isItemValid() const { return true; }	\
    bool C::Private::isItemEqual(const Item &x) const {		\
	T val; if (x.F(&val)) { return m_val == val; }		\
	return false;						\
    }								\
    QVariant C::Private::toVariant() const {			\
	return QVariant::fromValue(m_val);			\
    }								\
    bool C::Private::toString(QString *val) const {		\
	T x;							\
	if (!F(&x)) return false;				\
	if (val)						\
	    val->setNum(x);					\
	return true;						\
    }								\
    bool C::Private::toInt(int32_t *val) const {		\
	if (val) *val = (int32_t) m_val;			\
        return true;						\
    }								\
    bool C::Private::toUInt(uint32_t *val) const {		\
	if (val) *val = (uint32_t) m_val;			\
        return true;						\
    }								\
    bool C::Private::toLong(int64_t *val) const {		\
	if (val) *val = (int64_t) m_val;			\
        return true;						\
    }								\
    bool C::Private::toULong(uint64_t *val) const {		\
	if (val) *val = (uint64_t) m_val;			\
        return true;						\
    }								\
    bool C::Private::toFloat(float *val) const {		\
	if (val) *val = (float) m_val;				\
        return true;						\
    }								\
    bool C::Private::toDouble(double *val) const {		\
	if (val) *val = (double) m_val;				\
        return true;						\
    }


class MAKSI_EXPORT Item_QName : public Item
{
public:
    MAKSI_ITEM_ATOMIC_PRIVATE(Item_QName, name_t, toQName);
};

class MAKSI_EXPORT Item_ByteArray : public Item
{
public:
    MAKSI_ITEM_ATOMIC_PRIVATE(Item_ByteArray, QByteArray, toByteArray);
};

class MAKSI_EXPORT Item_Bool : public Item
{
public:
    MAKSI_ITEM_ATOMIC_PRIVATE(Item_Bool, bool, toBool);
};

class MAKSI_EXPORT Item_Int : public Item
{
public:
    MAKSI_ITEM_NUMERIC_PRIVATE(Item_Int, int32_t);
};

class MAKSI_EXPORT Item_UInt : public Item
{
public:
    MAKSI_ITEM_NUMERIC_PRIVATE(Item_UInt, uint32_t);
};

class MAKSI_EXPORT Item_Long : public Item
{
public:
    MAKSI_ITEM_NUMERIC_PRIVATE(Item_Long, int64_t);
};

class MAKSI_EXPORT Item_ULong : public Item
{
public:
    MAKSI_ITEM_NUMERIC_PRIVATE(Item_ULong, uint64_t);
};

class MAKSI_EXPORT Item_Float : public Item
{
public:
    MAKSI_ITEM_NUMERIC_PRIVATE(Item_Float, float);
};

class MAKSI_EXPORT Item_Double : public Item
{
public:
    MAKSI_ITEM_NUMERIC_PRIVATE(Item_Double, double);
};

class MAKSI_EXPORT Item_Url : public Item
{
public:
    MAKSI_ITEM_SIMPLE_PRIVATE(Item_Url, QUrl, toUrl);
};

class MAKSI_EXPORT Item_DateTime : public Item
{
public:
    MAKSI_ITEM_SIMPLE_PRIVATE(Item_DateTime, QDateTime, toDate);
};

class MAKSI_EXPORT Item_Date : public Item
{
public:
    MAKSI_ITEM_SIMPLE_PRIVATE(Item_Date, QDate, toDate);
};

class MAKSI_EXPORT Item_Time : public Item
{
public:
    MAKSI_ITEM_SIMPLE_PRIVATE(Item_Time, QTime, toTime);
};

class MAKSI_EXPORT Item_List : public Item
{
public:
    class MAKSI_EXPORT Private : public Item::Private
    {
    public:
	Private(name_t name, const Type &type) : Item::Private(name, type) {}
	Private(name_t name, const Type &type, const QVector<Item> &items) : Item::Private(name, type), m_items(items) {}
	~Private() override;
	Private *clone() const override;
	int itemCount() const override;
	Item item(int index) const override;
	bool itemAppend(const Item &value) override;

	QVariant toVariant() const override;

	bool toString(QString *val) const override;
	bool isItemValid() const override;
	bool isItemEqual(const Item &elem) const override;

    protected:
	QVector<Item> m_items;
    };
};

class MAKSI_EXPORT Item_Struct : public Item_List
{
public:
    class MAKSI_EXPORT Private : public Item_List::Private {
    public:
	Private(name_t name, const Type &type) : Item_List::Private(name, type) {}
	Private(name_t name, const Type &type, QVector<Item> val, QMap<name_t, Item> attr) : Item_List::Private(name, type, val), m_attributes(attr) {}
	~Private() override;
	Private *clone() const override;
	QVector<name_t> attributeNames() const override;
	bool hasAttribute(name_t name) const override;
	Item attribute(name_t name) const override;
	bool setAttribute(name_t name, const Item &value) override;

	QVariant toVariant() const override;

	bool toString(QString *val) const override;
	bool isItemValid() const override;
	bool isItemEqual(const Item &elem) const override;

    protected:
	QMap<name_t, Item> m_attributes;
    };
};


};

#endif
