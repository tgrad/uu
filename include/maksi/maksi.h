/* -*- mode:C++; tab-width:8; coding:utf-8; -*- */
/*
 *
 * Copyright (C) 2010-2015, 2017, 2018, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 *
 */

/**
 * @file    maksi.h
 * @author  ivan demakov <ivan.demakov@gmail.com>
 * @date    Fri May 21 19:07:53 2010
 *
 */

#ifndef QT_MAKSI_H
#define QT_MAKSI_H

#include "iface.h"
#include "type.h"


#include <QObject>


class QNetworkAccessManager;

//! maksi namespace
namespace maksi
{

class Engine;
class Script;
class Package;
class TypeFactory;

};


//! The Maksi class provides interface to the maksi library
class MAKSI_EXPORT Maksi : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(Maksi);

    Maksi();                    /**< Private constructor */
public:

    explicit Maksi(const QString &name); /**< constructor */

    ~Maksi();                   /**< destructor */


    /** Get name of the maksi instance
     *
     * @return name of the maksi instance
     */
    const QString &name() const { return m_name; }

    void sendMessage(maksi::message_t type, QString msg);
    void sendMessage(maksi::message_t type, const QUrl &url, int line, QString msg);
    void sendMessage(maksi::message_t type, maksi::xml::item_t e, QString msg);

    void setMessageHandler(void (*message)(int, const QString &)) { m_message = message; }


    /** Get schema type
     *
     * @param name type name
     *
     * @return the type
     */
    maksi::Type type(maksi::name_t name) const;


    /** Add schema type
     *
     * @param name
     * @param type
     *
     * @return pointer to the added type
     */
    maksi::Type *addType(maksi::name_t name, maksi::Type type);


    /** Register type
     *
     * @param name
     * @param factory
     *
     */
    void registerTypeFactory(maksi::name_t name, QSharedPointer<maksi::TypeFactory> factory);


    /** Get schema attribute
     *
     * @param name attribute name
     *
     * @return the attribute
     */
    maksi::TypeAttribute attribute(maksi::name_t name) const;


    /** Add achema attribute
     *
     *
     * @param name
     * @param attr
     */
    void addAttribute(maksi::name_t name, maksi::TypeAttribute attr);


    /** Get schema element
     *
     * @param name element name
     *
     * @return the element
     */
    maksi::TypeElement element(maksi::name_t name) const;


    /** Add schema element
     *
     *
     * @param name
     * @param elem
     */
    void addElement(maksi::name_t name, maksi::TypeElement elem);


    /** Get schema group
     *
     * @param name group name
     *
     * @return the group
     */
    maksi::TypeGroup group(maksi::name_t name) const;


    /** Add schema group
     *
     *
     * @param name
     * @param group
     */
    void addGroup(maksi::name_t name, maksi::TypeGroup group);


    /** Get script engine by name
     *
     * @param name engine name
     *
     * @return pointer to the engine
     */
    maksi::Engine *engine(const QString &name) const;


    /** Add engine
     *
     *
     * @param engine
     */
    void addEngine(maksi::Engine *engine);


    /** Get script by name
     *
     * @param name script name
     *
     * @return the script
     */
    maksi::Script *script(const QString &name) const;


    /** Add script
     *
     *
     * @param script
     */
    void addScript(maksi::Script *script);


    /** Get script names
     *
     * @return the list of script names
     */
    QList<QString> scriptNames() const;


    /** Get eval context by name
     *
     * @param name name
     *
     * @return eval context
     */
    maksi::Functor *func(const QString &name) const;


    /** Add eval context
     *
     * @param name context name
     * @param proc eval context
     */
    void addFunc(const QString &name, maksi::Functor *proc);


    /** Get func names
     *
     * @return the list of func names
     */
    QList<QString> funcNames() const;


    /** Add package
     *
     *
     * @param package
     */
    void addPackage(maksi::Package *package);
    bool loadPackageData(const QUrl &url, QByteArray *data);


    /** Load config
     *
     * Read config, and initialize maksi.
     *
     * @param url config URL
     */
    bool loadConfig(const QString &url);


    /** Clear config data
     *
     * Clear data that used while config loading.
     *
     */
    void clearConfig();


    /** Get network manager
     *
     * Return common network manager intended for plugins use
     *
     * @return network manager to work
     */
    QNetworkAccessManager *network();

private:
    QString m_name;

    QHash<maksi::name_t, maksi::Type> m_types;
    QHash<maksi::name_t, QSharedPointer<maksi::TypeFactory>> m_factories;
    QHash<maksi::name_t, maksi::TypeAttribute> m_attributes;
    QHash<maksi::name_t, maksi::TypeElement> m_elements;
    QHash<maksi::name_t, maksi::TypeGroup> m_groups;

    QHash<QString, maksi::Engine*> m_engines;
    QHash<QString, maksi::Script*> m_scripts;
    QHash<QString, maksi::functor_t> m_funcs;

    QList<maksi::Package*> m_packages;
    maksi::Package* m_defPackage;

    void (*m_message)(int type, const QString &msg);

    QNetworkAccessManager *m_network;
};


#endif

/* End of file */
