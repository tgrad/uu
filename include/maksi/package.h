/* -*- mode:C++; tab-width:8 -*- */
/*
 *
 * Copyright (C) 2015, MaKsi Lop.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 *
 */

/**
 * @file    package.h
 * @author  ivan demakov <ivan@demakov.net>
 * @date    Thu Jun 11 18:38:07 2015
 *
 * @brief   package interface
 *
 */

#ifndef PACKAGE_H
#define PACKAGE_H

#include "iface.h"

#include <QUrl>
#include <QByteArray>
#include <QObject>
#include <QSslError>
#include <QSslPreSharedKeyAuthenticator>


class Maksi;

class QNetworkReply;


//! maksi namespace
namespace maksi
{

class MAKSI_EXPORT Package : public QObject
{
    Q_OBJECT

public:
    Package(Maksi *maksi, const QUrl &baseUrl);
    virtual ~Package();

    virtual bool init();
    virtual bool load(const QUrl &url, QByteArray *data) = 0;
    virtual void clear();

public slots:
    void sslErrors(const QList<QSslError> &);
    void preSharedKeyAuthenticationRequired(QSslPreSharedKeyAuthenticator *);
    void progress(qint64 recv, qint64 total);

protected:
    bool loadData(const QUrl &url, QByteArray *data, maksi::message_t msg);

    Maksi *m_maksi;
    QUrl m_baseUrl;
};


class MAKSI_EXPORT PackageDir : public Package
{
    Q_OBJECT

public:
    PackageDir(Maksi *maksi, const QUrl &baseUrl);
    virtual ~PackageDir();

    virtual bool load(const QUrl &url, QByteArray *data) override;
};

class MAKSI_EXPORT PackageZip : public Package
{
    Q_OBJECT

public:
    PackageZip(Maksi *maksi, const QUrl &baseUrl);
    virtual ~PackageZip();

    virtual bool init() override;
    virtual bool load(const QUrl &url, QByteArray *data) override;
    virtual void clear();

private:
    QHash<QString, QByteArray> m_data;
};

};

#endif

/* End of file */
