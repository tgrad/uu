/* -*- mode:C++; tab-width:8 -*- */
/*
 *
 * Copyright (C) 2011-2018, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 *
 */

/**
 * @file    type.h
 * @author  ivan demakov <ksion@users.sourceforge.net>
 * @date    Wed Apr 20 15:56:29 2011
 *
 * @brief   XSchema interface
 *
 */

#ifndef MAKSI_TYPE_H
#define MAKSI_TYPE_H

#include "iface.h"
#include "xml.h"
#include "item.h"

#include <QUrl>
#include <QSharedPointer>


//! maksi namespace
namespace maksi
{

class TypeAttribute;
class TypeElement;
class TypeGroup;
class TypeFactory;
class TypePartition;
class TypePrivate;
class TypeAttributePrivate;
class TypeParticle;
class TypeGroupPrivate;
class TypeElementPrivate;


//! Use mode
enum use_t {
    USE_OPTIONAL,
    USE_REQUIRED,
    USE_PROHIBITED,
    USE_FIXED,
    USE_FIXED_REQUIRED,
    USE_DEFAULT
};


class MAKSI_EXPORT Type
{
public:
    enum ws_facet_t {
        WS_PRESERVE,
        WS_REPLACE,
        WS_COLLAPSE
    };

    Type();
    ~Type();

    Type(const Type &x);
    Type &operator=(const Type &x);
    Type clone();
    void clear();

    bool isValid() const;
    bool isSimple() const;
    bool isAtomic() const;
    bool isString() const;
    bool isBinary() const;
    bool isName() const;
    bool isDate() const;
    bool isTime() const;
    bool isBool() const;
    bool isNumber() const;
    bool isInt() const;
    bool isList() const;
    bool isStruct() const;
    bool isEmpty() const;
    bool isMixed() const;

    void setFactory(QSharedPointer<TypeFactory> factory);
    QSharedPointer<TypeFactory> factory() const;

    Item createNew(Maksi *maksi, xml::item_t conf, name_t elemName, bool hereNode) const;
    Item createNew(Maksi *maksi, xml::item_t conf) const;
    QVariant fromItem(Maksi *maksi, const Item &from) const;

    name_t targetName() const;

    static const Type& noType();
    static const Type& anyType();
    static Type stdType(name_t name);

    static bool simpleType(Maksi *maksi, xml::item_t conf, name_t targetName, Type *declareType);
    static bool complexType(Maksi *maksi, xml::item_t conf, name_t targetName, Type *declareType, bool define);

    static TypeAttribute attribute(Maksi *maksi, xml::item_t conf, name_t targetName, bool global);
    static TypeElement element(Maksi *maksi, xml::item_t conf, name_t targetName);
    static TypeGroup group(Maksi *maksi, xml::item_t conf, name_t targetName);

    QVector<TypeAttribute> attributes() const;
    Type contentType() const;

    bool operator==(const Type &x) const { return d == x.d; }
    bool operator!=(const Type &x) const { return d != x.d; }

private:
    Type(TypePrivate *x);

    TypePrivate *d;

    void setWsFacet(ws_facet_t val);
    void addEnum(const Item &val);
    void addAttribute(TypeAttribute attr);

    QSharedPointer<TypeParticle> particle() const;
    void setParticle(QSharedPointer<TypeParticle> p);

    bool createElements(Maksi *maksi, xml::item_t conf, QSharedPointer<TypePartition> part, Item *element) const;

    static bool restrictionType(Maksi *maksi, xml::item_t conf, name_t targetName, bool simple, Type *declareType);
    static bool listType(Maksi *maksi, xml::item_t conf, name_t targetName, Type *declareType);
    static bool unionType(Maksi *maksi, xml::item_t conf, name_t targetName, Type *declareType);
    static bool structType(Maksi *maksi, xml::item_t conf, name_t targetName, Type baseType, Type *declareType);

    static bool simpleContent(Maksi *maksi, maksi::xml::item_t conf, name_t targetName, Type *declareType, bool define);
    static bool complexContent(Maksi *maksi, maksi::xml::item_t conf, name_t targetName, Type *declareType, bool define);
    static bool complexRestriction(Maksi *maksi, maksi::xml::item_t conf, name_t targetName, Type baseType, bool mixed, Type *declareType, bool define);
    static bool complexExtension(Maksi *maksi, maksi::xml::item_t conf, name_t targetName, Type baseType, bool mixed, Type *declareType, bool define);

    static bool setMinMaxOccurs(Maksi *maksi, maksi::xml::item_t conf, TypeParticle *p);

    static QSharedPointer<TypeParticle> modelGroup(Maksi *maksi, maksi::xml::item_t conf, name_t targetName);
    static TypeGroup allGroup(Maksi *maksi, maksi::xml::item_t conf, name_t targetName);
    static TypeGroup choiceGroup(Maksi *maksi, maksi::xml::item_t conf, name_t targetName);
    static TypeGroup sequenceGroup(Maksi *maksi, maksi::xml::item_t conf, name_t targetName);

public:
    static void staticInit();

    static name_t xs_string;
    static name_t xs_normString;
    static name_t xs_token;
    static name_t xs_nmtoken;
    static name_t xs_name;
    static name_t xs_ncname;
    static name_t xs_id;
    static name_t xs_idref;
    static name_t xs_qname;

    static name_t xs_hex;
    static name_t xs_base64;
    static name_t xs_anyURI;
    static name_t xs_datetime;
    static name_t xs_date;
    static name_t xs_time;

    static name_t xs_bool;
    static name_t xs_double;
    static name_t xs_float;
    static name_t xs_decimal;
    static name_t xs_long;
    static name_t xs_ulong;
    static name_t xs_int;
    static name_t xs_uint;
    static name_t xs_short;
    static name_t xs_ushort;
    static name_t xs_byte;
    static name_t xs_ubyte;

    static name_t xs_any;
    static name_t xs_anySimple;
    static name_t xs_anyAtomic;
};


class MAKSI_EXPORT TypeAttribute
{
public:
    TypeAttribute();
    TypeAttribute(name_t name, Type type);
    ~TypeAttribute();

    TypeAttribute(const TypeAttribute &x);
    TypeAttribute &operator=(const TypeAttribute &x);

    bool isValid() const;

    void setUse(use_t use);
    void setFixed(const Item &value);
    void setDefault(const Item &value);

    name_t name() const;
    Type type() const;
    use_t use() const;
    Item value() const;

private:
    void detach();

    TypeAttributePrivate *d;
};


class MAKSI_EXPORT TypeGroup
{
public:
    enum compositor_t {
        COMPOSITOR_INVALID,
        COMPOSITOR_ALL,
        COMPOSITOR_CHOICE,
        COMPOSITOR_SEQUENCE
    };

    TypeGroup();
    ~TypeGroup();

    TypeGroup(const TypeGroup &x);
    TypeGroup &operator=(const TypeGroup &x);

    bool isValid() const;
    bool isEmpty() const;

    compositor_t compositor() const;
    void setCompositor(compositor_t val);

    void appendParticle(QSharedPointer<TypeParticle> p);
    void appendChildParticles(QSharedPointer<TypeParticle> p);

private:
    void detach();

    TypeGroupPrivate *d;
    friend class TypeParticle_Group;
};


class MAKSI_EXPORT TypeElement
{
public:
    TypeElement();
    TypeElement(name_t name, Type type);
    ~TypeElement();

    TypeElement(const TypeElement &x);
    TypeElement &operator=(const TypeElement &x);

    bool isValid() const;
    name_t elementName() const;
    const Type elementType() const;

private:
    void detach();

    TypeElementPrivate *d;
    friend class TypeParticle_Elem;
};


class MAKSI_EXPORT TypeFactory
{
public:
    TypeFactory();
    virtual ~TypeFactory();

    virtual bool isSimple() const;
    virtual bool isAtomic() const;
    virtual bool isBinary() const;
    virtual bool isString() const;
    virtual bool isName() const;
    virtual bool isDate() const;
    virtual bool isTime() const;
    virtual bool isBool() const;
    virtual bool isNumber() const;
    virtual bool isInt() const;
    virtual bool isList() const;
    virtual bool isStruct() const;

    virtual Type contentType() const;

    virtual Item createNew(Maksi *maksi, const Type &type, xml::item_t from) const;
    virtual QVariant fromItem(Maksi *maksi, const Type &type, Item::Private *from) const;
};


class MAKSI_EXPORT SimpleTypeFactory : public TypeFactory
{
public:
    bool isSimple() const override;
    Item createNew(Maksi *maksi, const Type &type, xml::item_t from) const override;
};

class MAKSI_EXPORT AtomicTypeFactory : public SimpleTypeFactory
{
public:
    bool isAtomic() const override;
    Item createNew(Maksi *maksi, const Type &type, xml::item_t from) const override;
};

class MAKSI_EXPORT StringTypeFactory : public AtomicTypeFactory
{
public:
    bool isString() const override;
    Item createNew(Maksi *maksi, const Type &type, xml::item_t from) const override;
};

class MAKSI_EXPORT NormStringTypeFactory : public StringTypeFactory
{
public:
    Item createNew(Maksi *maksi, const Type &type, xml::item_t from) const override;
};

class MAKSI_EXPORT TokenTypeFactory : public NormStringTypeFactory
{
public:
    Item createNew(Maksi *maksi, const Type &type, xml::item_t from) const override;
};

class MAKSI_EXPORT NMTokenTypeFactory : public TokenTypeFactory
{
public:
    Item createNew(Maksi *maksi, const Type &type, xml::item_t from) const override;
};

class MAKSI_EXPORT NameTypeFactory : public NMTokenTypeFactory
{
public:
    bool isName() const override;
    Item createNew(Maksi *maksi, const Type &type, xml::item_t from) const override;
};

class MAKSI_EXPORT NCNameTypeFactory : public NameTypeFactory
{
public:
    Item createNew(Maksi *maksi, const Type &type, xml::item_t from) const override;
};

class MAKSI_EXPORT QNameTypeFactory : public NameTypeFactory
{
public:
    Item createNew(Maksi *maksi, const Type &type, xml::item_t from) const override;
};

class MAKSI_EXPORT HexTypeFactory : public StringTypeFactory
{
public:
    bool isBinary() const override;
    Item createNew(Maksi *maksi, const Type &type, xml::item_t from) const override;
};

class MAKSI_EXPORT Base64TypeFactory : public HexTypeFactory
{
public:
    Item createNew(Maksi *maksi, const Type &type, xml::item_t from) const override;
};

class MAKSI_EXPORT URITypeFactory : public StringTypeFactory
{
public:
    Item createNew(Maksi *maksi, const Type &type, xml::item_t from) const override;
};

class MAKSI_EXPORT DateTimeTypeFactory : public StringTypeFactory
{
public:
    bool isDate() const override;
    bool isTime() const override;
    Item createNew(Maksi *maksi, const Type &type, xml::item_t from) const override;
};

class MAKSI_EXPORT DateTypeFactory : public StringTypeFactory
{
public:
    bool isDate() const override;
    Item createNew(Maksi *maksi, const Type &type, xml::item_t from) const override;
};

class MAKSI_EXPORT TimeTypeFactory : public StringTypeFactory
{
public:
    bool isTime() const override;
    Item createNew(Maksi *maksi, const Type &type, xml::item_t from) const override;
};

class MAKSI_EXPORT BoolTypeFactory : public TokenTypeFactory
{
public:
    bool isBool() const override;
    Item createNew(Maksi *maksi, const Type &type, xml::item_t from) const override;
};

class MAKSI_EXPORT NumberTypeFactory : public TokenTypeFactory
{
public:
    bool isNumber() const override;
    Item createNew(Maksi *maksi, const Type &type, xml::item_t from) const override;
};

class MAKSI_EXPORT LongTypeFactory : public NumberTypeFactory
{
public:
    bool isInt() const override;
    Item createNew(Maksi *maksi, const Type &type, xml::item_t from) const override;
};

class MAKSI_EXPORT ULongTypeFactory : public NumberTypeFactory
{
public:
    bool isInt() const override;
    Item createNew(Maksi *maksi, const Type &type, xml::item_t from) const override;
};

class MAKSI_EXPORT IntTypeFactory : public LongTypeFactory
{
public:
    Item createNew(Maksi *maksi, const Type &type, xml::item_t from) const override;
};

class MAKSI_EXPORT UIntTypeFactory : public ULongTypeFactory
{
public:
    Item createNew(Maksi *maksi, const Type &type, xml::item_t from) const override;
};

class MAKSI_EXPORT FloatTypeFactory : public NumberTypeFactory
{
public:
    Item createNew(Maksi *maksi, const Type &type, xml::item_t from) const override;
};

class MAKSI_EXPORT DoubleTypeFactory : public NumberTypeFactory
{
public:
    Item createNew(Maksi *maksi, const Type &type, xml::item_t from) const override;
};

class MAKSI_EXPORT ListTypeFactory : public SimpleTypeFactory
{
public:
    explicit ListTypeFactory(const Type &itemType) : m_itemType(itemType) {}

    bool isList() const override;
    Item createNew(Maksi *maksi, const Type &type, xml::item_t from) const override;

    Type m_itemType;
};

class MAKSI_EXPORT UnionTypeFactory : public SimpleTypeFactory
{
public:
    explicit UnionTypeFactory(QVector<Type> types) : m_memberTypes(types) {}

    Item createNew(Maksi *maksi, const Type &type, xml::item_t from) const override;

    QVector<Type> m_memberTypes;
};

class MAKSI_EXPORT StructTypeFactory : public TypeFactory
{
public:
    explicit StructTypeFactory(const Type &baseType) : m_baseType(baseType) {}

    bool isStruct() const override;
    Type contentType() const override;

    Item createNew(Maksi *maksi, const Type &type, xml::item_t from) const override;

    Type m_baseType;
};

};

Q_DECLARE_METATYPE(maksi::Type);


#endif

/* End of file */
