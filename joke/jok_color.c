/*
 * jok_color.c
 * color manipulation
 *
 * Copyright (C) 1997-2000, Ivan Demakov
 *
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose, without fee, and without a written agreement
 * is hereby granted, provided that the above copyright notice and this
 * paragraph and the following two paragraphs appear in all copies.
 * Modifications to this software may be copyrighted by their authors
 * and need not follow the licensing terms described here, provided that
 * the new terms are clearly indicated on the first page of each file where
 * they apply.
 *
 * IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY
 * FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES,
 * INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS
 * DOCUMENTATION, EVEN IF THE AUTHORS HAS BEEN ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIMS ANY WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS
 * ON AN "AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAS NO OBLIGATIONS
 * TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 *
 *
 * Author:        Ivan Demakov <demakov@users.sourceforge.net>
 * Creation date: Sun Jul  6 13:04:37 1997
 * Last Update:   Tue Mar 21 18:47:22 2000
 *
 */

#include "jok_int.h"
#include "toolkit.h"

/*
 * Colortab -- Hash table that maps color name on RGB value
 *
 */

typedef struct
{
    const char *name;
    ksi_color red, green, blue;
} colorec;


static size_t
hash_colorec (void* rec, size_t num, void* data)
{
    size_t h = 0;
    size_t len = strlen(((colorec*)rec)->name);
    while (len--)
	h = ((h << 8) + ((unsigned)(((colorec*)rec)->name[len]))) % num;
    return h;
}

static int
cmp_colorec (void* r1, void* r2, void* data)
{
    return ksi_strcasecmp (((colorec*) r1) -> name, ((colorec*) r2) -> name);
}


int
ksi_lookup_color(const char * name, ksi_color * r, ksi_color * g, ksi_color * b)
{
    colorec rec, *ptr;
    rec.name = name;

    if (joke_colortab == 0)
	return 0;

    ptr = (colorec*)ksi_lookup_vtab(joke_colortab, &rec, 0);
    if (ptr)
    {
	if (r) *r = ptr->red;
	if (g) *g = ptr->green;
	if (b) *b = ptr->blue;
	return 1;
    }

    return 0;
}

void
ksi_append_color (const char* name, ksi_color r, ksi_color g, ksi_color b)
{
  colorec* rec = ksi_malloc(sizeof(*rec));
  rec -> name = name;
  rec -> red = r;
  rec -> green = g;
  rec -> blue = b;

  if (joke_colortab == 0)
      joke_colortab = ksi_new_valtab(100, hash_colorec, cmp_colorec, 0);


  ksi_lookup_vtab (joke_colortab, rec, 1);
}


/*
 *  ksi_alloc_color --
 *
 *	Map name of color to pixel value.
 *	First looks up already allocated colors, if not found
 *	tries allocate new color, if fails finds closest available
 *	color.
 *
 *  Results:
 *	return pixel value, or ksi_false if can't allocate color
 */

ksi_obj
ksi_alloc_color (ksi_obj dc, ksi_obj str)
{
  const wchar_t	*ptr;
  ksi_color	r, g, b;
  ksi_pixel	pix;

  KSI_CHECK (dc, DC_P (dc), "alloc-color: invalid dc in arg1");
  KSI_CHECK (str, KSI_STR_P (str) || KSI_SYM_P (str), "alloc-color: invalid string in arg2");

  ptr = KSI_SYM_P(str) ? KSI_SYM_PTR(str) : KSI_STR_PTR(str);

  if (!ksi_parse_color (DC_CMAP (dc), ksi_local(ptr), &r, &g, &b))
    return ksi_false;

  pix = ksi_alloc_pixel (DC_CMAP (dc), r, g, b);
  if (pix == (ksi_pixel) -1)
    return ksi_false;

  return ksi_uint2num (pix);
}


ksi_obj
ksi_free_color (ksi_obj dc, ksi_obj val)
{
  ksi_pixel	pixel;

  KSI_CHECK (dc, DC_P (dc), "free-color: invalid dc in arg1");
  KSI_CHECK (val, KSI_EINT_P (val), "free-color: invalid pixel value in arg2");

  pixel = ksi_num2uint (val, "free-color");
  ksi_free_pixel (DC_CMAP (dc), pixel);

  return ksi_unspec;
}


static struct Ksi_Prim_Def defs [] =
{
  { L"alloc-color",	ksi_alloc_color,	KSI_CALL_ARG2, 2 },
  { L"free-color",	ksi_free_color,		KSI_CALL_ARG2, 2 },

  { 0 },
};

void
ksi_joke_init_color (ksi_env env)
{
  ksi_reg_unit (defs, env);
  ksi_init_colormap (env);
}

 /* End of code */
