/*
 * jok_dc.c
 * draw-context utils
 *
 * Copyright (C) 1997-2000, 2017, Ivan Demakov
 *
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose, without fee, and without a written agreement
 * is hereby granted, provided that the above copyright notice and this
 * paragraph and the following two paragraphs appear in all copies.
 * Modifications to this software may be copyrighted by their authors
 * and need not follow the licensing terms described here, provided that
 * the new terms are clearly indicated on the first page of each file where
 * they apply.
 *
 * IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY
 * FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES,
 * INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS
 * DOCUMENTATION, EVEN IF THE AUTHORS HAS BEEN ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIMS ANY WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS
 * ON AN "AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAS NO OBLIGATIONS
 * TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 *
 *
 * Author:        Ivan Demakov <demakov@users.sourceforge.net>
 * Creation date: Mon Aug 18 21:45:42 1997
 *
 */

#include "jok_int.h"
#include "toolkit.h"


static int
dc_tag_equal (struct Ksi_EObj *x1, struct Ksi_EObj *x2, int deep)
{
  return 0;
}

static const wchar_t*
dc_tag_print (struct Ksi_EObj *x, int slashify)
{
  ksi_dc dc = (ksi_dc) x;
  wchar_t *buf;

#ifdef X11_GRAPH
  buf = ksi_aprintf("#<dc %p>", dc->gc);
#elif defined(WIN_GRAPH)
  buf = ksi_aprintf("#<dc %p>", (void*) dc->hdc);
#else
  buf = ksi_default_tag_print (tag, x, slashify);
#endif

  return buf;
}

static struct Ksi_ETag tag_dc =
{
  L"dc",
  dc_tag_equal,
  dc_tag_print
};


ksi_obj
ksi_dc_p (ksi_obj x)
{
  return DC_P (x) ? ksi_true : ksi_false;
}


static inline ksi_dc
alloc_dc (void)
{
  ksi_dc dc = (ksi_dc) ksi_malloc (sizeof *dc);
  dc->ko.o.itag = KSI_TAG_EXTENDED;
  dc->ko.etag = tc_dc;

  return dc;
}


#if defined(X11_GRAPH)

ksi_obj
ksi_make_x11_dc (Window wnd, Drawable drawable, GC gc)
{
  ksi_dc dc;

  dc = alloc_dc ();
  dc->dpy = joke_dpy;
  dc->drawable = drawable;
  dc->gc = gc;
  dc->cmap = ksi_get_window_colormap (wnd);

  dc->draw_mode = GXcopy;
  dc->background = 0;
  dc->foreground = 1;

  return (ksi_obj) dc;
}

#elif defined(WIN_GRAPH)

ksi_obj
ksi_make_win_dc (HWND hwnd, HDC hdc)
{
  ksi_dc dc;

  dc = alloc_dc ();
  dc->hwnd = hwnd;
  dc->hdc = hdc;

  dc->draw_mode = GXcopy;

  return (ksi_obj) dc;
}

#endif


ksi_obj
ksi_free_dc (ksi_obj x)
{
  ksi_dc dc = (ksi_dc) x;
  KSI_CHECK (x, DC_P (x), "free-dc: invalid draw-context in arg1");

  dc->ko.o.itag   = KSI_TAG_BROKEN;
  dc->dashes = 0;
  dc->font   = 0;

#if defined(X11_GRAPH)

  XFreeGC (joke_dpy, dc->gc);
  dc->gc   = 0;
  dc->cmap = 0;

#elif defined(MSWIN32)

  ReleaseDC (dc->hwnd, dc->hdc);

#endif

  return ksi_unspec;
}


/*
 * ksi_sync_dc --
 *
 *	Syncronize display.
 */

ksi_obj
ksi_sync_dc (ksi_obj dc, ksi_obj discard)
{
  KSI_CHECK (dc, DC_P (dc), "sync-dc: invalid draw-context in arg1");

#if defined(X11_GRAPH)

  if (discard == ksi_null)
    discard = ksi_false;

  XSync (DC_DISPLAY (dc), discard == ksi_false ? False : True);

#elif defined(MSWIN32)

  GdiFlush ();

#endif

  return ksi_unspec;
}

ksi_obj
ksi_clear_window (ksi_obj dc, ksi_obj expose)
{
  KSI_CHECK (dc, DC_P (dc), "clear-window: invalid dc");
  if (!expose)
    expose = ksi_false;

#if defined(X11_GRAPH)

  XClearArea (DC_DISPLAY (dc), DC_DRAWABLE (dc), 0, 0, 0, 0,
	      (expose != ksi_false ? True : False));

#else

  RedrawWindow (DC_HWND (dc), 0, 0, RDW_ERASE | RDW_INVALIDATE | RDW_ERASENOW);

#endif

  return ksi_unspec;
}

ksi_obj
ksi_clear_area (ksi_obj dc, ksi_obj xx, ksi_obj yy, ksi_obj width, ksi_obj height, ksi_obj expose)
{
  int x, y, w, h;

  KSI_CHECK (dc, DC_P (dc), "clear-window: invalid dc in arg1");
  KSI_CHECK (xx, KSI_REAL_P (xx), "clear-window: invalid number in arg2");
  KSI_CHECK (yy, KSI_REAL_P (yy), "clear-window: invalid number in arg3");
  KSI_CHECK (width, KSI_REAL_P (width), "clear-window: invalid number in arg4");
  KSI_CHECK (height, KSI_REAL_P (height), "clear-window: invalid number in arg5");

  if (!expose)
    expose = ksi_false;

  x = ksi_num2int (xx, "clear-window");
  y = ksi_num2int (yy, "clear-window");
  w = ksi_num2int (width, "clear-window");
  h = ksi_num2int (height, "clear-window");

#if defined(X11_GRAPH)

  XClearArea (DC_DISPLAY (dc), DC_DRAWABLE (dc), x, y, w, h, (expose != ksi_false ? True : False));

#else

  {
    RECT rect;
    rect.left = x;
    rect.right = x + w;
    rect.top = y;
    rect.bottom = y + h;

    RedrawWindow (DC_HWND (dc), &rect, 0, RDW_ERASE | RDW_INVALIDATE | RDW_ERASENOW);
  }

#endif

  return ksi_unspec;
}


ksi_obj
ksi_set_draw_mode (ksi_obj dc, ksi_obj mode)
{
  int m;
  KSI_CHECK (dc, DC_P (dc), "set-draw-mode!: invalid dc in arg1");

  if (mode == key_xor)
    m = GXxor;
  else if (mode == key_or)
    m = GXor;
  else if (mode == key_and)
    m = GXand;
  else
    m = GXcopy;

  ((ksi_dc) dc) -> draw_mode = m;
  return ksi_unspec;
}


/*
 * ksi_set_foreground
 *      Устанавливает в DC цвет рисования.
 *      Этот цвет используется при рисовании сплошных линий,
 *      заливок и текста.
 *      Цвет задается системно-зависимым способом.
 *
 * Scheme procedure
 *      set-foreground dc color
 */

ksi_obj
ksi_set_foreground (ksi_obj dc, ksi_obj x)
{
  KSI_CHECK (dc, DC_P (dc), "set-foreground!: invalid draw-context in arg1");

  if (KSI_STR_P (x) || KSI_SYM_P (x))
    x = ksi_alloc_color (dc, x);
  else
    KSI_CHECK (x, KSI_EINT_P (x), "set-foreground!: invalid color in arg2");

  ((ksi_dc) dc) -> foreground = ksi_num2int(x, "set-foreground!");
  return ksi_unspec;
}


/*
 * ksi_set_background
 *      Устанавливает в DC цвет фона.
 *      Этот цвет используется при рисовании фона прерывистых линий,
 *      несплошных заливок и фона текста.
 *
 * Scheme procedure
 *      set-background dc color
 */

ksi_obj
ksi_set_background (ksi_obj dc, ksi_obj x)
{
  KSI_CHECK (dc, DC_P (dc), "set-background!: invalid draw-context in arg1");

  if (KSI_STR_P (x) || KSI_SYM_P (x))
    x = ksi_alloc_color (dc, x);
  else
    KSI_CHECK (x, KSI_EINT_P (x), "set-background!: invalid color in arg2");

  ((ksi_dc) dc) -> background = ksi_num2int(x, "set-background!");
  return ksi_unspec;
}


/*
 * ksi_set_line_style
 *      Устанавливает в DC ширину и стиль линии
 *      Ширина измеряется в пикселях
 *      Стиль задается ключевым словом
 *        :line-solid           сплошная линия
 *        :line-dash            штриховая линия
 *        :line-double-dash     штриховая линия, промежутки
 *                              между между штрихами рисуются
 *                              цветом фона
 *
 * Scheme procrdure
 *      set-line-style! dc width style
 *
 * Return
 *      ksi_unspec
 *
 * Bugs
 *      Windows не умеет рисовать штриховые линии произвольной ширины
 */

ksi_obj
ksi_set_line_style (ksi_obj dc, ksi_obj width, ksi_obj style)
{
  KSI_CHECK (dc, DC_P (dc), "set-line-style!: invalid draw-context in arg1");
  KSI_CHECK (width, KSI_REAL_P (width), "set-line-style!: invalid width in arg2");

  ((ksi_dc) dc) -> line_width = ksi_num2int (width, "set-line-style!");

  if (style == key_line_dash)
    ((ksi_dc) dc) -> line_style = LineOnOffDash;
  else if (style == key_line_double_dash)
    ((ksi_dc) dc) -> line_style = LineDoubleDash;
  else
    ((ksi_dc) dc) -> line_style = LineSolid;

  return ksi_unspec;
}


/*
 * ksi_set_cap_style
 *      Устанавливает в DC стиль концевых точек для широких линий
 *      Стиль задается ключевым словом
 *        :cap-not-last         не рисуется последняя точка
 *        :cap-butt             рисуются все точки (не доступно в Windows)
 *        :cap-round            концы линии закруглены
 *        :cap-projecting       (не доступно в Windows)
 *
 * Scheme procrdure
 *      set-cap-style! dc style
 *
 */

ksi_obj
ksi_set_cap_style (ksi_obj dc, ksi_obj style)
{
  KSI_CHECK (dc, DC_P (dc), "set-cap-style!: invalid draw-context in arg1");

  if (style == key_cap_butt)
    ((ksi_dc) dc) -> cap_style = CapButt;
  else if (style == key_cap_round)
    ((ksi_dc) dc) -> cap_style = CapRound;
  else if (style == key_cap_projecting)
    ((ksi_dc) dc) -> cap_style = CapProjecting;
  else
    ((ksi_dc) dc) -> cap_style = CapNotLast;

  return ksi_unspec;
}


/*
 * ksi_set_join_style
 *      Устанавливает в DC стиль соединения отрезков ломаной линии
 *      Стиль задается ключевым словом
 *        :join-mitter          соединения острые
 *        :join-round           соединения закруглены
 *        :join-bevel           соединения срезанные
 *
 * Scheme procrdure
 *      set-cap-style! dc style
 *
 */

ksi_obj
ksi_set_join_style (ksi_obj dc, ksi_obj style)
{
  KSI_CHECK (dc, DC_P (dc), "set-join-style!: invalid draw-context in arg1");

  if (style == key_join_round)
    ((ksi_dc) dc) -> join_style = JoinRound;
  else if (style == key_join_bevel)
    ((ksi_dc) dc) -> join_style = JoinBevel;
  else
    ((ksi_dc) dc) -> join_style = JoinMiter;

  return ksi_unspec;
}

/*
 * ksi_set_fill_style
 *      Устанавливает в DC стиль заполнения сплошных фигур и
 *      толстых линий.
 *      Стиль задается ключевым словом
 *        :fill-solid           заполнение цветом foreground
 *        :fill-tiled           заполнение цветным шаблоном
 *        :fill-stippled        заполнение монохромным шаблоном
 *                              еденичные биты рисуются цветом
 *                              foreground, нулевые не рисуются
 *        :fill-opaque-stippled заполнение монохромным шаблоном
 *                              еденичные биты рисуются цветом
 *                              foreground, нулевые - background
 *
 * Scheme procedure
 *      set-fill-style! style
 *
 * Return
 *      ksi_unspec
 *
 */

ksi_obj
ksi_set_fill_style (ksi_obj dc, ksi_obj style)
{
  int fill;
  KSI_CHECK (dc, DC_P (dc), "set-fill-style!: invalid draw-context in arg1");

  if (style == key_fill_solid)
    fill = FillSolid;
  else if (style == key_fill_tiled)
    fill = FillTiled;
  else if (style == key_fill_stippled)
    fill = FillStippled;
  else if (style == key_fill_opaque_stippled)
    fill = FillOpaqueStippled;
  else
    fill = FillSolid;

  ((ksi_dc) dc) -> fill_mode = fill;
  return ksi_unspec;
}


ksi_obj
ksi_set_fill_rule (ksi_obj dc, ksi_obj style)
{
  int rule;
  KSI_CHECK (dc, DC_P (dc), "set-fill-rule!: invalid draw-context in arg1");

  if (style == key_even_odd_rule)
    rule = EvenOddRule;
  else
    rule = WindingRule;

  ((ksi_dc) dc) -> fill_rule = rule;
  return ksi_unspec;
}


ksi_obj
ksi_set_arc_mode (ksi_obj dc, ksi_obj style)
{
  int rule;
  KSI_CHECK (dc, DC_P (dc), "set-arc-mode!: invalid draw-context in arg1");

  if (style == key_arc_chord)
    rule = ArcChord;
  else if (style == key_arc_pie_slice)
    rule = ArcPieSlice;
  else
    rule = ArcPieSlice;

  ((ksi_dc) dc) -> arc_mode = rule;
  return ksi_unspec;
}


/*
 * ksi_set_dashes
 *      Устанавливает в DC начало и список длин штрихов для
 *      рисования штриховых линий.
 *
 * Scheme procedure
 *      set-dashes dash-list
 *
 * Bugs
 *      Windows-95 не умеет рисовать штриховые линии с произвольной
 *      длиной штриха
 */

ksi_obj
ksi_set_dashes (ksi_obj dc, ksi_obj dash_list)
{
  KSI_CHECK (dc, DC_P (dc), "set-dashes!: invalid draw-context in arg1");

  if (dash_list == ksi_nil || dash_list == key_dash)
    {
      ((ksi_dc) dc) -> dash_mode = Dash;
      ((ksi_dc) dc) -> dashes = 0;
    }
  else if (dash_list == key_dot)
    {
      ((ksi_dc) dc) -> dash_mode = Dot;
      ((ksi_dc) dc) -> dashes = 0;
    }
  else if (dash_list == key_dash_dot)
    {
      ((ksi_dc) dc) -> dash_mode = DashDot;
      ((ksi_dc) dc) -> dashes = 0;
    }
  else if (dash_list == key_dash_dot_dot)
    {
      ((ksi_dc) dc) -> dash_mode = DashDotDot;
      ((ksi_dc) dc) -> dashes = 0;
    }
  else
    {
      int i, len = KSI_LIST_LEN (dash_list);
      char *dashes;
      KSI_CHECK (dash_list, len > 0, "set-dashes!: invalid list in arg3");

      dashes = (char*) ksi_malloc_data (sizeof (*dashes) * len);
      for (i = 0; dash_list != ksi_nil; dash_list = KSI_CDR (dash_list))
	{
	  ksi_obj x = KSI_CAR (dash_list);
	  KSI_CHECK (x, KSI_REAL_P (x), "set-dashes!: invalid dash length");
	  dashes[i++] = ksi_num2int (x, "set-dashes!");
	}

      ((ksi_dc) dc) -> dash_mode = UserDash;
      ((ksi_dc) dc) -> dashes = dashes;
      ((ksi_dc) dc) -> dash_len = len;
    }

  return ksi_unspec;
}


/*
 * ksi_set_font
 *      Устанавливает в DC текущий шрифт FONT
 */

ksi_obj
ksi_set_font (ksi_obj dc, ksi_obj font, ksi_obj size, ksi_obj angle)
{
  KSI_CHECK (dc, DC_P (dc), "set-font!: invalid dc in arg1");
  KSI_CHECK (font, FONT_P (font), "set-font!: invalid font in arg2");
  KSI_CHECK (size, KSI_REAL_P (size), "set-font!: invalid font size in arg3");
  KSI_CHECK (angle, !angle || KSI_REAL_P (angle), "set-font!: invalid font angle in arg4");

  ((ksi_dc) dc) -> font = (ksi_font) font;
  ((ksi_dc) dc) -> font_size = ksi_num2int (size, "set-font!");
  ((ksi_dc) dc) -> font_angle = angle ? ksi_num2int (angle, "set-font!") : 0;

  return ksi_unspec;
}

/*
 * ksi_joke_init_dc
 *	Вызывается из ksi_joke_init для инициализации функций
 *	связанных с контекстом вывода.
 */

static struct Ksi_Prim_Def defs [] =
{
  { L"dc?",		ksi_dc_p,		KSI_CALL_ARG1, 1 },
  { L"free-dc",		ksi_free_dc,		KSI_CALL_ARG2, 1 },
  { L"sync-dc",		ksi_sync_dc,		KSI_CALL_ARG2, 1 },

  { L"clear-window",	ksi_clear_window,	KSI_CALL_ARG2, 1 },
  { L"clear-area",	ksi_clear_area,		KSI_CALL_ARG6, 5 },

  { L"set-draw-mode!",	ksi_set_draw_mode,	KSI_CALL_ARG2, 2 },
  { L"set-foreground!",	ksi_set_foreground,	KSI_CALL_ARG2, 2 },
  { L"set-background!",	ksi_set_background,	KSI_CALL_ARG2, 2 },
  { L"set-line-style!",	ksi_set_line_style,	KSI_CALL_ARG3, 3 },
  { L"set-cap-style!",	ksi_set_cap_style,	KSI_CALL_ARG2, 2 },
  { L"set-join-style!",	ksi_set_join_style,	KSI_CALL_ARG2, 2 },
  { L"set-dashes!",	ksi_set_dashes,		KSI_CALL_ARG2, 2 },
  { L"set-fill-style!",	ksi_set_fill_style,	KSI_CALL_ARG2, 2 },
  { L"set-fill-rule!",	ksi_set_fill_rule,	KSI_CALL_ARG2, 2 },
  { L"set-arc-mode!",	ksi_set_arc_mode,	KSI_CALL_ARG2, 2 },
  { L"set-font!",	ksi_set_font,		KSI_CALL_ARG4, 3 },

  { 0 }
};

void
ksi_joke_init_dc (ksi_env env)
{
  tc_dc = &tag_dc;

  key_copy = ksi_str02key (L"copy");
  key_xor = ksi_str02key (L"xor");
  key_or = ksi_str02key (L"or");
  key_and = ksi_str02key (L"and");
  key_line_solid = ksi_str02key (L"line-solid");
  key_line_dash = ksi_str02key (L"line-dash");
  key_line_double_dash = ksi_str02key (L"line-double-dash");
  key_dot = ksi_str02key (L"dot");
  key_dash = ksi_str02key (L"dash");
  key_dash_dot = ksi_str02key (L"dash-dot");
  key_dash_dot_dot = ksi_str02key (L"dash-dot-dot");
  key_cap_not_last = ksi_str02key (L"cap-not-last");
  key_cap_butt = ksi_str02key (L"cap-butt");
  key_cap_round = ksi_str02key (L"cap-round");
  key_cap_projecting = ksi_str02key (L"cap-projecting");
  key_join_miter = ksi_str02key (L"join-miter");
  key_join_round = ksi_str02key (L"join-round");
  key_join_bevel = ksi_str02key (L"join-bevel");
  key_fill_solid = ksi_str02key (L"fill-solid");
  key_fill_tiled = ksi_str02key (L"fill-tiled");
  key_fill_stippled = ksi_str02key (L"fill-stippled");
  key_fill_opaque_stippled = ksi_str02key (L"fill-opaque-stippled");
  key_even_odd_rule = ksi_str02key (L"even-odd-rule");
  key_winding_rule = ksi_str02key (L"winding-rule");
  key_arc_chord = ksi_str02key (L"chord");
  key_arc_pie_slice = ksi_str02key (L"pie-slice");
  key_coord_mode_origin = ksi_str02key (L"origin");
  key_coord_mode_previous = ksi_str02key (L"previous");

  ksi_reg_unit (defs, env);
}

 /* End of code */
