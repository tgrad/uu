/*
 * jok_draw.c
 * drawing primitives
 *
 * Copyright (C) 1997-2000, Ivan Demakov
 *
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose, without fee, and without a written agreement
 * is hereby granted, provided that the above copyright notice and this
 * paragraph and the following two paragraphs appear in all copies.
 * Modifications to this software may be copyrighted by their authors
 * and need not follow the licensing terms described here, provided that
 * the new terms are clearly indicated on the first page of each file where
 * they apply.
 *
 * IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY
 * FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES,
 * INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS
 * DOCUMENTATION, EVEN IF THE AUTHORS HAS BEEN ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIMS ANY WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS
 * ON AN "AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAS NO OBLIGATIONS
 * TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 *
 *
 * Author:        Ivan Demakov <demakov@users.sourceforge.net>
 * Creation date: Sat Jul  5 22:04:54 1997
 * Last Update:   Tue Mar 21 18:56:27 2000
 *
 */

/* Comments:
 *
 */

#include "jok_int.h"
#include "jok_draw.h"
#include <math.h>

#pragma warning(disable: 4244)

#ifndef M_PI
# define M_PI           3.14159265358979323846
#endif

#define TORAD(x)	((x) / 1800.0 * M_PI)

#ifndef MAX
# define MAX(x,y) ((x) < (y) ? (y) : (x))
#endif

#ifndef MIN
# define MIN(x,y) ((x) > (y) ? (y) : (x))
#endif

#ifndef ABS
# define ABS(x) ((x) < 0 ? (-(x)) : (x))
#endif


/*
 * is_coord --
 *      проверяет является ли обьект координатой
 *      координата задается парой,
 *      в которой car содержит x, а cdr - y.
 *      x и y должны быть целыми числами
 *
 * Scheme procedure
 *      None
 *
 * Return
 *      0 - если обьект является координатой
 *      1 - если нет
 *
 * Side effects
 *      None
 */

static int
is_coord (ksi_obj z)
{
  if (KSI_PAIR_P (z))
    {
      ksi_obj x = KSI_CAR (z);
      ksi_obj y = KSI_CDR (z);
      if (!KSI_REAL_P (x))
	return 0;
      if (KSI_REAL_P (y))
	return 1;
      if (KSI_PAIR_P (y) && KSI_CDR (y) == ksi_nil
	  && KSI_REAL_P (KSI_CAR (y)))
        return 1;
    }
  else if (KSI_VEC_P (z) && KSI_VEC_LEN (z) == 2
	   && KSI_REAL_P (KSI_VEC_REF (z, 0))
	   && KSI_REAL_P (KSI_VEC_REF (z, 1)))
    return 1;
  return 0;
}

static ksi_obj
get_x (ksi_obj z)
{
  return KSI_PAIR_P (z) ? KSI_CAR (z) : KSI_VEC_REF (z, 0);
}

static ksi_obj
get_y (ksi_obj z)
{
  if (KSI_PAIR_P (z))
    {
      z = KSI_CDR (z);
      if (KSI_PAIR_P (z))
	z = KSI_CAR (z);
    }
  else
    z = KSI_VEC_REF (z, 1);

  return z;
}

#if 0
/*
 * is_size --
 *      Проверяет является ли обьект размером.
 *      Размер залается парой,
 *      в которой car содержит ширину, а cdr - высоту
 *
 * Scheme procedure
 *      None
 *
 * Return
 *      0 - если обьект является размером
 *      1 - если нет
 *
 * Side effects
 *      None
 */

static int
is_size (ksi_obj x)
{
  if (is_coord (x)
      && ksi_negative_p (get_x (x)) == ksi_false
      && ksi_negative_p (get_y (x)) == ksi_false)
    return 1;

  return 0;
}
#endif


ksi_obj
ksi_draw_point (ksi_obj dc, ksi_obj x, ksi_obj y)
{
  KSI_CHECK (dc, DC_P (dc), "draw-point: invalid draw-context in arg1");

  if (!y && is_coord (x))
    {
      y = get_y (x);
      x = get_x (x);
    }
  else
    {
      KSI_CHECK (x, KSI_REAL_P (x), "draw-point: invalid x-coord in arg2");
      KSI_CHECK (y, KSI_REAL_P (y), "draw-point: invalid x-coord in arg3");
    }

#if defined(X11_GRAPH)

  {
    XGCValues v;
    v.function = ((ksi_dc) dc) -> draw_mode;
    v.foreground = ((ksi_dc) dc) -> foreground;

    XChangeGC (DC_DISPLAY (dc), DC_GC (dc),
	       GCFunction | GCForeground,
	       &v);

    XDrawPoint (DC_DISPLAY (dc), DC_DRAWABLE (dc), DC_GC (dc),
		ksi_num2int (x), ksi_num2int (y));
  }

#elif defined(WIN_GRAPH)

  SetROP2 (DC_HDC (dc), ((ksi_dc) dc) -> draw_mode);
  SetPixel (DC_HDC (dc), ksi_num2int (x, "draw-point"), ksi_num2int (y, "draw-point"),
            ((ksi_dc) dc) -> foreground);

#endif

  return ksi_unspec;
}


ksi_obj
ksi_draw_points (ksi_obj dc, ksi_obj pnt, ksi_obj coord_mode)
{
  int mode;
  size_t i, len;
  XPoint *points;

  KSI_CHECK (dc, DC_P (dc), "draw-points: invalid draw-context in arg1");

  if (KSI_VEC_P (pnt))
    {
      len = KSI_VEC_LEN (pnt);
      points = (XPoint*) alloca (sizeof (XPoint) * len);
      for (i = 0; i < len; ++i)
        {
          ksi_obj z = KSI_VEC_REF (pnt, i);
          KSI_CHECK (z, is_coord (z), "draw-points: invalid coord");

          points[i].x = ksi_num2int (get_x (z), "draw-points");
          points[i].y = ksi_num2int (get_y (z), "draw-points");
        }
    }
  else
    {
      len = KSI_LIST_LEN (pnt);
      KSI_CHECK (pnt, len >= 0, "draw-points: invalid list in arg2");
      points = (XPoint*) alloca (sizeof (XPoint) * len);
      for (i = 0; i < len; ++i, pnt = KSI_CDR (pnt))
        {
          ksi_obj z = KSI_CAR (pnt);
          KSI_CHECK (z, is_coord (z), "draw-points: invalid coord");

          points[i].x = ksi_num2int (get_x (z), "draw-points");
          points[i].y = ksi_num2int (get_y (z), "draw-points");
        }
    }

  if (coord_mode == key_coord_mode_previous)
    mode = CoordModePrevious;
  else
    mode = CoordModeOrigin;

#if defined(X11_GRAPH)

  {
    XGCValues v;
    v.function = ((ksi_dc) dc) -> draw_mode;
    v.foreground = ((ksi_dc) dc) -> foreground;

    XChangeGC (DC_DISPLAY (dc), DC_GC (dc),
	       GCFunction | GCForeground,
	       &v);

    XDrawPoints (DC_DISPLAY (dc), DC_DRAWABLE (dc), DC_GC (dc),
		 points, len, mode);
  }

#elif defined(WIN_GRAPH)

  if (mode == CoordModePrevious)
    for (i = 1; i < len; ++i)
      {
        points[i].x += points[i-1].x;
        points[i].y += points[i-1].y;
      }

  SetROP2 (DC_HDC (dc), ((ksi_dc) dc) -> draw_mode);
  for (i = 0; i < len; ++i)
    SetPixel (DC_HDC (dc), points[i].x, points[i].y,
              ((ksi_dc) dc) -> foreground);

#endif

  return ksi_unspec;
}


#if defined(X11_GRAPH)

static void
set_line_mode (ksi_dc dc)
{
  XGCValues v;
  v.function = dc -> draw_mode;
  v.foreground = dc -> foreground;
  v.background = dc -> background;
  v.line_width = dc -> line_width;
  v.line_style = dc -> line_style;
  v.cap_style = dc -> cap_style;
  v.join_style = dc -> join_style;
  v.fill_style = FillSolid;

  XChangeGC (DC_DISPLAY (dc), DC_GC (dc),
	     GCFunction | GCForeground | GCBackground
	     | GCLineWidth | GCLineStyle | GCCapStyle | GCJoinStyle
	     | GCFillStyle,
	     &v);

  if (dc -> line_style != LineSolid)
    {
      int len;
      char* dashes;

      switch (dc -> dash_mode)
	{
	case Dot:
	  len = 2;
	  dashes = (char*) alloca (2 * sizeof *dashes);
	  dashes [0] = dc -> line_width;
	  dashes [1] = dc -> line_width;
	  break;
	case Dash:
	  len = 2;
	  dashes = (char*) alloca (2 * sizeof *dashes);
	  dashes [0] = dc -> line_width * 4;
	  dashes [1] = dc -> line_width;
	  break;
	case DashDot:
	  len = 4;
	  dashes = (char*) alloca (4 * sizeof *dashes);
	  dashes [0] = dc -> line_width * 4;
	  dashes [1] = dc -> line_width;
	  dashes [2] = dc -> line_width;
	  dashes [3] = dc -> line_width;
	  break;
	case DashDotDot:
	  len = 6;
	  dashes = (char*) alloca (6 * sizeof *dashes);
	  dashes [0] = dc -> line_width * 4;
	  dashes [1] = dc -> line_width;
	  dashes [2] = dc -> line_width;
	  dashes [3] = dc -> line_width;
	  dashes [4] = dc -> line_width;
	  dashes [5] = dc -> line_width;
	  break;
	default:
	  len = dc -> dash_len;
	  dashes = dc -> dashes;
	}

      XSetDashes (DC_DISPLAY (dc), DC_GC (dc), 0, dashes, len);
    }
}

#elif defined(WIN_GRAPH)

/*
 * ksi_set_pen
 *      Подготавливает DC к рисованию линий.
 *
 * Return
 *      None
 *
 * Side effect
 *      Устанавливает новое перо в контекст устройства.
 */

HPEN
ksi_set_pen (ksi_dc dc)
{
  DWORD		style, *dashes;
  HPEN		old;
  int		i, simple, len;

  simple = 1;
  dashes = 0;
  len = 0;
  if (dc->line_style == LineSolid)
    style = PS_SOLID;
  else
    {
      if (dc->line_style == LineOnOffDash)
	SetBkMode (DC_HDC (dc), TRANSPARENT);
      else
	{
          SetBkMode (DC_HDC (dc), OPAQUE);
          SetBkColor (DC_HDC (dc), dc->background);
	}

      switch (dc -> dash_mode)
	{
	case Dot:
	  style = PS_DOT;
	  break;
	case Dash:
	  style = PS_DASH;
	  break;
	case DashDot:
	  style = PS_DASHDOT;
	  break;
	case DashDotDot:
	  style = PS_DASHDOTDOT;
	  break;
	default:
	  simple = 0;
	  style = PS_USERSTYLE;
	  len = dc -> dash_len;
	  dashes = (DWORD*) alloca (len * sizeof *dashes);
	  for (i = 0; i < len; i++)
	    dashes [i] = dc->dashes [i];
	}
    }

#if 0
  if (ksi_joke->win_nt)
    style |= dc->cap_join | PS_GEOMETRIC;
#endif

  if (simple && dc->fill_mode == FillSolid)
    {
      old = SelectObject (DC_HDC (dc),
			  CreatePen (style,
				     dc->line_width,
				     dc->foreground));
    }
  else
    {
      LOGBRUSH brush;
      switch (dc->fill_mode)
	{
	case FillTiled:
	case FillStippled:
	case FillOpaqueStippled:
	  simple = 0;
	  brush.lbStyle = BS_SOLID;
	  brush.lbColor = dc->foreground;
	  brush.lbHatch = 0;
	  break;
	default:
	  brush.lbStyle = BS_SOLID;
	  brush.lbColor = dc->foreground;
	  break;
	}

      old = SelectObject (DC_HDC (dc),
			  ExtCreatePen (style,
					dc->line_width,
					&brush,
					len, dashes));
    }

  return old;
}

#endif


ksi_obj
ksi_draw_line (ksi_obj dc, ksi_obj c1, ksi_obj c2)
{
  XPoint pnt [2];

  KSI_CHECK (dc, DC_P (dc), "draw-line: invalid draw-context in arg1");
  KSI_CHECK (c1, is_coord (c1), "draw-line: invalid coord in arg2");
  KSI_CHECK (c2, is_coord (c2), "draw-line: invalid coord in arg3");

  pnt[0].x = ksi_num2int (get_x (c1), "draw-line");
  pnt[0].y = ksi_num2int (get_y (c1), "draw-line");
  pnt[1].x = ksi_num2int (get_x (c2), "draw-line");
  pnt[1].y = ksi_num2int (get_y (c2), "draw-line");

  if (pnt[0].x == pnt[1].x && pnt[0].y == pnt[1].y)
    return ksi_int2num(0);

#if defined(X11_GRAPH)

  set_line_mode ((ksi_dc) dc);
  XDrawLine (DC_DISPLAY (dc), DC_DRAWABLE (dc), DC_GC (dc),
	     pnt[0].x, pnt[0].y, pnt[1].x, pnt[1].y);

#elif defined(WIN_GRAPH)

  {
    HPEN old_pen = ksi_set_pen ((ksi_dc) dc);

    SetROP2 (DC_HDC (dc), ((ksi_dc) dc) -> draw_mode);
    MoveToEx (DC_HDC (dc), pnt[0].x, pnt[0].y, NULL);
    LineTo (DC_HDC (dc), pnt[1].x, pnt[1].y);

    DeleteObject (SelectObject (DC_HDC (dc), old_pen));
  }

#endif

  return ksi_int2num(2);
}

ksi_obj
ksi_draw_lines (ksi_obj dc, ksi_obj pnt, ksi_obj coord_mode, ksi_obj close)
{
  size_t len, i;
  int num;
  XPoint *points;

  KSI_CHECK (dc, DC_P (dc), "draw-lines: invalid draw-context in arg1");
  if (!close)
    close = ksi_false;

  if (KSI_VEC_P (pnt))
    len = KSI_VEC_LEN (pnt);
  else
    {
      len = KSI_LIST_LEN (pnt);
      KSI_CHECK (pnt, len >= 0, "draw-lines: invalid list in arg3");
    }

  points = (XPoint*) alloca (sizeof (XPoint) * (len+1));
  for (i = 0, num = 0; i < len; ++i)
    {
      ksi_obj z;
      if (KSI_VEC_P (pnt))
	z = KSI_VEC_REF (pnt, i);
      else
	{
	  z = KSI_CAR (pnt);
	  pnt = KSI_CDR (pnt);
	}

      KSI_CHECK (z, is_coord (z), "draw-lines: invalid coord");
      points[num].x = ksi_num2int (get_x (z), "draw-lines");
      points[num].y = ksi_num2int (get_y (z), "draw-lines");
      if (i > 0 && coord_mode == key_coord_mode_previous)
	{
	  points[num].x += points[num-1].x;
	  points[num].y += points[num-1].y;
	}
      if (num == 0
	  || points[num].x != points[num-1].x
	  || points[num].y != points[num-1].y)
	{
	  num++;
	}
    }

  if (num <= 1)
    return ksi_int2num(0);

  if (KSI_TRUE_P (close) && num > 2
      && (points[0].x != points[num-1].x || points[0].y != points[num-1].y))
    {
      points[num].x = points[0].x;
      points[num].y = points[0].y;
      num++;
    }

#if defined(X11_GRAPH)

  set_line_mode ((ksi_dc) dc);
  XDrawLines (DC_DISPLAY (dc), DC_DRAWABLE (dc), DC_GC (dc),
              points, num, CoordModeOrigin);

#elif defined(WIN_GRAPH)

  {
    HPEN old_pen = ksi_set_pen ((ksi_dc) dc);

    SetROP2 (DC_HDC (dc), ((ksi_dc) dc) -> draw_mode);
    Polyline (DC_HDC (dc), points, num);

    DeleteObject (SelectObject (DC_HDC (dc), old_pen));
  }

#endif

  return ksi_int2num(num);
}

ksi_obj
ksi_draw_rect (ksi_obj dc, ksi_obj c1, ksi_obj c2)
{
  int x, y, w, h;

  KSI_CHECK (dc, DC_P (dc), "draw-rectangle: invalid draw-context in arg1");
  KSI_CHECK (c1, is_coord (c1), "draw-rectangle: invalid coord in arg2");
  KSI_CHECK (c2, KSI_REAL_P (c2) || is_coord (c2), "draw-rectangle: invalid size in arg3");

  x = ksi_num2int (get_x (c1), "draw-rectangle");
  y = ksi_num2int (get_y (c1), "draw-rectangle");
  w = ksi_num2int (KSI_REAL_P (c2) ? c2 : get_x (c2), "draw-rectangle");
  h = ksi_num2int (KSI_REAL_P (c2) ? c2 : get_y (c2), "draw-rectangle");

  if (w < 0)
    x += w, w = -w;
  if (h < 0)
    y += h, h = -h;

#if defined(X11_GRAPH)

  set_line_mode ((ksi_dc) dc);
  XDrawRectangle (DC_DISPLAY (dc), DC_DRAWABLE (dc), DC_GC (dc),
                  x, y, w, h);

#elif defined(WIN_GRAPH)

  {
    HPEN old_pen;
    HBRUSH old_brush;

    old_pen = ksi_set_pen ((ksi_dc) dc);
    old_brush = SelectObject (DC_HDC (dc), GetStockObject (NULL_BRUSH));

    SetROP2 (DC_HDC (dc), ((ksi_dc) dc) -> draw_mode);
    Rectangle (DC_HDC (dc), x, y, x+w, y+h);

    DeleteObject (SelectObject (DC_HDC (dc), old_pen));
    SelectObject (DC_HDC (dc), old_brush);
  }

#endif

  return ksi_unspec;
}

ksi_obj
ksi_draw_ellipse (ksi_obj dc, ksi_obj c1, ksi_obj c2)
{
  int x, y, w, h;

  KSI_CHECK (dc, DC_P (dc), "draw-ellipse: invalid draw-context in arg1");
  KSI_CHECK (c1, is_coord (c1), "draw-ellipse: invalid coord in arg2");
  KSI_CHECK (c2, KSI_REAL_P (c2) || is_coord (c2), "draw-ellipse: invalid size in arg3");

  x = ksi_num2int (get_x (c1), "draw-ellipse");
  y = ksi_num2int (get_y (c1), "draw-ellipse");
  w = ksi_num2int (KSI_REAL_P (c2) ? c2 : get_x (c2), "draw-ellipse");
  h = ksi_num2int (KSI_REAL_P (c2) ? c2 : get_y (c2), "draw-ellipse");

  if (w < 0)
    x += w, w = -w;
  if (h < 0)
    y += h, h = -h;

#if defined(X11_GRAPH)

  set_line_mode ((ksi_dc) dc);
  XDrawArc (DC_DISPLAY (dc), DC_DRAWABLE (dc), DC_GC (dc),
            x, y, w, h, 0, 360*64);

#elif defined(WIN_GRAPH)

  {
    HPEN old_pen;
    HBRUSH old_brush;

    old_pen = ksi_set_pen ((ksi_dc) dc);
    old_brush = SelectObject (DC_HDC (dc), GetStockObject (NULL_BRUSH));

    SetROP2 (DC_HDC (dc), ((ksi_dc) dc) -> draw_mode);
    Ellipse (DC_HDC (dc), x, y, x+w, y+h);

    DeleteObject (SelectObject (DC_HDC (dc), old_pen));
    SelectObject (DC_HDC (dc), old_brush);
  }

#endif

  return ksi_unspec;
}


ksi_obj
ksi_draw_arc (ksi_obj dc, ksi_obj c1, ksi_obj c2, ksi_obj a1, ksi_obj a2)
{
  int x, y, w, h, s, l;

  KSI_CHECK (dc, DC_P (dc), "draw-arc: invalid draw-context in arg1");
  KSI_CHECK (c1, is_coord (c1), "draw-arc: invalid coord in arg2");
  KSI_CHECK (c2, KSI_REAL_P (c2) || is_coord (c2), "draw-arc: invalid size in arg3");
  KSI_CHECK (a1, KSI_REAL_P (a1), "draw-arc: invalid angle in arg4");
  KSI_CHECK (a2, KSI_REAL_P (a2), "draw-arc: invalid angle in arg5");

  x = ksi_num2int (get_x (c1), "draw-arc");
  y = ksi_num2int (get_y (c1), "draw-arc");
  w = ksi_num2int (KSI_REAL_P (c2) ? c2 : get_x (c2), "draw-arc");
  h = ksi_num2int (KSI_REAL_P (c2) ? c2 : get_y (c2), "draw-arc");
  s = ksi_num2int (a1, "draw-arc") % 3600;
  l = ksi_num2int (a2, "draw-arc") % 3600;

  if (w < 0)
    x += w, w = -w;
  if (h < 0)
    y += h, h = -h;

#if defined(X11_GRAPH)

  set_line_mode ((ksi_dc) dc);
  XDrawArc (DC_DISPLAY (dc), DC_DRAWABLE (dc), DC_GC (dc),
            x, y, w, h, (s*64+5)/10, (l*64+5)/10);

#elif defined(WIN_GRAPH)

  if (s < 0)
    s += 3600;
  if (l < 0)
    {
      s += l;
      l = -l;
    }

  {
    HPEN old_pen;
    HBRUSH old_brush;
    double r = sqrt ((double)w * w + (double)h * h + 100.0);
    double s1 = s / 1800.0 * M_PI;
    double s2 = (s + l) / 1800.0 * M_PI;

    old_pen = ksi_set_pen ((ksi_dc) dc);
    old_brush = SelectObject (DC_HDC (dc), GetStockObject (NULL_BRUSH));

    SetROP2 (DC_HDC (dc), ((ksi_dc) dc) -> draw_mode);
    Arc (DC_HDC (dc),
	 x, y, x + w, y + h,
	 x + w/2 + r * cos (s1),
	 y + h/2 - r * sin (s1),
	 x + w/2 + r * cos (s2),
	 y + h/2 - r * sin (s2));

    DeleteObject (SelectObject (DC_HDC (dc), old_pen));
    SelectObject (DC_HDC (dc), old_brush);
  }

#endif

  return ksi_unspec;
}


#if defined(X11_GRAPH)

static void
set_fill_mode (ksi_dc dc)
{
  XGCValues v;
  v.function = dc -> draw_mode;
  v.foreground = dc -> foreground;
  v.background = dc -> background;
  v.fill_style = FillSolid;

  XChangeGC (DC_DISPLAY (dc), DC_GC (dc),
	     GCFunction | GCForeground | GCBackground
	     | GCFillStyle,
	     &v);

}

#elif defined(WIN_GRAPH)
/*
 * ksi_set_brush
 *      Подготавливает DC к рисованию заполненных областей.
 *
 * Return
 *      None
 *
 * Side effect
 *      Устанавливает новую кисть в контекст устройства.
 */

HBRUSH
ksi_set_brush (ksi_dc dc)
{
  HBRUSH	old;
  LOGBRUSH	brush;

  switch (dc->fill_mode)
    {
    case FillTiled:
    case FillStippled:
    case FillOpaqueStippled:
      brush.lbStyle = BS_SOLID;
      brush.lbColor = dc->foreground;
      brush.lbHatch = 0;
      break;

    default:
      brush.lbStyle = BS_SOLID;
      brush.lbColor = dc->foreground;
      break;
    }

  old = SelectObject (DC_HDC (dc), CreateBrushIndirect (&brush));

  return old;
}

#endif


ksi_obj
ksi_fill_rect (ksi_obj dc, ksi_obj c1, ksi_obj c2)
{
  int x, y, w, h;
  KSI_CHECK (dc, DC_P (dc), "fill-rectangle: invalid draw-context in arg1");
  KSI_CHECK (c1, is_coord (c1), "fill-rectangle: invalid coord in arg2");
  KSI_CHECK (c2, KSI_REAL_P (c2) || is_coord (c2), "fill-rectangle: invalid size in arg3");

  x = ksi_num2int (get_x (c1), "fill-rectangle");
  y = ksi_num2int (get_y (c1), "fill-rectangle");
  w = ksi_num2int (KSI_REAL_P (c2) ? c2 : get_x (c2), "fill-rectangle");
  h = ksi_num2int (KSI_REAL_P (c2) ? c2 : get_y (c2), "fill-rectangle");

  if (w < 0)
    x += w, w = -w;
  if (h < 0)
    y += h, h = -h;

#if defined(X11_GRAPH)

  set_fill_mode ((ksi_dc) dc);
  XFillRectangle (DC_DISPLAY (dc), DC_DRAWABLE (dc), DC_GC (dc),
                  x, y, w, h);

#else

  {
    HPEN old_pen;
    HBRUSH old_brush;

    old_pen = SelectObject (DC_HDC (dc), GetStockObject (NULL_PEN));
    old_brush = ksi_set_brush ((ksi_dc) dc);

    SetROP2 (DC_HDC (dc), ((ksi_dc) dc) -> draw_mode);
    Rectangle (DC_HDC (dc), x, y, x+w, y+h);

    SelectObject (DC_HDC (dc), old_pen);
    DeleteObject (SelectObject (DC_HDC (dc), old_brush));
  }

#endif

  return ksi_unspec;
}

ksi_obj
ksi_fill_ellipse (ksi_obj dc, ksi_obj c1, ksi_obj c2)
{
  int x, y, w, h;

  KSI_CHECK (dc, DC_P (dc), "fill-ellipse: invalid draw-context in arg1");
  KSI_CHECK (c1, is_coord (c1), "fill-ellipse: invalid coord in arg2");
  KSI_CHECK (c2, KSI_REAL_P (c2) || is_coord (c2), "fill-ellipse: invalid size in arg3");

  x = ksi_num2int (get_x (c1), "fill-ellipse");
  y = ksi_num2int (get_y (c1), "fill-ellipse");
  w = ksi_num2int (KSI_REAL_P (c2) ? c2 : get_x (c2), "fill-ellipse");
  h = ksi_num2int (KSI_REAL_P (c2) ? c2 : get_y (c2), "fill-ellipse");

  if (w < 0)
    x += w, w = -w;
  if (h < 0)
    y += h, h = -h;

#if defined(X11_GRAPH)

  set_fill_mode ((ksi_dc) dc);
  XFillArc (DC_DISPLAY (dc), DC_DRAWABLE (dc), DC_GC (dc),
            x, y, w, h, 0, 360*64);

#else

  {
    HPEN old_pen;
    HBRUSH old_brush;

    old_pen = SelectObject (DC_HDC (dc), GetStockObject (NULL_PEN));
    old_brush = ksi_set_brush ((ksi_dc) dc);

    SetROP2 (DC_HDC (dc), ((ksi_dc) dc) -> draw_mode);
    Ellipse (DC_HDC (dc), x, y, x+w, y+h);

    SelectObject (DC_HDC (dc), old_pen);
    DeleteObject (SelectObject (DC_HDC (dc), old_brush));
  }

#endif

  return ksi_unspec;
}

ksi_obj
ksi_fill_arc (ksi_obj dc, ksi_obj c1, ksi_obj c2, ksi_obj a1, ksi_obj a2)
{
  int x, y, w, h, s, l;

  KSI_CHECK (dc, DC_P (dc), "fill-arc: invalid draw-context in arg1");
  KSI_CHECK (c1, is_coord (c1), "fill-arc: invalid coord in arg2");
  KSI_CHECK (c2, KSI_REAL_P (c2) || is_coord (c2), "fill-arc: invalid size in arg3");
  KSI_CHECK (a1, KSI_REAL_P (a1), "fill-arc: invalid angle in arg4");
  KSI_CHECK (a2, KSI_REAL_P (a2), "fill-arc: invalid angle in arg5");

  x = ksi_num2int (get_x (c1), "fill-arc");
  y = ksi_num2int (get_y (c1), "fill-arc");
  w = ksi_num2int (KSI_REAL_P (c2) ? c2 : get_x (c2), "fill-arc");
  h = ksi_num2int (KSI_REAL_P (c2) ? c2 : get_y (c2), "fill-arc");
  s = ksi_num2int (a1, "fill-arc") % 3600;
  l = ksi_num2int (a2, "fill-arc") % 3600;

  if (w < 0)
    x += w, w = -w;
  if (h < 0)
    y += h, h = -h;

#if defined(X11_GRAPH)

  XSetArcMode (DC_DISPLAY (dc), DC_GC (dc), ((ksi_dc) dc) -> arc_mode);
  set_fill_mode ((ksi_dc) dc);
  XFillArc (DC_DISPLAY (dc), DC_DRAWABLE (dc), DC_GC (dc),
            x, y, w, h, (s*64+5)/10, (l*64+5)/10);

#else

  if (s < 0)
    s += 3600;
  if (l < 0)
    {
      s += l;
      l = -l;
    }

  {
    HPEN old_pen;
    HBRUSH old_brush;
    double r = sqrt ((double)w*w + (double)h*h + 100.0);
    double s1 = s / 1800.0 * M_PI;
    double s2 = (s + l) / 1800.0 * M_PI;

    old_pen = SelectObject (DC_HDC (dc), GetStockObject (NULL_PEN));
    old_brush = ksi_set_brush ((ksi_dc) dc);

    SetROP2 (DC_HDC (dc), ((ksi_dc) dc) -> draw_mode);
    if (((ksi_dc) dc) -> arc_mode == ArcPieSlice)
      Pie (DC_HDC (dc), x, y, x + w, y + h,
	   x + w/2 + r * cos (s1),
	   y + h/2 - r * sin (s1),
	   x + w/2 + r * cos (s2),
	   y + h/2 - r * sin (s2));
    else
      Chord (DC_HDC (dc), x, y, x+w, y+h,
	     x + w/2 + r * cos (s1),
	     y + h/2 - r * sin (s1),
	     x + w/2 + r * cos (s2),
	     y + h/2 - r * sin (s2));

    SelectObject (DC_HDC (dc), old_pen);
    DeleteObject (SelectObject (DC_HDC (dc), old_brush));
  }

#endif

  return ksi_unspec;
}


ksi_obj
ksi_fill_polygon (ksi_obj dc, ksi_obj pnt, ksi_obj coord_mode)
{
  size_t i, len;
  int mode;
  XPoint *points;

  KSI_CHECK (dc, DC_P (dc), "fill-polygon: invalid draw-context in arg1");

  if (KSI_VEC_P (pnt))
    {
      len = KSI_VEC_LEN (pnt);
      points = (XPoint*) alloca (sizeof (XPoint) * (len+1));
      for (i = 0; i < len; ++i)
        {
          ksi_obj z = KSI_VEC_REF (pnt, i);
          KSI_CHECK (z, is_coord (z), "fill-polygon: invalid coord");

          points[i].x = ksi_num2int (get_x (z), "fill-polygon");
          points[i].y = ksi_num2int (get_y (z), "fill-polygon");
        }
    }
  else
    {
      len = KSI_LIST_LEN (pnt);
      KSI_CHECK (pnt, len >= 0, "fill-polygon: invalid list in arg3:");
      points = (XPoint*) alloca (sizeof (XPoint) * (len+1));
      for (i = 0; i < len; ++i, pnt = KSI_CDR (pnt))
        {
          ksi_obj z = KSI_CAR (pnt);
          KSI_CHECK (z, is_coord (z), "fill-polygon: invalid coord");

          points[i].x = ksi_num2int (get_x (z), "fill-polygon");
          points[i].y = ksi_num2int (get_y (z), "fill-polygon");
        }
    }

  if (len <= 0)
    return ksi_unspec;

  if (coord_mode == key_coord_mode_previous)
    mode = CoordModePrevious;
  else
    mode = CoordModeOrigin;

  if (mode == CoordModePrevious)
    for (i = 1; i < len; ++i)
      {
        points[i].x += points[i-1].x;
        points[i].y += points[i-1].y;
      }

  points[len].x = points[0].x;
  points[len].y = points[0].y;

#if defined(X11_GRAPH)

  set_fill_mode ((ksi_dc) dc);
  XFillPolygon (DC_DISPLAY (dc), DC_DRAWABLE (dc), DC_GC (dc),
                points, len, Complex, CoordModeOrigin);

#elif defined(WIN_GRAPH)

  {
    HPEN old_pen;
    HBRUSH old_brush;

    old_pen = SelectObject (DC_HDC (dc), GetStockObject (NULL_PEN));
    old_brush = ksi_set_brush ((ksi_dc) dc);

    SetROP2 (DC_HDC (dc), ((ksi_dc) dc) -> draw_mode);
    Polygon (DC_HDC (dc), points, (int)len);

    SelectObject (DC_HDC (dc), old_pen);
    DeleteObject (SelectObject (DC_HDC (dc), old_brush));
  }

#endif

  return ksi_unspec;
}


#if defined(X11_GRAPH)

static char*
font_name (ksi_font font, int angle, int size)
{
  int		i, len;
  char		*buf, *ptr;

  buf = ksi_aprintf ("[%g %g %g %g]",  cos (TORAD (angle)) * size, sin (TORAD (angle)) * size,
				       -sin (TORAD (angle)) * size, cos (TORAD (angle)) * size);

  for (i = 0; buf [i]; i++)
    if (buf [i] == '-')
      buf [i] = '~';

  len = strlen (buf) + 20;
  len += strlen (FONT_FAMILY (font));
  len += strlen (FONT_WEIGHT (font));
  len += strlen (FONT_STYLE (font));
  len += strlen (FONT_CHARSET (font));

  ptr = ksi_aprintf ("-%s-%s-%s-%s-*-*-*-*-*-%s",
	   FONT_FAMILY (font),
	   FONT_WEIGHT (font),
	   FONT_STYLE (font),
	   buf,
	   FONT_CHARSET (font));

  return ptr;
}

static XFontStruct*
load_font (ksi_dc dc, ksi_font font, int angle, int size)
{
  char *name = font_name (font, angle, size);
  return XLoadQueryFont (DC_DISPLAY (dc), name);
}

static double
char_width (XFontStruct* fs, unsigned char ch)
{
  XCharStruct* cs;

  if (fs -> per_char == 0)
    cs = & (fs -> max_bounds);
  else
    {
      unsigned min_ch = fs -> min_char_or_byte2;
      unsigned max_ch = fs -> max_char_or_byte2;
      if (min_ch <= ch && ch <= max_ch)
	cs = & (fs -> per_char [ch - min_ch]);
      else
	cs = 0;
    }

  return cs ? ((double) cs -> attributes) / 1000.0 : 0;
}

static void
draw_string (Display* dpy, Drawable d, GC gc,
	     XFontStruct* fs, int size,
	     int x, int y, int angle, char* str, int len)
{
  if (angle == 0 && size > 8)
    XDrawString (dpy, d, gc, x, y, str, len);
  else
    {
      int	i;
      double	s = sin (TORAD (angle));
      double	c = cos (TORAD (angle));
      double	width = 0;

      for (i = 0; i < len; i++)
	{
	  XDrawString (dpy, d, gc,
		       x + ((int) (width * c)),
		       y - ((int) (width * s)),
		       & str[i], 1);

	  width += char_width (fs, str[i]) * size;
	}
    }
}

#elif defined(WIN_GRAPH)

static void
get_logfont (LOGFONTW *lf, ksi_font font, int angle, int size)
{
  int width = 0, weight = FW_DONTCARE;
  int italic = 0, underline = 0;
  int charset = DEFAULT_CHARSET;

  if (ksi_wcscasecmp (font->weight, L"extralight") == 0)
    weight = FW_EXTRALIGHT;
  else if (ksi_wcscasecmp (font->weight, L"light") == 0)
    weight = FW_LIGHT;
  else if (ksi_wcscasecmp (font->weight, L"normal") == 0)
    weight = FW_NORMAL;
  else if (ksi_wcscasecmp (font->weight, L"medium") == 0)
    weight = FW_MEDIUM;
  else if (ksi_wcscasecmp (font->weight, L"semibold") == 0)
    weight = FW_SEMIBOLD;
  else if (ksi_wcscasecmp (font->weight, L"bold") == 0)
    weight = FW_BOLD;
  else if (ksi_wcscasecmp (font->weight, L"extrabold") == 0)
    weight = FW_EXTRABOLD;

  if (font->style [0] == L'i' || font->style [0] == L'I'
      || font->style [1] == L'i' || font->style [1] == L'I')
    italic = 1;

  if (font->style [0] == L'u' || font->style [0] == L'U'
      || font->style [1] == L'u' || font->style [1] == L'U')
    underline = 1;

  if (ksi_wcscasecmp (font->charset, L"ansi") == 0)
    charset = ANSI_CHARSET;
  else if (ksi_wcscasecmp (font->charset, L"oem") == 0)
    charset = OEM_CHARSET;
  else if (ksi_wcscasecmp (font->charset, L"symbol") == 0)
    charset = SYMBOL_CHARSET;
  else if (ksi_wcscasecmp (font->charset, L"arabic") == 0)
    charset = ARABIC_CHARSET;
  else if (ksi_wcscasecmp (font->charset, L"greek") == 0)
    charset = GREEK_CHARSET;
  else if (ksi_wcscasecmp (font->charset, L"easteurope") == 0)
    charset = EASTEUROPE_CHARSET;
  else if (ksi_wcscasecmp (font->charset, L"russian") == 0)
    charset = RUSSIAN_CHARSET;
  else if (ksi_wcscasecmp (font->charset, L"baltic") == 0)
    charset = BALTIC_CHARSET;

  lf->lfHeight = size;
  lf->lfWidth  = width;
  lf->lfEscapement = angle;
  lf->lfOrientation = angle;
  lf->lfWeight = weight;
  lf->lfItalic = italic;
  lf->lfUnderline = underline;
  lf->lfStrikeOut = 0;
  lf->lfCharSet = charset;
  lf->lfOutPrecision = OUT_TT_PRECIS;
  lf->lfClipPrecision = CLIP_DEFAULT_PRECIS;
  lf->lfQuality = DEFAULT_QUALITY;
  lf->lfPitchAndFamily = DEFAULT_PITCH | FF_DONTCARE;
  if (font->family [0] == '*')
    lf->lfFaceName [0] = '\0';
  else
    wcsncpy (lf->lfFaceName, font->family, sizeof (lf->lfFaceName));
}

static HFONT
load_font (ksi_dc dc, ksi_font font, int angle, int size)
{
  LOGFONTW lf;

  get_logfont (&lf, font, angle, size);
  return CreateFontIndirectW(&lf);
}

void
CalcTextBox (HDC hdc, int x, int y, int angle,
	     const wchar_t* str, int len, RECT* box)
{
  /* this function correct only if Y-axiz is from top to down */

  SIZE ext;
  TEXTMETRICW tm;
  int dy, W, H;

  GetTextExtentPointW (hdc, str, len, &ext);
  GetTextMetricsW (hdc, &tm);

  dy = MAX (tm.tmDescent, 0);
  W = ext.cx + 2 * dy;
  H = ext.cy;

  if (angle == 0)
    {
      box -> left = x - dy;
      box -> right = box -> left + W;
      box -> bottom = y + dy;
      box -> top = box -> bottom - H;
    }
  else
    {
      double a = TORAD (angle);
      double wx = W * cos (a);
      double wy = -W * sin (a);
      double hx = -H * sin (a);
      double hy = -H * cos (a);
      double x1 = (-dy * cos (a) + dy * sin (a));
      double y1 = (dy * cos (a) + dy * sin (a));
      double x2 = x1 + wx;
      double y2 = y1 + wy;
      double x3 = x2 + hx;
      double y3 = y2 + hy;
      double x4 = x3 - wx;
      double y4 = y3 - wy;

      box -> left = x + (int) MIN (MIN (x1, x2), MIN (x3, x4));
      box -> right = x + (int) MAX (MAX (x1, x2), MAX (x3, x4));
      box -> top = y + (int) MIN (MIN (y1, y2), MIN (y3, y4));
      box -> bottom = y + (int) MAX (MAX (y1, y2), MAX (y3, y4));
    }
}

static int CALLBACK
enum_font_proc (CONST LOGFONTW *lplf,
		CONST TEXTMETRICW *lpntm,
		DWORD FontType,
		LPARAM  lParam)
{
  ksi_obj *res = (ksi_obj*) lParam;
  *res = ksi_cons (ksi_str02string (lplf->lfFaceName), *res);
  return 1;
}

#endif


static ksi_obj
ksi_font_names (ksi_obj dc)
{
#if defined(X11_GRAPH)

  char **list, *name;
  int i, num;
  ksi_obj res, x;

  KSI_CHECK (dc, DC_P (dc), "font-names: invalid dc in arg1");

  name = font_name (((ksi_dc) dc) -> font,
		    ((ksi_dc) dc) -> font_angle,
		    ((ksi_dc) dc) -> font_size);

  list = XListFonts (DC_DISPLAY (dc), name, 400, &num);
  for (i = 0, res = ksi_nil; i < num; i++)
    {
      x = ksi_str02string (list [i]);
      res = ksi_cons (x, res);
    }
  XFreeFontNames (list);

  return res;

#elif defined(WIN_GRAPH)

  LOGFONTW lf;
  ksi_obj res = ksi_nil;

  KSI_CHECK (dc, DC_P (dc), "font-names: invalid dc in arg1");

  get_logfont (&lf,
	       ((ksi_dc) dc) -> font,
	       ((ksi_dc) dc) -> font_angle,
	       ((ksi_dc) dc) -> font_size);

  EnumFontFamiliesW (DC_HDC (dc),
		    lf.lfFaceName,
		    enum_font_proc,
		    (LPARAM) &res);

  return res;

#endif
}

ksi_obj
ksi_draw_string (ksi_obj dc, ksi_obj pnt, ksi_obj str)
{
  int x, y, size, angle;

  KSI_CHECK (dc, DC_P (dc), "draw-string: invalid draw-context in arg1");
  KSI_CHECK (pnt, is_coord (pnt), "draw-string: invalid coord in arg2");
  KSI_CHECK (str, KSI_STR_P (str), "draw-string: invalid string in arg3");

  x     = ksi_num2int (get_x (pnt), "draw-string");
  y     = ksi_num2int (get_y (pnt), "draw-string");
  size  = ((ksi_dc) dc) -> font_size;
  angle = ((ksi_dc) dc) -> font_angle;

#if defined(X11_GRAPH)

  {
    XFontStruct *fs;

    fs = load_font ((ksi_dc) dc, ((ksi_dc) dc) -> font, angle, size);
    if (fs)
      {
	XSetFont (DC_DISPLAY (dc), DC_GC (dc), fs->fid);
	set_fill_mode ((ksi_dc) dc);
	draw_string (DC_DISPLAY (dc), DC_DRAWABLE (dc), DC_GC (dc),
		     fs, size, x, y, angle,
		     KSI_STR_PTR (str), KSI_STR_LEN (str));

	XFreeFont (DC_DISPLAY (dc), fs);
      }
  }

#elif defined(WIN_GRAPH)

  {
    HFONT hf;

    hf = load_font ((ksi_dc) dc, ((ksi_dc) dc) -> font, angle, size);
    if (hf)
      {
	if (((ksi_dc) dc) -> draw_mode == GXcopy)
	  {
	    HFONT old = SelectObject (DC_HDC (dc), hf);
	    SetTextAlign (DC_HDC (dc), TA_LEFT | TA_BASELINE);
	    SetTextColor (DC_HDC (dc), ((ksi_dc) dc) -> foreground);
	    TextOutW (DC_HDC (dc), x, y, KSI_STR_PTR(str), (int)KSI_STR_LEN(str));
	    DeleteObject (SelectObject (DC_HDC (dc), old));
	  }
	else
	  {
	    RECT	box, dev_box;
	    SIZE	old_win_ext, old_view_ext;
	    POINT	old_win_org, old_view_org;
	    HDC		hdc, mem;
	    HBITMAP	bmp, old_bmp;
	    HFONT	old_fnt;
	    HBRUSH	old_brush;
	    DWORD	rop;
	    int		w, h, old_map_mode;
	    const char	*err;

	    hdc = DC_HDC (dc);
	    mem = CreateCompatibleDC (hdc);
	    if (!mem)
	      {
		DeleteObject (hf);
		ksi_exn_error (0, 0, "draw-string: can't alloc memory hdc");
	      }

	    old_fnt = SelectObject (hdc, hf);
	    CalcTextBox (hdc, x, y, angle, KSI_STR_PTR(str), (int)KSI_STR_LEN(str), &box);
	    SelectObject (hdc, old_fnt);

	    dev_box = box;
	    LPtoDP (hdc, (POINT*) &dev_box, 2);

	    w = (ABS (dev_box.right - dev_box.left + 2) + 31) & ~31;
	    h = ABS (dev_box.bottom - dev_box.top + 2);
#if 1
	    bmp = CreateBitmap (w, h, 1, 1, 0);
#else
	    bmp = CreateCompatibleBitmap (hdc, w, h);
#endif
	    if (!bmp)
	      {
		DeleteObject (hf);
		DeleteDC (mem);
		ksi_exn_error (0, 0, "draw-string: can't alloc bitmap: %d x %d", w, h);
	      }

	    old_bmp = SelectObject (mem, bmp);
	    old_fnt = SelectObject (mem, hf);
	    PatBlt (mem, 0, 0, w, h, WHITENESS);

	    old_map_mode = GetMapMode (hdc);
	    GetWindowExtEx (hdc, &old_win_ext);
	    GetViewportExtEx (hdc, &old_view_ext);
	    GetWindowOrgEx (hdc, &old_win_org);
	    GetViewportOrgEx (hdc, &old_view_org);

	    SetMapMode (mem, old_map_mode);
	    SetWindowExtEx (mem, old_win_ext.cx, old_win_ext.cy, 0);
	    SetViewportExtEx (mem, old_view_ext.cx, old_view_ext.cy, 0);

	    SetTextAlign (mem, TA_LEFT | TA_BASELINE);
	    SetTextColor (mem, 0);
	    SetBkMode (mem, TRANSPARENT);
	    TextOutW (mem, x - box.left, y - box.top, KSI_STR_PTR(str), (int)KSI_STR_LEN(str));

	    switch (((ksi_dc) dc) -> draw_mode)
	      {
	      case R2_XORPEN:	rop = 0x009a0709; break; /* xor */
	      case R2_MERGEPEN:	rop = 0x00ba0b09; break; /* or */
	      case R2_MASKPEN:	rop = 0x00a803a9; break; /* and */
	      default:		rop = 0x00b8074a; break; /* copy */
	      }

	    old_brush = ksi_set_brush ((ksi_dc) dc);

	    SetMapMode (mem, MM_TEXT);
	    SetViewportOrgEx (mem, 0, 0, 0);
	    SetWindowOrgEx (mem, 0, 0, 0);

	    SetMapMode (hdc, MM_TEXT);
	    SetViewportOrgEx (hdc, 0, 0, 0);
	    SetWindowOrgEx (hdc, 0, 0, 0);

	    if (!BitBlt (hdc, dev_box.left, dev_box.top,
			 ABS (dev_box.right - dev_box.left),
			 ABS (dev_box.bottom - dev_box.top),
			 mem, 0, 0,
			 rop))
	      {
		err = ksi_get_last_error ("");
	      }
	    else
	      {
		err = 0;
	      }

	    SetMapMode (hdc, old_map_mode);
	    SetWindowExtEx (hdc, old_win_ext.cx, old_win_ext.cy, 0);
	    SetViewportExtEx (hdc, old_view_ext.cx, old_view_ext.cy, 0);
	    SetWindowOrgEx (hdc, old_win_org.x, old_win_org.y, 0);
	    SetViewportOrgEx (hdc, old_view_org.x, old_view_org.y, 0);

	    DeleteObject (SelectObject (hdc, old_brush));
	    DeleteObject (SelectObject (mem, old_bmp));
	    DeleteObject (SelectObject (mem, old_fnt));
	    DeleteDC (mem);

	    if (err)
	      {
		ksi_exn_error (0, 0, "draw-string: can't output bitmap: %s\n"
			   "\tleft=%d right=%d top=%d bottom=%d\n"
			   "\tleft=%d right=%d top=%d bottom=%d\n"
			   "\tw=%d h=%d",
			   err,
			   box.left, box.right, box.top, box.bottom,
			   dev_box.left, dev_box.right,
			   dev_box.top, dev_box.bottom,
			   w, h);
	      }
	  }
      }
  }

#endif

  return ksi_unspec;
}

ksi_obj
ksi_string_width (ksi_obj dc, ksi_obj str)
{
  int size, angle;

  KSI_CHECK (dc, DC_P (dc), "string-width: invalid draw-context in arg1");
  KSI_CHECK (str, KSI_STR_P (str), "string-width: invalid string in arg2");

  size  = ((ksi_dc) dc) -> font_size;
  angle = ((ksi_dc) dc) -> font_angle;

#if defined(X11_GRAPH)

  {
    XFontStruct *fs;
    double	w = 0.0;

    fs = load_font ((ksi_dc) dc, ((ksi_dc) dc) -> font, angle, size);
    if (fs)
      {
	int		i, len = KSI_STR_LEN (str);
	const char	*ptr = KSI_STR_PTR (str);

	for (i = 0; i < len; i++)
	  w += char_width (fs, ptr [i]) * ((ksi_dc) dc) -> font_size;

	XFreeFont (DC_DISPLAY (dc), fs);
      }

    return ksi_double2exact (w);
  }

#elif defined(WIN_GRAPH)

  {
    SIZE sz;
    HFONT hf;

    hf = load_font ((ksi_dc) dc, ((ksi_dc) dc) -> font, angle, size);
    hf = SelectObject (DC_HDC (dc), hf);
    GetTextExtentPointW (DC_HDC (dc), KSI_STR_PTR(str), (int)KSI_STR_LEN(str), &sz);
    DeleteObject (SelectObject (DC_HDC (dc), hf));

    return ksi_int2num(sz.cx);
  }

#endif
}

ksi_obj
ksi_string_height (ksi_obj dc, ksi_obj str)
{
  KSI_CHECK (dc, DC_P (dc), "string-height: invalid draw-context in arg1");
  KSI_CHECK (str, KSI_STR_P (str), "string-width: invalid string in arg2");

  return ksi_int2num (((ksi_dc) dc) -> font_size);
}


static struct Ksi_Prim_Def defs [] =
{
  { L"draw-point",	ksi_draw_point,		KSI_CALL_ARG3, 2 },
  { L"draw-points",	ksi_draw_points,	KSI_CALL_ARG3, 2 },
  { L"draw-line",	ksi_draw_line,		KSI_CALL_ARG3, 3 },
  { L"draw-lines",	ksi_draw_lines,		KSI_CALL_ARG4, 2 },
  { L"draw-rectangle",	ksi_draw_rect,		KSI_CALL_ARG3, 3 },
  { L"draw-ellipse",	ksi_draw_ellipse,	KSI_CALL_ARG3, 3 },
  { L"draw-arc",	ksi_draw_arc,		KSI_CALL_ARG5, 5 },

  { L"fill-rectangle",	ksi_fill_rect,		KSI_CALL_ARG3, 3 },
  { L"fill-polygon",	ksi_fill_polygon,	KSI_CALL_ARG3, 2 },
  { L"fill-ellipse",	ksi_fill_ellipse,	KSI_CALL_ARG3, 3 },
  { L"fill-arc",	ksi_fill_arc,		KSI_CALL_ARG5, 5 },

  { L"draw-string",	ksi_draw_string,	KSI_CALL_ARG3, 3 },
  { L"string-width",	ksi_string_width,	KSI_CALL_ARG2, 2 },
  { L"string-height",	ksi_string_height,	KSI_CALL_ARG2, 2 },

  { L"font-names",	ksi_font_names,		KSI_CALL_ARG1, 1 },

  { 0 },
};

void
ksi_joke_init_draw (ksi_env env)
{
  ksi_reg_unit (defs, env);
}

 /* End of code */
