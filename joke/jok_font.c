/*
 * jok_font.c
 * fonts
 *
 * Copyright (C) 1997-2000, Ivan Demakov
 *
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose, without fee, and without a written agreement
 * is hereby granted, provided that the above copyright notice and this
 * paragraph and the following two paragraphs appear in all copies.
 * Modifications to this software may be copyrighted by their authors
 * and need not follow the licensing terms described here, provided that
 * the new terms are clearly indicated on the first page of each file where
 * they apply.
 *
 * IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY
 * FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES,
 * INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS
 * DOCUMENTATION, EVEN IF THE AUTHORS HAS BEEN ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIMS ANY WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS
 * ON AN "AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAS NO OBLIGATIONS
 * TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 *
 *
 * Author:        Ivan Demakov <demakov@users.sourceforge.net>
 * Creation date: Wed Oct  8 16:11:55 1997
 * Last Update:   Tue Mar 21 18:58:19 2000
 *
 */

#include "jok_int.h"

static int
font_tag_equal (struct Ksi_EObj *x1, struct Ksi_EObj *x2, int deep)
{
  if (deep
      && ksi_wcscasecmp (FONT_FAMILY (x1), FONT_FAMILY (x2)) == 0
      && ksi_wcscasecmp (FONT_CHARSET (x1), FONT_CHARSET (x2)) == 0
      && ksi_wcscasecmp (FONT_WEIGHT (x1), FONT_WEIGHT (x2)) == 0
      && ksi_wcscasecmp (FONT_STYLE (x1), FONT_STYLE (x2)) == 0)
    {
      return 1;
    }

  return 0;
}

static const wchar_t*
font_tag_print (struct Ksi_EObj *x, int slashify)
{
    return ksi_aprintf("#<font -%ls-%ls-%ls-*-%ls>",
	   FONT_FAMILY (x), FONT_WEIGHT (x),
	   FONT_STYLE (x), FONT_CHARSET (x));
}

static struct Ksi_ETag tag_font =
{
  L"font",
  font_tag_equal,
  font_tag_print
};


ksi_obj
ksi_make_font (ksi_obj family, ksi_obj weight, ksi_obj style, ksi_obj chset)
{
  ksi_font font;

  if (!weight)
    weight = def_font_weight;
  if (!style)
    style = def_font_style;
  if (!chset)
    chset = def_font_charset;

  KSI_CHECK (family, KSI_STR_P (family), "make-font: invalid string in arg1");
  KSI_CHECK (weight, KSI_STR_P (weight), "make-font: invalid string in arg2");
  KSI_CHECK (style, KSI_STR_P (style), "make-font: invalid string in arg3");
  KSI_CHECK (chset, KSI_STR_P (chset), "make-font: invalid string in arg4");

  font = (ksi_font) ksi_malloc (sizeof *font);
  font->ko.o.itag = KSI_TAG_EXTENDED;
  font->ko.etag = tc_font;

  font->family  = KSI_STR_PTR(family);
  font->weight  = KSI_STR_PTR(weight);
  font->style   = KSI_STR_PTR(style);
  font->charset = KSI_STR_PTR(chset);

  return (ksi_obj) font;
}


static ksi_obj
set_def_font_weight (ksi_obj x)
{
  KSI_CHECK (x, KSI_STR_P (x), "set-default-font-weight: invalid string");
  def_font_weight = x;
  return ksi_unspec;
}

static ksi_obj
set_def_font_style (ksi_obj x)
{
  KSI_CHECK (x, KSI_STR_P (x), "set-default-font-style: invalid string");
  def_font_style = x;
  return ksi_unspec;
}

static ksi_obj
set_def_font_charset (ksi_obj x)
{
  KSI_CHECK (x, KSI_STR_P (x), "set-default-font-charset: invalid string");
  def_font_charset = x;
  return ksi_unspec;
}


/*
 * ksi_joke_init_font
 *	���������� �� ksi_joke_init ��� ������������� �������
 *	��������� � ��������.
 */

static struct Ksi_Prim_Def defs [] =
{
  { L"set-default-font-weight",	set_def_font_weight,	KSI_CALL_ARG1, 1 },
  { L"set-default-font-style",	set_def_font_style,	KSI_CALL_ARG1, 1 },
  { L"set-default-font-charset", set_def_font_charset,	KSI_CALL_ARG1, 1 },

  { L"make-font",		ksi_make_font,		KSI_CALL_ARG4, 1 },

  { 0 }
};

void
ksi_joke_init_font (ksi_env env)
{
  tc_font = &tag_font;

  def_font_weight  = ksi_str02string (L"*");
  def_font_style   = ksi_str02string (L"*-*-");
  def_font_charset = ksi_str02string (L"*");

  ksi_reg_unit (defs, env);
}


 /* End of code */
