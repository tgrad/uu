/*
 * jok_init.c
 * joke init
 *
 * Copyright (C) 1997-2000, 2017, Ivan Demakov
 *
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose, without fee, and without a written agreement
 * is hereby granted, provided that the above copyright notice and this
 * paragraph and the following two paragraphs appear in all copies.
 * Modifications to this software may be copyrighted by their authors
 * and need not follow the licensing terms described here, provided that
 * the new terms are clearly indicated on the first page of each file where
 * they apply.
 *
 * IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY
 * FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES,
 * INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS
 * DOCUMENTATION, EVEN IF THE AUTHORS HAS BEEN ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIMS ANY WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS
 * ON AN "AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAS NO OBLIGATIONS
 * TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 *
 *
 * Author:        Ivan Demakov <demakov@users.sourceforge.net>
 * Creation date: Sat Jul  5 15:46:59 1997
 * Last Update:   Tue Apr 18 17:14:42 2000
 *
 */

#include "jok_int.h"
#include "toolkit.h"

struct Ksi_Joke*	ksi_joke;


static ksi_obj
ksi_joke_version (void)
{
  return ksi_str02string (ksi_utf(KSI_VERSION));
}

static ksi_obj
ksi_joke_sys ()
{
#if defined(X11_GRAPH)
  return ksi_str02sym (L"x11");
#elif defined(WIN_GRAPH)
  return ksi_str02sym (L"windows");
#endif
}


/*
 * ksi_init_joke
 *
 *	platform independet initialization
 */

static struct Ksi_Prim_Def defs [] =
{
  { L"joke:version",	ksi_joke_version,	KSI_CALL_ARG0, 0 },
  { L"joke:system",	ksi_joke_sys,		KSI_CALL_ARG0, 0 },

  { 0 }
};

static void
ksi_reg_joke (ksi_env env)
{
  ksi_reg_unit(defs, env);

  ksi_joke_init_dc (env);
  ksi_joke_init_color (env);
  ksi_joke_init_font (env);
  ksi_joke_init_draw (env);
  ksi_joke_init_win (env);
  ksi_joke_init_toolkit (env);
}

ksi_env
ksi_joke_env (void)
{
    return ksi_joke->top_env;
}


#if defined(X11_GRAPH)

static XrmOptionDescRec opTable[] =
{
  {"-display",	".display",		XrmoptionSepArg,	NULL},
  {"-xrm",	NULL,			XrmoptionResArg,	NULL},

  {"-geometry",	".geometry",		XrmoptionSepArg,	NULL},
  {"-iconic",	".iconic",		XrmoptionNoArg,		"on"},
  {"-title",	".title",		XrmoptionSepArg,	NULL},

  {"-bg",	"*background",		XrmoptionSepArg,	NULL},
  {"-fg",	"*foreground",		XrmoptionSepArg,	NULL},
  {"-fn",	"*font",		XrmoptionSepArg,	NULL}
};

#define opTableSize	(sizeof (opTable) / sizeof (opTable [0]))


static int
ksi_error_handler (Display* dpy, XErrorEvent* err)
{
  char buf[512];
  XGetErrorText (dpy, err->error_code, buf, sizeof buf);
  ksi_warn ("%s", buf);
  return 0;
}

static int
ksi_io_error_handler (Display* dpy)
{
  ksi_exn_error ("system", 0, strerror (errno));
  return 0;
}


int
ksi_joke_x11_init (Display* dpy, int argc, char** argv)
{
  int			i;
  char			*app_name, *type_name, *dpy_name;
  char			*buff;
  XrmDatabase		rdb;
  XrmValue		value;

  if (ksi_joke)
    return 1;

  ksi_joke = (struct Ksi_Joke*) ksi_malloc_eternal (sizeof *ksi_joke);
  memset (ksi_joke, 0, sizeof *ksi_joke);

  app_name = (char*) alloca (strlen (argv[0]) + 1);
  strcpy (app_name, argv[0]);

  /* Remove directory from pathname. */
  for (i = strlen (app_name); i > 0; --i)
    if (app_name[i-1] == '/')
      {
	memmove (app_name, app_name + i, strlen (app_name + i) + 1);
	break;
      }

  /* Remove suffix form filename. */
  for (i = strlen (app_name) - 1; i > 0; --i)
    if (app_name[i] == '.')
      {
	app_name [i] = '\0';
	break;
      }

  XSetErrorHandler (ksi_error_handler);
  XSetIOErrorHandler (ksi_io_error_handler);

  rdb = 0;
  XrmParseCommand (&rdb, opTable, opTableSize, app_name, &argc, argv);

  if (dpy == 0)
    {
      buff = ksi_aprintf (".%s.display", app_name);
      if (XrmGetResource (rdb, buff, buff, &type_name, &value))
	dpy_name = value.addr;
      else
	dpy_name = 0;

      dpy = XOpenDisplay (dpy_name);
      if (!dpy)
	ksi_error ("Can't open display %s", dpy_name);
    }

  if (dpy)
    {
      ksi_joke->dpy = dpy;
      ksi_joke->rdb = rdb;

      ksi_reg_joke ();
      ksi_load_init_file(KSI_JOKE_FILE);

      return 1;
    }

  XrmDestroyDatabase (rdb);
  return 0;
}

#elif defined(WIN_GRAPH)

int
ksi_joke_win_init (const char *app, HINSTANCE hInstance, const char *logfile)
{
    ksi_env env;
    char *name;

    if (ksi_joke)
	return 1;

    ksi_init_gc();

    ksi_joke = (struct Ksi_Joke*) ksi_malloc_eternal (sizeof *ksi_joke);
    ksi_joke->tk = 0;
    ksi_joke->instance = hInstance;

    ksi_joke->ksi_ctx = ksi_malloc_eternal(sizeof *ksi_joke->ksi_ctx);
    ksi_init_context(ksi_joke->ksi_ctx, &env);
    ksi_set_current_context(ksi_joke->ksi_ctx);

    if (logfile && *logfile) {
	ksi_open_errlog(ksi_str02string(ksi_utf(logfile)));
	ksi_errlog_priority(ksi_int2num(KSI_ERRLOG_ALL));
    }

    ksi_joke->top_env = ksi_top_level_env();

    env = ksi_get_lib_env(L"ksi", L"core", L"system", 0);
    ksi_defsym(L"application", ksi_str02string(ksi_utf(app)), env);
    ksi_defsym(L"argv", ksi_nil, env);

    ksi_reg_joke (ksi_joke->top_env);

    name = (char *) alloca(strlen(app) + 1 + strlen(KSI_JOKE_INIT_FILE) + 1);
    strcat(strcat(strcpy(name, app), "/"), KSI_JOKE_INIT_FILE);
    ksi_load_boot_file(name, ksi_joke->top_env);

    name = (char *) alloca(strlen(app) + 1 + strlen(KSI_JOKE_APP_FILE) + 1);
    strcat(strcat(strcpy(name, app), "/"), KSI_JOKE_APP_FILE);
    ksi_load_boot_file(name, ksi_joke->top_env);

    return 1;
}

#endif


void
ksi_joke_term ()
{
  if (!ksi_joke)
    return;

  ksi_joke_term_toolkit ();
  /*ksi_joke_free_font_resourses ();*/

#if defined(X11_GRAPH)
  XrmDestroyDatabase (ksi_joke->rdb);
  ksi_joke->rdb = 0;
#endif

  ksi_free (ksi_joke->ksi_ctx);
  ksi_free (ksi_joke);
  ksi_joke = 0;
}

 /* End of code */
