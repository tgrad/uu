/* -*-mode:C++-*- */
/*
 * jok_int.h
 * joke internals
 *
 * Copyright (C) 1997-2000, 2017, Ivan Demakov
 *
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose, without fee, and without a written agreement
 * is hereby granted, provided that the above copyright notice and this
 * paragraph and the following two paragraphs appear in all copies.
 * Modifications to this software may be copyrighted by their authors
 * and need not follow the licensing terms described here, provided that
 * the new terms are clearly indicated on the first page of each file where
 * they apply.
 *
 * IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY
 * FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES,
 * INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS
 * DOCUMENTATION, EVEN IF THE AUTHORS HAS BEEN ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIMS ANY WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS
 * ON AN "AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAS NO OBLIGATIONS
 * TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 *
 *
 * Author:        Ivan Demakov <demakov@users.sourceforge.net>
 * Creation date: Sat Jul  5 15:49:48 1997
 *
 *
 */

#ifndef JOK_INT_H
#define JOK_INT_H


#include "joke.h"

#define KSI_JOKE_INIT_FILE "Joke"
#define KSI_JOKE_APP_FILE  "Init"


#if defined(X11_GRAPH)

#include <X11/Xresource.h>
#include <X11/Xutil.h>
#include <X11/cursorfont.h>

#elif defined(WIN_GRAPH)

#if defined(_MSC_VER)
#  define KSI_LIB_NAME "ksi-" KSI_VERSION ".lib"
#  pragma comment(lib, KSI_LIB_NAME)
#endif

#define XID			HANDLE
#define Window			HWND

#define GXxor			R2_XORPEN
#define GXor			R2_MERGEPEN
#define GXand			R2_MASKPEN
#define GXcopy			R2_COPYPEN

#define LineSolid		0
#define LineOnOffDash   	1
#define LineDoubleDash		2

#define CapNotLast		PS_ENDCAP_FLAT
#define CapButt         	PS_ENDCAP_FLAT
#define CapRound		PS_ENDCAP_ROUND
#define CapProjecting   	PS_ENDCAP_SQUARE

#define JoinMiter		PS_JOIN_MITER
#define JoinRound       	PS_JOIN_ROUND
#define JoinBevel       	PS_JOIN_BEVEL

#define FillSolid		0
#define FillTiled               1
#define FillStippled            2
#define FillOpaqueStippled      3

#define EvenOddRule		ALTERNATE
#define WindingRule             WINDING

#define ArcChord		0
#define ArcPieSlice		1

#define CoordModePrevious	0
#define CoordModeOrigin		1

#define XPoint POINT

#endif

#define Dot			0
#define Dash			1
#define DashDot			2
#define DashDotDot		3
#define UserDash		4


struct Ksi_Toolkit;
struct Ksi_Colormap;
struct Ksi_Window;
struct Ksi_WindowInfo;

typedef struct Ksi_Toolkit	*ksi_toolkit;
typedef struct Ksi_Colormap	*ksi_colormap;
typedef struct Ksi_Window	*ksi_window;
typedef struct Ksi_WindowInfo	*ksi_window_info;


struct Ksi_Joke
{
    ksi_env		top_env;
    ksi_context_t	ksi_ctx;

    ksi_valtab_t	colortab;
    ksi_colormap	cmap_list;
    ksi_font		font_list;
    ksi_valtab_t	wintab;
    ksi_toolkit		tk;

#if defined(X11_GRAPH)

    Display*		dpy;
    XrmDatabase		rdb;

#elif defined(WIN_GRAPH)

    HINSTANCE		instance;

#endif

    ksi_etag		tc_dc;
    ksi_etag		tc_font;
    ksi_etag		tc_win;

    ksi_obj		key_copy;
    ksi_obj		key_xor;
    ksi_obj		key_or;
    ksi_obj		key_and;
    ksi_obj		key_line_solid;
    ksi_obj		key_line_dash;
    ksi_obj		key_line_double_dash;
    ksi_obj		key_dot;
    ksi_obj		key_dash;
    ksi_obj		key_dash_dot;
    ksi_obj		key_dash_dot_dot;
    ksi_obj		key_cap_not_last;
    ksi_obj		key_cap_butt;
    ksi_obj		key_cap_round;
    ksi_obj		key_cap_projecting;
    ksi_obj		key_join_miter;
    ksi_obj		key_join_round;
    ksi_obj		key_join_bevel;
    ksi_obj		key_fill_solid;
    ksi_obj		key_fill_tiled;
    ksi_obj		key_fill_stippled;
    ksi_obj		key_fill_opaque_stippled;
    ksi_obj		key_even_odd_rule;
    ksi_obj		key_winding_rule;
    ksi_obj		key_arc_chord;
    ksi_obj		key_arc_pie_slice;
    ksi_obj		key_coord_mode_origin;
    ksi_obj		key_coord_mode_previous;

    ksi_obj		def_font_weight;
    ksi_obj		def_font_style;
    ksi_obj		def_font_charset;
};

extern struct Ksi_Joke*	ksi_joke;


#define joke_colortab		ksi_joke->colortab
#define joke_font_list		ksi_joke->font_list
#define joke_wintab		ksi_joke->wintab
#define joke_tk			ksi_joke->tk

#if defined(X11_GRAPH)
# define joke_dpy		ksi_joke->dpy
# define joke_rdb		ksi_joke->rdb
#elif defined(WIN_GRAPH)
# define joke_instance		ksi_joke->instance
#endif

#define tc_dc			ksi_joke->tc_dc
#define tc_font			ksi_joke->tc_font
#define tc_win			ksi_joke->tc_win

#define key_copy                ksi_joke->key_copy
#define key_xor                 ksi_joke->key_xor
#define key_or                  ksi_joke->key_or
#define key_and                 ksi_joke->key_and
#define key_line_solid          ksi_joke->key_line_solid
#define key_line_dash           ksi_joke->key_line_dash
#define key_line_double_dash    ksi_joke->key_line_double_dash
#define key_cap_not_last        ksi_joke->key_cap_not_last
#define key_cap_butt            ksi_joke->key_cap_butt
#define key_cap_round           ksi_joke->key_cap_round
#define key_cap_projecting      ksi_joke->key_cap_projecting
#define key_join_miter          ksi_joke->key_join_miter
#define key_join_round          ksi_joke->key_join_round
#define key_join_bevel          ksi_joke->key_join_bevel
#define key_fill_solid          ksi_joke->key_fill_solid
#define key_fill_tiled          ksi_joke->key_fill_tiled
#define key_fill_stippled       ksi_joke->key_fill_stippled
#define key_fill_opaque_stippled ksi_joke->key_fill_opaque_stippled
#define key_even_odd_rule       ksi_joke->key_even_odd_rule
#define key_winding_rule        ksi_joke->key_winding_rule
#define key_arc_chord           ksi_joke->key_arc_chord
#define key_arc_pie_slice       ksi_joke->key_arc_pie_slice
#define key_coord_mode_origin   ksi_joke->key_coord_mode_origin
#define key_coord_mode_previous ksi_joke->key_coord_mode_previous
#define key_dot			ksi_joke->key_dot
#define key_dash		ksi_joke->key_dash
#define key_dash_dot		ksi_joke->key_dash_dot
#define key_dash_dot_dot	ksi_joke->key_dash_dot_dot

#define def_font_weight		ksi_joke->def_font_weight
#define def_font_style		ksi_joke->def_font_style
#define def_font_charset	ksi_joke->def_font_charset


struct Ksi_DC
{
  struct Ksi_EObj ko;

  int		draw_mode;
  int		foreground;
  int		background;
  int		line_width;
  int		line_style;
  int		cap_style;
  int		join_style;
  int   	fill_mode;
  int		fill_rule;
  int		arc_mode;
  int		dash_mode;
  int		dash_len;
  char*		dashes;
  ksi_font	font;
  int		font_size;
  int		font_angle;

#if defined(X11_GRAPH)

  Display*	dpy;
  Drawable	drawable;
  GC		gc;
  ksi_colormap	cmap;

#elif defined(WIN_GRAPH)

  HWND		hwnd;
  HDC		hdc;

#endif
};

#define DC_P(x)		(KSI_EXT_IS ((x), tc_dc))

#if defined(X11_GRAPH)
# define DC_DISPLAY(x)	(((ksi_dc) (x)) -> dpy)
# define DC_DRAWABLE(x)	(((ksi_dc) (x)) -> drawable)
# define DC_GC(x)	(((ksi_dc) (x)) -> gc)
# define DC_CMAP(x)	(((ksi_dc) (x)) -> cmap)
#elif defined(WIN_GRAPH)
# define DC_HWND(x)	(((ksi_dc) (x)) -> hwnd)
# define DC_HDC(x)	(((ksi_dc) (x)) -> hdc)
# define DC_CMAP(x)	((ksi_colormap) 0)
#endif


struct Ksi_Font
{
  struct Ksi_EObj ko;

  const wchar_t *family;
  const wchar_t *weight;
  const wchar_t *style;
  const wchar_t *charset;
};

#define FONT_P(x)		(KSI_EXT_IS ((x), tc_font))
#define FONT_FAMILY(x)		(((ksi_font) (x)) -> family)
#define FONT_WEIGHT(x)		(((ksi_font) (x)) -> weight)
#define FONT_STYLE(x)		(((ksi_font) (x)) -> style)
#define FONT_CHARSET(x)		(((ksi_font) (x)) -> charset)


struct Ksi_Window
{
  struct Ksi_EObj ko;

  Window		wid;
  ksi_obj		events;
  ksi_window_info	tk_data;
};

#define WIN_P(x)		(KSI_EXT_IS ((x), tc_win))


void
ksi_joke_init_dc (ksi_env env);

void
ksi_joke_init_color (ksi_env env);

void
ksi_joke_init_draw (ksi_env env);

void
ksi_joke_init_font (ksi_env env);

void
ksi_joke_init_win (ksi_env env);

int
ksi_lookup_color(const char* name, ksi_color* r, ksi_color* g, ksi_color* b);

void
ksi_append_color (const char* name, ksi_color r, ksi_color g, ksi_color b);


/* jok_win.c */

ksi_valtab_t
ksi_new_wintab (void);

ksi_window
ksi_append_window (ksi_valtab_t tab, Window wid);

ksi_window
ksi_lookup_window (ksi_valtab_t tab, Window wid);

ksi_window
ksi_remove_window (ksi_valtab_t tab, Window wid);

ksi_obj
ksi_append_window_event (ksi_obj wnd, ksi_obj type, ksi_obj proc);

ksi_obj
ksi_remove_window_event (ksi_obj wnd, ksi_obj type, ksi_obj proc);

ksi_obj
ksi_apply_window_event (ksi_obj wnd, ksi_obj evt);



#endif

/* End of file */
