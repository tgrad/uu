/*
 * JOK_WIN.C
 *
 *
 * Copyright (C) 1999-2000, Ivan Demakov.
 *
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose, without fee, and without a written
 * agreement is hereby granted, provided that the above copyright notice
 * and this paragraph and the following two paragraphs appear in all copies.
 * Modifications to this software may be copyrighted by their authors
 * and need not follow the licensing terms described here, provided that
 * the new terms are clearly indicated on the first page of each file where
 * they apply.
 *
 * IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY
 * FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES,
 * INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS
 * DOCUMENTATION, EVEN IF THE AUTHORS HAS BEEN ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIMS ANY WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS
 * ON AN "AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAS NO OBLIGATIONS
 * TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 *
 *
 * Author:        Ivan Demakov <demakov@users.sourceforge.net>
 * Creation date: Wed Apr 14 02:25:33 1999
 * Last Update:   Tue Mar 21 19:24:36 2000
 *
 */

#include "jok_int.h"

static int
win_tag_equal (struct Ksi_EObj *x1, struct Ksi_EObj *x2, int deep)
{
  return 0;
}

static const wchar_t*
win_tag_print (struct Ksi_EObj *x, int slashify)
{
  ksi_window wnd = (ksi_window) x;
  wchar_t *buf;

#if defined(X11_GRAPH) || defined(WIN_GRAPH)
  buf = ksi_aprintf ("#<window %p>", (void*) wnd->wid);
#else
  buf = ksi_default_tag_print (tag, x, slashify);
#endif

  return buf;
}

static struct Ksi_ETag tag_win =
{
  L"window",
  win_tag_equal,
  win_tag_print
};


static size_t
hash_win (void* rec, size_t num, void* data)
{
  return (((size_t) (((ksi_window) rec) -> wid)) % num);
}

static int
cmp_win (void* r1, void* r2, void* data)
{
  size_t wid1 = (size_t) (((ksi_window) r1) -> wid);
  size_t wid2 = (size_t) (((ksi_window) r2) -> wid);
  return (int) (wid1 - wid2);
}

ksi_valtab_t
ksi_new_wintab (void)
{
  return ksi_new_valtab (10, hash_win, cmp_win, 0);
}

ksi_window
ksi_append_window (ksi_valtab_t tab, Window wid)
{
  ksi_window win;

#ifdef WIN_GRAPH
  if (!IsWindow (wid))
    ksi_exn_error (0, 0, "ksi_append_window: invalid window id");
#endif

  win = ksi_malloc(sizeof(*win));
  win->ko.o.itag= KSI_TAG_EXTENDED;
  win->ko.etag	= tc_win;
  win->wid	= wid;
  win->events	= ksi_nil;
  win->tk_data	= 0;

  ksi_lookup_vtab(tab, win, 1);
  return win;
}

ksi_window
ksi_lookup_window (ksi_valtab_t tab, Window wid)
{
  struct Ksi_Window rec;
  rec.wid = wid;

  return (ksi_window) ksi_lookup_vtab (tab, &rec, 0);
}

ksi_window
ksi_remove_window (ksi_valtab_t tab, Window wid)
{
  struct Ksi_Window rec;
  rec.wid = wid;

  return (ksi_window) ksi_remove_vtab (tab, &rec);
}

ksi_obj
ksi_window_p (ksi_obj x)
{
  return WIN_P (x) ? ksi_true : ksi_false;
}


/* ksi_append_window_event
 *
 * Добавляет обработчик PROC для события типа TYPE в окно WND.
 *
 * TYPE может быть либо символом, задающим конкретное событие,
 * либо `#t', для обработчика по-умолчанию.
 */

ksi_obj
ksi_append_window_event (ksi_obj wnd, ksi_obj type, ksi_obj proc)
{
  ksi_obj x;

  KSI_CHECK (wnd, WIN_P (wnd), "append-window-event: invalid window in arg1");
  KSI_CHECK (proc, ksi_procedure_p (proc) != ksi_false, "append-window-event: invalid procedure in arg2");

  x = ksi_assq (type, ((ksi_window) wnd) -> events);
  if (x != ksi_false)
    KSI_CDR (x) = ksi_cons (proc, KSI_CDR (x));
  else
    {
      x = KSI_LIST2 (type, proc);
      x = ksi_cons (x, ((ksi_window) wnd) -> events);
      ((ksi_window) wnd) -> events = x;
    }

  return ksi_unspec;
}


/* ksi_remove_window_event
 *
 * Удаляет из окна WND обработчик PROC события типа TYPE.
 * Если PROC не указан, то удаляются все обработчики события типа TYPE.
 */

ksi_obj
ksi_remove_window_event (ksi_obj wnd, ksi_obj type, ksi_obj proc)
{
  ksi_obj z, x;

  KSI_CHECK (wnd, WIN_P (wnd), "remove-window-event: invalid window in arg1");

  x = ksi_assq (type, ((ksi_window) wnd) -> events);
  if (x != ksi_false)
    {
      if (!proc)
	KSI_CDR (x) = ksi_nil;
      else
	{
	  for (z = KSI_CDR (x); z != ksi_nil; x = z, z = KSI_CDR (z))
	    {
	      if (KSI_CAR (z) == proc)
		{
		  KSI_CDR (x) = KSI_CDR (z);
		  break;
		}
	    }
	}
    }

  return ksi_unspec;
}


/* ksi_apply_window_event
 *
 * Вызывает все обработчики события E.
 * E должно быть вектором, в первом элементе которого, задан тип
 * события.  Если в окне не установлены обработчики для события
 * данного типа, то вызываются все обработчики по-умолчанию.
 *
 * Обработчики вызываются с двумя параметрами: окном WND и событием E.
 * Порядок вызова обработчиков, в общем случае, не определен.
 * Возвращаемые значения обработчиков игнорируются.
 */

ksi_obj
ksi_apply_window_event (ksi_obj wnd, ksi_obj e)
{
  ksi_obj x;

  KSI_CHECK (wnd, WIN_P (wnd), "apply-window-event: invalid window in arg1");
  KSI_CHECK (e, KSI_VEC_P (e), "apply-window-event: invalid vector in arg2");

  x = ksi_assq (KSI_VEC_REF (e, 0), ((ksi_window) wnd) -> events);
  if (x == ksi_false || KSI_CDR (x) == ksi_nil)
    x = ksi_assq (ksi_true, ((ksi_window) wnd) -> events);

  if (x == ksi_false)
    return ksi_false;

  for (x = KSI_CDR (x); x != ksi_nil; x = KSI_CDR (x))
    {
      ksi_apply_2 (KSI_CAR (x), wnd, e);
    }

  return ksi_true;
}


static struct Ksi_Prim_Def defs [] =
{
  { L"window?",		   ksi_window_p,	    KSI_CALL_ARG1, 1 },
  { L"append-window-event", ksi_append_window_event, KSI_CALL_ARG3, 3 },
  { L"remove-window-event", ksi_remove_window_event, KSI_CALL_ARG3, 2 },
  { L"apply-window-event",  ksi_apply_window_event,  KSI_CALL_ARG2, 2 },

  { 0 }
};

void
ksi_joke_init_win (ksi_env env)
{
  tc_win = &tag_win;

  ksi_reg_unit (defs, env);
}

 /* End of code */
