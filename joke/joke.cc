/*
 * joke.cc
 * main () for joke
 *
 * Copyright (C) 1997-2000, Ivan Demakov
 *
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose, without fee, and without a written agreement
 * is hereby granted, provided that the above copyright notice and this
 * paragraph and the following two paragraphs appear in all copies.
 * Modifications to this software may be copyrighted by their authors
 * and need not follow the licensing terms described here, provided that
 * the new terms are clearly indicated on the first page of each file where
 * they apply.
 *
 * IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY
 * FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES,
 * INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS
 * DOCUMENTATION, EVEN IF THE AUTHORS HAS BEEN ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIMS ANY WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS
 * ON AN "AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAS NO OBLIGATIONS
 * TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 *
 *
 * Author:        Ivan Demakov <demakov@users.sourceforge.net>
 * Creation date: Sun Aug 17 21:19:41 1997
 * Last Update:   Tue Mar 21 19:25:02 2000
 *
 */
static char rcsid[] = "$Id: joke.cc,v 1.9 2000/04/07 03:19:03 ivan Exp $";

/* Comments:
 *
 */

#include "joke.h"
#define WIN_GRAPH

#ifdef X11_GRAPH

#include <stdio.h>
#include <stdlib.h>


main (int argc, char* argv[])
{
  ksi_init_std_ports (stdin, stdout, stderr);
  if (ksi_joke_x11_init (0, argc, argv))
    {
      ksi_toplevel (argc, argv, ksi_joke_term);
    }

  return 0;
}

#elif defined(WIN_GRAPH)

#include <windows.h>
#include <stdio.h>
#include <process.h>


int WINAPI
WinMain (HINSTANCE hInst, HINSTANCE hPrevInstance,
	 PSTR szCmd, int iCmdShow)
{
  ksi_joke_win_init (hInst, szCmd);
  ksi_init_console ("Joke console");
  ksi_toplevel (0, 0, ksi_joke_term);

  return 0;
}


#endif


 /* End of code */
