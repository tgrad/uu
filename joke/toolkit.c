/*
 * toolkit.c
 * 
 *
 * Copyright (C) 1999-2000, Ivan Demakov.
 *
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose, without fee, and without a written
 * agreement is hereby granted, provided that the above copyright notice
 * and this paragraph and the following two paragraphs appear in all copies.
 * Modifications to this software may be copyrighted by their authors
 * and need not follow the licensing terms described here, provided that
 * the new terms are clearly indicated on the first page of each file where
 * they apply.
 *
 * IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY
 * FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES,
 * INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS
 * DOCUMENTATION, EVEN IF THE AUTHORS HAS BEEN ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIMS ANY WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS
 * ON AN "AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAS NO OBLIGATIONS
 * TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 *
 *
 * Author:        Ivan Demakov <demakov@users.sourceforge.net>
 * Creation date: Sun Aug 29 12:46:28 1999
 * Last Update:   Tue Mar 21 19:25:50 2000
 *
 */

#include "toolkit.h"


static char rcsid[] = "$Id: toolkit.c,v 1.6 2000/04/07 03:19:03 ivan Exp $";


void
ksi_tk_add_timer (double time, void (*proc) (ksi_window, void*),
		  ksi_window wnd, void *data)
{
  tk_timer tm = (tk_timer) ksi_malloc (sizeof *tm);

  tm->next = joke_tk->timers;
  tm->proc = proc;
  tm->wnd  = wnd;
  tm->data = data;
  tm->time = ksi_real_time () + time;

  joke_tk->timers = tm;
}

void
ksi_tk_del_timer (void (*proc) (ksi_window, void*), ksi_window wnd, void *data)
{
  tk_timer *prev = & joke_tk->timers, curr;
  
  for (curr = *prev; curr; /**/)
    {
      if (curr->proc == proc && curr->wnd == wnd && curr->data == data)
	{
	  tk_timer this = curr;
	  *prev = curr = curr->next;
	  this->next = 0;
	}
      else
	{
	  prev = & curr->next;
	  curr = curr->next;
	}
    }
}

void
ksi_tk_del_win_timers (ksi_window wnd)
{
  tk_timer *prev = & joke_tk->timers, curr;
  
  for (curr = *prev; curr; /**/)
    {
      if (curr->wnd == wnd)
	{
	  tk_timer this = curr;
	  *prev = curr = curr->next;
	  this->next = 0;
	}
      else
	{
	  prev = & curr->next;
	  curr = curr->next;
	}
    }
}


int
ksi_tk_call_timers (void)
{
  tk_timer *prev = & joke_tk->timers, curr;
  int executed = 0;

  for (curr = *prev; curr; /**/)
    {
      if (curr->time <= joke_tk->curr_time)
	{
	  tk_timer this = curr;
	  *prev = curr = this->next;
	  this->next = 0;
	  this->proc (this->wnd, this->data);
	  executed += 1;
	}
      else
	{
	  prev = & curr->next;
	  curr = curr->next;
	}
    }

  return executed;
}


void
ksi_tk_destroy_window (ksi_window w)
{
  if (w)
    {
      ksi_tk_del_win_timers (w);
      ksi_remove_window (joke_wintab, w->wid);

      w->ko.o.itag    = KSI_TAG_BROKEN;
      w->wid     = 0;
      w->events  = ksi_nil;
      w->tk_data = 0;
    }
}


void
ksi_joke_init_toolkit(ksi_env env)
{
  joke_tk = (ksi_toolkit) ksi_malloc_eternal (sizeof *joke_tk);
  memset (joke_tk, 0, sizeof *joke_tk);

  joke_tk->sym_button_press = ksi_str02sym (L"button-press");
  joke_tk->sym_button_release = ksi_str02sym (L"button-release");
  joke_tk->sym_key_press = ksi_str02sym (L"key-press");
  joke_tk->sym_key_release = ksi_str02sym (L"key-release");
  joke_tk->sym_motion = ksi_str02sym (L"motion");
  joke_tk->sym_expose = ksi_str02sym (L"expose");
  joke_tk->sym_configure_notify = ksi_str02sym (L"configure-notify");
  joke_tk->sym_destroy_notify = ksi_str02sym (L"destroy-notify");
  joke_tk->sym_map_notify = ksi_str02sym (L"map-notify");
  joke_tk->sym_unmap_notify = ksi_str02sym (L"unmap-notify");
  joke_tk->sym_client_message = ksi_str02sym (L"client-message");

  joke_tk->sym_scroll = ksi_str02sym (L"scroll");
  joke_tk->sym_draw = ksi_str02sym (L"draw");
  joke_tk->sym_line_left = ksi_str02sym (L"line-left");
  joke_tk->sym_line_right = ksi_str02sym (L"line-right");
  joke_tk->sym_line_up = ksi_str02sym (L"line-up");
  joke_tk->sym_line_down = ksi_str02sym (L"line-down");
  joke_tk->sym_page_left = ksi_str02sym (L"page-left");
  joke_tk->sym_page_right = ksi_str02sym (L"page-right");
  joke_tk->sym_page_up = ksi_str02sym (L"page-up");
  joke_tk->sym_page_down = ksi_str02sym (L"page-down");
  joke_tk->sym_start_move = ksi_str02sym (L"start-move");
  joke_tk->sym_stop_move = ksi_str02sym (L"stop-move");
  joke_tk->sym_move = ksi_str02sym (L"move");

  joke_tk->key_background = ksi_str02key (L"background");
  joke_tk->key_width = ksi_str02key (L"width");
  joke_tk->key_height = ksi_str02key (L"height");
  joke_tk->key_min_width = ksi_str02key (L"min-width");
  joke_tk->key_min_height = ksi_str02key (L"min-height");
  joke_tk->key_max_width = ksi_str02key (L"max-width");
  joke_tk->key_max_height = ksi_str02key (L"max-height");
  joke_tk->key_name = ksi_str02key (L"name");
  joke_tk->key_orient = ksi_str02key (L"orient");
  joke_tk->key_vertical = ksi_str02key (L"vertical");
  joke_tk->key_horizontal = ksi_str02key (L"horizontal");
  joke_tk->key_window = ksi_str02key (L"window");
  joke_tk->key_value = ksi_str02key (L"value");
  joke_tk->key_begin_value = ksi_str02key (L"begin-value");
  joke_tk->key_end_value = ksi_str02key (L"end-value");
  joke_tk->key_work = ksi_str02key (L"work");
  joke_tk->key_view = ksi_str02key (L"view");

  joke_tk->curr_time = ksi_real_time ();
  joke_tk->no_idle_work = 0;
  joke_tk->timers = 0;

  joke_wintab = ksi_new_wintab ();

  ksi_joke_init_system_toolkit(env);
}

void
ksi_joke_term_toolkit (void)
{
  ksi_joke_term_system_toolkit ();

  ksi_free (joke_tk);
  joke_tk = 0;
}


 /* End of code */
