/* -*-mode:C++-*- */
/*
 * toolkit.h
 * common include for all systems
 *
 * Copyright (C) 1999-2000, Ivan Demakov.
 *
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose, without fee, and without a written
 * agreement is hereby granted, provided that the above copyright notice
 * and this paragraph and the following two paragraphs appear in all copies.
 * Modifications to this software may be copyrighted by their authors
 * and need not follow the licensing terms described here, provided that
 * the new terms are clearly indicated on the first page of each file where
 * they apply.
 *
 * IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY
 * FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES,
 * INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS
 * DOCUMENTATION, EVEN IF THE AUTHORS HAS BEEN ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIMS ANY WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS
 * ON AN "AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAS NO OBLIGATIONS
 * TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 *
 *
 * Author:        Ivan Demakov <demakov@users.sourceforge.net>
 * Creation date: Thu May 20 21:00:55 1999
 * Last Update:   Tue Mar 21 19:26:00 2000
 *
 * 
 * $Id: toolkit.h,v 1.9 2000/04/15 14:03:15 ivan Exp $
 *
 */

#ifndef TOOLKIT_H
#define TOOLKIT_H

#include "jok_int.h"

enum
{
  TOPLEVEL_WINDOW,
  MENUBAR_WINDOW,
  STATUS_WINDOW,
  SCROLLBAR_WINDOW,
  CANVAS_WINDOW
};

enum
{
  RELIEF_FLAT,
  RELIEF_RAISED,
  RELIEF_SUNKEN,
  RELIEF_RIDGE,
  RELIEF_GROOVE
};


#if defined(X11_GRAPH)

typedef struct Pixelrec
{
  struct Pixelrec*	next;
  ksi_pixel		pixel;
  ksi_color		red, green, blue;
} pixelrec;

typedef struct Pixeltab
{
  pixelrec**		pixeltab;
  size_t		tabsize;
  size_t		count;
} pixeltab;

struct Ksi_Colormap
{
  ksi_colormap	next;
  Display*	dpy;
  Colormap	cmap;
  Screen*	screen;
  Visual*	visual;

  pixeltab*	pixtab;
  XColor*	colors;
  size_t	num_colors;
};

typedef struct Tk_Color_3d
{
  ksi_color	red, green, blue;
  ksi_pixel	back, light, dark;
  GC		back_gc, light_gc, dark_gc;
} *tk_color_3d;

typedef struct Tk_Font
{
  ksi_color	red, green, blue;
  ksi_pixel	pixel;
  XFontStruct	*fs;
  GC		gc;
} *tk_font;

#elif defined(WIN_GRAPH)

#define XEvent	MSG

typedef struct Tk_Color_3d
{
  HBRUSH	back, light, dark;
} Tk_Color_3d, *tk_color_3d;

#endif


typedef struct Ksi_Tk_Timer
{
  struct Ksi_Tk_Timer	*next;
  void			(*proc) (ksi_window, void*);
  ksi_window		wnd;
  void			*data;
  double		time;
} Tk_Timer, *tk_timer;


typedef struct Ksi_WindowInfo
{
  void		(*proc) (ksi_window w, XEvent *xe, void *proc_data);
  void		*proc_data;
  ksi_window	parent;
  int		type;
  int		x, y, w, h;
  unsigned	superwin : 1;
  unsigned	mapped : 1;
  unsigned	idledraw : 1;
} WindowInfo, *window_info;


struct Ksi_Toolkit
{
#if defined(X11_GRAPH)

  int		screen;
  Visual	*visual;
  Window	root;
  ksi_colormap	cmap;

  Atom		atom_wm_protocols;
  Atom		atom_wm_delete_window;

  tk_color_3d	top_back;
  tk_color_3d	status_back;
  tk_color_3d	scroll_back;
  tk_font	status_font;
  int		status_sp, status_height;
  int		scroll_size, scroll_arrow_size;

  ksi_obj	canvas_scroll_event;

  ksi_obj	sym_wm_protocols;
  ksi_obj	sym_wm_delete_window;

#elif defined(WIN_GRAPH)

  Tk_Color_3d	status_back;
  HFONT		status_font;
  int		x_ppi, y_ppi;
  int		status_sp, status_height, status_sb;

#endif

  ksi_obj	sym_button_press, sym_button_release, sym_motion;
  ksi_obj	sym_key_press, sym_key_release;
  ksi_obj	sym_expose;
  ksi_obj	sym_configure_notify;
  ksi_obj	sym_destroy_notify;
  ksi_obj	sym_map_notify, sym_unmap_notify;
  ksi_obj	sym_client_message;

  ksi_obj	sym_scroll, sym_draw;
  ksi_obj	sym_line_left, sym_line_right, sym_line_up, sym_line_down;
  ksi_obj	sym_page_left, sym_page_right, sym_page_up, sym_page_down;
  ksi_obj	sym_start_move, sym_stop_move, sym_move;

  ksi_obj	key_background;
  ksi_obj	key_width, key_height;
  ksi_obj	key_min_width, key_min_height, key_max_width, key_max_height;
  ksi_obj	key_name;
  ksi_obj	key_orient, key_vertical, key_horizontal;
  ksi_obj	key_window;
  ksi_obj	key_value, key_begin_value, key_end_value;
  ksi_obj	key_work, key_view;

  double	curr_time;
  int		no_idle_work;
  tk_timer	timers;
};


#define WIN_IS(x,t)	(WIN_P(x) && ((ksi_window)(x))->tk_data->type == (t))

#define TOP_P(x)	WIN_IS((x), TOPLEVEL_WINDOW)
#define MENUBAR_P(x)	WIN_IS((x), MENUBAR_WINDOW)
#define STATUSBAR_P(x)	WIN_IS((x), STATUS_WINDOW)
#define SCROLLBAR_P(x)	WIN_IS((x), SCROLLBAR_WINDOW)
#define CANVAS_P(x)	WIN_IS((x), CANVAS_WINDOW)

#define SUPERWIN_P(x)	(WIN_P(x) && ((ksi_window)(x))->tk_data->superwin)


#if defined(X11_GRAPH)
#  include "x11_toolkit.h"
#  define ksi_joke_init_system_toolkit(x) ksi_joke_init_x11_toolkit(x)
#  define ksi_joke_term_system_toolkit() ksi_joke_term_x11_toolkit()
#elif defined(WIN_GRAPH)
#  include "win_toolkit.h"
#  define ksi_joke_init_system_toolkit(x) ksi_joke_init_win_toolkit(x)
#  define ksi_joke_term_system_toolkit() ksi_joke_term_win_toolkit()
#endif


/* toolkit.c */

void
ksi_joke_init_toolkit (ksi_env env);

void
ksi_joke_term_toolkit (void);

void
ksi_tk_add_timer (double time, void (*proc) (ksi_window, void*),
		  ksi_window wnd, void *data);

void
ksi_tk_del_timer (void (*proc) (ksi_window, void*),
		  ksi_window wnd, void *data);

void
ksi_tk_del_win_timers (ksi_window wnd);

int
ksi_tk_call_timers (void);

void
ksi_tk_destroy_window (ksi_window w);


#endif

 /* End of file */
