#
# makefile for Visual C
#
# $Id: visual.mak,v 1.2 1999/08/14 21:35:03 ivan Exp $
#

!include $(SRCDIR)\makedefs.mak

CUR_LIB=$(BUILDDIR)\$(CUR_TARGET).lib
CUR_DLL=$(BUILDDIR)\$(CUR_TARGET).dll
BUILD_TARGETS=$(CUR_LIB)

!include $(TOPDIR)\visual.mif

LIBS=$(BUILDDIR)\ksi.lib
OBJS=$(joke_OBJS)
TARGET_CFLAGS=$(LIB_CFLAGS)

!include $(TOPDIR)\visual1.mif


initdir = $(TOPDIR)\$(DISTDIR)\$(KSI_MAJOR_VERSION).$(KSI_MINOR_VERSION)

all: do_curdir

clean: do_curdir_clean

dist:
	@for %i in ($(joke_HEADERS)) do copy $(SRCDIR)\%i $(TOPDIR)\$(DISTDIR)\include
	copy $(CUR_LIB) $(TOPDIR)\$(DISTDIR)\lib
!ifdef MAKE_AS_DLL
	copy $(CUR_DLL) $(TOPDIR)\$(DISTDIR)\bin
!endif
	@for %i in ($(init_DATA)) do copy $(SRCDIR)\%i $(initdir)
