#
# makefile for watcom
#
# $Id: watcom.mak,v 1.5 1999/01/13 21:03:23 ivan Exp $
#

BUILD_TARGETS=$(CUR_LIB)

!include $(TOPDIR)\watcom.mif
!include $(SRCDIR)\makedefs.mak

CUR_LIB=$(BUILDDIR)\$(CUR_TARGET).lib
CUR_DLL=$(BUILDDIR)\$(CUR_TARGET).dll
OBJS=$($(CUR_TARGET)_OBJS)
CFLAGS=$(LIB_CFLAGS)
LIBS=$(BUILDDIR)\ksi.lib

!include $(TOPDIR)\watcom1.mif

initdir = $(TOPDIR)\$(DISTDIR)\$(KSI_VERSION)

all: .SYMBOLIC
	@%make do_curdir

clean: .SYMBOLIC
	@%make do_curdir_clean

dist: .SYMBOLIC
	@for %i in ($($(CUR_TARGET)_HEADERS)) do copy $(SRCDIR)\%i $(TOPDIR)\$(DISTDIR)\include
	copy $(CUR_LIB) $(TOPDIR)\$(DISTDIR)\lib
!ifdef MAKE_AS_DLL
	copy $(CUR_DLL) $(TOPDIR)\$(DISTDIR)\bin
!endif
	@for %i in ($(init_DATA)) do copy $(SRCDIR)\%i $(initdir)
