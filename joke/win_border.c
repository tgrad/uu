/*
 * win_border.c
 * 
 *
 * Copyright (C) 1999-2000, Ivan Demakov.
 *
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose, without fee, and without a written
 * agreement is hereby granted, provided that the above copyright notice
 * and this paragraph and the following two paragraphs appear in all copies.
 * Modifications to this software may be copyrighted by their authors
 * and need not follow the licensing terms described here, provided that
 * the new terms are clearly indicated on the first page of each file where
 * they apply.
 *
 * IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY
 * FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES,
 * INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS
 * DOCUMENTATION, EVEN IF THE AUTHORS HAS BEEN ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIMS ANY WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS
 * ON AN "AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAS NO OBLIGATIONS
 * TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 *
 *
 * Author:        Ivan Demakov <demakov@users.sourceforge.net>
 * Creation date: Fri Sep 17 17:07:12 1999
 * Last Update:   Tue Mar 21 19:26:24 2000
 *
 */

#include "toolkit.h"

static char rcsid[] = "$Id: win_border.c,v 1.3 2000/04/15 14:03:15 ivan Exp $";


static void
draw_vert_3d (HDC d, tk_color_3d gc,
	      int x, int y, int w, int h, int relief, int left)
{
  int half;
  RECT rc;
  HBRUSH l_gc, r_gc;

  rc.left   = x;
  rc.top    = y;
  rc.right  = x + w;
  rc.bottom = y + h;

  switch (relief)
    {
    case RELIEF_FLAT:
      FillRect (d, &rc, gc->back);
      break;

    case RELIEF_RAISED:
      FillRect (d, &rc, left ? gc->light : gc->dark);
      break;

    case RELIEF_SUNKEN:
      FillRect (d, &rc, left ? gc->dark : gc->light);
      break;

    case RELIEF_RIDGE:
      l_gc = gc->light;
      r_gc = gc->dark;
    groove:
      half = w / 2;
      if (!left && (w & 1)) half += 1;
      rc.right = x + half;
      FillRect (d, &rc, l_gc);
      rc.left = x + half;
      rc.right = x + w;
      FillRect (d, &rc, r_gc);
      break;

    case RELIEF_GROOVE:
      r_gc = gc->light;
      l_gc = gc->dark;
      goto groove;
    }
}

static void
draw_horz_3d (HDC d, tk_color_3d gc,
	      int x, int y, int w, int h, int relief,
	      int top, int left, int right)
{
  int half, x1, x2, d1, d2, b;
  HBRUSH t_gc, b_gc;
  RECT rc;

  switch (relief)
    {
    case RELIEF_FLAT:
      t_gc = b_gc = gc->back;
      break;
    case RELIEF_RAISED:
      t_gc = b_gc = (top ? gc->light : gc->dark);
      break;
    case RELIEF_SUNKEN:
      t_gc = b_gc = (top ? gc->dark : gc->light);
      break;
    case RELIEF_RIDGE:
      t_gc = gc->light;
      b_gc = gc->dark;
      break;
    case RELIEF_GROOVE:
      t_gc = gc->dark;
      b_gc = gc->light;
      break;

    default:
      return;
    }

  x1 = x;
  if (!left) x1 += h;
  x2 = x + w; 
  if (!right) x2 -= h;
  d1 = left ? 1 : -1;
  d2 = right ? -1 : 1;
  half = y + h / 2;
  if (!top && (h & 1))
    half += 1;
  b = y + h;

  for (/**/; y < b; y++, x1 += d1, x2 += d2)
    {
      if (x1 < x2)
	{
	  rc.left   = x1;
	  rc.top    = y;
	  rc.right  = x2;
	  rc.bottom = y + 1;
	  FillRect (d, &rc, (y < half ? t_gc : b_gc));
	}
    }
}


void
ksi_tk_draw_3d_rectangle (HDC d, tk_color_3d color,
			  int x, int y, int w, int h, int bw, int relief)
{
  if (w < 2 * bw) bw = w / 2;
  if (h < 2 * bw) bw = h / 2;

  draw_vert_3d (d, color, x, y, bw, h, relief, 1);
  draw_vert_3d (d, color, x+w-bw, y, bw, h, relief, 0);
  draw_horz_3d (d, color, x, y, w, bw, relief, 1, 1, 1);
  draw_horz_3d (d, color, x, y+h-bw, w, bw, relief, 0, 0, 0);
}


void
ksi_tk_fill_3d_rectangle (HDC d, tk_color_3d color,
			  int x, int y, int w, int h, int bw, int relief)
{
  int dbl;
  RECT rc;

  if (relief == RELIEF_FLAT)
    bw = 0;

  dbl = 2 * bw;
  if (w > dbl && h > dbl)
    {
      rc.left   = x + bw;
      rc.right  = x + w - dbl;
      rc.top    = y + bw;
      rc.bottom = y + h - dbl;

      FillRect (d, &rc, color->back);
    }

  if (bw)
    ksi_tk_draw_3d_rectangle (d, color, x, y, w, h, bw, relief);
}


void
ksi_tk_init_border (ksi_env env)
{
}

 /* End of code */
