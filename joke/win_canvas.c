/*
 * win_canvas.c
 * 
 *
 * Copyright (C) 1999-2000, Ivan Demakov.
 *
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose, without fee, and without a written
 * agreement is hereby granted, provided that the above copyright notice
 * and this paragraph and the following two paragraphs appear in all copies.
 * Modifications to this software may be copyrighted by their authors
 * and need not follow the licensing terms described here, provided that
 * the new terms are clearly indicated on the first page of each file where
 * they apply.
 *
 * IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY
 * FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES,
 * INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS
 * DOCUMENTATION, EVEN IF THE AUTHORS HAS BEEN ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIMS ANY WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS
 * ON AN "AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAS NO OBLIGATIONS
 * TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 *
 *
 * Author:        Ivan Demakov <demakov@users.sourceforge.net>
 * Creation date: Tue Oct 19 08:30:54 1999
 * Last Update:   Tue Mar 21 19:27:10 2000
 *
 */

#include "toolkit.h"


static const char *canvas_class_name = "Ksi_Joke_CanvasWindow_Class";

#define DRAW_TIMEOUT 0.300


static void
canvas_geometry (ksi_window wnd)
{
  canvas_info info = (canvas_info) wnd->tk_data;
  int draw_w, draw_h, view_w, view_h, work_w, work_h;
  double k;
  RECT rc;
  SCROLLINFO si;

  GetClientRect (wnd->wid, &rc);

  info->draw_x1 = rc.left;
  info->draw_y1 = rc.top;
  info->draw_x2 = rc.right;
  info->draw_y2 = rc.bottom;

  draw_w = info->draw_x2 - info->draw_x1;
  draw_h = info->draw_y2 - info->draw_y1;
  view_w = (int) (draw_w / info->zoom_k + 0.5);
  view_h = (int) (draw_h / info->zoom_k + 0.5);
  work_w = info->work_x2 - info->work_x1;
  work_h = info->work_y2 - info->work_y1;

  if (view_w < 0)
    view_w = 0;
  if (view_h < 0)
    view_h = 0;

  if (work_w < 0)
    work_w = 0;
  if (work_h < 0)
    work_h = 0;

  if (info->view_off_x + view_w > info->work_x2)
    info->view_off_x = info->work_x2 - view_w;
  if (info->view_off_x < info->work_x1)
    info->view_off_x = info->work_x1;

  if (info->view_off_y + view_h > info->work_y2)
    info->view_off_y = info->work_y2 - view_h;
  if (info->view_off_y < info->work_y1)
    info->view_off_y = info->work_y1;

  info->line_step_x = (int) (1.0 / info->zoom_k + 0.5);
  if (info->line_step_x <= 0)
    info->line_step_x = 1;

  info->page_step_x = (int) (0.8 * view_w + 0.5);
  if (info->page_step_x <= 0)
    info->page_step_x = 1;

  info->line_step_y = (int) (1.0 / info->zoom_k + 0.5);
  if (info->line_step_y <= 0)
    info->line_step_y = 1;

  info->page_step_y = (int) (0.8 * view_h + 0.5);
  if (info->page_step_y <= 0)
    info->page_step_y = 1;

  si.cbSize = sizeof si;
  si.fMask  = SIF_PAGE | SIF_POS | SIF_RANGE;

  si.nMin   = 0;
  si.nMax   = work_w;
  si.nPage  = view_w;
  si.nPos   = info->view_off_x - info->work_x1;

  if (work_w >= 65535)
    {
      k = 65000.0 / work_w;
      si.nMax   = (int) (si.nMax * k);
      si.nPage  = (int) (si.nPage * k);
      si.nPos   = (int) (si.nPos * k);
    }

  SetScrollInfo (wnd->wid, SB_HORZ, &si, TRUE);

  si.nMin   = 0;
  si.nMax   = work_h;
  si.nPage  = view_h;
  si.nPos   = info->view_off_y - info->work_y1;

  if (work_h >= 65535)
    {
      k = 65000.0 / work_h;
      si.nMax   = (int) (si.nMax * k);
      si.nPage  = (int) (si.nPage * k);
      si.nPos   = (int) (si.nPos * k);
    }

  SetScrollInfo (wnd->wid, SB_VERT, &si, TRUE);
}

static void
idle_draw (ksi_window wnd, void *data)
{
  canvas_info info = (canvas_info) wnd->tk_data;
  int draw_w, draw_h, pix_w, pix_h;
  int off_x, off_y, work_w, work_h;
  int work_x1, work_y1, work_x2, work_y2;
  HDC hdc;
  HBITMAP old;
  RECT rc;
  ksi_obj v, e;

  info->base.idledraw = 0;

  draw_w = info->draw_x2 - info->draw_x1;
  draw_h = info->draw_y2 - info->draw_y1;

  if (draw_w <= 0 || draw_h <= 0)
    return;

  if ((hdc = GetDC (wnd->wid)) == NULL)
    return;

  if (!info->offscreen_draw)
    {
      rc.left   = info->draw_x1;
      rc.top    = info->draw_y1;
      rc.right  = info->draw_x2;
      rc.bottom = info->draw_y2;
      FillRect (hdc, &rc, info->back);

      v = ksi_tk_get_canvas_view_area ((ksi_obj) wnd);

      e = ksi_make_vector (ksi_int2num(4), 0);
      KSI_VEC_REF (e, 0) = joke_tk->sym_draw;
      KSI_VEC_REF (e, 1) = (ksi_obj) wnd;
      KSI_VEC_REF (e, 2) = v;
      KSI_VEC_REF (e, 3) = ksi_double2num (info->zoom_k);

      info->hdc = hdc;
      ksi_apply_window_event ((ksi_obj) wnd, e);
      info->hdc = NULL;

      ReleaseDC (wnd->wid, hdc);
      return;
    }

  if ((info->hdc = CreateCompatibleDC (hdc)) == NULL)
    {
      ReleaseDC (wnd->wid, hdc);
      return;
    }

  if (info->pixmap == NULL || info->pix_w < draw_w || info->pix_h < draw_h)
    {
      if (info->pixmap != NULL)
	{
	  DeleteObject (info->pixmap);
	  info->pixmap = NULL;
	}

      pix_w = draw_w + draw_w / 2;
      pix_h = draw_h + draw_h / 2;

      info->pix_w = pix_w;
      info->pix_h = pix_h;
      info->pixmap = CreateCompatibleBitmap (hdc, pix_w, pix_h);
      if (info->pixmap == NULL)
	{
	  DeleteDC (info->hdc);
	  info->hdc = NULL;
	  ReleaseDC (wnd->wid, hdc);
	  return;
	}
    }

  work_w = (int) (info->pix_w / info->zoom_k + 0.5);
  work_h = (int) (info->pix_h / info->zoom_k + 0.5);

  off_x = ((int) (work_w - draw_w / info->zoom_k + 0.5)) / 2;
  off_y = ((int) (work_h - draw_h / info->zoom_k + 0.5)) / 2;

  work_x1 = info->view_off_x - off_x;
  work_y1 = info->view_off_y - off_y;
  if (work_x1 < info->work_x1)
    work_x1 = info->work_x1;
  if (work_y1 < info->work_y1)
    work_y1 = info->work_y1;

  work_x2 = work_x1 + work_w;
  work_y2 = work_y1 + work_h;
  if (work_x2 > info->work_x2)
    {
      work_x2 = info->work_x2;
      work_x1 = work_x2 - work_w;
    }
  if (work_y2 > info->work_y2)
    {
      work_y2 = info->work_y2;
      work_y1 = work_y2 - work_h;
    }
  if (work_x1 < info->work_x1)
    work_x1 = info->work_x1;
  if (work_y1 < info->work_y1)
    work_y1 = info->work_y1;

  info->pix_x1 = work_x1;
  info->pix_y1 = work_y1;
  info->pix_x2 = work_x2;
  info->pix_y2 = work_y2;

  old = SelectObject (info->hdc, info->pixmap);

  rc.left   = 0;
  rc.top    = 0;
  rc.right  = info->pix_w;
  rc.bottom = info->pix_h;
  FillRect (info->hdc, &rc, info->back);

  if (work_x1 < work_x2 && work_y1 < work_y2)
    {
      v = ksi_make_vector (ksi_int2num (4), 0);
      KSI_VEC_REF (v, 0) = ksi_int2num (info->pix_x1);
      KSI_VEC_REF (v, 1) = ksi_int2num (info->pix_y1);
      KSI_VEC_REF (v, 2) = ksi_int2num (info->pix_x2);
      KSI_VEC_REF (v, 3) = ksi_int2num (info->pix_y2);

      e = ksi_make_vector (ksi_int2num (4), 0);
      KSI_VEC_REF (e, 0) = joke_tk->sym_draw;
      KSI_VEC_REF (e, 1) = (ksi_obj) wnd;
      KSI_VEC_REF (e, 2) = v;
      KSI_VEC_REF (e, 3) = ksi_double2num (info->zoom_k);

      ksi_apply_window_event ((ksi_obj) wnd, e);
    }

  SelectObject (info->hdc, old);
  DeleteDC (info->hdc);
  info->hdc = NULL;
  ReleaseDC (wnd->wid, hdc);

  InvalidateRect (wnd->wid, NULL, FALSE);
}

static void
draw_canvas (ksi_window wnd, int x, int y, int w, int h, HDC hdc)
{
  canvas_info info = (canvas_info) wnd->tk_data;
  int work_x1, work_y1, work_x2, work_y2;
  RECT rc;

  if (hdc)
    {
      rc.left   = x;
      rc.top    = y;
      rc.right  = x + w;
      rc.bottom = y + h;
      FillRect (hdc, &rc, info->back);
    }

  work_x1 = x - info->draw_x1;
  work_y1 = y - info->draw_y1;
  work_x2 = work_x1 + w;
  work_y2 = work_y1 + h;

  work_x1 = (int) (work_x1 / info->zoom_k + 0.5) + info->view_off_x;
  work_y1 = (int) (work_y1 / info->zoom_k + 0.5) + info->view_off_y;
  work_x2 = (int) (work_x2 / info->zoom_k + 0.5) + info->view_off_x;
  work_y2 = (int) (work_y2 / info->zoom_k + 0.5) + info->view_off_y;

  if (work_x1 < info->work_x1)
    work_x1 = info->work_x1;
  if (work_y1 < info->work_y1)
    work_y1 = info->work_y1;

  if (work_x2 > info->work_x2)
    work_x2 = info->work_x2;
  if (work_y2 > info->work_y2)
    work_y2 = info->work_y2;

  if (work_x1 < work_x2 && work_y1 < work_y2)
    {
      if (info->base.idledraw)
	ksi_tk_del_timer (idle_draw, wnd, 0);

      info->base.idledraw = 1;
      ksi_tk_add_timer (DRAW_TIMEOUT, idle_draw, wnd, 0);
    }
}

static void
paint_canvas (ksi_window wnd, PAINTSTRUCT *ps)
{
  canvas_info info = (canvas_info) wnd->tk_data;
  int x1, y1, x2, y2;
  int pix_x, pix_y, pix_w, pix_h, off_x, off_y;
  ksi_obj e;

  x1 = ps->rcPaint.left;
  y1 = ps->rcPaint.top;
  x2 = ps->rcPaint.right;
  y2 = ps->rcPaint.bottom;

  e = ksi_make_win_paint (wnd->wid, x1, y1, x2 - x1, y2 - x1);
  ksi_apply_window_event ((ksi_obj) wnd, e);

  if (!info->pixmap)
    {
      draw_canvas (wnd, x1, y1, x2 - x1, y2 - y1, ps->hdc);
      return;
    }

  off_x = (int) ((info->view_off_x - info->pix_x1) * info->zoom_k + 0.5);
  off_y = (int) ((info->view_off_y - info->pix_y1) * info->zoom_k + 0.5);

  pix_x = x1 - info->draw_x1 + off_x;
  pix_y = y1 - info->draw_y1 + off_y;
  pix_w = x2 - x1;
  pix_h = y2 - y1;

  if (pix_x < 0)
    {
      draw_canvas (wnd, x1, y1, -pix_x, pix_h, ps->hdc);
      x1 -= pix_x;
      pix_w += pix_x;
      pix_x = 0;
    }

  if (pix_y < 0)
    {
      draw_canvas (wnd, x1, y1, pix_w, -pix_y, ps->hdc);
      y1 -= pix_y;
      pix_h += pix_y;
      pix_y = 0;
    }

  if (pix_x + pix_w > info->pix_w)
    {
      x2 = x1 + info->pix_w - pix_x;
      draw_canvas (wnd, x2, y1, pix_w - (x2 - x1), pix_h, ps->hdc);
      pix_w = x2 - x1;
    }

  if (pix_y + pix_h > info->pix_h)
    {
      y2 = y1 + info->pix_h - pix_y;
      draw_canvas (wnd, x1, y2, pix_w, pix_h - (y2 - y1), ps->hdc);
      pix_h = y2 - y1;
    }

  if (pix_w > 0 && pix_h > 0)
    {
      HDC		mem;
      HBITMAP	old;

      if ((mem = CreateCompatibleDC (ps->hdc)) == NULL)
	return;

      old = SelectObject (mem, info->pixmap);
      BitBlt (ps->hdc, x1, y1, pix_w, pix_h, mem, pix_x, pix_y, SRCCOPY);

      SelectObject (mem, old);
      DeleteDC (mem);
    }
}


static void
scroll_canvas (ksi_window wnd, int dx, int dy)
{
  canvas_info info = (canvas_info) wnd->tk_data;
  int x, y;
  RECT rc;

  x = info->view_off_x;
  y = info->view_off_y;

  info->view_off_x += dx;
  info->view_off_y += dy;

  canvas_geometry (wnd);

  dx = info->view_off_x - x;
  dy = info->view_off_y - y;

  if (dx != 0 || dy != 0)
    {
      if (info->offscreen_draw)
	InvalidateRect (wnd->wid, NULL, FALSE);
      else
	{
	  dx = (int) (dx * info->zoom_k + 0.5);
	  dy = (int) (dy * info->zoom_k + 0.5);

	  if (dx != 0 || dy != 0)
	    {
	      GetClientRect (wnd->wid, &rc);
	      ScrollWindowEx (wnd->wid, -dx, -dy, NULL, &rc,
			      NULL, NULL, SW_INVALIDATE);
	    }
	}
    }
}

static int
scroll_pos (canvas_info info, int pos, int vert)
{
  int size;

  if (vert)
    size = info->work_y2 - info->work_y1;
  else
    size = info->work_x2 - info->work_x1;

  if (size >= 65535)
    pos = (int) ((double) pos * size / 65000.0);

  if (vert)
    pos = pos + info->work_y1 - info->view_off_y;
  else
    pos = pos + info->work_x1 - info->view_off_x;

  return pos;
}

static LRESULT CALLBACK
CanvasWndProc (HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
  ksi_window	wnd;
  ksi_obj	evt;
  LPWINDOWPOS	lpwp;
  PAINTSTRUCT	ps;
  canvas_info	info;
  int		size_changed;

  wnd = (ksi_window) GetWindowLong (hwnd, GWL_USERDATA);
  info = (wnd ? (canvas_info) wnd->tk_data : 0);

  switch (iMsg)
    {
    case WM_DESTROY:
      evt = ksi_make_win_destroy (hwnd);
      ksi_apply_window_event ((ksi_obj) wnd, evt);

      if (info->pixmap)
	{
	  DeleteObject (info->pixmap);
	  info->pixmap = NULL;
	}

      ksi_tk_destroy_window (wnd);
      break;

    case WM_LBUTTONDOWN:
      evt = ksi_make_win_left_down (hwnd,
				    LOWORD (lParam), HIWORD (lParam),
				    wParam);
      ksi_apply_window_event ((ksi_obj) wnd, evt);
      return 0;

    case WM_RBUTTONDOWN:
      evt = ksi_make_win_right_down (hwnd,
				     LOWORD (lParam), HIWORD (lParam),
				     wParam);
      ksi_apply_window_event ((ksi_obj) wnd, evt);
      return 0;

    case WM_MBUTTONDOWN:
      evt = ksi_make_win_middle_down (hwnd,
				      LOWORD (lParam), HIWORD (lParam),
				      wParam);
      ksi_apply_window_event ((ksi_obj) wnd, evt);
      return 0;

    case WM_LBUTTONUP:
      evt = ksi_make_win_left_up (hwnd,
				  LOWORD (lParam), HIWORD (lParam),
				  wParam);
      ksi_apply_window_event ((ksi_obj) wnd, evt);
      return 0;

    case WM_RBUTTONUP:
      evt = ksi_make_win_right_up (hwnd,
				   LOWORD (lParam), HIWORD (lParam),
				   wParam);
      ksi_apply_window_event ((ksi_obj) wnd, evt);
      return 0;

    case WM_MBUTTONUP:
      evt = ksi_make_win_middle_up (hwnd,
				    LOWORD (lParam), HIWORD (lParam),
				    wParam);
      ksi_apply_window_event ((ksi_obj) wnd, evt);
      return 0;

    case WM_MOUSEMOVE:
      evt = ksi_make_win_mouse_move (hwnd,
				     LOWORD (lParam), HIWORD (lParam),
				     wParam);
      ksi_apply_window_event ((ksi_obj) wnd, evt);
      return 0;

    case WM_KEYDOWN:
      evt = ksi_make_win_key_down (hwnd, wParam);
      ksi_apply_window_event ((ksi_obj) wnd, evt);
      break;

    case WM_KEYUP:
      evt = ksi_make_win_key_up (hwnd, wParam);
      ksi_apply_window_event ((ksi_obj) wnd, evt);
      break;

    case WM_CHAR:
      evt = ksi_make_win_char (hwnd, wParam);
      ksi_apply_window_event ((ksi_obj) wnd, evt);
      return 0;

    case WM_WINDOWPOSCHANGED:
      if (!info)
	break;

      lpwp = (LPWINDOWPOS) lParam;
      size_changed = (info->base.w != lpwp->cx || info->base.h != lpwp->cy);

      info->base.x = lpwp->x;
      info->base.y = lpwp->y;
      info->base.w = lpwp->cx;
      info->base.h = lpwp->cy;

      if (size_changed)
	{
	  canvas_geometry (wnd);
	  InvalidateRect (hwnd, NULL, TRUE);
	}

      evt = ksi_make_win_windowposchanged (hwnd,
					   lpwp->x, lpwp->y,
					   lpwp->cx, lpwp->cy);
      ksi_apply_window_event ((ksi_obj) wnd, evt);
      return 0;

    case WM_PAINT:
      if (GetUpdateRect (hwnd, NULL, FALSE))
	{
	  BeginPaint (hwnd, &ps);
	  if (wnd)
	    paint_canvas (wnd, &ps);
	  EndPaint (hwnd, &ps);
	  return 0;
	}
      break;

    case WM_VSCROLL:
      switch (LOWORD (wParam))
	{
	case SB_LINEDOWN:
	  scroll_canvas (wnd, 0, info->line_step_y);
	  break;
	case SB_LINEUP:
	  scroll_canvas (wnd, 0, -info->line_step_y);
	  break;
	case SB_PAGEDOWN:
	  scroll_canvas (wnd, 0, info->page_step_y);
	  break;
	case SB_PAGEUP:
	  scroll_canvas (wnd, 0, -info->page_step_y);
	  break;
	case SB_THUMBPOSITION:
	case SB_THUMBTRACK:
	  scroll_canvas (wnd, 0, scroll_pos (info, HIWORD (wParam), 1));
	  break;
	}
      return 0;

    case WM_HSCROLL:
      switch (LOWORD (wParam))
	{
	case SB_LINEDOWN:
	  scroll_canvas (wnd, info->line_step_x, 0);
	  break;
	case SB_LINEUP:
	  scroll_canvas (wnd, -info->line_step_x, 0);
	  break;
	case SB_PAGEDOWN:
	  scroll_canvas (wnd, info->page_step_x, 0);
	  break;
	case SB_PAGEUP:
	  scroll_canvas (wnd, -info->page_step_x, 0);
	  break;
	case SB_THUMBPOSITION:
	case SB_THUMBTRACK:
	  scroll_canvas (wnd, scroll_pos (info, HIWORD (wParam), 0), 0);
	  break;
	}
      return 0;
    }

  return DefWindowProc (hwnd, iMsg, wParam, lParam);
}

static void
register_canvas_class (const char *name, HBRUSH back)
{
  WNDCLASS wndclass;
  memset (&wndclass, 0, sizeof wndclass);

  wndclass.style		= 0; /*CS_HREDRAW | CS_VREDRAW;*/
  wndclass.lpfnWndProc		= CanvasWndProc;
  wndclass.hInstance		= joke_instance;
  wndclass.hCursor		= LoadCursor (NULL, IDC_ARROW);
  wndclass.hbrBackground	= back;
  wndclass.lpszClassName	= name;

  RegisterClass (&wndclass);
}

ksi_window
ksi_tk_create_canvas (ksi_window top, ksi_obj args)
{
  HWND wid;
  ksi_window wnd;
  canvas_info info;
  HBRUSH back = (HBRUSH)(COLOR_WINDOW + 1);

  static int already_registered = 0;

  KSI_CHECK ((ksi_obj) top, SUPERWIN_P (top), "tk-create-canvas: invalid parent window in arg1");

  if (!already_registered)
    {
      already_registered = 1;
      register_canvas_class (canvas_class_name, back);
    }

  if (!args)
    args = ksi_nil;

  wid = CreateWindow (canvas_class_name,	/* window class name */
		      NULL,			/* window caption */
		      WS_CHILD | WS_VISIBLE | WS_HSCROLL | WS_VSCROLL,
		      0, 0, 1, 1,
		      top->wid,			/* parent window handle */
		      NULL,			/* window menu handle */
		      joke_instance,		/* program instance handle */
		      NULL);			/* creation parameters */


  if (!wid)
    return (ksi_window) ksi_false;

  info = (canvas_info) ksi_malloc (sizeof *info);
  /* memset (info, 0, sizeof *info); */
  info->base.proc = 0;
  info->base.parent = top;
  info->base.type = CANVAS_WINDOW;
  info->base.superwin = 1;
  info->base.x = 0;
  info->base.y = 0;
  info->base.w = 1;
  info->base.h = 1;
  info->zoom_k = 1.0;
  info->back   = back;
  info->pixmap = NULL;
  info->offscreen_draw = ksi_tk_bool_resource ("canvas.offscreenDraw",
					       "Canvas.OffscreenDraw",
					       0);

  ksi_tk_rect_arg (args, joke_tk->key_work, 0, 0,
		   &info->work_x1, &info->work_y1,
		   &info->work_x2, &info->work_y2);

  wnd = ksi_append_window (joke_wintab, wid);
  wnd->tk_data = (ksi_window_info) info;

  SetWindowLong (wid, GWL_USERDATA, (long) wnd);

  canvas_geometry (wnd);
  return wnd;
}


static void
redraw_canvas (ksi_window wnd, int clear)
{
  canvas_info info = (canvas_info) wnd->tk_data;

  if (!info->pixmap || !clear)
    InvalidateRect (wnd->wid, NULL, FALSE);
  else
    {
      if (info->base.idledraw)
	ksi_tk_del_timer (idle_draw, wnd, 0);

      info->base.idledraw = 1;
      ksi_tk_add_timer (DRAW_TIMEOUT, idle_draw, wnd, 0);
    }
}


ksi_obj
ksi_tk_get_canvas_draw_area (ksi_obj wnd)
{
  ksi_obj v;
  canvas_info info;

  KSI_CHECK (wnd, CANVAS_P (wnd),
	     "tk-get-canvas-draw-area: invalid canvas window in arg1");

  info = (canvas_info) ((ksi_window) wnd) -> tk_data;

  v = ksi_make_vector (ksi_int2num (4), 0);
  KSI_VEC_REF (v, 0) = ksi_int2num (info->draw_x1);
  KSI_VEC_REF (v, 1) = ksi_int2num (info->draw_y1);
  KSI_VEC_REF (v, 2) = ksi_int2num (info->draw_x2);
  KSI_VEC_REF (v, 3) = ksi_int2num (info->draw_y2);

  return v;
}

ksi_obj
ksi_tk_get_canvas_work_area (ksi_obj wnd)
{
  ksi_obj v;
  canvas_info info;

  KSI_CHECK (wnd, CANVAS_P (wnd),
	     "tk-get-canvas-work-area: invalid canvas window in arg1");

  info = (canvas_info) ((ksi_window) wnd) -> tk_data;

  v = ksi_make_vector (ksi_int2num (4), 0);
  KSI_VEC_REF (v, 0) = ksi_int2num (info->work_x1);
  KSI_VEC_REF (v, 1) = ksi_int2num (info->work_y1);
  KSI_VEC_REF (v, 2) = ksi_int2num (info->work_x2);
  KSI_VEC_REF (v, 3) = ksi_int2num (info->work_y2);

  return v;
}

ksi_obj
ksi_tk_set_canvas_work_area (ksi_obj wnd, ksi_obj v)
{
  canvas_info info;
  int tmp;

  KSI_CHECK (wnd, CANVAS_P (wnd), "tk-set-canvas-work-area: invalid canvas window in arg1");

  KSI_CHECK (v, KSI_VEC_P (v) && KSI_VEC_LEN (v) == 4, "tk-set-canvas-work-area: invalid vector or vector length in arg2");

  info = (canvas_info) ((ksi_window) wnd) -> tk_data;

  info->work_x1 = ksi_num2int (KSI_VEC_REF (v, 0), "tk-set-canvas-work-area");
  info->work_y1 = ksi_num2int (KSI_VEC_REF (v, 1), "tk-set-canvas-work-area");
  info->work_x2 = ksi_num2int (KSI_VEC_REF (v, 2), "tk-set-canvas-work-area");
  info->work_y2 = ksi_num2int (KSI_VEC_REF (v, 3), "tk-set-canvas-work-area");

  if (info->work_x1 > info->work_x2)
    {
      tmp = info->work_x1;
      info->work_x1 = info->work_x2;
      info->work_x2 = tmp;
    }

  if (info->work_y1 > info->work_y2)
    {
      tmp = info->work_y1;
      info->work_y1 = info->work_y2;
      info->work_y2 = tmp;
    }

  canvas_geometry ((ksi_window) wnd);
  redraw_canvas ((ksi_window) wnd, TRUE);

  return ksi_unspec;
}

ksi_obj
ksi_tk_get_canvas_view_off (ksi_obj wnd)
{
  ksi_obj v;
  canvas_info info;

  KSI_CHECK (wnd, CANVAS_P (wnd),
	     "tk-get-canvas-view-offset: invalid canvas window in arg1");

  info = (canvas_info) ((ksi_window) wnd) -> tk_data;

  v = ksi_make_vector (ksi_int2num (2), 0);
  KSI_VEC_REF (v, 0) = ksi_int2num (info->view_off_x);
  KSI_VEC_REF (v, 1) = ksi_int2num (info->view_off_y);

  return v;
}

ksi_obj
ksi_tk_set_canvas_view_off (ksi_obj wnd, ksi_obj x, ksi_obj y)
{
  canvas_info info;
  int off_x, off_y;

  KSI_CHECK (wnd, CANVAS_P (wnd), "tk-set-canvas-view-offset: invalid canvas window in arg1");

  info = (canvas_info) ((ksi_window) wnd) -> tk_data;

  if (!y)
    {
      KSI_CHECK (x, KSI_VEC_P (x) && KSI_VEC_LEN (x) == 2, "tk-set-canvas-view-offset: invalid vector or vector length in arg2");
      off_x = ksi_num2int (KSI_VEC_REF (x, 0), "tk-set-canvas-view-offset");
      off_y = ksi_num2int (KSI_VEC_REF (y, 0), "tk-set-canvas-view-offset");
    }
  else
    {
      KSI_CHECK (x, KSI_REAL_P (x), "tk-set-canvas-view-offset: invalid number in arg2");
      KSI_CHECK (y, KSI_REAL_P (y), "tk-set-canvas-view-offset: invalid number in arg3");
      off_x = ksi_num2int (x, "tk-set-canvas-view-offset");
      off_y = ksi_num2int (y, "tk-set-canvas-view-offset");
    }

  info->view_off_x = off_x;
  info->view_off_y = off_y;

  canvas_geometry ((ksi_window) wnd);
  redraw_canvas ((ksi_window) wnd, FALSE);

  return ksi_unspec;
}

ksi_obj
ksi_tk_get_canvas_view_zoom (ksi_obj wnd)
{
  canvas_info info;

  KSI_CHECK (wnd, CANVAS_P (wnd), "tk-get-canvas-view-zoom: invalid canvas window in arg1");

  info = (canvas_info) ((ksi_window) wnd) -> tk_data;

  return ksi_double2num (info->zoom_k);
}

ksi_obj
ksi_tk_set_canvas_view_zoom (ksi_obj wnd, ksi_obj k)
{
  canvas_info info;
  double zoom_k;

  KSI_CHECK (wnd, CANVAS_P (wnd), "tk-set-canvas-view-zoom: invalid canvas window in arg1");

  KSI_CHECK (k, KSI_REAL_P (k), "tk-set-canvas-view-zoom: invalid number in arg2");

  zoom_k = ksi_num2double (k, "tk-set-canvas-view-zoom");
  KSI_CHECK (k, zoom_k > 0.0, "tk-set-canvas-view-zoom: invalid number in arg2");


  info = (canvas_info) ((ksi_window) wnd) -> tk_data;
  info->zoom_k = zoom_k;

  canvas_geometry ((ksi_window) wnd);
  redraw_canvas ((ksi_window) wnd, TRUE);

  return ksi_unspec;
}

ksi_obj
ksi_tk_get_canvas_view_area (ksi_obj wnd)
{
  ksi_obj v;
  canvas_info info;
  int view_w, view_h;

  KSI_CHECK (wnd, CANVAS_P (wnd), "tk-get-canvas-view-area: invalid canvas window in arg1");

  info = (canvas_info) ((ksi_window) wnd) -> tk_data;

  view_w = info->draw_x2 - info->draw_x1;
  view_h = info->draw_y2 - info->draw_y1;
  view_w = (int) (view_w / info->zoom_k + 0.5);
  view_h = (int) (view_h / info->zoom_k + 0.5);

  if (view_w < 0)
    view_w = 0;
  if (view_h < 0)
    view_h = 0;

  v = ksi_make_vector (ksi_int2num (4), 0);
  KSI_VEC_REF (v, 0) = ksi_int2num (info->view_off_x);
  KSI_VEC_REF (v, 1) = ksi_int2num (info->view_off_y);
  KSI_VEC_REF (v, 2) = ksi_int2num (info->view_off_x + view_w);
  KSI_VEC_REF (v, 3) = ksi_int2num (info->view_off_y + view_h);

  return v;
}

ksi_obj
ksi_tk_get_canvas_dc (ksi_obj wnd)
{
  canvas_info	info;
  HWND		wid;
  HDC		hdc;

  KSI_CHECK (wnd, CANVAS_P (wnd), "tk-get-canvas-dc: invalid canvas window in arg1");

  info = (canvas_info) ((ksi_window) wnd) -> tk_data;
  wid = (((ksi_window) wnd) -> wid);
  hdc = info->hdc;

  if (hdc == NULL)
    return ksi_false;

  return ksi_make_win_dc (wid, hdc);
}


ksi_obj
ksi_tk_canvas_work_x (ksi_obj wnd, ksi_obj num)
{
  canvas_info	info;
  int c;

  KSI_CHECK (wnd, CANVAS_P (wnd), "tk-canvas-work-x: invalid canvas window in arg1");
  KSI_CHECK (num, KSI_REAL_P (num), "tk-canvas-work-x: invalid number in arg2");

  info = (canvas_info) ((ksi_window) wnd) -> tk_data;
  c = ksi_num2int (num, "tk-canvas-work-x");
  c = (int) (((c - info->draw_x1) / info->zoom_k) + info->view_off_x + 0.5);
  return ksi_int2num (c);
}

ksi_obj
ksi_tk_canvas_work_y (ksi_obj wnd, ksi_obj num)
{
  canvas_info	info;
  int c;

  KSI_CHECK (wnd, CANVAS_P (wnd), "tk-canvas-work-y: invalid canvas window in arg1");
  KSI_CHECK (num, KSI_REAL_P (num), "tk-canvas-work-y: invalid number in arg2");

  info = (canvas_info) ((ksi_window) wnd) -> tk_data;
  c = ksi_num2int (num, "tk-canvas-work-y");
  c = (int) (((c - info->draw_y1) / info->zoom_k) + info->view_off_y + 0.5);
  return ksi_int2num (c);
}

ksi_obj
ksi_tk_canvas_draw_x (ksi_obj wnd, ksi_obj num)
{
  canvas_info	info;
  int c;

  KSI_CHECK (wnd, CANVAS_P (wnd), "tk-canvas-draw-x: invalid canvas window in arg1");
  KSI_CHECK (num, KSI_REAL_P (num), "tk-canvas-draw-x: invalid number in arg2");

  info = (canvas_info) ((ksi_window) wnd) -> tk_data;
  c = ksi_num2int (num, "tk-canvas-draw-x");

  if (info->hdc && info->pixmap)
    c -= info->pix_x1;
  else
    c -= info->view_off_x;

  c = (int) (c * info->zoom_k + info->draw_x1 + 0.5);
  return ksi_int2num (c);
}

ksi_obj
ksi_tk_canvas_draw_y (ksi_obj wnd, ksi_obj num)
{
  canvas_info	info;
  int c;

  KSI_CHECK (wnd, CANVAS_P (wnd), "tk-canvas-draw-y: invalid canvas window in arg1");
  KSI_CHECK (num, KSI_REAL_P (num), "tk-canvas-draw-y: invalid number in arg2");

  info = (canvas_info) ((ksi_window) wnd) -> tk_data;
  c = ksi_num2int (num, "tk-canvas-draw-y");

  if (info->hdc && info->pixmap)
    c -= info->pix_y1;
  else
    c -= info->view_off_y;

  c = (int) (c * info->zoom_k + info->draw_y1 + 0.5);
  return ksi_int2num (c);
}


static struct Ksi_Prim_Def defs [] =
{
  { L"tk-create-canvas",	(ksi_proc_t)ksi_tk_create_canvas,	   KSI_CALL_ARG2, 1},
  { L"tk-get-canvas-draw-area", ksi_tk_get_canvas_draw_area, KSI_CALL_ARG1, 1},
  { L"tk-get-canvas-work-area", ksi_tk_get_canvas_work_area, KSI_CALL_ARG1, 1},
  { L"tk-set-canvas-work-area", ksi_tk_set_canvas_work_area, KSI_CALL_ARG2, 2},
  { L"tk-get-canvas-view-offset",ksi_tk_get_canvas_view_off, KSI_CALL_ARG1, 1},
  { L"tk-set-canvas-view-offset",ksi_tk_set_canvas_view_off,KSI_CALL_ARG3, 2},
  { L"tk-get-canvas-view-zoom", ksi_tk_get_canvas_view_zoom, KSI_CALL_ARG1, 1},
  { L"tk-set-canvas-view-zoom", ksi_tk_set_canvas_view_zoom, KSI_CALL_ARG2, 2},
  { L"tk-get-canvas-view-area", ksi_tk_get_canvas_view_area, KSI_CALL_ARG1, 1},
  { L"tk-get-canvas-dc",	ksi_tk_get_canvas_dc,	   KSI_CALL_ARG1, 1},
  { L"tk-canvas-work-x",        ksi_tk_canvas_work_x,	   KSI_CALL_ARG2, 2},
  { L"tk-canvas-work-y",        ksi_tk_canvas_work_y,	   KSI_CALL_ARG2, 2},
  { L"tk-canvas-draw-x",        ksi_tk_canvas_draw_x,	   KSI_CALL_ARG2, 2},
  { L"tk-canvas-draw-y",        ksi_tk_canvas_draw_y,	   KSI_CALL_ARG2, 2},

  { 0 },
};

void
ksi_tk_init_canvas (ksi_env env)
{
  ksi_reg_unit (defs, env);
}

 /* End of code */
