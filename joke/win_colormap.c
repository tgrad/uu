/*
 * win_colormap.c
 *
 *
 * Copyright (C) 1999-2000, Ivan Demakov.
 *
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose, without fee, and without a written
 * agreement is hereby granted, provided that the above copyright notice
 * and this paragraph and the following two paragraphs appear in all copies.
 * Modifications to this software may be copyrighted by their authors
 * and need not follow the licensing terms described here, provided that
 * the new terms are clearly indicated on the first page of each file where
 * they apply.
 *
 * IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY
 * FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES,
 * INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS
 * DOCUMENTATION, EVEN IF THE AUTHORS HAS BEEN ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIMS ANY WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS
 * ON AN "AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAS NO OBLIGATIONS
 * TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 *
 *
 * Author:        Ivan Demakov <demakov@users.sourceforge.net>
 * Creation date: Fri Apr  9 23:14:01 1999
 * Last Update:   Tue Mar 21 19:27:24 2000
 *
 */

#include "toolkit.h"

#pragma warning(disable: 4102)

/*
 * The SystemColorEntries array contains the names and index values for the
 * Windows indirect system color names.
 */

struct SystemColorEntry
{
  char *name;
  int index;
};

static struct SystemColorEntry sys_colors[] =
{
  "SystemActiveBorder",		COLOR_ACTIVEBORDER,
  "SystemActiveCaption",	COLOR_ACTIVECAPTION,
  "SystemAppWorkspace",		COLOR_APPWORKSPACE,
  "SystemBackground",		COLOR_BACKGROUND,
  "SystemButtonFace",		COLOR_BTNFACE,
  "SystemButtonHighlight",	COLOR_BTNHIGHLIGHT,
  "SystemButtonShadow",		COLOR_BTNSHADOW,
  "SystemButtonText",		COLOR_BTNTEXT,
  "SystemCaptionText",		COLOR_CAPTIONTEXT,
  "SystemDisabledText",		COLOR_GRAYTEXT,
  "SystemHighlight",		COLOR_HIGHLIGHT,
  "SystemHighlightText",	COLOR_HIGHLIGHTTEXT,
  "SystemInactiveBorder",	COLOR_INACTIVEBORDER,
  "SystemInactiveCaption",	COLOR_INACTIVECAPTION,
  "SystemInactiveCaptionText",	COLOR_INACTIVECAPTIONTEXT,
  "SystemMenu",			COLOR_MENU,
  "SystemMenuText",		COLOR_MENUTEXT,
  "SystemScrollbar",		COLOR_SCROLLBAR,
  "SystemWindow",		COLOR_WINDOW,
  "SystemWindowFrame",		COLOR_WINDOWFRAME,
  "SystemWindowText",		COLOR_WINDOWTEXT,
  0,				0
};


/*
 * The CommonColorEntries array contains the names and rgb values for the
 * most common colors.
 */

struct CommonColorEntry
{
  char		*name;
  ksi_pixel	rgb;
};

static struct CommonColorEntry com_colors[] =
{
  "white",	RGB (255, 255, 255),
  "black",	RGB (  0,   0,   0),
  "gray",	RGB (190, 190, 190),
  "blue",	RGB (  0,   0, 255),
  "cyan",	RGB (  0, 255, 255),
  "green",	RGB (  0, 255,   0),
  "yellow",	RGB (255, 255,   0),
  "magenta",	RGB (255,   0, 255),
  "red",	RGB (255,   0,   0),
  "navy",	RGB (  0,   0, 128),
  "brown",	RGB (165,  42,  42),
  "wheat",	RGB (245, 222, 179),
  "violet",	RGB (139, 130, 239),
  "tan",	RGB (210, 180, 140),
  "plum",	RGB (221, 160, 221),
  "pink",	RGB (255, 192, 203),
  "orange",	RGB (255, 165,   0),
  "maroon",	RGB (176,  48,  96),
  "gold",	RGB (255, 215,   0),
  "coral",	RGB (255, 127,  80),
  "snow",	RGB (255, 250, 250),
  "seashell",	RGB (255, 245, 238),
  "bisque",	RGB (255, 228, 196),
  "PeachPuff",	RGB (255, 218, 185),
  "cornsilk",	RGB (255, 248, 220),
  "ivory",	RGB (255, 255, 240),
  "honeydew",	RGB (240, 255, 240),
  "MistyRose",	RGB (255, 228, 225),
  "azure",	RGB (240, 255, 255),
  "turquoise",	RGB (  0, 245, 255),
  "aquamarine",	RGB (127, 255, 212),
  "khaki",	RGB (255, 246, 143),
  "sienna",	RGB (255, 130,  71),
  "orchid",	RGB (255, 131, 250),
  "purple",	RGB (155,  48, 255),

  0,		0
};


#ifdef __WATCOMC__
static int
get_hex (char c)
{
  if ('0' <= c && c <= '9')
    return c - '0';
  if ('a' <= c && c <= 'f')
    return c - 'a' + 10;
  if ('A' <= c && c <= 'F')
    return c - 'A' + 10;
  return 0;
}
#endif

int
ksi_parse_color (ksi_colormap dc, const char *name,
		 ksi_color* pr, ksi_color* pg, ksi_color* pb)
{
  if (*name == '#')
    {
      int r, g, b;
      size_t i = strlen (name+1);
      char fmt[16];

      if (i % 3)
	return 0;

      /* string of the form:
       * "#RGB", "#RRGGBB", "#RRRGGGBBB", or "#RRRRGGGGBBBB"
       */

      i /= 3;
      if (i == 0 || i > 4)
	return 0;

#ifdef __WATCOMC__
      /* another watcom workaround */
      if (i == 1)
	{
	  r = get_hex (name[1]) << 4;
	  g = get_hex (name[2]) << 4;
	  b = get_hex (name[3]) << 4;
	  goto sharp_parsed;
	}
#endif

      sprintf(fmt, "%%%dx%%%dx%%%dx", (int)i, (int)i, (int)i);
      sscanf(name+1, fmt, &r, &g, &b);

      /* Scale the parse values into 8 bits. */
      if (i == 1)
        {
	  r <<= 4;
	  g <<= 4;
	  b <<= 4;
	}
      else if (i != 2)
        {
	  r >>= (4*(i-2));
	  g >>= (4*(i-2));
	  b >>= (4*(i-2));
	}

    sharp_parsed:
      if (pr) *pr = r;
      if (pg) *pg = g;
      if (pb) *pb = b;
    }
  else
    {
      ksi_color r, g, b;
      if (!ksi_lookup_color (name, &r, &g, &b))
	return 0;

      if (pr) *pr = r;
      if (pg) *pg = g;
      if (pb) *pb = b;
    }

  return 1;
}

ksi_pixel
ksi_alloc_pixel (ksi_colormap dc, ksi_color r, ksi_color g, ksi_color b)
{
  return RGB (r, g, b);
}

void
ksi_free_pixel (ksi_colormap dc, ksi_pixel pixel)
{
}


void
ksi_init_colormap (ksi_env env)
{
  int		i;

  for (i = 0; sys_colors[i].name != 0; i++)
    {
      COLORREF color = GetSysColor (sys_colors[i].index);
      ksi_append_color (sys_colors[i].name,
			GetRValue (color),
			GetGValue (color),
			GetBValue (color));
    }

  for (i = 0; com_colors[i].name != 0; i++)
    {
      ksi_append_color (com_colors[i].name,
			GetRValue (com_colors[i].rgb),
			GetGValue (com_colors[i].rgb),
			GetBValue (com_colors[i].rgb));
    }
}


 /* End of code */
