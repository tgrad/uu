/*
 * win_event.c
 * event utils
 *
 * Copyright (C) 1997-2000, Ivan Demakov
 *
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose, without fee, and without a written agreement
 * is hereby granted, provided that the above copyright notice and this
 * paragraph and the following two paragraphs appear in all copies.
 * Modifications to this software may be copyrighted by their authors
 * and need not follow the licensing terms described here, provided that
 * the new terms are clearly indicated on the first page of each file where
 * they apply.
 *
 * IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY
 * FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES,
 * INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS
 * DOCUMENTATION, EVEN IF THE AUTHORS HAS BEEN ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIMS ANY WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS
 * ON AN "AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAS NO OBLIGATIONS
 * TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 *
 *
 * Author:        Ivan Demakov <demakov@users.sourceforge.net>
 * Creation date: Tue Aug 19 20:27:40 1997
 * Last Update:   Tue Mar 21 19:27:37 2000
 *
 */

/* Comments:
 *
 */

#include "toolkit.h"

static ksi_obj
make_state (unsigned int state)
{
  ksi_obj lst = ksi_nil;
  if (state & MK_SHIFT)
    lst = ksi_cons (ksi_str02sym (L"shift"), lst);
  if (state & MK_CONTROL)
    lst = ksi_cons (ksi_str02sym (L"control"), lst);

  if (state & MK_LBUTTON)
    lst = ksi_cons (ksi_str02sym (L"button1"), lst);
  if (state & MK_MBUTTON)
    lst = ksi_cons (ksi_str02sym (L"button2"), lst);
  if (state & MK_RBUTTON)
    lst = ksi_cons (ksi_str02sym (L"button3"), lst);

  return lst;
}

static ksi_obj
get_state ()
{
  ksi_obj lst = ksi_nil;
  if (GetKeyState (VK_SHIFT) & 0x8000)
    lst = ksi_cons (ksi_str02sym (L"shift"), lst);
  if (GetKeyState (VK_CONTROL) & 0x8000)
    lst = ksi_cons (ksi_str02sym (L"control"), lst);

  if (GetKeyState (VK_LBUTTON) & 0x8000)
    lst = ksi_cons (ksi_str02sym (L"button1"), lst);
  if (GetKeyState (VK_MBUTTON) & 0x8000)
    lst = ksi_cons (ksi_str02sym (L"button2"), lst);
  if (GetKeyState (VK_RBUTTON) & 0x8000)
    lst = ksi_cons (ksi_str02sym (L"button3"), lst);

  return lst;
}

static ksi_obj
make_key (unsigned key)
{
  switch (key)
    {
    case VK_CLEAR:	return ksi_str02sym (L"clear");
    case VK_HOME:	return ksi_str02sym (L"home");
    case VK_LEFT:	return ksi_str02sym (L"left");
    case VK_UP:		return ksi_str02sym (L"up");
    case VK_RIGHT:	return ksi_str02sym (L"right");
    case VK_DOWN:	return ksi_str02sym (L"down");
    case VK_PRIOR:	return ksi_str02sym (L"prior");
    case VK_NEXT:	return ksi_str02sym (L"next");
    case VK_END:	return ksi_str02sym (L"end");
    case VK_DELETE:	return ksi_str02sym (L"delete");
    case VK_INSERT:	return ksi_str02sym (L"insert");

    case VK_F1:		return ksi_str02sym (L"f1");
    case VK_F2:		return ksi_str02sym (L"f2");
    case VK_F3:		return ksi_str02sym (L"f3");
    case VK_F4:		return ksi_str02sym (L"f4");
    case VK_F5:		return ksi_str02sym (L"f5");
    case VK_F6:		return ksi_str02sym (L"f6");
    case VK_F7:		return ksi_str02sym (L"f7");
    case VK_F8:		return ksi_str02sym (L"f8");
    case VK_F9:		return ksi_str02sym (L"f9");
    case VK_F10:	return ksi_str02sym (L"f10");
    case VK_F11:	return ksi_str02sym (L"f11");
    case VK_F12:	return ksi_str02sym (L"f12");
    case VK_F13:	return ksi_str02sym (L"f13");
    case VK_F14:	return ksi_str02sym (L"f14");
    case VK_F15:	return ksi_str02sym (L"f15");
    case VK_F16:	return ksi_str02sym (L"f16");

    }

  return ksi_uint2num (key);
}

static ksi_obj
wid2wnd (Window wid)
{
  ksi_window wnd = ksi_lookup_window (joke_wintab, wid);
  return (wnd ? (ksi_obj) wnd : ksi_ulonglong2num ((size_t) wid));
}


ksi_obj
ksi_make_win_left_down (HWND wnd, int x, int y, unsigned state)
{
  ksi_obj vec = ksi_make_vector (ksi_int2num (6), ksi_false);
  KSI_VEC_REF (vec, 0) = joke_tk->sym_button_press;
  KSI_VEC_REF (vec, 1) = wid2wnd (wnd);
  KSI_VEC_REF (vec, 2) = ksi_int2num (x);
  KSI_VEC_REF (vec, 3) = ksi_int2num (y);
  KSI_VEC_REF (vec, 4) = make_state (state);
  KSI_VEC_REF (vec, 5) = ksi_int2num (1);

  return vec;
}

ksi_obj
ksi_make_win_middle_down (HWND wnd, int x, int y, unsigned state)
{
  ksi_obj vec = ksi_make_vector (ksi_int2num (6), ksi_false);
  KSI_VEC_REF (vec, 0) = joke_tk->sym_button_press;
  KSI_VEC_REF (vec, 1) = wid2wnd (wnd);
  KSI_VEC_REF (vec, 2) = ksi_int2num (x);
  KSI_VEC_REF (vec, 3) = ksi_int2num (y);
  KSI_VEC_REF (vec, 4) = make_state (state);
  KSI_VEC_REF (vec, 5) = ksi_int2num (2);

  return vec;
}

ksi_obj
ksi_make_win_right_down (HWND wnd, int x, int y, unsigned state)
{
  ksi_obj vec = ksi_make_vector (ksi_int2num (6), ksi_false);
  KSI_VEC_REF (vec, 0) = joke_tk->sym_button_press;
  KSI_VEC_REF (vec, 1) = wid2wnd (wnd);
  KSI_VEC_REF (vec, 2) = ksi_int2num (x);
  KSI_VEC_REF (vec, 3) = ksi_int2num (y);
  KSI_VEC_REF (vec, 4) = make_state (state);
  KSI_VEC_REF (vec, 5) = ksi_int2num (3);

  return vec;
}

ksi_obj
ksi_make_win_left_up (HWND wnd, int x, int y, unsigned state)
{
  ksi_obj vec = ksi_make_vector (ksi_int2num (6), ksi_false);
  KSI_VEC_REF (vec, 0) = joke_tk->sym_button_release;
  KSI_VEC_REF (vec, 1) = wid2wnd (wnd);
  KSI_VEC_REF (vec, 2) = ksi_int2num (x);
  KSI_VEC_REF (vec, 3) = ksi_int2num (y);
  KSI_VEC_REF (vec, 4) = make_state (state);
  KSI_VEC_REF (vec, 5) = ksi_int2num (1);

  return vec;
}

ksi_obj
ksi_make_win_middle_up (HWND wnd, int x, int y, unsigned state)
{
  ksi_obj vec = ksi_make_vector (ksi_int2num (6), ksi_false);
  KSI_VEC_REF (vec, 0) = joke_tk->sym_button_release;
  KSI_VEC_REF (vec, 1) = wid2wnd (wnd);
  KSI_VEC_REF (vec, 2) = ksi_int2num (x);
  KSI_VEC_REF (vec, 3) = ksi_int2num (y);
  KSI_VEC_REF (vec, 4) = make_state (state);
  KSI_VEC_REF (vec, 5) = ksi_int2num (2);

  return vec;
}

ksi_obj
ksi_make_win_right_up (HWND wnd, int x, int y, unsigned state)
{
  ksi_obj vec = ksi_make_vector (ksi_int2num (6), ksi_false);
  KSI_VEC_REF (vec, 0) = joke_tk->sym_button_release;
  KSI_VEC_REF (vec, 1) = wid2wnd (wnd);
  KSI_VEC_REF (vec, 2) = ksi_int2num (x);
  KSI_VEC_REF (vec, 3) = ksi_int2num (y);
  KSI_VEC_REF (vec, 4) = make_state (state);
  KSI_VEC_REF (vec, 5) = ksi_int2num (3);

  return vec;
}

ksi_obj
ksi_make_win_mouse_move (HWND wnd, int x, int y, unsigned state)
{
  ksi_obj vec = ksi_make_vector (ksi_int2num (6), ksi_false);
  KSI_VEC_REF (vec, 0) = joke_tk->sym_motion;
  KSI_VEC_REF (vec, 1) = wid2wnd (wnd);
  KSI_VEC_REF (vec, 2) = ksi_int2num (x);
  KSI_VEC_REF (vec, 3) = ksi_int2num (y);
  KSI_VEC_REF (vec, 4) = make_state (state);

  if (state & MK_LBUTTON)
    KSI_VEC_REF (vec, 5) = ksi_int2num (1);
  else if (state & MK_MBUTTON)
    KSI_VEC_REF (vec, 5) = ksi_int2num (2);
  else if (state & MK_RBUTTON)
    KSI_VEC_REF (vec, 5) = ksi_int2num (3);
  else
    KSI_VEC_REF (vec, 5) = ksi_int2num(0);

  return vec;
}

ksi_obj
ksi_make_win_key_down (HWND wnd, unsigned keycode)
{
  ksi_obj vec = ksi_make_vector (ksi_int2num (6), ksi_false);
  KSI_VEC_REF (vec, 0) = joke_tk->sym_key_press;
  KSI_VEC_REF (vec, 1) = wid2wnd (wnd);
  KSI_VEC_REF (vec, 2) = ksi_int2num(0);
  KSI_VEC_REF (vec, 3) = ksi_int2num(0);
  KSI_VEC_REF (vec, 4) = get_state ();
  KSI_VEC_REF (vec, 5) = make_key (keycode);

  return vec;
}

ksi_obj
ksi_make_win_key_up (HWND wnd, unsigned keycode)
{
  ksi_obj vec = ksi_make_vector (ksi_int2num (6), ksi_false);
  KSI_VEC_REF (vec, 0) = joke_tk->sym_key_release;
  KSI_VEC_REF (vec, 1) = wid2wnd (wnd);
  KSI_VEC_REF (vec, 2) = ksi_int2num(0);
  KSI_VEC_REF (vec, 3) = ksi_int2num(0);
  KSI_VEC_REF (vec, 4) = get_state ();
  KSI_VEC_REF (vec, 5) = make_key (keycode);

  return vec;
}

ksi_obj
ksi_make_win_char (HWND wnd, unsigned keycode)
{
  ksi_obj vec = ksi_make_vector (ksi_int2num (6), ksi_false);
  KSI_VEC_REF (vec, 0) = joke_tk->sym_key_press;
  KSI_VEC_REF (vec, 1) = wid2wnd (wnd);
  KSI_VEC_REF (vec, 2) = ksi_int2num(0);
  KSI_VEC_REF (vec, 3) = ksi_int2num(0);
  KSI_VEC_REF (vec, 4) = get_state ();
  KSI_VEC_REF (vec, 5) = ksi_int2char (keycode);

  return vec;
}

ksi_obj
ksi_make_win_paint (HWND wnd, int x, int y, int width, int height)
{
  ksi_obj vec = ksi_make_vector (ksi_int2num (7), ksi_false);
  KSI_VEC_REF (vec, 0) = joke_tk->sym_expose;
  KSI_VEC_REF (vec, 1) = wid2wnd (wnd);
  KSI_VEC_REF (vec, 2) = ksi_int2num (x);
  KSI_VEC_REF (vec, 3) = ksi_int2num (y);
  KSI_VEC_REF (vec, 4) = ksi_int2num (width);
  KSI_VEC_REF (vec, 5) = ksi_int2num (height);
  KSI_VEC_REF (vec, 6) = ksi_int2num(0);

  return vec;
}

ksi_obj
ksi_make_win_windowposchanged (HWND wnd, int x, int y, int w, int h)
{
  ksi_obj vec = ksi_make_vector (ksi_int2num (7), 0);
  KSI_VEC_REF (vec, 0) = joke_tk->sym_configure_notify;
  KSI_VEC_REF (vec, 1) = wid2wnd (wnd);
  KSI_VEC_REF (vec, 2) = KSI_VEC_REF (vec, 1);
  KSI_VEC_REF (vec, 3) = ksi_int2num (x);
  KSI_VEC_REF (vec, 4) = ksi_int2num (y);
  KSI_VEC_REF (vec, 5) = ksi_int2num (w);
  KSI_VEC_REF (vec, 6) = ksi_int2num (h);

  return vec;
}

ksi_obj
ksi_make_win_destroy (HWND wnd)
{
  ksi_obj vec = ksi_make_vector (ksi_int2num (3), ksi_false);
  KSI_VEC_REF (vec, 0) = joke_tk->sym_destroy_notify;
  KSI_VEC_REF (vec, 1) = wid2wnd (wnd);
  KSI_VEC_REF (vec, 2) = KSI_VEC_REF (vec, 1);

  return vec;
}


ksi_obj
ksi_tk_event_loop (ksi_obj end_proc)
{
//  ksi_window	w;
  MSG		msg;

  joke_tk->no_idle_work = 0;
  while (1)
    {
      joke_tk->curr_time = ksi_real_time ();

      if (end_proc)
	{
	  joke_tk->no_idle_work = 0;
	  if (ksi_apply_0 (end_proc) == ksi_false)
	    break;
	}

      if (joke_wintab->count == 0)
	break;

      if (joke_tk->no_idle_work == 0
	  && !PeekMessage (&msg, NULL, 0, 0, PM_NOREMOVE)
	  && ksi_tk_call_timers () == 0
	  && ksi_gcollect (0) == 0)
	{
	  if (joke_tk->timers == 0)
	    joke_tk->no_idle_work = 1;
	}
      else
	{
	  joke_tk->no_idle_work = 0;

	  if (!GetMessage (&msg, NULL, 0, 0))
	    break;

#if 0
	  w = ksi_lookup_window (joke_wintab, msg.hwnd);
	  if (w && w->tk_data && w->tk_data->proc)
	    w->tk_data->proc (w, &msg, w->tk_data->proc_data);
#endif

	  if (msg.hwnd)
	    {
	      TranslateMessage (&msg);
	      DispatchMessage (&msg);
	    }
	}
    }

  joke_tk->no_idle_work = 0;
  return ksi_false;
}


static struct Ksi_Prim_Def defs [] =
{
  { L"tk-event-loop",		ksi_tk_event_loop,	KSI_CALL_ARG1, 0 },

  { 0 },
};


void
ksi_tk_init_events (ksi_env env)
{
  ksi_reg_unit (defs, env);
}

 /* End of code */
