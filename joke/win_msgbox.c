/*
 * win_msgbox.c
 * 
 *
 * Copyright (C) 2000, Ivan Demakov.
 *
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose, without fee, and without a written
 * agreement is hereby granted, provided that the above copyright notice
 * and this paragraph and the following two paragraphs appear in all copies.
 * Modifications to this software may be copyrighted by their authors
 * and need not follow the licensing terms described here, provided that
 * the new terms are clearly indicated on the first page of each file where
 * they apply.
 *
 * IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY
 * FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES,
 * INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS
 * DOCUMENTATION, EVEN IF THE AUTHORS HAS BEEN ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIMS ANY WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS
 * ON AN "AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAS NO OBLIGATIONS
 * TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 *
 *
 * Author:        Ivan Demakov <demakov@users.sourceforge.net>
 * Creation date: Tue Mar 21 13:42:42 2000
 * Last Update:   Tue Mar 21 14:56:49 2000
 *
 */

#include "toolkit.h"

static UINT
sym2mbtype (ksi_obj x)
{
  if (x == ksi_str02sym (L"ok"))                  return MB_OK;
  if (x == ksi_str02sym (L"ok-cancel"))		  return MB_OKCANCEL;
  if (x == ksi_str02sym (L"abort-retry-ignore"))  return MB_ABORTRETRYIGNORE;
  if (x == ksi_str02sym (L"retry-cancel"))	  return MB_RETRYCANCEL;
  if (x == ksi_str02sym (L"yes-no"))		  return MB_YESNO;
  if (x == ksi_str02sym (L"yes-no-cancel"))	  return MB_YESNOCANCEL;
  if (x == ksi_str02sym (L"warning"))		  return MB_ICONWARNING;
  if (x == ksi_str02sym (L"information"))	  return MB_ICONINFORMATION;
  if (x == ksi_str02sym (L"question"))		  return MB_ICONQUESTION;
  if (x == ksi_str02sym (L"error"))		  return MB_ICONERROR;
  if (x == ksi_str02sym (L"topmost"))		  return MB_TOPMOST;
  return 0;
}

ksi_obj
ksi_tk_message_box (ksi_obj msg, ksi_obj name, ksi_obj args)
{
  const wchar_t *text  = ksi_obj2name (msg);
  const wchar_t *title = (name ? ksi_obj2name (name) : L"Ksi Message");
  UINT        type;

  if (!args)
    type = MB_ICONASTERISK | MB_OK | MB_TASKMODAL;
  else if (KSI_SYM_P (args))
    type = sym2mbtype (args) | MB_TASKMODAL;
  else
    for (type = MB_TASKMODAL; KSI_PAIR_P (args); args = KSI_CDR (args))
      type |= sym2mbtype (KSI_CAR (args));

  type = MessageBoxW (NULL, text, title, type);
  switch (type)
    {
    case IDOK:		return ksi_str02sym (L"ok");
    case IDCANCEL:	return ksi_str02sym (L"cancel");
    case IDABORT:	return ksi_str02sym (L"abort");
    case IDRETRY:	return ksi_str02sym (L"retry");
    case IDIGNORE:	return ksi_str02sym (L"ignore");
    case IDYES:		return ksi_str02sym (L"yes");
    case IDNO:		return ksi_str02sym (L"no");
    }
  return ksi_void;
}


static struct Ksi_Prim_Def defs [] =
{
  { L"tk-message-box",		ksi_tk_message_box,	KSI_CALL_ARG3, 1},

  { 0 }
};

void
ksi_tk_init_msgbox (ksi_env env)
{
  ksi_reg_unit (defs, env);
}

 /* End of code */
