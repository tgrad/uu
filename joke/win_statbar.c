/*
 * win_statbar.c
 * 
 *
 * Copyright (C) 1999-2000, Ivan Demakov.
 *
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose, without fee, and without a written
 * agreement is hereby granted, provided that the above copyright notice
 * and this paragraph and the following two paragraphs appear in all copies.
 * Modifications to this software may be copyrighted by their authors
 * and need not follow the licensing terms described here, provided that
 * the new terms are clearly indicated on the first page of each file where
 * they apply.
 *
 * IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY
 * FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES,
 * INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS
 * DOCUMENTATION, EVEN IF THE AUTHORS HAS BEEN ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIMS ANY WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS
 * ON AN "AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAS NO OBLIGATIONS
 * TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 *
 *
 * Author:        Ivan Demakov <demakov@users.sourceforge.net>
 * Creation date: Fri Sep 17 11:47:03 1999
 * Last Update:   Tue Mar 21 19:28:04 2000
 *
 */

#include "toolkit.h"

static char rcsid[] = "$Id: win_statbar.c,v 1.4 2000/04/15 14:03:16 ivan Exp $";


#define MINW	(2 + 2 * joke_tk->status_sp)


static void
alloc_panes (status_info info, ksi_obj args)
{
  int i, num, w;
  ksi_obj x;

  num =  ksi_list_len (args);
  info->num_panes = num;
  info->panes = (status_pane) ksi_malloc (num * sizeof (StatusPane));

  for (i = 0; i < num; i++, args = KSI_CDR (args))
    {
      x = KSI_CAR (args);
      w = KSI_EINT_P (x) ? ksi_num2int(x, 0) : 0;
	
      info->panes[i].w = w;
      info->panes[i].fixed = w > 0;
      info->panes[i].str = "";
    }
}

static void
reorder_panes (status_info info)
{
  int i, x, w, used, num;

  num  = 0;
  used = joke_tk->status_sp + joke_tk->status_sb;
  for (i = 0; i < info->num_panes; i++)
    {
      if (info->panes[i].fixed)
	used += info->panes[i].w;
      else
	num += 1;
      used += joke_tk->status_sp;
    }

  w = num ? (info->base.w - used) / num : 0;

  x = joke_tk->status_sp;
  for (i = 0; i < info->num_panes; i++)
    {
      if (!info->panes[i].fixed)
	info->panes[i].w = w;

      info->panes[i].x = x;
      if (info->panes[i].w > MINW)
	x += info->panes[i].w + joke_tk->status_sp;
    }
}


static void
draw_status_text (HWND wid, HDC hdc, status_info info, int i)
{
  RECT r;
  HRGN rgn;
  HGDIOBJ font;

  if (info->panes[i].str[0] || info->panes[i].proc != 0)
    {
      r.left   = info->panes[i].x + joke_tk->status_sp;
      r.right  = r.left + info->panes[i].w - 2 * joke_tk->status_sp;
      r.top    = joke_tk->status_sp;
      r.bottom = r.top + info->base.h - 2 * joke_tk->status_sp;

      if (info->panes[i].str[0])
	{
	  font = SelectObject (hdc, joke_tk->status_font);
	  SetTextColor (hdc, GetSysColor (COLOR_BTNTEXT));
	  SetBkMode (hdc, TRANSPARENT);
	  SetTextAlign (hdc, TA_LEFT | TA_BOTTOM);
	  ExtTextOut (hdc, r.left, r.bottom,
		      ETO_CLIPPED, &r,
		      info->panes[i].str,
		      strlen (info->panes[i].str),
		      NULL);
	  SelectObject (hdc, font);
	}
      else
	{
	  ksi_obj dc, rc;

	  dc = ksi_make_win_dc (wid, hdc);

	  rc = ksi_make_vector (ksi_int2num (4), 0);
	  KSI_VEC_REF (rc, 0) = ksi_int2num (r.left);
	  KSI_VEC_REF (rc, 1) = ksi_int2num (r.top);
	  KSI_VEC_REF (rc, 2) = ksi_int2num (r.right - r.left);
	  KSI_VEC_REF (rc, 3) = ksi_int2num (r.bottom - r.top);

	  ksi_apply_2 (info->panes[i].proc, dc, rc);
	}
    }
}

static void
draw_status (ksi_window wnd, PAINTSTRUCT *ps)
{
  int i, x, y, h, w;
  status_info info = (status_info) wnd->tk_data;

  ksi_tk_draw_3d_rectangle (ps->hdc, &joke_tk->status_back,
			    0, 0, info->base.w, info->base.h,
			    1, RELIEF_RAISED);

  y = joke_tk->status_sp;
  h = info->base.h - 2 * joke_tk->status_sp;
  for (i = 0; i < info->num_panes; i++)
    {
      w = info->panes[i].w;
      if (i == info->num_panes - 1)
	w += joke_tk->status_sb;

      if (w > MINW)
	{
	  ksi_tk_draw_3d_rectangle (ps->hdc, &joke_tk->status_back,
				    info->panes[i].x, y,
				    w, h,
				    1, RELIEF_SUNKEN);
	  draw_status_text (wnd->wid, ps->hdc, info, i);
	}
    }

  /* draw the size box in the bottom right corner */
  if (joke_tk->status_sb > 0)
    {
      HPEN pen, old;
      int cx = joke_tk->status_sb;

      x = info->base.w - joke_tk->status_sp - cx + 1;
      y = info->base.h - joke_tk->status_sp - 1;

      pen = CreatePen (PS_SOLID, 0, GetSysColor (COLOR_BTNHIGHLIGHT));
      old = SelectObject (ps->hdc, pen);
      for (i = 0; i < cx; i += 4)
	{
	  MoveToEx (ps->hdc, x + i, y, NULL);
	  LineTo (ps->hdc, x + cx, y - cx + i);
	}

      pen = CreatePen (PS_SOLID, 0, GetSysColor (COLOR_BTNSHADOW));
      DeleteObject (SelectObject (ps->hdc, pen));
      for (i = 1; i < cx; i += 4)
	{
	  MoveToEx (ps->hdc, x + i, y, NULL);
	  LineTo (ps->hdc, x + cx, y - cx + i);
	}
      for (i = 2; i < cx; i += 4)
	{
	  MoveToEx (ps->hdc, x + i, y, NULL);
	  LineTo (ps->hdc, x + cx, y - cx + i);
	}

      DeleteObject (SelectObject (ps->hdc, old));
    }
}


static LRESULT CALLBACK
StatusWndProc (HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
  ksi_window	wnd;
  ksi_obj	evt;
  RECT		rc;
  POINT		pt;
  LPWINDOWPOS	lpwp;
  PAINTSTRUCT	ps;
  status_info	info;
  int		size_changed;

  wnd = (ksi_window) GetWindowLong (hwnd, GWL_USERDATA);
  info = (wnd ? (status_info) wnd->tk_data : 0);

  switch (iMsg)
    {
    case WM_DESTROY:
      evt = ksi_make_win_destroy (hwnd);
      ksi_apply_window_event ((ksi_obj) wnd, evt);
      ksi_tk_destroy_window (wnd);
      break;

    case WM_WINDOWPOSCHANGED:
      if (!info)
	break;

      lpwp = (LPWINDOWPOS) lParam;
      size_changed = (info->base.w != lpwp->cx || info->base.h != lpwp->cy);

      info->base.x = lpwp->x;
      info->base.y = lpwp->y;
      info->base.w = lpwp->cx;
      info->base.h = lpwp->cy;

      if (size_changed)
	{
	  reorder_panes (info);
	  InvalidateRect (hwnd, NULL, TRUE);
	}

      evt = ksi_make_win_windowposchanged (hwnd,
					   lpwp->x, lpwp->y,
					   lpwp->cx, lpwp->cy);
      ksi_apply_window_event ((ksi_obj) wnd, evt);
      return 0;

    case WM_PAINT:
      if (GetUpdateRect (hwnd, NULL, FALSE))
	{
	  BeginPaint (hwnd, &ps);
	  if (wnd)
	    draw_status (wnd, &ps);
	  EndPaint (hwnd, &ps);
	}
      return 0;

    case WM_NCHITTEST:
      if (info && joke_tk->status_sb > 0)
	{
	  int cx = joke_tk->status_sp + joke_tk->status_sb - 2;

	  rc.left   = info->base.w - cx;
	  rc.top    = info->base.h - cx;
	  rc.right  = info->base.w;
	  rc.bottom = info->base.h;
	  MapWindowPoints (hwnd, NULL, (LPPOINT) &rc, 2);

	  pt.x = LOWORD (lParam);
	  pt.y = HIWORD (lParam);
	  if (PtInRect (&rc, pt))
	    return HTBOTTOMRIGHT;
	}
      break;

    case WM_SYSCOMMAND:
      if (info && joke_tk->status_sb > 0 && (wParam & 0xFFF0) == SC_SIZE)
	{
	  ksi_window p = info->base.parent;
	  if (p && p->wid)
	    {
	      SendMessage (p->wid, WM_SYSCOMMAND, wParam, lParam);
	      return 0;
	    }
	}
    }

  return DefWindowProc (hwnd, iMsg, wParam, lParam);
}


static void
register_status_class (const char *name)
{
  WNDCLASS wndclass;
  memset (&wndclass, 0, sizeof wndclass);

  wndclass.style		= 0;
  wndclass.lpfnWndProc		= StatusWndProc;
  wndclass.hInstance		= joke_instance;
  wndclass.hCursor		= LoadCursor (NULL, IDC_ARROW);
  wndclass.hbrBackground	= (HBRUSH)(COLOR_BTNFACE + 1);
  wndclass.lpszClassName	= name;

  RegisterClass (&wndclass);
}

ksi_window
ksi_tk_create_status (ksi_window top, ksi_obj args)
{
  HWND wid;
  ksi_window wnd;
  status_info info;

  static int already_registered = 0;

  KSI_CHECK ((ksi_obj) top, TOP_P (top),
	     "tk-create-status: invalid top window in arg1");

  if (!already_registered)
    {
      already_registered = 1;
      register_status_class (status_class_name);
    }

  if (!args)
    args = ksi_nil;

  wid = CreateWindow (status_class_name,	/* window class name */
		      NULL,			/* window caption */
		      WS_CHILD | WS_VISIBLE,	/* window style */
		      0, 0, 1, joke_tk->status_height,
		      top->wid,			/* parent window handle */
		      NULL,			/* window menu handle */
		      joke_instance,		/* program instance handle */
		      NULL);			/* creation parameters */

  if (!wid)
    return (ksi_window) ksi_false;

  info = (status_info) ksi_malloc (sizeof *info);
  /* memset (info, 0, sizeof *info); */
  info->base.proc = 0;
  info->base.proc_data = 0;
  info->base.parent = top;
  info->base.type = STATUS_WINDOW;
  info->base.x = 0;
  info->base.y = 0;
  info->base.w = 1;
  info->base.h = joke_tk->status_height;

  alloc_panes (info, args);
  reorder_panes (info);

  wnd = ksi_append_window (joke_wintab, wid);
  wnd->tk_data = (ksi_window_info) info;

  SetWindowLong (wid, GWL_USERDATA, (long) wnd);
  return wnd;
}


ksi_obj
ksi_tk_set_status_pane (ksi_obj stat, ksi_obj num, ksi_obj txt)
{
  int i, txt_is_proc = 0;
  const char *str;
  status_info info;

  KSI_CHECK (stat, STATUSBAR_P (stat),
	     "tk-set-status-pane: invalid statusbar window in arg1");
  KSI_CHECK (num, KSI_EINT_P (num),
	     "tk-set-status-pane: invalid integer in arg2");

  info = (status_info) ((ksi_window) stat) -> tk_data;
  i = KSI_SINT_COD (num);

  if (0 <= i && i < info->num_panes)
    {
      if (ksi_procedure_p (txt) != ksi_false)
	txt_is_proc = 1;
      else
	str = ksi_obj2name (txt);

      if (txt_is_proc || strcmp (str, info->panes[i].str) != 0)
	{
	  if (txt_is_proc)
	    {
	      info->panes[i].str  = "";
	      info->panes[i].proc = txt;
	    }
	  else
	    {
	      info->panes[i].str  = str;
	      info->panes[i].proc = 0;
	    }

	  if (info->panes[i].w > MINW)
	    {
	      RECT rc;

	      rc.left   = info->panes[i].x + 1;
	      rc.top    = 2 + joke_tk->status_sp;
	      rc.right  = rc.left + info->panes[i].w - 2;
	      rc.bottom = rc.top + info->base.h - 4 - 2 * joke_tk->status_sp;

	      InvalidateRect (((ksi_window) stat) -> wid, &rc, TRUE);
	    }
	}
    }

  return ksi_unspec;
}


static struct Ksi_Prim_Def defs [] =
{
  {"tk-create-status",		ksi_tk_create_status,	KSI_CALL_ARG2, 2},
  {"tk-set-status-pane",	ksi_tk_set_status_pane,	KSI_CALL_ARG3, 3},

  { 0 }
};

void
ksi_tk_init_statbar (ksi_env env)
{
  ksi_reg_unit (defs, env);
}


 /* End of code */
