/*
 * win_toolkit.c
 * 
 *
 * Copyright (C) 1999-2000, Ivan Demakov.
 *
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose, without fee, and without a written
 * agreement is hereby granted, provided that the above copyright notice
 * and this paragraph and the following two paragraphs appear in all copies.
 * Modifications to this software may be copyrighted by their authors
 * and need not follow the licensing terms described here, provided that
 * the new terms are clearly indicated on the first page of each file where
 * they apply.
 *
 * IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY
 * FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES,
 * INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS
 * DOCUMENTATION, EVEN IF THE AUTHORS HAS BEEN ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIMS ANY WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS
 * ON AN "AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAS NO OBLIGATIONS
 * TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 *
 *
 * Author:        Ivan Demakov <demakov@users.sourceforge.net>
 * Creation date: Thu May 20 22:34:56 1999
 * Last Update:   Tue Mar 21 13:45:15 2000
 *
 */

#include "toolkit.h"


static char*
load_string (HINSTANCE hinst, UINT res)
{
  char *buf, *ptr;
  int len, sz;

  for (sz = 256; sz > 0; sz *= 2)
    {
      buf = (char*) alloca (sz);
      if (!buf)
	return 0;
      len = LoadString (hinst, res, buf, sz);

      if (len <= 0)
	return 0;
      if (len < sz)
	{
	  ptr = (char*) ksi_malloc_data (len + 1);
	  memcpy (ptr, buf, len);
	  ptr [len] = '\0';
	  return ptr;
	}
    }

  return 0;
}


char*
ksi_tk_string_resource (char *resource, char *class, char *def)
{
  char *rs, *cl, *tp;
  long len;

  if (class == 0)
    class = resource;

  if (resource)
    {
      rs = (char*)alloca(strlen(resource) + 20);
      sprintf(rs, "Software\\Ksi\\Joke\\%s", resource);

      if (RegQueryValue(HKEY_CURRENT_USER, rs, 0, &len) == ERROR_SUCCESS)
      {
	  tp = (char*)ksi_malloc_data(len);
	  RegQueryValue(HKEY_CURRENT_USER, rs, tp, &len);
	  return tp;
      }

      cl = (char*) alloca (strlen (class) + 20);
      sprintf (cl, "SOFTWARE\\Ksi\\Joke\\%s", class);
      if (RegQueryValue (HKEY_LOCAL_MACHINE, cl, 0, &len) == ERROR_SUCCESS)
	{
	  tp = (char*) ksi_malloc_data (len);
	  RegQueryValue (HKEY_LOCAL_MACHINE, cl, tp, &len);
	  return tp;
	}
    }

  return def;
}

int
ksi_tk_int_resource (char *resource, char *class, int def)
{
  int val;
  char *str = ksi_tk_string_resource (resource, class, 0);
  char *end;

  if (str)
    {
      val = strtol (str, &end, 0);
      if (str != end)
	return val;
    }

  return def;
}

int
ksi_tk_bool_resource (char *resource, char *class, int def)
{
  char *str = ksi_tk_string_resource (resource, class, 0);

  if (str)
    {
      if (ksi_strcasecmp (str, "on") == 0
	  || ksi_strcasecmp (str, "true") == 0
	  || ksi_strcasecmp (str, "yes") == 0)
	return 1;

      if (ksi_strcasecmp (str, "off") == 0
	  || ksi_strcasecmp (str, "false") == 0
	  || ksi_strcasecmp (str, "no") == 0)
	return 0;
    }

  return def;
}


char*
ksi_tk_string_arg (ksi_obj args, ksi_obj key,
		   char *res, char *cls, char *def)
{
  ksi_obj	val;
  char		*ptr;

  val = ksi_get_arg (key, args, 0);
  if (val != ksi_unspec)
    {
      if (KSI_EINT_P (val))
	ptr = load_string (joke_instance, ksi_num2int(val, 0));
      else
	ptr = (char*) ksi_obj2name (val);

      if (ptr)
	return ptr;
    }

  return (res ? ksi_tk_string_resource (res, cls, def) : def);
}


int
ksi_tk_int_arg (ksi_obj args, ksi_obj key, char *res, char* cls, int def)
{
  ksi_obj val;

  val = ksi_get_arg (key, args, 0);
  if (KSI_EINT_P (val))
    return ksi_num2int(val, 0);

  return (res ? ksi_tk_int_resource (res, cls, def) : def);
}


int
ksi_tk_rect_arg (ksi_obj args, ksi_obj key, char* res, char *cls,
		 int *x1, int *y1, int *x2, int *y2)
{
  ksi_obj val;
  int tmp;

  val = ksi_get_arg (key, args, 0);
  if (KSI_VEC_P (val) && KSI_VEC_LEN (val) == 4)
    {
      *x1 = ksi_num2int (KSI_VEC_REF (val, 0), 0);
      *y1 = ksi_num2int (KSI_VEC_REF (val, 1), 0);
      *x2 = ksi_num2int (KSI_VEC_REF (val, 2), 0);
      *y2 = ksi_num2int (KSI_VEC_REF (val, 3), 0);

      if (*x1 > *x2)
	tmp = *x1, *x1 = *x2, *x2 = tmp;
      if (*y1 > *y2)
	tmp = *y1, *y1 = *y2, *y2 = tmp;

      return 1;
    }

  return 0;
}


void
ksi_joke_init_win_toolkit (ksi_env env)
{
  char		*tmp;
  LOGFONT	logfont;
  TEXTMETRIC	tm;
  HDC		hdc;
  HGDIOBJ	hgdi;
  int		h1, h2;

  hdc = GetDC (NULL);

  joke_tk->x_ppi = GetDeviceCaps (hdc, LOGPIXELSX);
  joke_tk->x_ppi = GetDeviceCaps (hdc, LOGPIXELSY);

  /* init status bar params */
  tmp = ksi_tk_string_resource ("status.font",
				"Status.Font",
				"MS Sans Serif");

  memset (&logfont, 0, sizeof (logfont));
  logfont.lfCharSet = DEFAULT_CHARSET;
  logfont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
  logfont.lfWeight = FW_NORMAL;
  logfont.lfHeight = -MulDiv (8, joke_tk->y_ppi, 72);

  strncpy (logfont.lfFaceName, tmp, sizeof (logfont.lfFaceName) - 1);

  joke_tk->status_font = CreateFontIndirect (&logfont);
  if (joke_tk->status_font == NULL)
    joke_tk->status_font = (HFONT) GetStockObject (SYSTEM_FONT);

  hgdi = SelectObject (hdc, joke_tk->status_font);
  GetTextMetrics (hdc, &tm);
  SelectObject (hdc, hgdi);

  h1 = tm.tmHeight;
  h2 = GetSystemMetrics (SM_CXFRAME);

  joke_tk->status_sp = h2;
  joke_tk->status_height = 2 * h2 + h1;
  joke_tk->status_sb = min (h1, 1 + GetSystemMetrics (SM_CXVSCROLL));
  joke_tk->status_back.back  = (HBRUSH) (COLOR_BTNFACE + 1);
  joke_tk->status_back.dark  = (HBRUSH) (COLOR_BTNSHADOW + 1);
  joke_tk->status_back.light = (HBRUSH) (COLOR_BTNHIGHLIGHT + 1);

  ReleaseDC (NULL, hdc);

  ksi_tk_init_border (env);
  ksi_tk_init_events (env);
  //ksi_tk_init_topwin (env);
  //ksi_tk_init_statbar (env);
  //ksi_tk_init_canvas (env);
  ksi_tk_init_msgbox (env);
}

void
ksi_joke_term_win_toolkit (void)
{
  if (joke_tk->status_font)
    DeleteObject (joke_tk->status_font);
}


 /* End of code */
