/*
 * win_topwin.c
 * 
 *
 * Copyright (C) 1999-2000, Ivan Demakov.
 *
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose, without fee, and without a written
 * agreement is hereby granted, provided that the above copyright notice
 * and this paragraph and the following two paragraphs appear in all copies.
 * Modifications to this software may be copyrighted by their authors
 * and need not follow the licensing terms described here, provided that
 * the new terms are clearly indicated on the first page of each file where
 * they apply.
 *
 * IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY
 * FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES,
 * INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS
 * DOCUMENTATION, EVEN IF THE AUTHORS HAS BEEN ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIMS ANY WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS
 * ON AN "AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAS NO OBLIGATIONS
 * TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 *
 *
 * Author:        Ivan Demakov <demakov@users.sourceforge.net>
 * Creation date: Fri Sep 03 14:02:11 1999
 * Last Update:   Tue Mar 21 19:29:08 2000
 *
 */

#include "toolkit.h"

static char rcsid[] = "$Id: win_topwin.c,v 1.5 2000/04/15 14:03:16 ivan Exp $";

static const char *topwin_class_name = "Ksi_Joke_TopWindow_Class";


static top_info tmp_info = 0;


static void
reorder_childs (ksi_window wnd)
{
  int x, y, w, h;
  RECT rc;
  top_info info = (top_info) wnd->tk_data;
  ksi_window stat = info->status;
  ksi_obj top = info->top;
  ksi_obj bottom = info->bottom;
  ksi_obj left = info->left;
  ksi_obj right = info->right;

  GetClientRect (wnd->wid, &rc);
  x = rc.left;
  y = rc.top;
  w = rc.right - rc.left;
  h = rc.bottom - rc.top;

  if (stat)
    {
      if (ksi_joke->win_32s)
	{
	  MoveWindow (stat->wid,
		      x, y + h - stat->tk_data->h,
		      w, stat->tk_data->h,
		      TRUE);

	  h -= stat->tk_data->h;
	}
      else
	{
	  MoveWindow (stat->wid,
		      x - joke_tk->status_sp,
		      y + h - stat->tk_data->h + joke_tk->status_sp,
		      w + 2 * joke_tk->status_sp,
		      stat->tk_data->h,
		      TRUE);

	  h -= stat->tk_data->h - joke_tk->status_sp;
	}
    }

  while (top != ksi_nil)
    {
      ksi_window curw = (ksi_window) KSI_CAR (top);

      MoveWindow (curw->wid, x, y, w, curw->tk_data->h, TRUE);
      y += curw->tk_data->h;
      h -= curw->tk_data->h;

      top = KSI_CDR (top);
    }

  while (bottom != ksi_nil)
    {
      ksi_window curw = (ksi_window) KSI_CAR (bottom);

      MoveWindow (curw->wid,
		  x, y + h - curw->tk_data->h,
		  w, curw->tk_data->h,
		  TRUE);

      h -= curw->tk_data->h;

      bottom = KSI_CDR (bottom);
    }

  if (h <= 0)
    h = 1;

  while (left != ksi_nil)
    {
      ksi_window curw = (ksi_window) KSI_CAR (left);

      MoveWindow (curw->wid, x, y, curw->tk_data->w, h, TRUE);
      x += curw->tk_data->w;
      w -= curw->tk_data->w;

      left = KSI_CDR (left);
    }

  while (right != ksi_nil)
    {
      ksi_window curw = (ksi_window) KSI_CAR (right);

      MoveWindow (curw->wid,
		  x + w - curw->tk_data->w, y,
		  curw->tk_data->w, h,
		  TRUE);

      w -= curw->tk_data->w;

      right = KSI_CDR (right);
    }

  if (w <= 0)
    w = 1;

  if (info->main)
    {
      MoveWindow (info->main->wid, x, y, w, h, TRUE);
    }
}


static LRESULT CALLBACK
TopWndProc (HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
  ksi_window	wnd;
  ksi_obj	evt;
  LPMINMAXINFO	lpmmi;
  LPWINDOWPOS	lpwp;
  top_info	info;
  int		size_changed;

  wnd = (ksi_window) GetWindowLong (hwnd, GWL_USERDATA);
  info = (wnd ? (top_info) wnd->tk_data : tmp_info);

  switch (iMsg)
    {
    case WM_DESTROY:
      evt = ksi_make_win_destroy (hwnd);
      ksi_apply_window_event ((ksi_obj) wnd, evt);
      ksi_tk_destroy_window (wnd);
      break;

    case WM_WINDOWPOSCHANGED:
      lpwp = (LPWINDOWPOS) lParam;
      size_changed = (info->base.w != lpwp->cx || info->base.h != lpwp->cy);

      info->base.x = lpwp->x;
      info->base.y = lpwp->y;
      info->base.w = lpwp->cx;
      info->base.h = lpwp->cy;

      if (wnd)
	{
	  if (size_changed)
	    reorder_childs (wnd);

	  evt = ksi_make_win_windowposchanged (hwnd,
					       lpwp->x, lpwp->y,
					       lpwp->cx, lpwp->cy);
	  ksi_apply_window_event ((ksi_obj) wnd, evt);
	}
      return 0;

    case WM_GETMINMAXINFO:
      lpmmi = (LPMINMAXINFO) lParam;
      if (info->maxw > 0 && info->maxh > 0)
	{
	  lpmmi->ptMaxSize.x = info->maxw;
	  lpmmi->ptMaxSize.y = info->maxh;
	  lpmmi->ptMaxTrackSize.x = info->maxw;
	  lpmmi->ptMaxTrackSize.y = info->maxh;
	}
      if (info->minw > 0 && info->minh > 0)
	{
	  lpmmi->ptMinTrackSize.x = info->minw;
	  lpmmi->ptMinTrackSize.y = info->minh;
	}
      return 0;

    case WM_SETFOCUS:
      if (info && info->main)
	{
	  SetFocus (info->main->wid);
	  return 0;
	}
      break;
    }

  return DefWindowProc (hwnd, iMsg, wParam, lParam);
}

static void
register_top_class (const char *name)
{
  WNDCLASSEX wndclass;
  memset (&wndclass, 0, sizeof wndclass);

  wndclass.cbSize		= sizeof (wndclass);
  wndclass.style		= CS_HREDRAW | CS_VREDRAW;
  wndclass.lpfnWndProc		= TopWndProc;
  wndclass.cbClsExtra		= 0;
  wndclass.cbWndExtra		= 0;
  wndclass.hInstance		= joke_instance;
  wndclass.hIcon		= LoadIcon (NULL, IDI_APPLICATION);
  wndclass.hCursor		= LoadCursor (NULL, IDC_ARROW);
  wndclass.hbrBackground	= (HBRUSH)(COLOR_APPWORKSPACE + 1);
  wndclass.lpszMenuName		= NULL;
  wndclass.lpszClassName	= name;
  wndclass.hIconSm		= LoadIcon (NULL, IDI_APPLICATION);

  RegisterClassEx (&wndclass);
}

ksi_window
ksi_tk_create_top (ksi_obj args)
{
  HWND		hwnd;
  ksi_window	wnd;
  top_info	info;
  char		*name;
  int		w, h, x, y, minw, maxw, minh, maxh;

  static int already_registered = 0;
  if (!already_registered)
    {
      already_registered = 1;
      register_top_class (topwin_class_name);
    }

  if (!args)
    args = ksi_nil;

  name = ksi_tk_string_arg (args, joke_tk->key_name,
			    "topLevel.name", "TopLevel.Name",
			    "Top Level");

  w = ksi_tk_int_arg (args, joke_tk->key_width,
		      "topLevel.width", "TopLevel.Width",
		      CW_USEDEFAULT);

  h = ksi_tk_int_arg (args, joke_tk->key_height,
		      "topLevel.height", "TopLevel.Height",
		      CW_USEDEFAULT);

  minw = ksi_tk_int_arg (args, joke_tk->key_min_width,
			 "topLevel.minWidth", "TopLevel.MinWidth", 0);

  minh = ksi_tk_int_arg (args, joke_tk->key_min_height,
			 "topLevel.minHeight", "TopLevel.MinHeight", 0);

  maxw = ksi_tk_int_arg (args, joke_tk->key_max_width,
			 "topLevel.maxWidth", "TopLevel.MaxWidth", 0);

  maxh = ksi_tk_int_arg (args, joke_tk->key_max_height,
			 "topLevel.maxHeight", "TopLevel.MaxHeight", 0);


  x = CW_USEDEFAULT;
  y = CW_USEDEFAULT;
  if (w <= 0) w = CW_USEDEFAULT;
  if (h <= 0) h = CW_USEDEFAULT;

  info = (top_info) ksi_malloc (sizeof *info);
  /* memset (info, 0, sizeof *info); */
  info->base.type	= TOPLEVEL_WINDOW;
  info->base.superwin	= 1;
  info->base.x		= x;
  info->base.y		= y;
  info->base.w		= w;
  info->base.h		= h;
  info->top		= ksi_nil;
  info->bottom		= ksi_nil;
  info->right		= ksi_nil;
  info->left		= ksi_nil;
  info->right		= ksi_nil;
  info->minw		= minw;
  info->minh		= minh;
  info->maxw		= maxw;
  info->maxh		= maxh;

  tmp_info = info;
  hwnd = CreateWindow (topwin_class_name,	/* window class name */
		       name,			/* window caption */
		       WS_OVERLAPPEDWINDOW,	/* window style */
		       x, y, w, h,
		       NULL,			/* parent window handle */
		       NULL,			/* window menu handle */
		       joke_instance,		/* program instance handle */
		       NULL);			/* creation parameters */
  tmp_info = 0;

  if (!hwnd)
    return (ksi_window) ksi_false;

  wnd = ksi_append_window (joke_wintab, hwnd);
  wnd->tk_data = (ksi_window_info) info;

  SetWindowLong (hwnd, GWL_USERDATA, (long) wnd);
  reorder_childs (wnd);

  return wnd;
}


ksi_obj
ksi_tk_set_top_name (ksi_window wnd, ksi_obj name)
{
  KSI_CHECK ((ksi_obj) wnd, WIN_P (wnd),
	     "tk-set-top-name: invalid window in arg1");

  SetWindowText (wnd->wid, ksi_obj2name (name));
  return ksi_unspec;
}

ksi_obj
ksi_tk_set_top_icon_name (ksi_window wnd, ksi_obj name)
{
  KSI_CHECK ((ksi_obj) wnd, TOP_P (wnd),
	     "tk-set-top-icon-name: invalid top window in arg1");

  SetWindowText (wnd->wid, ksi_obj2name (name));
  return ksi_unspec;
}

ksi_obj
ksi_tk_set_top_size (ksi_window wnd, ksi_obj w, ksi_obj h)
{
  int ww, hh;

  KSI_CHECK ((ksi_obj) wnd, TOP_P (wnd),
	     "tk-set-top-size: invalid top window in arg1");
  KSI_CHECK (w, KSI_EINT_P (w), "tk-set-top-size: invalid width in arg2");
  KSI_CHECK (h, KSI_EINT_P (h), "tk-set-top-size: invalid height in arg3");

  ww = KSI_SINT_COD (w);
  hh = KSI_SINT_COD (h);
  if (ww <= 0) ww = 1;
  if (hh <= 0) hh = 1;

  MoveWindow (wnd->wid, wnd->tk_data->x, wnd->tk_data->y, ww, hh, TRUE);
  return ksi_unspec;
}

ksi_obj
ksi_tk_set_top_status (ksi_window top, ksi_window stat)
{
  top_info info;

  KSI_CHECK ((ksi_obj) top, TOP_P (top),
	     "tk-set-top-status: invalid top window in arg1");
  KSI_CHECK ((ksi_obj) stat, STATUSBAR_P (stat),
	     "tk-set-top-status: invalid statusbar window in arg2");

  info = (top_info) top->tk_data;

  if (info->status)
    ShowWindow (info->status->wid, SW_HIDE);

  info->status = stat;
  reorder_childs (top);

  ShowWindow (stat->wid, SW_SHOW);
  return ksi_unspec;
}

ksi_obj
ksi_tk_set_top_main (ksi_window top, ksi_window wnd)
{
  top_info info;

  KSI_CHECK ((ksi_obj) top, TOP_P (top),
	     "tk-set-top-main: invalid top window in arg1");
  KSI_CHECK ((ksi_obj) wnd, WIN_P (wnd),
	     "tk-set-top-main: invalid window in arg2");

  info = (top_info) top->tk_data;

  if (info->main)
    ShowWindow (info->main->wid, SW_HIDE);

  info->main = wnd;
  reorder_childs (top);

  ShowWindow (wnd->wid, SW_SHOW);
  SetFocus (wnd->wid);
  return ksi_unspec;
}

ksi_obj
ksi_tk_set_top_left (ksi_window top, ksi_obj lst)
{
  top_info info;
  ksi_obj cur;

  KSI_CHECK ((ksi_obj) top, TOP_P (top),
	     "tk-set-top-left: invalid top window in arg1");

  for (cur = lst; cur != ksi_nil; cur = KSI_CDR (cur))
    {
      KSI_CHECK (lst, KSI_PAIR_P (cur),
		 "tk-set-top-left: invalid list in arg2");
      KSI_CHECK (KSI_CAR (cur), WIN_P (KSI_CAR (cur)),
		 "tk-set-top-left: invalid window");
    }

  info = (top_info) top->tk_data;

  for (cur = info->left; cur != ksi_nil; cur = KSI_CDR (cur))
    ShowWindow (((ksi_window) KSI_CAR (cur)) -> wid, SW_HIDE);

  info->left = lst;
  reorder_childs (top);

  for (cur = lst; cur != ksi_nil; cur = KSI_CDR (cur))
    ShowWindow (((ksi_window) KSI_CAR (cur)) -> wid, SW_SHOW);

  return ksi_unspec;
}

ksi_obj
ksi_tk_set_top_right (ksi_window top, ksi_obj lst)
{
  top_info info;
  ksi_obj cur;

  KSI_CHECK ((ksi_obj) top, TOP_P (top),
	     "tk-set-top-rigth: invalid top window in arg1");

  for (cur = lst; cur != ksi_nil; cur = KSI_CDR (cur))
    {
      KSI_CHECK (lst, KSI_PAIR_P (cur),
		 "tk-set-top-right: invalid list in arg2");
      KSI_CHECK (KSI_CAR (cur), WIN_P (KSI_CAR (cur)),
		 "tk-set-top-right: invalid window");
    }

  info = (top_info) top->tk_data;

  for (cur = info->right; cur != ksi_nil; cur = KSI_CDR (cur))
    ShowWindow (((ksi_window) KSI_CAR (cur)) -> wid, SW_HIDE);

  info->right = lst;
  reorder_childs (top);

  for (cur = lst; cur != ksi_nil; cur = KSI_CDR (cur))
    ShowWindow (((ksi_window) KSI_CAR (cur)) -> wid, SW_SHOW);

  return ksi_unspec;
}

ksi_obj
ksi_tk_set_top_top (ksi_window top, ksi_obj lst)
{
  top_info info;
  ksi_obj cur;

  KSI_CHECK ((ksi_obj) top, TOP_P (top),
	     "tk-set-top-top: invalid top window in arg1");

  for (cur = lst; cur != ksi_nil; cur = KSI_CDR (cur))
    {
      KSI_CHECK (lst, KSI_PAIR_P (cur),
		 "tk-set-top-top: invalid list in arg2");
      KSI_CHECK (KSI_CAR (cur), WIN_P (KSI_CAR (cur)),
		 "tk-set-top-top: invalid window");
    }

  info = (top_info) top->tk_data;

  for (cur = info->top; cur != ksi_nil; cur = KSI_CDR (cur))
    ShowWindow (((ksi_window) KSI_CAR (cur)) -> wid, SW_HIDE);

  info->top = lst;
  reorder_childs (top);

  for (cur = lst; cur != ksi_nil; cur = KSI_CDR (cur))
    ShowWindow (((ksi_window) KSI_CAR (cur)) -> wid, SW_SHOW);

  return ksi_unspec;
}

ksi_obj
ksi_tk_set_top_bottom (ksi_window top, ksi_obj lst)
{
  top_info info;
  ksi_obj cur;

  KSI_CHECK ((ksi_obj) top, TOP_P (top),
	     "tk-set-top-bottom: invalid top window in arg1");

  for (cur = lst; cur != ksi_nil; cur = KSI_CDR (cur))
    {
      KSI_CHECK (lst, KSI_PAIR_P (cur),
		 "tk-set-top-bottom: invalid list in arg2");
      KSI_CHECK (KSI_CAR (cur), WIN_P (KSI_CAR (cur)),
		 "tk-set-top-bottom: invalid window");
    }

  info = (top_info) top->tk_data;

  for (cur = info->bottom; cur != ksi_nil; cur = KSI_CDR (cur))
    ShowWindow (((ksi_window) KSI_CAR (cur)) -> wid, SW_HIDE);

  info->bottom = lst;
  reorder_childs (top);

  for (cur = lst; cur != ksi_nil; cur = KSI_CDR (cur))
    ShowWindow (((ksi_window) KSI_CAR (cur)) -> wid, SW_SHOW);

  return ksi_unspec;
}

ksi_obj
ksi_tk_map_top (ksi_obj wnd)
{
  KSI_CHECK (wnd, TOP_P (wnd), "tk-map-top: invalid top window in arg1");

  ShowWindow (((ksi_window) wnd) -> wid, SW_SHOW);
  return ksi_unspec;
}

ksi_obj
ksi_tk_unmap_top (ksi_obj wnd)
{
  KSI_CHECK (wnd, TOP_P (wnd), "tk-unmap-top: invalid top window in arg1");

  ShowWindow (((ksi_window) wnd) -> wid, SW_HIDE);
  return ksi_unspec;
}


static struct Ksi_Prim_Def defs [] =
{
  {"tk-create-top",		ksi_tk_create_top,	KSI_CALL_ARG1, 0},

  {"tk-set-top-name",		ksi_tk_set_top_name,	KSI_CALL_ARG2, 2},
  {"tk-set-top-icon-name",	ksi_tk_set_top_icon_name,KSI_CALL_ARG2, 2},
  {"tk-set-top-size",		ksi_tk_set_top_size,	KSI_CALL_ARG3, 3},
#if 0
  {"tk-set-top-menu",		ksi_tk_set_top_menu,	KSI_CALL_ARG2, 2},
#endif
  {"tk-set-top-status",		ksi_tk_set_top_status,	KSI_CALL_ARG2, 2},
  {"tk-set-top-main",		ksi_tk_set_top_main,	KSI_CALL_ARG2, 2},
  {"tk-set-top-left",		ksi_tk_set_top_left,	KSI_CALL_ARG2, 2},
  {"tk-set-top-right",		ksi_tk_set_top_right,	KSI_CALL_ARG2, 2},
  {"tk-set-top-top",		ksi_tk_set_top_top,	KSI_CALL_ARG2, 2},
  {"tk-set-top-bottom",		ksi_tk_set_top_bottom,	KSI_CALL_ARG2, 2},

  { "tk-map-top",		ksi_tk_map_top,		KSI_CALL_ARG1, 1 },
  { "tk-unmap-top",		ksi_tk_unmap_top,	KSI_CALL_ARG1, 1 },

  { 0 }
};

void
ksi_tk_init_topwin (ksi_ev env)
{
  ksi_reg_unit (defs, env);
}

 /* End of code */
