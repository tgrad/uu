/*
 * x11_border.c
 * 3d border drawing
 *
 * Copyright (C) 1999-2000, Ivan Demakov.
 *
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose, without fee, and without a written
 * agreement is hereby granted, provided that the above copyright notice
 * and this paragraph and the following two paragraphs appear in all copies.
 * Modifications to this software may be copyrighted by their authors
 * and need not follow the licensing terms described here, provided that
 * the new terms are clearly indicated on the first page of each file where
 * they apply.
 *
 * IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY
 * FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES,
 * INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS
 * DOCUMENTATION, EVEN IF THE AUTHORS HAS BEEN ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIMS ANY WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS
 * ON AN "AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAS NO OBLIGATIONS
 * TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 *
 *
 * Author:        Ivan Demakov <demakov@users.sourceforge.net>
 * Creation date: Mon Apr 19 19:37:08 1999
 * Last Update:   Tue Mar 21 19:29:21 2000
 *
 */

#include "toolkit.h"
#include <math.h>

static char rcsid[] = "$Id: x11_border.c,v 1.5 2000/04/15 14:03:16 ivan Exp $";

static int shift_table [129];


static void
draw_vert_3d (Drawable d, tk_color_3d gc,
	      int x, int y, int w, int h, int relief, int left)
{
  int half;
  GC l_gc, r_gc;

  switch (relief)
    {
    case RELIEF_FLAT:
      XFillRectangle (joke_dpy, d, gc->back_gc, x, y, w, h);
      break;

    case RELIEF_RAISED:
      XFillRectangle (joke_dpy, d, left ? gc->light_gc : gc->dark_gc,
		      x, y, w, h);
      break;

    case RELIEF_SUNKEN:
      XFillRectangle (joke_dpy, d, left ? gc->dark_gc : gc->light_gc,
		      x, y, w, h);
      break;

    case RELIEF_RIDGE:
      l_gc = gc->light_gc;
      r_gc = gc->dark_gc;
    groove:
      half = w / 2;
      if (!left && (w & 1)) half += 1;
      XFillRectangle (joke_dpy, d, l_gc, x, y, half, h);
      XFillRectangle (joke_dpy, d, r_gc, x+half, y, w-half, h);
      break;

    case RELIEF_GROOVE:
      r_gc = gc->light_gc;
      l_gc = gc->dark_gc;
      goto groove;
    }
}

static void
draw_horz_3d (Drawable d, tk_color_3d gc,
	      int x, int y, int w, int h, int relief,
	      int top, int left, int right)
{
  int half, x1, x2, d1, d2, b;
  GC t_gc, b_gc;

  switch (relief)
    {
    case RELIEF_FLAT:
      t_gc = b_gc = gc->back_gc;
      break;
    case RELIEF_RAISED:
      t_gc = b_gc = (top ? gc->light_gc : gc->dark_gc);
      break;
    case RELIEF_SUNKEN:
      t_gc = b_gc = (top ? gc->dark_gc : gc->light_gc);
      break;
    case RELIEF_RIDGE:
      t_gc = gc->light_gc;
      b_gc = gc->dark_gc;
      break;
    case RELIEF_GROOVE:
      t_gc = gc->dark_gc;
      b_gc = gc->light_gc;
      break;

    default:
      return;
    }

  x1 = x;
  if (!left) x1 += h;
  x2 = x + w; 
  if (!right) x2 -= h;
  d1 = left ? 1 : -1;
  d2 = right ? -1 : 1;
  half = y + h / 2;
  if (!top && (h & 1))
    half += 1;
  b = y + h;

  for (/**/; y < b; y++, x1 += d1, x2 += d2)
    {
      if (x1 < x2)
	{
	  XFillRectangle (joke_dpy, d, (y < half ? t_gc : b_gc),
			  x1, y, x2 - x1, 1);
	}
    }
}


static void
shiftline (XPoint *p1, XPoint *p2, int distance, XPoint *p3)
{
  int dx, dy, dx_neg = 0, dy_neg = 0;

  *p3 = *p1;
  dx = p2->x - p1->x;
  dy = p2->y - p1->y;

  if (dy < 0)
    dy_neg = 1, dy = -dy;

  if (dx < 0)
    dx_neg = 1, dx = -dx;

  if (dy <= dx)
    {
      dy = ((distance * shift_table[(dy << 7) / dx]) + 64) >> 7;
      if (!dx_neg)
	dy = -dy;

      p3->y += dy;
    }
  else
    {
      dx = ((distance * shift_table[(dx << 7) / dy]) + 64) >> 7;
      if (dy_neg)
	dx = -dx;

      p3->x += dx;
    }
}

static int
intersect (XPoint *a1, XPoint *a2, XPoint *b1, XPoint *b2, XPoint *ip)
{
  int dxadyb, dxbdya, dxadxb, dyadyb, p, q;

  /*
   * The code below is just a straightforward manipulation of two
   * equations of the form y = (x-x1)*(y2-y1)/(x2-x1) + y1 to solve
   * for the x-coordinate of intersection, then the y-coordinate.
   */

  dxadyb = (a2->x - a1->x) * (b2->y - b1->y);
  dxbdya = (b2->x - b1->x) * (a2->y - a1->y);
  dxadxb = (a2->x - a1->x) * (b2->x - b1->x);
  dyadyb = (a2->y - a1->y) * (b2->y - b1->y);

  if (dxadyb == dxbdya)
    return -1;

  p = a1->x * dxbdya - b1->x * dxadyb + (b1->y - a1->y) * dxadxb;
  q = dxbdya - dxadyb;
  if (q < 0)
    p = -p, q = -q;

  if (p < 0)
    ip->x = - ((-p + q/2) / q);
  else
    ip->x = (p + q/2) / q;

  p = (a1->y * dxadyb - b1->y * dxbdya + (b1->x - a1->x) * dyadyb);
  q = dxadyb - dxbdya;
  if (q < 0)
    p = -p, q = -q;

  if (p < 0)
    ip->y = - ((-p + q/2) / q);
  else
    ip->y = (p + q/2) / q;

  return 0;
}


void
ksi_tk_draw_3d_rectangle (Drawable d, tk_color_3d color,
			  int x, int y, int w, int h, int bw, int relief)
{
  if (w < 2 * bw) bw = w / 2;
  if (h < 2 * bw) bw = h / 2;

  draw_vert_3d (d, color, x, y, bw, h, relief, 1);
  draw_vert_3d (d, color, x+w-bw, y, bw, h, relief, 0);
  draw_horz_3d (d, color, x, y, w, bw, relief, 1, 1, 1);
  draw_horz_3d (d, color, x, y+h-bw, w, bw, relief, 0, 0, 0);
}


void
ksi_tk_draw_3d_polygon (Drawable d, tk_color_3d color,
			XPoint *pnt, int num, int bw, int relief)
{
  if (relief == RELIEF_GROOVE || relief == RELIEF_RIDGE)
    {
      ksi_tk_draw_3d_polygon (d, color, pnt, num, bw/2,
			      (relief == RELIEF_GROOVE
			       ? RELIEF_RAISED
			       : RELIEF_SUNKEN));
      ksi_tk_draw_3d_polygon(d, color, pnt, num, -bw/2,
			     (relief == RELIEF_GROOVE
			      ? RELIEF_SUNKEN
			      : RELIEF_RAISED));
    }
  else if (num >= 3)
    {
      XPoint *p1, *p2, b1, b2, nb1, nb2, poly[4];
      XPoint perp, c, shift1, shift2;
      int i, dx, dy, parallel, onleft, seen = 0;
      GC gc;

      if (pnt[0].x == pnt[num-1].x && pnt[0].y == pnt[num-1].y)
	num -= 1;

      for (i = -2, p1 = & pnt [num - 2], p2 = p1 + 1;
	   i < num;
	   i++, p1 = p2, p2++)
	{
	  if (i == -1 || i == num - 1)
	    p2 = pnt;

	  if (p2->x == p1->x && p2->y == p1->y)
	    continue;

	  shiftline (p1, p2, bw, &nb1);

	  nb2.x = nb1.x + (p2->x - p1->x);
	  nb2.y = nb1.y + (p2->y - p1->y);
	  poly[3] = *p1;

	  parallel = 0;
	  if (seen >= 1)
	    {
	      parallel = intersect (&nb1, &nb2, &b1, &b2, &poly[2]);
	      if (parallel)
		{
		  perp.x = p1->x + (p2->y - p1->y);
		  perp.y = p1->y - (p2->x - p1->x);
		  intersect (p1, &perp, &b1, &b2, &poly[2]);
		  intersect (p1, &perp, &nb1, &nb2, &c);
		  shiftline(p1, &perp, bw, &shift1);
		  shift2.x = shift1.x + (perp.x - p1->x);
		  shift2.y = shift1.y + (perp.y - p1->y);
		  intersect (p1, p2, &shift1, &shift2, &poly[3]);
		}
	    }

	  if (seen >= 2)
	    {
	      dx = poly[3].x - poly[0].x;
	      dy = poly[3].y - poly[0].y;
	      if (dx > 0)
		onleft = (dy <= dx);
	      else
		onleft = (dy < dx);

	      if (onleft ^ (relief == RELIEF_RAISED))
		gc = color->light_gc;
	      else
		gc = color->dark_gc;

	      XFillPolygon (joke_dpy, d, gc, poly, 4, Convex, CoordModeOrigin);
	    }

	  b1.x = nb1.x;
	  b1.y = nb1.y;
	  b2.x = nb2.x;
	  b2.y = nb2.y;
	  poly[0].x = poly[3].x;
	  poly[0].y = poly[3].y;
	  if (parallel)
	    {
	      poly[1].x = c.x;
	      poly[1].y = c.y;
	    }
	  else if (seen >= 1)
	    {
	      poly[1].x = poly[2].x;
	      poly[1].y = poly[2].y;
	    }

	  seen++;
	}
    }
}


void
ksi_tk_fill_3d_rectangle (Drawable d, tk_color_3d color,
			  int x, int y, int w, int h, int bw, int relief)
{
  int dbl;

  if (relief == RELIEF_FLAT)
    bw = 0;

  dbl = 2 * bw;
  if (w > dbl && h > dbl)
    XFillRectangle (joke_dpy, d, color->back_gc, x+bw, y+bw, w-dbl, h-dbl);

  if (bw)
    ksi_tk_draw_3d_rectangle (d, color, x, y, w, h, bw, relief);
}


void
ksi_tk_fill_3d_polygon (Drawable d, tk_color_3d color,
			XPoint *pnt, int num, int bw, int relief)
{
  XFillPolygon (joke_dpy, d, color->back_gc, pnt, num,
		Complex, CoordModeOrigin);

  if (relief != RELIEF_FLAT)
    ksi_tk_draw_3d_polygon (d, color, pnt, num, bw, relief);
}


void
ksi_tk_init_border (void)
{
  int i;
  double tangent, cosine;

  ignore (rcsid);

  for (i = 0; i <= 128; i++)
    {
      tangent = i / 128.0;
      cosine = 128 / cos (atan (tangent)) + 0.5;
      shift_table[i] = (int) cosine;
    }
}


 /* End of code */

