/*
 * x11_canvas.c
 * 
 *
 * Copyright (C) 1999,2000, Ivan Demakov.
 *
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose, without fee, and without a written
 * agreement is hereby granted, provided that the above copyright notice
 * and this paragraph and the following two paragraphs appear in all copies.
 * Modifications to this software may be copyrighted by their authors
 * and need not follow the licensing terms described here, provided that
 * the new terms are clearly indicated on the first page of each file where
 * they apply.
 *
 * IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY
 * FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES,
 * INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS
 * DOCUMENTATION, EVEN IF THE AUTHORS HAS BEEN ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIMS ANY WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS
 * ON AN "AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAS NO OBLIGATIONS
 * TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 *
 *
 * Author:        Ivan Demakov <demakov@users.sourceforge.net>
 * Creation date: Thu Apr 29 18:56:18 1999
 * Last Update:   Tue Mar 21 19:29:52 2000
 *
 */

#include "toolkit.h"

static char rcsid[] = "$Id: x11_canvas.c,v 1.13 2000/05/11 18:10:37 ivan Exp $";


static void
expose_canvas (ksi_window wnd, int x, int y, int w, int h);


#define DRAW_TIMEOUT 0.400


static void
map_childs (ksi_window wnd)
{
  canvas_info info = (canvas_info) wnd->tk_data;
  ksi_window horz, vert;

  vert = info->vert_scroll;
  horz = info->horz_scroll;

  if (vert)
    XMapWindow (joke_dpy, vert->wid);
  if (horz)
    XMapWindow (joke_dpy, horz->wid);
}


static void
reorder_childs (ksi_window wnd)
{
  canvas_info info = (canvas_info) wnd->tk_data;
  int x, y, w, h;
  ksi_window horz, vert;

  vert = info->vert_scroll;
  horz = info->horz_scroll;

  if (vert)
    {
      x = info->base.w - vert->tk_data->w;
      y = 0;
      w = vert->tk_data->w;
      h = info->base.h;
      if (horz)
	h -= horz->tk_data->h;

      XMoveResizeWindow (joke_dpy, vert->wid, x, y, w, h > 0 ? h : 1);
    }

  if (horz)
    {
      x = 0;
      y = info->base.h - horz->tk_data->h;
      w = info->base.w;
      if (vert)
	w -= vert->tk_data->w;
      h = horz->tk_data->h;

      XMoveResizeWindow (joke_dpy, horz->wid, x, y, w > 0 ? w : 1, h);
    }
}

static void
canvas_geometry (ksi_window wnd)
{
  canvas_info info = (canvas_info) wnd->tk_data;
  int draw_w, draw_h, view_w, view_h, work_w, work_h;
  double beg, end;

  info->draw_x1 = 0;
  info->draw_y1 = 0;
  info->draw_x2 = info->base.w;
  info->draw_y2 = info->base.h;

  if (info->vert_scroll)
    info->draw_x2 -= info->vert_scroll->tk_data->w;
  if (info->horz_scroll)
    info->draw_y2 -= info->horz_scroll->tk_data->h;

  draw_w = info->draw_x2 - info->draw_x1;
  draw_h = info->draw_y2 - info->draw_y1;
  view_w = (int) (draw_w / info->zoom_k + 0.5);
  view_h = (int) (draw_h / info->zoom_k + 0.5);
  work_w = info->work_x2 - info->work_x1;
  work_h = info->work_y2 - info->work_y1;

  if (view_w < 0)
    view_w = 0;
  if (view_h < 0)
    view_h = 0;

  if (info->view_off_x + view_w > info->work_x2)
    info->view_off_x = info->work_x2 - view_w;
  if (info->view_off_x < info->work_x1)
    info->view_off_x = info->work_x1;

  if (info->view_off_y + view_h > info->work_y2)
    info->view_off_y = info->work_y2 - view_h;
  if (info->view_off_y < info->work_y1)
    info->view_off_y = info->work_y1;

  info->line_step_x = (int) (1.0 / info->zoom_k + 0.5);
  if (info->line_step_x <= 0)
    info->line_step_x = 1;

  info->page_step_x = (int) (0.8 * view_w + 0.5);
  if (info->page_step_x <= 0)
    info->page_step_x = 1;

  info->line_step_y = (int) (1.0 / info->zoom_k + 0.5);
  if (info->line_step_y <= 0)
    info->line_step_y = 1;

  info->page_step_y = (int) (0.8 * view_h + 0.5);
  if (info->page_step_y <= 0)
    info->page_step_y = 1;

  if (info->horz_scroll)
    {
      if (work_w <= 0)
	beg = 0.0, end = 1.0;
      else
	{
	  beg = ((double) info->view_off_x - info->work_x1) / work_w;
	  end = ((double) info->view_off_x + view_w - info->work_x1) / work_w;
	}

      ksi_tk_set_scrolbar_value ((ksi_obj) info->horz_scroll,
				 ksi_double2num (beg),
				 ksi_double2num (end));
    }

  if (info->vert_scroll)
    {
      if (work_h <= 0)
	beg = 0.0, end = 1.0;
      else
	{
	  beg = ((double) info->view_off_y - info->work_y1) / work_h;
	  end = ((double) info->view_off_y + view_h - info->work_y1) / work_h;
	}

      ksi_tk_set_scrolbar_value ((ksi_obj) info->vert_scroll,
				 ksi_double2num (beg),
				 ksi_double2num (end));
    }
}

static void
alloc_pix_gc (ksi_window wnd)
{
  canvas_info info = (canvas_info) wnd->tk_data;
  if (info->pix_gc == None)
    {
      XGCValues val;
      val.foreground = info->background;
      val.graphics_exposures = False;
      info->pix_gc = XCreateGC (joke_dpy, wnd->wid,
				GCForeground | GCGraphicsExposures,
				&val);
    }
}

static void
alloc_draw_gc (ksi_window wnd)
{
  canvas_info info = (canvas_info) wnd->tk_data;
  if (info->draw_gc == None)
    {
      XGCValues val;
      val.graphics_exposures = False;
      info->draw_gc = XCreateGC (joke_dpy, wnd->wid,
				 GCGraphicsExposures,
				 &val);
    }
}


static void
idle_draw (ksi_window wnd, void *data)
{
  canvas_info info = (canvas_info) wnd->tk_data;
  int draw_w, draw_h, pix_w, pix_h;
  int off_x, off_y, work_w, work_h;
  int work_x1, work_y1, work_x2, work_y2;

  info->base.idledraw = 0;

  draw_w = info->draw_x2 - info->draw_x1;
  draw_h = info->draw_y2 - info->draw_y1;

  if (draw_w <= 0 || draw_h <= 0)
    return;

  if (!info->offscreen_draw)
    {
      ksi_obj v, e;

      XClearArea (joke_dpy, wnd->wid,
		  info->draw_x1, info->draw_y1, draw_w, draw_h, False);

      v = ksi_tk_get_canvas_view_area ((ksi_obj) wnd);

      e = ksi_make_vector (ksi_int2num (4), 0);
      KSI_VEC_REF (e, 0) = joke_tk->sym_draw;
      KSI_VEC_REF (e, 1) = (ksi_obj) wnd;
      KSI_VEC_REF (e, 2) = v;
      KSI_VEC_REF (e, 3) = ksi_double2num (info->zoom_k);

      ksi_apply_window_event ((ksi_obj) wnd, e);
      return;
    }

  if (info->pixmap == None || info->pix_w < draw_w || info->pix_h < draw_h)
    {
      if (info->pixmap != None)
	{
	  XFreePixmap (joke_dpy, info->pixmap);
	  info->pixmap = None;
	}

      pix_w = draw_w + draw_w / 2;
      pix_h = draw_h + draw_h / 2;

      info->pix_w = pix_w;
      info->pix_h = pix_h;
      info->pixmap = XCreatePixmap (joke_dpy, wnd->wid, pix_w, pix_h,
				    DefaultDepth (joke_dpy, joke_tk->screen));
    }

  work_w = (int) (info->pix_w / info->zoom_k + 0.5);
  work_h = (int) (info->pix_h / info->zoom_k + 0.5);

  off_x = ((int) (work_w - draw_w / info->zoom_k + 0.5)) / 2;
  off_y = ((int) (work_h - draw_h / info->zoom_k + 0.5)) / 2;

  work_x1 = info->view_off_x - off_x;
  work_y1 = info->view_off_y - off_y;
  if (work_x1 < info->work_x1)
    work_x1 = info->work_x1;
  if (work_y1 < info->work_y1)
    work_y1 = info->work_y1;

  work_x2 = work_x1 + work_w;
  work_y2 = work_y1 + work_h;
  if (work_x2 > info->work_x2)
    {
      work_x2 = info->work_x2;
      work_x1 = work_x2 - work_w;
    }
  if (work_y2 > info->work_y2)
    {
      work_y2 = info->work_y2;
      work_y1 = work_y2 - work_h;
    }
  if (work_x1 < info->work_x1)
    work_x1 = info->work_x1;
  if (work_y1 < info->work_y1)
    work_y1 = info->work_y1;

  info->pix_x1 = work_x1;
  info->pix_y1 = work_y1;
  info->pix_x2 = work_x2;
  info->pix_y2 = work_y2;

  alloc_pix_gc (wnd);
  XFillRectangle (joke_dpy, info->pixmap, info->pix_gc,
		  0, 0, info->pix_w, info->pix_h);

  if (work_x1 < work_x2 && work_y1 < work_y2)
    {
      ksi_obj v, e;

      v = ksi_make_vector (ksi_int2num (4), 0);
      KSI_VEC_REF (v, 0) = ksi_int2num (info->pix_x1);
      KSI_VEC_REF (v, 1) = ksi_int2num (info->pix_y1);
      KSI_VEC_REF (v, 2) = ksi_int2num (info->pix_x2);
      KSI_VEC_REF (v, 3) = ksi_int2num (info->pix_y2);

      e = ksi_make_vector (ksi_int2num (4), 0);
      KSI_VEC_REF (e, 0) = joke_tk->sym_draw;
      KSI_VEC_REF (e, 1) = (ksi_obj) wnd;
      KSI_VEC_REF (e, 2) = v;
      KSI_VEC_REF (e, 3) = ksi_double2num (info->zoom_k);

      ksi_apply_window_event ((ksi_obj) wnd, e);
    }

  expose_canvas (wnd, info->draw_x1, info->draw_y1,
		 info->draw_x2 - info->draw_x1,
		 info->draw_y2 - info->draw_y1);
}


static void
draw_canvas (ksi_window wnd, int x, int y, int w, int h, int expose)
{
  canvas_info info = (canvas_info) wnd->tk_data;
  int work_x1, work_y1, work_x2, work_y2;

  XClearArea (joke_dpy, wnd->wid, x, y, w, h, expose);

  work_x1 = x - info->draw_x1;
  work_y1 = y - info->draw_y1;
  work_x2 = work_x1 + w;
  work_y2 = work_y1 + h;

  work_x1 = (int) (work_x1 / info->zoom_k + 0.5) + info->view_off_x;
  work_y1 = (int) (work_y1 / info->zoom_k + 0.5) + info->view_off_y;
  work_x2 = (int) (work_x2 / info->zoom_k + 0.5) + info->view_off_x;
  work_y2 = (int) (work_y2 / info->zoom_k + 0.5) + info->view_off_y;

  if (work_x1 < info->work_x1)
    work_x1 = info->work_x1;
  if (work_y1 < info->work_y1)
    work_y1 = info->work_y1;

  if (work_x2 > info->work_x2)
    work_x2 = info->work_x2;
  if (work_y2 > info->work_y2)
    work_y2 = info->work_y2;

  if (work_x1 < work_x2 && work_y1 < work_y2)
    {
      if (info->base.idledraw)
	ksi_tk_del_timer (idle_draw, wnd, 0);

      info->base.idledraw = 1;
      ksi_tk_add_timer (DRAW_TIMEOUT, idle_draw, wnd, 0);
    }
}


static void
expose_canvas (ksi_window wnd, int x, int y, int w, int h)
{
  canvas_info info = (canvas_info) wnd->tk_data;
  int pix_x, pix_y, pix_w, pix_h, off_x, off_y;

  if (x < info->draw_x1)
    {
      w -= info->draw_x1 - x;
      x = info->draw_x1;
    }
  if (y < info->draw_y1)
    {
      h -= info->draw_y1 - y;
      y = info->draw_y1;
    }
  if (x + w > info->draw_x2)
    w = info->draw_x2 - x;
  if (y + h > info->draw_y2)
    h = info->draw_y2 - y;

  if (w <= 0 || h <= 0)
    return;

  if (info->pixmap == None)
    draw_canvas (wnd, x, y, w, h, False);
  else
    {
      off_x = (int) ((info->view_off_x - info->pix_x1) * info->zoom_k + 0.5);
      off_y = (int) ((info->view_off_y - info->pix_y1) * info->zoom_k + 0.5);

      pix_x = x - info->draw_x1 + off_x;
      pix_y = y - info->draw_y1 + off_y;
      pix_w = w;
      pix_h = h;

      if (pix_x < 0)
	{
	  draw_canvas (wnd, x, y, -pix_x, pix_h, False);
	  x -= pix_x;
	  pix_w += pix_x;
	  pix_x = 0;
	}

      if (pix_y < 0)
	{
	  draw_canvas (wnd, x, y, pix_w, -pix_y, False);
	  y -= pix_y;
	  pix_h += pix_y;
	  pix_y = 0;
	}

      if (pix_x + pix_w > info->pix_w)
	{
	  w = info->pix_w - pix_x;
	  draw_canvas (wnd, x + w, y, pix_w - w, pix_h, False);
	  pix_w = w;
	}

      if (pix_y + pix_h > info->pix_h)
	{
	  h = info->pix_h - pix_y;
	  draw_canvas (wnd, x, y + h, pix_w, pix_h - h, False);
	  pix_h = h;
	}

      if (pix_w > 0 && pix_h > 0)
	{
	  alloc_pix_gc (wnd);
	  XCopyArea (joke_dpy, info->pixmap, wnd->wid, info->pix_gc,
		     pix_x, pix_y, pix_w, pix_h, x, y);
	}
    }
}


static void
canvas_window_event_proc (ksi_window wnd, XEvent *xe, void *data)
{
  ksi_obj ek;
  canvas_info info = (canvas_info) wnd->tk_data;

  switch (xe->type)
    {
    case MapNotify:
      wnd->tk_data->mapped = 1;
      break;

    case UnmapNotify:
      wnd->tk_data->mapped = 0;
      if (info->pixmap != None)
	{
	  XFreePixmap (joke_dpy, info->pixmap);
	  info->pixmap = None;
	}
      if (info->pix_gc != None)
	{
	  XFreeGC (joke_dpy, info->pix_gc);
	  info->pix_gc = None;
	}
      if (info->draw_gc != None)
	{
	  XFreeGC (joke_dpy, info->draw_gc);
	  info->draw_gc = None;
	}
      break;

    case ConfigureNotify:
      wnd->tk_data->x = xe->xconfigure.x;
      wnd->tk_data->y = xe->xconfigure.y;
      wnd->tk_data->w = xe->xconfigure.width;
      wnd->tk_data->h = xe->xconfigure.height;
      reorder_childs (wnd);
      canvas_geometry (wnd);
      break;

    case Expose:
      expose_canvas (wnd,
		     xe->xexpose.x, xe->xexpose.y,
		     xe->xexpose.width, xe->xexpose.height);
      break;
    }

  ek = ksi_make_x11_event (xe);
  if (ek != ksi_false)
    ksi_apply_window_event ((ksi_obj) wnd, ek);
}

static ksi_obj
ksi_tk_canvas_scroll_event (ksi_window wnd, ksi_obj ev)
{
  canvas_info info = (canvas_info) wnd->tk_data;
  int dx, dy, x, y, w, h;
  double kx, ky;

  if (KSI_VEC_REF (ev, 0) != joke_tk->sym_scroll)
    return ksi_false;

  if (KSI_VEC_REF (ev, 3) == joke_tk->sym_line_up)
    dx = 0, dy = - info->line_step_y;
  else if (KSI_VEC_REF (ev, 3) == joke_tk->sym_line_down)
    dx = 0, dy = info->line_step_y;
  else if (KSI_VEC_REF (ev, 3) == joke_tk->sym_line_left)
    dx = - info->line_step_x, dy = 0;
  else if (KSI_VEC_REF (ev, 3) == joke_tk->sym_line_right)
    dx = info->line_step_x, dy = 0;
  else if (KSI_VEC_REF (ev, 3) == joke_tk->sym_page_up)
    dx = 0, dy = - info->page_step_y;
  else if (KSI_VEC_REF (ev, 3) == joke_tk->sym_page_down)
    dx = 0, dy = info->page_step_y;
  else if (KSI_VEC_REF (ev, 3) == joke_tk->sym_page_left)
    dx = - info->page_step_x, dy = 0;
  else if (KSI_VEC_REF (ev, 3) == joke_tk->sym_page_right)
    dx = info->page_step_x, dy = 0;
  else if (KSI_VEC_REF (ev, 3) == joke_tk->sym_move)
    {
      kx = ksi_num2double (KSI_VEC_REF (ev, 4));
      ky = ksi_num2double (KSI_VEC_REF (ev, 5));
      dx = (int) ((info->work_x2 - info->work_x1) * kx);
      dy = (int) ((info->work_y2 - info->work_y1) * ky);
    }
  else
    return ksi_true;

  x = info->view_off_x;
  y = info->view_off_y;

  info->view_off_x += dx;
  info->view_off_y += dy;

  canvas_geometry (wnd);

  dx = info->view_off_x - x;
  dy = info->view_off_y - y;

  if (dx != 0 || dy != 0)
    {
      x = info->draw_x1;
      y = info->draw_y1;
      w = info->draw_x2 - x;
      h = info->draw_y2 - y;

      if (info->offscreen_draw)
	expose_canvas (wnd, x, y, w, h);
      else
	{
	  dx = (int) (dx * info->zoom_k + 0.5);
	  dy = (int) (dy * info->zoom_k + 0.5);

	  alloc_pix_gc (wnd);
	  if (dx < 0)
	    {
	      XCopyArea (joke_dpy, wnd->wid, wnd->wid, info->pix_gc,
			 x, y, w + dx, h, x - dx, y);
	      XClearArea (joke_dpy, wnd->wid, x, y, -dx, h, True);
	    }
	  else if (dx > 0)
	    {
	      XCopyArea (joke_dpy, wnd->wid, wnd->wid, info->pix_gc,
			 x + dx, y, w - dx, h, x, y);
	      XClearArea (joke_dpy, wnd->wid, x + w - dx, y, dx, h, True);
	    }

	  if (dy < 0)
	    {
	      XCopyArea (joke_dpy, wnd->wid, wnd->wid, info->pix_gc,
			 x, y, w, h + dy, x, y - dy);
	      XClearArea (joke_dpy, wnd->wid, x, y, w, -dy, True);
	    }
	  else if (dy > 0)
	    {
	      XCopyArea (joke_dpy, wnd->wid, wnd->wid, info->pix_gc,
			 x, y + dy, w, h - dy, x, y);
	      XClearArea (joke_dpy, wnd->wid, x, y + h - dy, w, dy, True);
	    }
	}
    }

  return ksi_true;
}


ksi_window
ksi_tk_create_canvas (ksi_window top, ksi_obj args)
{
  Window wid;
  XSetWindowAttributes attr;
  ksi_window wnd;
  canvas_info info;
  ksi_pixel background;

  KSI_CHECK ((ksi_obj) top, SUPERWIN_P (top),
	     "tk-create-canvas: invalid parent window in arg1");

  if (!args)
    args = ksi_nil;

  background = ksi_tk_pixel_arg (args, joke_tk->key_background,
				 "canvas.background",
				 "Canvas.Background",
				 joke_tk->top_back->back);

  attr.background_pixel = background;
  attr.event_mask = StructureNotifyMask | ExposureMask
    | ButtonPressMask | ButtonReleaseMask | PointerMotionMask
    | KeyPressMask;

  wid = XCreateWindow (joke_dpy, top->wid,
		       0, 0, 1, 1,
		       0,
		       CopyFromParent,
		       InputOutput,
		       joke_tk->visual,
		       CWBackPixel | CWEventMask,
		       &attr);

  if (!wid)
    return (ksi_window) ksi_false;

  info = (canvas_info) ksi_malloc (sizeof *info);
  /* memset (info, 0, sizeof *info); */
  info->base.proc = canvas_window_event_proc;
  info->base.parent = top;
  info->base.type = CANVAS_WINDOW;
  info->base.superwin = 1;
  info->base.x = 0;
  info->base.y = 0;
  info->base.w = 1;
  info->base.h = 1;
  info->zoom_k = 1.0;
  info->background = background;
  info->pixmap = None;
  info->pix_gc = None;
  info->draw_gc = None;
  info->offscreen_draw = ksi_tk_bool_resource ("canvas.offscreenDraw",
					       "Canvas.OffscreenDraw",
					       0);

  ksi_tk_rect_arg (args, joke_tk->key_work, 0, 0,
		   &info->work_x1, &info->work_y1,
		   &info->work_x2, &info->work_y2);

  wnd = ksi_append_window (joke_wintab, wid);
  wnd->tk_data = (ksi_window_info) info;

  info->vert_scroll
    = ksi_tk_create_scrollbar (wnd, KSI_LIST4 (joke_tk->key_orient,
					       joke_tk->key_vertical,
					       joke_tk->key_window,
					       (ksi_obj) wnd));
  info->horz_scroll
    = ksi_tk_create_scrollbar (wnd, KSI_LIST4 (joke_tk->key_orient,
					       joke_tk->key_horizontal,
					       joke_tk->key_window,
					       (ksi_obj) wnd));

  map_childs (wnd);
  canvas_geometry (wnd);

  ksi_append_window_event ((ksi_obj) wnd,
			   joke_tk->sym_scroll,
			   joke_tk->canvas_scroll_event);

  return wnd;
}


static void
redraw_canvas (ksi_window wnd, int clear)
{
  canvas_info info = (canvas_info) wnd->tk_data;

  if (!info->base.mapped)
    return;

  if (info->pixmap != None && clear)
    {
      alloc_pix_gc (wnd);
      XFillRectangle (joke_dpy, info->pixmap, info->pix_gc,
		      0, 0, info->pix_w, info->pix_h);

      if (info->base.idledraw)
	ksi_tk_del_timer (idle_draw, wnd, 0);

      info->base.idledraw = 1;
      ksi_tk_add_timer (DRAW_TIMEOUT, idle_draw, wnd, 0);
    }

  expose_canvas (wnd, info->draw_x1, info->draw_y1,
		 info->draw_x2 - info->draw_x1,
		 info->draw_y2 - info->draw_y1);
}


ksi_obj
ksi_tk_get_canvas_draw_area (ksi_obj wnd)
{
  ksi_obj v;
  canvas_info info;

  KSI_CHECK (wnd, CANVAS_P (wnd),
	     "tk-get-canvas-draw-area: invalid canvas window in arg1");

  info = (canvas_info) ((ksi_window) wnd) -> tk_data;

  v = ksi_make_vector (ksi_int2num (4), 0);
  KSI_VEC_REF (v, 0) = ksi_int2num (info->draw_x1);
  KSI_VEC_REF (v, 1) = ksi_int2num (info->draw_y1);
  KSI_VEC_REF (v, 2) = ksi_int2num (info->draw_x2);
  KSI_VEC_REF (v, 3) = ksi_int2num (info->draw_y2);

  return v;
}

ksi_obj
ksi_tk_get_canvas_work_area (ksi_obj wnd)
{
  ksi_obj v;
  canvas_info info;

  KSI_CHECK (wnd, CANVAS_P (wnd),
	     "tk-get-canvas-work-area: invalid canvas window in arg1");

  info = (canvas_info) ((ksi_window) wnd) -> tk_data;

  v = ksi_make_vector (ksi_int2num (4), 0);
  KSI_VEC_REF (v, 0) = ksi_int2num (info->work_x1);
  KSI_VEC_REF (v, 1) = ksi_int2num (info->work_y1);
  KSI_VEC_REF (v, 2) = ksi_int2num (info->work_x2);
  KSI_VEC_REF (v, 3) = ksi_int2num (info->work_y2);

  return v;
}

ksi_obj
ksi_tk_set_canvas_work_area (ksi_obj wnd, ksi_obj v)
{
  canvas_info info;
  int tmp;

  KSI_CHECK (wnd, CANVAS_P (wnd),
	     "tk-set-canvas-work-area: invalid canvas window in arg1");

  KSI_CHECK (v, KSI_VEC_P (v) && KSI_VEC_LEN (v) == 4,
	     "tk-set-canvas-work-area: invalid vector or vector length in arg2");

  info = (canvas_info) ((ksi_window) wnd) -> tk_data;

  info->work_x1 = ksi_num2int (KSI_VEC_REF (v, 0));
  info->work_y1 = ksi_num2int (KSI_VEC_REF (v, 1));
  info->work_x2 = ksi_num2int (KSI_VEC_REF (v, 2));
  info->work_y2 = ksi_num2int (KSI_VEC_REF (v, 3));

  if (info->work_x1 > info->work_x2)
    {
      tmp = info->work_x1;
      info->work_x1 = info->work_x2;
      info->work_x2 = tmp;
    }

  if (info->work_y1 > info->work_y2)
    {
      tmp = info->work_y1;
      info->work_y1 = info->work_y2;
      info->work_y2 = tmp;
    }

  canvas_geometry ((ksi_window) wnd);
  redraw_canvas ((ksi_window) wnd, True);

  return ksi_unspec;
}

ksi_obj
ksi_tk_get_canvas_view_off (ksi_obj wnd)
{
  ksi_obj v;
  canvas_info info;

  KSI_CHECK (wnd, CANVAS_P (wnd),
	     "tk-get-canvas-view-offset: invalid canvas window in arg1");

  info = (canvas_info) ((ksi_window) wnd) -> tk_data;

  v = ksi_make_vector (ksi_int2num (2), 0);
  KSI_VEC_REF (v, 0) = ksi_int2num (info->view_off_x);
  KSI_VEC_REF (v, 1) = ksi_int2num (info->view_off_y);

  return v;
}

ksi_obj
ksi_tk_set_canvas_view_off (ksi_obj wnd, ksi_obj x, ksi_obj y)
{
  canvas_info info;
  int off_x, off_y;

  KSI_CHECK (wnd, CANVAS_P (wnd),
	     "tk-set-canvas-view-offset: invalid canvas window in arg1");

  info = (canvas_info) ((ksi_window) wnd) -> tk_data;

  if (!y)
    {
      KSI_CHECK (x, KSI_VEC_P (x) && KSI_VEC_LEN (x) == 2,
		 "tk-set-canvas-view-offset: invalid vector or vector length in arg2");
      off_x = ksi_num2int (KSI_VEC_REF (x, 0));
      off_y = ksi_num2int (KSI_VEC_REF (y, 0));
    }
  else
    {
      KSI_CHECK (x, KSI_REAL_P (x),
		 "tk-set-canvas-view-offset: invalid number in arg2");
      KSI_CHECK (y, KSI_REAL_P (y),
		 "tk-set-canvas-view-offset: invalid number in arg3");
      off_x = ksi_num2int (x);
      off_y = ksi_num2int (y);
    }

  info->view_off_x = off_x;
  info->view_off_y = off_y;

  canvas_geometry ((ksi_window) wnd);
  redraw_canvas ((ksi_window) wnd, False);

  return ksi_unspec;
}

ksi_obj
ksi_tk_get_canvas_view_zoom (ksi_obj wnd)
{
  canvas_info info;

  KSI_CHECK (wnd, CANVAS_P (wnd),
	     "tk-get-canvas-view-zoom: invalid canvas window in arg1");

  info = (canvas_info) ((ksi_window) wnd) -> tk_data;

  return ksi_double2num (info->zoom_k);
}

ksi_obj
ksi_tk_set_canvas_view_zoom (ksi_obj wnd, ksi_obj k)
{
  canvas_info info;
  double zoom_k;

  KSI_CHECK (wnd, CANVAS_P (wnd),
	     "tk-set-canvas-view-zoom: invalid canvas window in arg1");

  KSI_CHECK (k, KSI_REAL_P (k),
	     "tk-set-canvas-view-zoom: invalid number in arg2");

  zoom_k = ksi_num2double (k);
  KSI_CHECK (k, zoom_k > 0.0,
	     "tk-set-canvas-view-zoom: invalid number in arg2");

  info = (canvas_info) ((ksi_window) wnd) -> tk_data;
  info->zoom_k = zoom_k;

  canvas_geometry ((ksi_window) wnd);
  redraw_canvas ((ksi_window) wnd, True);

  return ksi_unspec;
}

ksi_obj
ksi_tk_get_canvas_view_area (ksi_obj wnd)
{
  ksi_obj v;
  canvas_info info;
  int view_w, view_h;

  KSI_CHECK (wnd, CANVAS_P (wnd),
	     "tk-get-canvas-view-area: invalid canvas window in arg1");

  info = (canvas_info) ((ksi_window) wnd) -> tk_data;

  view_w = info->draw_x2 - info->draw_x1;
  view_h = info->draw_y2 - info->draw_y1;
  view_w = (int) (view_w / info->zoom_k + 0.5);
  view_h = (int) (view_h / info->zoom_k + 0.5);

  if (view_w < 0)
    view_w = 0;
  if (view_h < 0)
    view_h = 0;

  v = ksi_make_vector (ksi_int2num (4), 0);
  KSI_VEC_REF (v, 0) = ksi_int2num (info->view_off_x);
  KSI_VEC_REF (v, 1) = ksi_int2num (info->view_off_y);
  KSI_VEC_REF (v, 2) = ksi_int2num (info->view_off_x + view_w);
  KSI_VEC_REF (v, 3) = ksi_int2num (info->view_off_y + view_h);

  return v;
}

ksi_obj
ksi_tk_get_canvas_dc (ksi_obj wnd)
{
  canvas_info info;
  Window wid;
  Drawable d;
  XRectangle r;

  KSI_CHECK (wnd, CANVAS_P (wnd),
	     "tk-get-canvas-dc: invalid canvas window in arg1");

  info = (canvas_info) ((ksi_window) wnd) -> tk_data;
  wid = (((ksi_window) wnd) -> wid);

  alloc_draw_gc ((ksi_window) wnd);

  d = info->pixmap;
  if (d == None)
    {
      d = wid;
      r.x = info->draw_x1;
      r.y = info->draw_y1;
      r.width = info->draw_x2 - info->draw_x1;
      r.height = info->draw_y2 - info->draw_y1;
      XSetClipRectangles (joke_dpy, info->draw_gc, 0, 0, &r, 1, YXBanded);
    }

  return ksi_make_x11_dc (wid, d, info->draw_gc);
}


ksi_obj
ksi_tk_canvas_work_x (ksi_obj wnd, ksi_obj num)
{
  canvas_info	info;
  int c;

  KSI_CHECK (wnd, CANVAS_P (wnd),
	     "tk-canvas-work-x: invalid canvas window in arg1");
  KSI_CHECK (num, KSI_REAL_P (num),
	     "tk-canvas-work-x: invalid number in arg2");

  info = (canvas_info) ((ksi_window) wnd) -> tk_data;
  c = ksi_num2int (num);
  c = (int) (((c - info->draw_x1) / info->zoom_k) + info->view_off_x + 0.5);
  return ksi_int2num (c);
}

ksi_obj
ksi_tk_canvas_work_y (ksi_obj wnd, ksi_obj num)
{
  canvas_info	info;
  int c;

  KSI_CHECK (wnd, CANVAS_P (wnd),
	     "tk-canvas-work-y: invalid canvas window in arg1");
  KSI_CHECK (num, KSI_REAL_P (num),
	     "tk-canvas-work-y: invalid number in arg2");

  info = (canvas_info) ((ksi_window) wnd) -> tk_data;
  c = ksi_num2int (num);
  c = (int) (((c - info->draw_y1) / info->zoom_k) + info->view_off_y + 0.5);
  return ksi_int2num (c);
}

ksi_obj
ksi_tk_canvas_draw_x (ksi_obj wnd, ksi_obj num)
{
  canvas_info	info;
  int c;

  KSI_CHECK (wnd, CANVAS_P (wnd),
	     "tk-canvas-draw-x: invalid canvas window in arg1");
  KSI_CHECK (num, KSI_REAL_P (num),
	     "tk-canvas-draw-x: invalid number in arg2");

  info = (canvas_info) ((ksi_window) wnd) -> tk_data;
  c = ksi_num2int (num);

  if (info->pixmap)
    c -= info->pix_x1;
  else
    c -= info->view_off_x;

  c = (int) (c * info->zoom_k + info->draw_x1 + 0.5);
  return ksi_int2num (c);
}

ksi_obj
ksi_tk_canvas_draw_y (ksi_obj wnd, ksi_obj num)
{
  canvas_info	info;
  int c;

  KSI_CHECK (wnd, CANVAS_P (wnd),
	     "tk-canvas-draw-y: invalid canvas window in arg1");
  KSI_CHECK (num, KSI_REAL_P (num),
	     "tk-canvas-draw-y: invalid number in arg2");

  info = (canvas_info) ((ksi_window) wnd) -> tk_data;
  c = ksi_num2int (num);

  if (info->pixmap)
    c -= info->pix_y1;
  else
    c -= info->view_off_y;

  c = (int) (c * info->zoom_k + info->draw_y1 + 0.5);
  return ksi_int2num (c);
}


static struct Ksi_Prim_Def defs [] =
{
  {"tk-create-canvas",	      ksi_tk_create_canvas,	   KSI_CALL_ARG2, 1},
  {"tk-get-canvas-draw-area", ksi_tk_get_canvas_draw_area, KSI_CALL_ARG1, 1},
  {"tk-get-canvas-work-area", ksi_tk_get_canvas_work_area, KSI_CALL_ARG1, 1},
  {"tk-set-canvas-work-area", ksi_tk_set_canvas_work_area, KSI_CALL_ARG2, 2},
  {"tk-get-canvas-view-offset",ksi_tk_get_canvas_view_off, KSI_CALL_ARG1, 1},
  {"tk-set-canvas-view-offset",ksi_tk_set_canvas_view_off, KSI_CALL_ARG3, 2},
  {"tk-get-canvas-view-zoom", ksi_tk_get_canvas_view_zoom, KSI_CALL_ARG1, 1},
  {"tk-set-canvas-view-zoom", ksi_tk_set_canvas_view_zoom, KSI_CALL_ARG2, 2},
  {"tk-get-canvas-view-area", ksi_tk_get_canvas_view_area, KSI_CALL_ARG1, 1},
  {"tk-get-canvas-dc",	      ksi_tk_get_canvas_dc,	   KSI_CALL_ARG1, 1},
  {"tk-canvas-work-x",        ksi_tk_canvas_work_x,	   KSI_CALL_ARG2, 2},
  {"tk-canvas-work-y",        ksi_tk_canvas_work_y,	   KSI_CALL_ARG2, 2},
  {"tk-canvas-draw-x",        ksi_tk_canvas_draw_x,	   KSI_CALL_ARG2, 2},
  {"tk-canvas-draw-y",        ksi_tk_canvas_draw_y,	   KSI_CALL_ARG2, 2},

  { 0 },
};

void
ksi_tk_init_canvas (void)
{
  ignore (rcsid);
  ksi_reg_unit (defs);

  joke_tk->canvas_scroll_event = ksi_defun ("tk-canvas-scroll-event",
					    ksi_tk_canvas_scroll_event,
					    KSI_CALL_ARG2, 2);
}


 /* End of code */
