/*
 * x11_colormap.c
 * 
 *
 * Copyright (C) 1999-2000, Ivan Demakov.
 *
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose, without fee, and without a written
 * agreement is hereby granted, provided that the above copyright notice
 * and this paragraph and the following two paragraphs appear in all copies.
 * Modifications to this software may be copyrighted by their authors
 * and need not follow the licensing terms described here, provided that
 * the new terms are clearly indicated on the first page of each file where
 * they apply.
 *
 * IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY
 * FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES,
 * INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS
 * DOCUMENTATION, EVEN IF THE AUTHORS HAS BEEN ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIMS ANY WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS
 * ON AN "AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAS NO OBLIGATIONS
 * TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 *
 *
 * Author:        Ivan Demakov <demakov@users.sourceforge.net>
 * Creation date: Fri Apr  9 20:56:29 1999
 * Last Update:   Tue Mar 21 19:30:08 2000
 *
 */

#include "toolkit.h"

static char rcsid[] = "$Id: x11_colormap.c,v 1.5 2000/04/15 14:03:16 ivan Exp $";


/*
 * Pixeltab -- Hash table that maps RGB value to pixel value
 *
 */

static pixeltab*
new_pixeltab (void)
{
  pixeltab *tab = KSI_NEW_TYPED (pixeltab, pixeltab, pixeltab);
  tab->pixeltab = (pixelrec**) ksi_malloc (113 * sizeof (pixelrec*));
  tab->tabsize = 113;
  tab->count = 0;
  return tab;
}

#define hash_pixel(r,g,b) (((r) << 16) ^ ((g) << 8) ^ ((b) >> 8))

static ksi_pixel*
lookup_pixel (pixeltab *tab, ksi_color red, ksi_color green, ksi_color blue)
{
  pixelrec* rec;
  size_t index = hash_pixel (red, green, blue) % tab->tabsize;

  for (rec = tab->pixeltab [index]; rec; rec = rec->next)
    {
      if (rec->red == red && rec->green == green && rec->blue == blue)
	return &rec->pixel;
    }

  return 0;
}

static void
append_pixel (pixeltab *tab, ksi_pixel pixel,
	      ksi_color red, ksi_color green, ksi_color blue)
{
  pixelrec *rec;
  size_t index = hash_pixel (red, green, blue) % tab->tabsize;

  rec = KSI_NEW_TYPED (pixelrec, next, next);
  rec->pixel = pixel;
  rec->red   = red;
  rec->green = green;
  rec->blue  = blue;

  rec->next = tab->pixeltab [index];
  tab->pixeltab [index] = rec;

  tab->count += 1;
}

static int
remove_pixel (pixeltab *tab, ksi_pixel pixel)
{
  size_t	i;
  pixelrec	*curr, **prev;

  for (i = 0; i < tab->tabsize; ++i)
    {
      for (curr = tab->pixeltab [i], prev = &tab->pixeltab[i];
	   curr != 0;
	   prev = &curr->next, curr = curr->next)
	{
	  if (curr->pixel == pixel)
	    {
	      *prev = curr->next;
	      tab->count -= 1;
	      return 1;
	    }
	}
    }

  return 0;
}

#if 0
static ksi_pixel*
get_pixels (pixeltab *tab)
{
  ksi_pixel	*pixels;
  size_t	i, num;

  if (tab->count == 0)
    return 0;

  pixels = ((ksi_pixel*) ksi_malloc)data (tab->count * sizeof (ksi_pixel)));

  for (num = 0, i = 0; i < tab->tabsize; ++i)
    {
      register pixelrec* rec;
      for (rec = tab->pixeltab [i]; rec; rec = rec->next)
	pixels[num++] = rec->pixel;
    }

  return pixels;
}
#endif


/*
 *  closest_color --
 *
 *	Find and allocate the closest available color
 *
 *  Results:
 *	return allocated color, or -1 if can't allocate color
 */

static ksi_pixel
closest_color (ksi_colormap dc, ksi_color red, ksi_color green, ksi_color blue)
{
  if (dc->colors == 0)
    {
      int num, i;
      XVisualInfo temp, *info_ptr;

      temp.visualid = XVisualIDFromVisual (dc->visual);
      info_ptr = XGetVisualInfo (dc->dpy, VisualIDMask, &temp, &num);
      if (num >= 1)
	{
	  dc->num_colors = info_ptr -> colormap_size;
	  XFree (info_ptr);
	  dc->colors = (XColor*) ksi_malloc)data (dc->num_colors
						     * sizeof (XColor));
	  for (i = 0; i < dc->num_colors; ++i)
	    dc->colors[i].pixel = i;

	  XQueryColors (dc->dpy, dc->cmap, dc->colors, dc->num_colors);
	}
    }

  while (1)
    {
      double	tmp, dst, closest_dst = 1e30;
      size_t	i, closest = 0;
      ksi_pixel	*pixel;

      if (dc->num_colors == 0)
	break;

      for (i = 0; i < dc->num_colors; ++i)
	{
	  tmp = 0.30 * (red - dc->colors[i].red);
	  dst = tmp * tmp;
	  tmp = 0.61 * (green - dc->colors[i].green);
	  dst += tmp * tmp;
	  tmp = 0.11 * (blue - dc->colors[i].blue);
	  dst += tmp * tmp;
	  if (dst < closest_dst)
	    {
	      closest = i;
	      closest_dst = dst;
	    }
	}

      /* If color already allocated, return it. Else try allocate color.
       * If fails, it must mean that the color was read-write
       * or it was already freed, then try again.
       */

      pixel = lookup_pixel (dc->pixtab,
			    dc->colors[closest].red,
			    dc->colors[closest].green,
			    dc->colors[closest].blue);
      if (pixel)
	return *pixel;

      if (XAllocColor (dc->dpy, dc->cmap, &dc->colors[closest]))
	{
	  append_pixel (dc->pixtab,
			dc->colors[closest].pixel,
			dc->colors[closest].red,
			dc->colors[closest].green,
			dc->colors[closest].blue);

	  return dc->colors[closest].pixel;
	}

      /* Couldn't allocate color. Remove it from the table and
       * look for the next color.
       */

      dc->colors[closest] = dc->colors[dc->num_colors - 1];
      dc->num_colors -= 1;
    }

  return (ksi_pixel) -1;
}

int
ksi_parse_color (ksi_colormap dc, const char *name,
		 ksi_color* r, ksi_color* g, ksi_color* b)
{
  XColor exact, color;

  if (*name != '#' && strchr (name, ':') == 0)
    {
      if (!ksi_lookup_color (name, &color.red, &color.green, &color.blue))
	{
  	  if (XLookupColor (dc->dpy, dc->cmap, name, &exact, &color))
	    ksi_append_color (name, color.red, color.green, color.blue);
	  else
	    return 0;
	}
    }
  else if (!XParseColor (dc->dpy, dc->cmap, name, &color))
    return 0;

  if (r) *r = color.red;
  if (g) *g = color.green;
  if (b) *b = color.blue;
  return 1;
}

ksi_pixel
ksi_alloc_pixel (ksi_colormap dc, ksi_color r, ksi_color g, ksi_color b)
{
  pixeltab	*tab;
  ksi_pixel	*pixptr;
  XColor	color;

  color.flags = DoRed | DoGreen | DoBlue;
  color.red   = r;
  color.green = g;
  color.blue  = b;

  tab = dc->pixtab;
  if (tab == 0)
    {
      tab = new_pixeltab ();
      dc->pixtab = tab;
    }

  pixptr = lookup_pixel (tab, color.red, color.green, color.blue);
  if (pixptr)
    return *pixptr;

  if (!XAllocColor (dc->dpy, dc->cmap, &color))
    return closest_color (dc, color.red, color.green, color.blue);
  else
    {
      if (dc->colors)
	{
	  ksi_free (dc->colors);
	  dc->colors = 0;
	  dc->num_colors = 0;
	}

      append_pixel (tab, color.pixel, color.red, color.green, color.blue);
    }

  return color.pixel;
}

void
ksi_free_pixel (ksi_colormap dc, ksi_pixel pixel)
{
  if (dc->visual->class != StaticGray && dc->visual->class != StaticColor
      && pixel != BlackPixelOfScreen (dc->screen)
      && pixel != WhitePixelOfScreen (dc->screen))
    {
      if (dc->pixtab && remove_pixel (dc->pixtab, pixel))
	XFreeColors (dc->dpy, dc->cmap, &pixel, 1, 0);

      if (dc->colors)
	{
	  ksi_free (dc->colors);
	  dc->colors = 0;
	  dc->num_colors = 0;
	}
    }
}


ksi_colormap
ksi_get_window_colormap (Window wnd)
{
  XWindowAttributes attr;
  Colormap cmap;
  ksi_colormap curr;

  XGetWindowAttributes (joke_dpy, wnd, &attr);
  if (attr.colormap == None)
    cmap = DefaultColormapOfScreen (attr.screen);
  else
    cmap = attr.colormap;

  for (curr = ksi_joke->cmap_list; curr; curr = curr->next)
    if (curr->cmap == cmap)
      return curr;

  curr = (ksi_colormap) ksi_malloc (sizeof *curr);
  curr->next = ksi_joke->cmap_list;
  curr->dpy = joke_dpy;
  curr->cmap = cmap;
  curr->screen = attr.screen;
  curr->visual = attr.visual;

  ksi_joke->cmap_list = curr;
  return curr;
}


void
ksi_init_colormap ()
{
  ignore (rcsid);
}

 /* End of code */
