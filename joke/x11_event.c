/*
 * x11_event.c
 * 
 *
 * Copyright (C) 1999-2000, Ivan Demakov.
 *
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose, without fee, and without a written
 * agreement is hereby granted, provided that the above copyright notice
 * and this paragraph and the following two paragraphs appear in all copies.
 * Modifications to this software may be copyrighted by their authors
 * and need not follow the licensing terms described here, provided that
 * the new terms are clearly indicated on the first page of each file where
 * they apply.
 *
 * IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY
 * FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES,
 * INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS
 * DOCUMENTATION, EVEN IF THE AUTHORS HAS BEEN ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIMS ANY WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS
 * ON AN "AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAS NO OBLIGATIONS
 * TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 *
 *
 * Author:        Ivan Demakov <demakov@users.sourceforge.net>
 * Creation date: Mon Apr 19 01:16:42 1999
 * Last Update:   Tue Mar 21 19:30:22 2000
 *
 */

#include <X11/keysym.h>
#include "toolkit.h"

static char rcsid[] = "$Id: x11_event.c,v 1.15 2000/04/15 14:03:17 ivan Exp $";


static ksi_obj
make_state (unsigned int state)
{
  ksi_obj lst = ksi_nil;
  if (state & ShiftMask)
    lst = ksi_cons (ksi_str02sym ("shift"), lst);
  if (state & LockMask)
    lst = ksi_cons (ksi_str02sym ("lock"), lst);
  if (state & ControlMask)
    lst = ksi_cons (ksi_str02sym ("control"), lst);
  if (state & Mod1Mask)
    lst = ksi_cons (ksi_str02sym ("mod1"), lst);
  if (state & Mod2Mask)
    lst = ksi_cons (ksi_str02sym ("mod2"), lst);
  if (state & Mod3Mask)
    lst = ksi_cons (ksi_str02sym ("mod3"), lst);
  if (state & Mod4Mask)
    lst = ksi_cons (ksi_str02sym ("mod4"), lst);
  if (state & Mod5Mask)
    lst = ksi_cons (ksi_str02sym ("mod5"), lst);

  if (state & Button1Mask)
    lst = ksi_cons (ksi_str02sym ("button1"), lst);
  if (state & Button2Mask)
    lst = ksi_cons (ksi_str02sym ("button2"), lst);
  if (state & Button3Mask)
    lst = ksi_cons (ksi_str02sym ("button3"), lst);
  if (state & Button4Mask)
    lst = ksi_cons (ksi_str02sym ("button4"), lst);
  if (state & Button5Mask)
    lst = ksi_cons (ksi_str02sym ("button5"), lst);

  return lst;
}

static ksi_obj
make_key (XKeyEvent* xe)
{
  char buf[4];
  KeySym key;

  if (XLookupString (xe, buf, sizeof buf, &key, NULL) == 1)
    return KSI_MK_CHAR (buf[0]);

  switch (key)
    {
    case XK_KP_Home:
    case XK_Home:	return ksi_str02sym ("home");
    case XK_KP_Left:
    case XK_Left:	return ksi_str02sym ("left");
    case XK_KP_Up:
    case XK_Up:		return ksi_str02sym ("up");
    case XK_KP_Right:
    case XK_Right:	return ksi_str02sym ("right");
    case XK_KP_Down:
    case XK_Down:	return ksi_str02sym ("down");
    case XK_KP_Prior:
    case XK_Prior:	return ksi_str02sym ("prior");
    case XK_KP_Next:
    case XK_Next:	return ksi_str02sym ("next");
    case XK_KP_End:
    case XK_End:	return ksi_str02sym ("end");
    case XK_KP_Begin:
    case XK_Begin:	return ksi_str02sym ("begin");
    case XK_KP_Delete:
    case XK_Delete:	return ksi_str02sym ("delete");
    case XK_KP_Insert:
    case XK_Insert:	return ksi_str02sym ("insert");

    case XK_F1:		return ksi_str02sym ("f1");
    case XK_F2:		return ksi_str02sym ("f2");
    case XK_F3:		return ksi_str02sym ("f3");
    case XK_F4:		return ksi_str02sym ("f4");
    case XK_F5:		return ksi_str02sym ("f5");
    case XK_F6:		return ksi_str02sym ("f6");
    case XK_F7:		return ksi_str02sym ("f7");
    case XK_F8:		return ksi_str02sym ("f8");
    case XK_F9:		return ksi_str02sym ("f9");
    case XK_F10:	return ksi_str02sym ("f10");
    case XK_F11:	return ksi_str02sym ("f11");
    case XK_F12:	return ksi_str02sym ("f12");
    case XK_F13:	return ksi_str02sym ("f13");
    case XK_F14:	return ksi_str02sym ("f14");
    case XK_F15:	return ksi_str02sym ("f15");
    case XK_F16:	return ksi_str02sym ("f16");
    case XK_F17:	return ksi_str02sym ("f17");
    case XK_F18:	return ksi_str02sym ("f18");
    case XK_F19:	return ksi_str02sym ("f19");
    case XK_F20:	return ksi_str02sym ("f20");
    }

  return ksi_uint2num (key);
}

static ksi_obj
atom2obj (Atom a)
{
  ksi_obj x;
  char *name;

  if (a == joke_tk->atom_wm_protocols)
    return joke_tk->sym_wm_protocols;
  if (a == joke_tk->atom_wm_delete_window)
    return joke_tk->sym_wm_delete_window;

  name = XGetAtomName (joke_dpy, a);
  x = (name ? ksi_str02sym (name) : ksi_false);
  XFree (name);

  return x;
}

static ksi_obj
wid2wnd (Window wid)
{
  ksi_window wnd = ksi_lookup_window (joke_wintab, wid);
  return wnd ? (ksi_obj) wnd : ksi_uint2num (wid);
}


ksi_obj
ksi_make_x11_event (XEvent* xe)
{
  ksi_obj vec = ksi_false;

  switch (xe->type)
    {
    case ButtonPress:
      vec = ksi_make_vector (ksi_int2num (6), 0);
      KSI_VEC_REF (vec, 0) = joke_tk->sym_button_press;
      KSI_VEC_REF (vec, 1) = wid2wnd (xe->xbutton.window);
      KSI_VEC_REF (vec, 2) = ksi_int2num (xe->xbutton.x);
      KSI_VEC_REF (vec, 3) = ksi_int2num (xe->xbutton.y);
      KSI_VEC_REF (vec, 4) = make_state (xe->xbutton.state);
      KSI_VEC_REF (vec, 5) = ksi_int2num (xe->xbutton.button);
      break;

    case ButtonRelease:
      vec = ksi_make_vector (ksi_int2num (6), 0);
      KSI_VEC_REF (vec, 0) = joke_tk->sym_button_release;
      KSI_VEC_REF (vec, 1) = wid2wnd (xe->xbutton.window);
      KSI_VEC_REF (vec, 2) = ksi_int2num (xe->xbutton.x);
      KSI_VEC_REF (vec, 3) = ksi_int2num (xe->xbutton.y);
      KSI_VEC_REF (vec, 4) = make_state (xe->xbutton.state);
      KSI_VEC_REF (vec, 5) = ksi_int2num (xe->xbutton.button);
      break;

    case MotionNotify:
      vec = ksi_make_vector (ksi_int2num (6), 0);
      KSI_VEC_REF (vec, 0) = joke_tk->sym_motion;
      KSI_VEC_REF (vec, 1) = wid2wnd (xe->xmotion.window);
      KSI_VEC_REF (vec, 2) = ksi_int2num (xe->xmotion.x);
      KSI_VEC_REF (vec, 3) = ksi_int2num (xe->xmotion.y);
      KSI_VEC_REF (vec, 4) = make_state (xe->xmotion.state);
      if (xe->xmotion.state & Button1Mask)
	KSI_VEC_REF (vec, 5) = ksi_int2num (1);
      else if (xe->xmotion.state & Button2Mask)
	KSI_VEC_REF (vec, 5) = ksi_int2num (2);
      else if (xe->xmotion.state & Button3Mask)
	KSI_VEC_REF (vec, 5) = ksi_int2num (3);
      else if (xe->xmotion.state & Button4Mask)
	KSI_VEC_REF (vec, 5) = ksi_int2num (4);
      else if (xe->xmotion.state & Button5Mask)
	KSI_VEC_REF (vec, 5) = ksi_int2num (5);
      else
	KSI_VEC_REF (vec, 5) = ksi_int2num(0);
      break;

    case KeyPress:
      vec = ksi_make_vector (ksi_int2num (6), 0);
      KSI_VEC_REF (vec, 0) = joke_tk->sym_key_press;
      KSI_VEC_REF (vec, 1) = wid2wnd (xe->xkey.window);
      KSI_VEC_REF (vec, 2) = ksi_int2num (xe->xkey.x);
      KSI_VEC_REF (vec, 3) = ksi_int2num (xe->xkey.y);
      KSI_VEC_REF (vec, 4) = make_state (xe->xkey.state);
      KSI_VEC_REF (vec, 5) = make_key (&xe->xkey);
      break;

    case KeyRelease:
      vec = ksi_make_vector (ksi_int2num (6), 0);
      KSI_VEC_REF (vec, 0) = joke_tk->sym_key_release;
      KSI_VEC_REF (vec, 1) = wid2wnd (xe->xkey.window);
      KSI_VEC_REF (vec, 2) = ksi_int2num (xe->xkey.x);
      KSI_VEC_REF (vec, 3) = ksi_int2num (xe->xkey.y);
      KSI_VEC_REF (vec, 4) = make_state (xe->xkey.state);
      KSI_VEC_REF (vec, 5) = make_key (&xe->xkey);
      break;

    case Expose:
      vec = ksi_make_vector (ksi_int2num (7), 0);
      KSI_VEC_REF (vec, 0) = joke_tk->sym_expose;
      KSI_VEC_REF (vec, 1) = wid2wnd (xe->xexpose.window);
      KSI_VEC_REF (vec, 2) = ksi_int2num (xe->xexpose.x);
      KSI_VEC_REF (vec, 3) = ksi_int2num (xe->xexpose.y);
      KSI_VEC_REF (vec, 4) = ksi_int2num (xe->xexpose.width);
      KSI_VEC_REF (vec, 5) = ksi_int2num (xe->xexpose.height);
      KSI_VEC_REF (vec, 6) = ksi_int2num (xe->xexpose.count);
      break;

    case ConfigureNotify:
      vec = ksi_make_vector (ksi_int2num (7), 0);
      KSI_VEC_REF (vec, 0) = joke_tk->sym_configure_notify;
      KSI_VEC_REF (vec, 1) = wid2wnd (xe->xconfigure.event);
      KSI_VEC_REF (vec, 2) = wid2wnd (xe->xconfigure.window);
      KSI_VEC_REF (vec, 3) = ksi_int2num (xe->xconfigure.x);
      KSI_VEC_REF (vec, 4) = ksi_int2num (xe->xconfigure.y);
      KSI_VEC_REF (vec, 5) = ksi_int2num (xe->xconfigure.width);
      KSI_VEC_REF (vec, 6) = ksi_int2num (xe->xconfigure.height);
      break;

    case DestroyNotify:
      vec = ksi_make_vector (ksi_int2num (3), 0);
      KSI_VEC_REF (vec, 0) = joke_tk->sym_destroy_notify;
      KSI_VEC_REF (vec, 1) = wid2wnd (xe->xdestroywindow.event);
      KSI_VEC_REF (vec, 2) = wid2wnd (xe->xdestroywindow.window);
      break;

    case MapNotify:
      vec = ksi_make_vector (ksi_int2num (3), 0);
      KSI_VEC_REF (vec, 0) = joke_tk->sym_map_notify;
      KSI_VEC_REF (vec, 1) = wid2wnd (xe->xmap.event);
      KSI_VEC_REF (vec, 2) = wid2wnd (xe->xmap.window);
      break;

    case UnmapNotify:
      vec = ksi_make_vector (ksi_int2num (3), 0);
      KSI_VEC_REF (vec, 0) = joke_tk->sym_unmap_notify;
      KSI_VEC_REF (vec, 1) = wid2wnd (xe->xunmap.event);
      KSI_VEC_REF (vec, 2) = wid2wnd (xe->xunmap.window);
      break;

    case ClientMessage:
      vec = ksi_make_vector (ksi_int2num (4), 0);
      KSI_VEC_REF (vec, 0) = joke_tk->sym_client_message;
      KSI_VEC_REF (vec, 1) = wid2wnd (xe->xclient.window);
      KSI_VEC_REF (vec, 2) = atom2obj (xe->xclient.message_type);
      KSI_VEC_REF (vec, 3) = atom2obj (xe->xclient.data.l[0]);
      break;
    }

  return vec;
}


ksi_obj
ksi_tk_event_loop (ksi_obj end_proc)
{
  XEvent		xe;
  Window		wid;
  ksi_window		w;

  joke_tk->no_idle_work = 0;
  while (1)
    {
      joke_tk->curr_time = ksi_real_time ();

      if (end_proc)
	{
	  /* ������������ �������� end_proc */
	  joke_tk->no_idle_work = 0;
	  if (ksi_apply_0 (end_proc) == ksi_false)
	    break;
	}

      /* ���� ������ ��� ���� -- ���� ������������ */
      if (joke_wintab->count == 0)
	break;

      if (joke_tk->no_idle_work == 0
	  && XEventsQueued (joke_dpy, QueuedAfterFlush) <= 0
	  && ksi_tk_call_timers () == 0
	  && ksi_gcollect (0) == 0)
	{
	  if (joke_tk->timers == 0)
	    joke_tk->no_idle_work = 1;
	}
      else
	{
	  joke_tk->no_idle_work = 0;

	  XNextEvent (joke_dpy, &xe);
	  wid = xe.xany.window;

	  w = ksi_lookup_window (joke_wintab, wid);
	  if (w && w->tk_data)
	    {
	      if (w->tk_data->proc)
		w->tk_data->proc (w, &xe, w->tk_data->proc_data);
	    }

	  if (xe.xany.type == DestroyNotify)
	    {
	      w = ksi_lookup_window (joke_wintab, xe.xdestroywindow.window);
	      ksi_tk_destroy_window (w);
	    }
	}
    }

  joke_tk->no_idle_work = 0;
  return ksi_false;
}


static struct Ksi_Prim_Def defs [] =
{
  { "tk-event-loop",		ksi_tk_event_loop,	KSI_CALL_ARG1, 0 },

  { 0 },
};

void
ksi_tk_init_events ()
{
  ignore (rcsid);
  ksi_reg_unit (defs);
}

 /* End of code */
