/*
 * x11_scroll.c
 * 
 *
 * Copyright (C) 1999-2000, Ivan Demakov.
 *
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose, without fee, and without a written
 * agreement is hereby granted, provided that the above copyright notice
 * and this paragraph and the following two paragraphs appear in all copies.
 * Modifications to this software may be copyrighted by their authors
 * and need not follow the licensing terms described here, provided that
 * the new terms are clearly indicated on the first page of each file where
 * they apply.
 *
 * IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY
 * FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES,
 * INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS
 * DOCUMENTATION, EVEN IF THE AUTHORS HAS BEEN ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIMS ANY WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS
 * ON AN "AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAS NO OBLIGATIONS
 * TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 *
 *
 * Author:        Ivan Demakov <demakov@users.sourceforge.net>
 * Creation date: Wed Apr 28 02:28:48 1999
 * Last Update:   Tue Mar 21 19:30:43 2000
 *
 */

#include "toolkit.h"

static char rcsid[] = "$Id: x11_scroll.c,v 1.7 2000/05/11 18:10:37 ivan Exp $";


#define INSET		2
#define MIN_LEN		(2 * INSET + 2)

#define OUTSIDE		0
#define TOP_ARROW	1
#define TOP_GAP		2
#define SLIDER		3
#define BOTTOM_GAP	4
#define BOTTOM_ARROW	5


static void
draw_scroll (ksi_window wnd)
{
  scroll_info info = (scroll_info) wnd->tk_data;
  XPoint points[4];
  int beg, end, size, coord_beg, coord_end;

  ksi_tk_fill_3d_rectangle (wnd->wid, joke_tk->scroll_back,
			    0, 0, info->base.w, info->base.h,
			    INSET, RELIEF_SUNKEN);

  /* draw up or left arrow */
  if (info->vertical)
    {
      points[0].x = INSET - 1;
      points[0].y = joke_tk->scroll_arrow_size - 1;
      points[1].x = info->base.w - INSET;
      points[1].y = points[0].y;
      points[2].x = info->base.w / 2;
      points[2].y = INSET - 1;
      beg = points[0].y + 1;
    }
  else
    {
      points[0].x = joke_tk->scroll_arrow_size - 1;
      points[0].y = INSET - 1;
      points[1].x = INSET - 1;
      points[1].y = info->base.h / 2;
      points[2].x = points[0].x;
      points[2].y = info->base.h - INSET;
      beg = points[0].x + 1;
    }

  ksi_tk_draw_3d_polygon (wnd->wid, joke_tk->scroll_back, points, 3,
			  INSET,
			  (info->active_part == TOP_ARROW
			   ? RELIEF_SUNKEN
			   : RELIEF_RAISED));


  /* draw down or right arrow */
  if (info->vertical)
    {
      points[0].x = INSET;
      points[0].y = info->base.h - joke_tk->scroll_arrow_size + 1;
      points[1].x = info->base.w / 2;
      points[1].y = info->base.h - INSET;
      points[2].x = info->base.w - INSET;
      points[2].y = points[0].y;
      end = points[0].y;
    }
  else
    {
      points[0].x = info->base.w - joke_tk->scroll_arrow_size + 1;
      points[0].y = INSET - 1;
      points[1].x = points[0].x;
      points[1].y = info->base.h - INSET;
      points[2].x = info->base.w - INSET;
      points[2].y = info->base.h / 2;
      end = points[0].x;
    }

  ksi_tk_draw_3d_polygon (wnd->wid, joke_tk->scroll_back, points, 3,
			  INSET,
			  (info->active_part == BOTTOM_ARROW
			   ? RELIEF_SUNKEN
			   : RELIEF_RAISED));

  /* draw slider */
  size = end - beg;
  if (size < 0)
    size = 0;

  coord_beg = beg + ((int) (info->beg * size + 0.5));
  coord_end = beg + ((int) (info->end * size + 0.5));

  if (coord_beg < beg)
    coord_beg = beg;
  if (coord_beg + MIN_LEN > coord_end)
    coord_end = coord_beg + MIN_LEN;
  if (coord_end > end)
    coord_end = end;
  if (coord_end < coord_beg)
    coord_end = coord_beg;

  if (info->active_part == SLIDER)
    {
      size = coord_end - coord_beg;
      coord_beg += info->move_cur - info->move_beg;
      if (coord_beg < beg)
	coord_beg = beg;
      if (coord_beg + size > end)
	coord_beg = end - size;
      coord_end = coord_beg + size;
    }

  if (info->vertical)
    {
      ksi_tk_fill_3d_rectangle(wnd->wid, joke_tk->scroll_back,
			       INSET, coord_beg,
			       info->base.w - 2 * INSET, coord_end - coord_beg,
			       INSET, RELIEF_RAISED);
    }
  else
    {
      ksi_tk_fill_3d_rectangle(wnd->wid, joke_tk->scroll_back,
			       coord_beg, INSET,
			       coord_end - coord_beg, info->base.h - 2 * INSET,
			       INSET, RELIEF_RAISED);
    }
}


static int
scroll_part (ksi_window wnd, int x, int y)
{
  scroll_info info = (scroll_info) wnd->tk_data;
  int beg, end, size, coord_beg, coord_end, coord;

  if (info->vertical)
    {
      beg = joke_tk->scroll_arrow_size;
      end = info->base.h - joke_tk->scroll_arrow_size + 1;
      coord = y;
    }
  else
    {
      beg = joke_tk->scroll_arrow_size;
      end = info->base.w - joke_tk->scroll_arrow_size + 1;
      coord = x;
    }

  if (coord < beg)
    return TOP_ARROW;
  if (coord >= end)
    return BOTTOM_ARROW;

  size = end - beg;
  if (size < 0)
    size = 0;

  coord_beg = beg + ((int) (info->beg * size + 0.5));
  coord_end = beg + ((int) (info->end * size + 0.5));

  if (coord_beg < beg)
    coord_beg = beg;
  if (coord_beg + MIN_LEN > coord_end)
    coord_end = coord_beg + MIN_LEN;
  if (coord_end > end)
    coord_end = end;
  if (coord_end < coord_beg)
    coord_end = coord_beg;

  if (coord < coord_beg)
    return TOP_GAP;
  if (coord >= coord_end)
    return BOTTOM_GAP;

  return SLIDER;
}


static void
do_scroll (ksi_window wnd, void *data)
{
  ksi_obj v;
  scroll_info info = (scroll_info) wnd->tk_data;

  v = ksi_make_vector (ksi_int2num (4), 0);
  KSI_VEC_REF (v, 0) = joke_tk->sym_scroll;
  KSI_VEC_REF (v, 1) = (ksi_obj) data;
  KSI_VEC_REF (v, 2) = (ksi_obj) wnd;

  if (info->active_part == TOP_ARROW)
    {
      if (info->vertical)
	KSI_VEC_REF (v, 3) = joke_tk->sym_line_up;
      else
	KSI_VEC_REF (v, 3) = joke_tk->sym_line_left;
    }
  else if (info->active_part == BOTTOM_ARROW)
    {
      if (info->vertical)
	KSI_VEC_REF (v, 3) = joke_tk->sym_line_down;
      else
	KSI_VEC_REF (v, 3) = joke_tk->sym_line_right;
    }
  else if (info->active_part == TOP_GAP)
    {
      if (info->vertical)
	KSI_VEC_REF (v, 3) = joke_tk->sym_page_up;
      else
	KSI_VEC_REF (v, 3) = joke_tk->sym_page_left;
    }
  else if (info->active_part == BOTTOM_GAP)
    {
      if (info->vertical)
	KSI_VEC_REF (v, 3) = joke_tk->sym_page_down;
      else
	KSI_VEC_REF (v, 3) = joke_tk->sym_page_right;
    }

  ksi_apply_window_event ((ksi_obj) data, v);
}

static void
scroll_auto (ksi_window wnd, void *data)
{
  do_scroll (wnd, data);
  ksi_tk_add_timer (0.100, scroll_auto, wnd, data);
}

static void
move (ksi_window wnd, void *data, int x, int y)
{
  ksi_obj	v;
  int		beg, end, cur;
  double	pos;
  scroll_info	info = (scroll_info) wnd->tk_data;

  if (info->active_part == SLIDER)
    {
      cur = (info->vertical ? y : x);
      if (info->move_cur != cur)
	{
	  info->move_cur = cur;

	  v = ksi_make_vector (ksi_int2num (6), 0);
	  KSI_VEC_REF (v, 0) = joke_tk->sym_scroll;
	  KSI_VEC_REF (v, 1) = (ksi_obj) data;
	  KSI_VEC_REF (v, 2) = (ksi_obj) wnd;
	  KSI_VEC_REF (v, 3) = joke_tk->sym_move;

	  if (info->vertical)
	    {
	      beg = joke_tk->scroll_arrow_size;
	      end = info->base.h - joke_tk->scroll_arrow_size + 1;
	      pos = ((double) info->move_cur - info->move_beg) / (end - beg);

	      KSI_VEC_REF (v, 4) = ksi_int2num(0);
	      KSI_VEC_REF (v, 5) = ksi_double2num (pos);
	    }
	  else
	    {
	      beg = joke_tk->scroll_arrow_size;
	      end = info->base.w - joke_tk->scroll_arrow_size + 1;
	      pos = ((double) info->move_cur - info->move_beg) / (end - beg);

	      KSI_VEC_REF (v, 4) = ksi_double2num (pos);
	      KSI_VEC_REF (v, 5) = ksi_int2num(0);
	    }

	  ksi_apply_window_event ((ksi_obj) data, v);

	  if (!info->updated)
	    draw_scroll (wnd);
	}
    }
}

static void
press (ksi_window wnd, void *data, int x, int y, int button)
{
  scroll_info info = (scroll_info) wnd->tk_data;
  ksi_obj v;

  if (button != 1 && button != 2)
    return;

  info->active_part = scroll_part (wnd, x, y);
  info->updated = 0;

  if (button == 2
      && info->active_part != TOP_ARROW && info->active_part != BOTTOM_ARROW)
    {
      int	beg, end;
      double	pos;

      pos = (info->beg + info->end) / 2;
      if (info->vertical)
	{
	  beg = joke_tk->scroll_arrow_size;
	  end = info->base.h - joke_tk->scroll_arrow_size + 1;
	}
      else
	{
	  beg = joke_tk->scroll_arrow_size;
	  end = info->base.w - joke_tk->scroll_arrow_size + 1;
	}

      info->active_part = SLIDER;
      info->move_beg = info->move_cur = (pos * (end - beg) + beg);

      v = ksi_make_vector (ksi_int2num (4), 0);
      KSI_VEC_REF (v, 0) = joke_tk->sym_scroll;
      KSI_VEC_REF (v, 1) = (ksi_obj) data;
      KSI_VEC_REF (v, 2) = (ksi_obj) wnd;
      KSI_VEC_REF (v, 3) = joke_tk->sym_start_move;

      ksi_apply_window_event ((ksi_obj) data, v);

      move (wnd, data, x, y);
    }
  else if (info->active_part == SLIDER)
    {
      if (info->vertical)
	info->move_beg = info->move_cur = y;
      else
	info->move_beg = info->move_cur = x;

      v = ksi_make_vector (ksi_int2num (4), 0);
      KSI_VEC_REF (v, 0) = joke_tk->sym_scroll;
      KSI_VEC_REF (v, 1) = (ksi_obj) data;
      KSI_VEC_REF (v, 2) = (ksi_obj) wnd;
      KSI_VEC_REF (v, 3) = joke_tk->sym_start_move;

      ksi_apply_window_event ((ksi_obj) data, v);
    }
  else
    {
      do_scroll (wnd, data);
      if (!info->updated)
	draw_scroll (wnd);

      ksi_tk_add_timer (0.300, scroll_auto, wnd, data);
    }
}

static void
release (ksi_window wnd, void *data, int x, int y, int button)
{
  scroll_info info = (scroll_info) wnd->tk_data;

  if (info->active_part == SLIDER)
    {
      ksi_obj v;

      info->active_part = OUTSIDE;
      info->updated = 0;

      v = ksi_make_vector (ksi_int2num (4), 0);
      KSI_VEC_REF (v, 0) = joke_tk->sym_scroll;
      KSI_VEC_REF (v, 1) = (ksi_obj) data;
      KSI_VEC_REF (v, 2) = (ksi_obj) wnd;
      KSI_VEC_REF (v, 3) = joke_tk->sym_stop_move;

      ksi_apply_window_event ((ksi_obj) data, v);

      if (!info->updated)
	draw_scroll (wnd);
    }
  else if (info->active_part != OUTSIDE)
    {
      info->active_part = OUTSIDE;
      draw_scroll (wnd);

      ksi_tk_del_timer (scroll_auto, wnd, data);
    }
}

static void
scroll_window_event_proc (ksi_window wnd, XEvent *xe, void *data)
{
  ksi_obj ek;

  switch (xe->type)
    {
    case MapNotify:
      wnd->tk_data->mapped = 1;
      break;

    case UnmapNotify:
      wnd->tk_data->mapped = 0;
      break;

    case ConfigureNotify:
      wnd->tk_data->x = xe->xconfigure.x;
      wnd->tk_data->y = xe->xconfigure.y;
      wnd->tk_data->w = xe->xconfigure.width;
      wnd->tk_data->h = xe->xconfigure.height;
      break;

    case Expose:
      if (xe->xexpose.count == 0)
	draw_scroll (wnd);
      break;

    case ButtonPress:
      press (wnd, data, xe->xbutton.x, xe->xbutton.y, xe->xbutton.button);
      break;

    case ButtonRelease:
      release (wnd, data, xe->xbutton.x, xe->xbutton.y, xe->xbutton.button);
      break;

    case MotionNotify:
      move (wnd, data, xe->xmotion.x, xe->xmotion.y);
      break;
    }

  ek = ksi_make_x11_event (xe);
  if (ek != ksi_false)
    ksi_apply_window_event ((ksi_obj) wnd, ek);
}

ksi_window
ksi_tk_create_scrollbar (ksi_window top, ksi_obj args)
{
  Window wid;
  XSetWindowAttributes attr;
  ksi_window wnd, event_wnd;
  scroll_info info;
  ksi_obj val;
  int vertical;
  double beg, end;

  KSI_CHECK ((ksi_obj) top, SUPERWIN_P (top),
	     "tk-create-scrollbar: invalid parent window in arg1");

  if (!args)
    args = ksi_nil;

  val = ksi_get_arg (joke_tk->key_orient, args, joke_tk->key_vertical);
  if (val == joke_tk->key_vertical)
    vertical = 1;
  else /*if (val == joke_tk->key_horizontal)*/
    vertical = 0;

  val = ksi_get_arg (joke_tk->key_window, args, (ksi_obj) top);
  KSI_CHECK (val, WIN_P (top),
	     "tk-create-scrollbar: invalid window in #:window attr");
  event_wnd = (ksi_window) val;

  val = ksi_get_arg (joke_tk->key_value, args, ksi_int2num(0));
  KSI_CHECK (val, KSI_REAL_P (val),
	     "tk-create-scrollbar: invalid number in #:value attr");

  val = ksi_get_arg (joke_tk->key_begin_value, args, val);
  KSI_CHECK (val, KSI_REAL_P (val),
	     "tk-create-scrollbar: invalid number in #:begin-value attr");
  beg = ksi_num2double (val);
  if (beg < 0.0)
    beg = 0.0;
  else if (beg > 1.0)
    beg = 1.0;

  val = ksi_get_arg (joke_tk->key_end_value, args, val);
  KSI_CHECK (val, KSI_REAL_P (val),
	     "tk-create-scrollbar: invalid number in #:end-value attr");
  end = ksi_num2double (val);
  if (end < beg)
    end = beg;
  else if (end > 1.0)
    end = 1.0;

  attr.background_pixel = joke_tk->scroll_back->back;
  attr.event_mask = (StructureNotifyMask | ExposureMask
		     | ButtonPressMask | ButtonReleaseMask
		     | Button1MotionMask | Button2MotionMask);

  wid = XCreateWindow (joke_dpy, top->wid,
		       0, 0, joke_tk->scroll_size, joke_tk->scroll_size,
		       0,
		       CopyFromParent,
		       InputOutput,
		       joke_tk->visual,
		       CWBackPixel | CWEventMask,
		       &attr);

  if (!wid)
    return (ksi_window) ksi_false;

  info = (scroll_info) ksi_malloc (sizeof *info);
  /* memset (info, 0, sizeof *info) */
  info->base.proc = scroll_window_event_proc;
  info->base.proc_data = event_wnd;
  info->base.parent = top;
  info->base.type = SCROLLBAR_WINDOW;
  info->base.x = 0;
  info->base.y = 0;
  info->base.w = joke_tk->scroll_size;
  info->base.h = joke_tk->scroll_size;
  info->beg = beg;
  info->end = end;
  info->vertical = vertical;
  info->active_part = OUTSIDE;

  wnd = ksi_append_window (joke_wintab, wid);
  wnd->tk_data = (ksi_window_info) info;
  return wnd;
}

ksi_obj
ksi_tk_set_scrolbar_value (ksi_obj wnd, ksi_obj beg, ksi_obj end)
{
  scroll_info info;
  double b, e;

  if (!end)
    end = beg;

  KSI_CHECK (wnd, SCROLLBAR_P (wnd),
	     "tk-set-scrollbar-value: invalid scrollbar window in arg1");
  KSI_CHECK (beg, KSI_REAL_P (beg),
	     "tk-set-scrollbar-value: invalid number in arg2");
  KSI_CHECK (end, KSI_REAL_P (end),
	     "tk-set-scrollbar-value: invalid number in arg3");

  info = (scroll_info) ((ksi_window) wnd) -> tk_data;
  b = ksi_num2double (beg);
  e = ksi_num2double (end);

  if (b < 0.0)
    b = 0.0;
  else if (b > 1.0)
    b = 1.0;

  if (e < b)
    e = b;
  else if (e > 1.0)
    e = 1.0;

  if (b != info->beg || e != info->end)
    {
      info->beg = b;
      info->end = e;
      info->updated = 1;

      if (info->active_part == SLIDER)
	info->move_beg = info->move_cur;

      if (info->base.mapped)
	draw_scroll ((ksi_window) wnd);
    }

  return ksi_unspec;
}


static struct Ksi_Prim_Def defs [] =
{
  { "tk-create-scrollbar",	ksi_tk_create_scrollbar,   KSI_CALL_ARG2, 1 },
  { "tk-set-scrollbar-value",	ksi_tk_set_scrolbar_value, KSI_CALL_ARG3, 2 },

  { 0 },
};

void
ksi_tk_init_scrollbar (void)
{
  char *tmp;

  ignore (rcsid);
  ksi_reg_unit (defs);

  tmp = ksi_tk_string_resource ("scrollbar.background",
				"Scrollbar.Background",
				0);
  if (tmp)
    joke_tk->scroll_back = ksi_tk_alloc_color_3d (tmp);
  else
    joke_tk->scroll_back = joke_tk->top_back;

  joke_tk->scroll_size = ksi_tk_int_resource ("scrollbar.size",
					      "Scrollbar.Size",
					      15);

  joke_tk->scroll_arrow_size = ksi_tk_int_resource ("scrollbar.arrowSize",
						    "Scrollbar.ArrowSize",
						    joke_tk->scroll_size);

}

 /* End of code */
