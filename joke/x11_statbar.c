/*
 * x11_statbar.c
 * 
 *
 * Copyright (C) 1999-2000, Ivan Demakov.
 *
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose, without fee, and without a written
 * agreement is hereby granted, provided that the above copyright notice
 * and this paragraph and the following two paragraphs appear in all copies.
 * Modifications to this software may be copyrighted by their authors
 * and need not follow the licensing terms described here, provided that
 * the new terms are clearly indicated on the first page of each file where
 * they apply.
 *
 * IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY
 * FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES,
 * INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS
 * DOCUMENTATION, EVEN IF THE AUTHORS HAS BEEN ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIMS ANY WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS
 * ON AN "AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAS NO OBLIGATIONS
 * TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 *
 *
 * Author:        Ivan Demakov <demakov@users.sourceforge.net>
 * Creation date: Tue Apr 20 03:02:22 1999
 * Last Update:   Tue Mar 21 19:30:58 2000
 *
 */

#include "toolkit.h"

static char rcsid[] = "$Id: x11_statbar.c,v 1.7 2000/04/15 14:03:17 ivan Exp $";


#define MINW	(2 + 2 * joke_tk->status_sp)


static void
alloc_panes (status_info info, ksi_obj args)
{
  int i, num, w;
  ksi_obj x;

  num =  ksi_list_len (args);
  info->num_panes = num;
  info->panes = (status_pane) ksi_malloc (num * sizeof (StatusPane));

  for (i = 0; i < num; i++, args = KSI_CDR (args))
    {
      x = KSI_CAR (args);
      w = KSI_EINT_P (x) ? KSI_SINT_COD (x) : 0;
	
      info->panes[i].w = w;
      info->panes[i].fixed = w > 0;
      info->panes[i].str = "";
    }
}

static void
reorder_panes (status_info info)
{
  int i, x, w, used, num;

  num  = 0;
  used = joke_tk->status_sp;
  for (i = 0; i < info->num_panes; i++)
    {
      if (info->panes[i].fixed)
	used += info->panes[i].w;
      else
	num += 1;
      used += joke_tk->status_sp + 2;
    }

  w = num ? (info->base.w - used) / num : 0;

  x = 1 + joke_tk->status_sp;
  for (i = 0; i < info->num_panes; i++)
    {
      if (!info->panes[i].fixed)
	info->panes[i].w = w;

      info->panes[i].x = x;
      if (info->panes[i].w > MINW)
	x += info->panes[i].w + 2 + joke_tk->status_sp;
    }
}

static void
draw_status_text (Window wid, status_info info, int i)
{
  XRectangle r[1];

  if (info->panes[i].str[0] || info->panes[i].proc != 0)
    {
      r[0].x = info->panes[i].x + 1 + joke_tk->status_sp;
      r[0].width = info->panes[i].w - 2 - 2 * joke_tk->status_sp;
      r[0].y = 3 + joke_tk->status_sp;
      r[0].height = info->base.h - 6 - 2 * joke_tk->status_sp;

      if (info->panes[i].str[0])
	{
	  XSetClipRectangles (joke_dpy, joke_tk->status_font->gc,
			      0, 0, r, 1, YXBanded);

	  XDrawString (joke_dpy, wid, joke_tk->status_font->gc,
		       r[0].x,
		       r[0].y + joke_tk->status_sp
		       + joke_tk->status_font->fs->ascent,
		       info->panes[i].str,
		       strlen (info->panes[i].str));
	}
      else
	{
	  ksi_obj dc, rc;

	  if (info->draw_gc == None)
	    {
	      XGCValues val;
	      val.graphics_exposures = False;
	      info->draw_gc = XCreateGC (joke_dpy, wid,
					 GCGraphicsExposures, &val);
	    }

	  XSetClipRectangles (joke_dpy, info->draw_gc,
			      0, 0, r, 1, YXBanded);

	  dc = ksi_make_x11_dc (wid, wid, info->draw_gc);

	  rc = ksi_make_vector (ksi_int2num (4), 0);
	  KSI_VEC_REF (rc, 0) = ksi_int2num (r[0].x);
	  KSI_VEC_REF (rc, 1) = ksi_int2num (r[0].y);
	  KSI_VEC_REF (rc, 2) = ksi_int2num (r[0].width);
	  KSI_VEC_REF (rc, 3) = ksi_int2num (r[0].height);

	  ksi_apply_2 (info->panes[i].proc, dc, rc);
	}
    }
}

static void
draw_status (ksi_window wnd)
{
  int i, y, h;
  status_info info = (status_info) wnd->tk_data;

  ksi_tk_fill_3d_rectangle (wnd->wid, joke_tk->status_back,
			    0, 0, info->base.w, info->base.h,
			    1, RELIEF_RAISED);

  y = 1 + joke_tk->status_sp;
  h = info->base.h - 2 - 2 * joke_tk->status_sp;
  for (i = 0; i < info->num_panes; i++)
    {
      if (info->panes[i].w > MINW)
	{
	  ksi_tk_draw_3d_rectangle (wnd->wid, joke_tk->status_back,
				    info->panes[i].x, y,
				    info->panes[i].w, h,
				    1, RELIEF_SUNKEN);

	  draw_status_text (wnd->wid, info, i);
	}
    }
}


static void
status_window_event_proc (ksi_window wnd, XEvent *xe, void *data)
{
  ksi_obj ek;
  status_info info = (status_info) wnd->tk_data;

  switch (xe->type)
    {
    case MapNotify:
      wnd->tk_data->mapped = 1;
      break;

    case UnmapNotify:
      wnd->tk_data->mapped = 0;
      if (info->draw_gc != None)
	{
	  XFreeGC (joke_dpy, info->draw_gc);
	  info->draw_gc = None;
	}
      break;

    case ConfigureNotify:
      wnd->tk_data->x = xe->xconfigure.x;
      wnd->tk_data->y = xe->xconfigure.y;
      wnd->tk_data->w = xe->xconfigure.width;
      wnd->tk_data->h = xe->xconfigure.height;
      reorder_panes (info);
      break;

    case Expose:
      if (xe->xexpose.count == 0)
	draw_status (wnd);
      break;
    }

  ek = ksi_make_x11_event (xe);
  if (ek != ksi_false)
    ksi_apply_window_event ((ksi_obj) wnd, ek);
}


ksi_window
ksi_tk_create_status (ksi_window top, ksi_obj args)
{
  Window wid;
  XSetWindowAttributes attr;
  ksi_window wnd;
  status_info info;

  KSI_CHECK ((ksi_obj) top, TOP_P (top),
	     "tk-create-status: invalid top window in arg1");

  if (!args)
    args = ksi_nil;

  attr.background_pixel = joke_tk->status_back->back;
  attr.event_mask = StructureNotifyMask | ExposureMask;

  wid = XCreateWindow (joke_dpy, top->wid,
		       0, 0, 1, joke_tk->status_height,
		       0,
		       CopyFromParent,
		       InputOutput,
		       joke_tk->visual,
		       CWBackPixel | CWEventMask,
		       &attr);

  if (!wid)
    return (ksi_window) ksi_false;

  info = (status_info) ksi_malloc (sizeof *info);
  /* memset (info, 0, sizeof *info); */
  info->base.proc = status_window_event_proc;
  info->base.proc_data = 0;
  info->base.parent = top;
  info->base.type = STATUS_WINDOW;
  info->base.x = 0;
  info->base.y = 0;
  info->base.w = 1;
  info->base.h = joke_tk->status_height;
  info->draw_gc = None;

  alloc_panes (info, args);

  wnd = ksi_append_window (joke_wintab, wid);
  wnd->tk_data = (ksi_window_info) info;
  return wnd;
}

ksi_obj
ksi_tk_set_status_pane (ksi_obj stat, ksi_obj num, ksi_obj txt)
{
  int i, txt_is_proc = 0;
  const char *str;
  status_info info;

  KSI_CHECK (stat, STATUSBAR_P (stat),
	     "tk-set-status-pane: invalid statusbar window in arg1");
  KSI_CHECK (num, KSI_EINT_P (num),
	     "tk-set-status-pane: invalid integer in arg2");

  info = (status_info) ((ksi_window) stat) -> tk_data;
  i = KSI_SINT_COD (num);

  if (0 <= i && i < info->num_panes)
    {
      if (ksi_procedure_p (txt) != ksi_false)
	txt_is_proc = 1;
      else
	str = ksi_obj2name (txt);

      if (txt_is_proc || strcmp (str, info->panes[i].str) != 0)
	{
	  if (txt_is_proc)
	    {
	      info->panes[i].str  = "";
	      info->panes[i].proc = txt;
	    }
	  else
	    {
	      info->panes[i].str  = str;
	      info->panes[i].proc = 0;
	    }

	  if (info->panes[i].w > MINW && info->base.mapped)
	    {
	      XClearArea (joke_dpy, ((ksi_window) stat) -> wid,
			  info->panes[i].x + 1,
			  2 + joke_tk->status_sp,
			  info->panes[i].w - 2,
			  info->base.h - 4 - 2 * joke_tk->status_sp,
			  False);

	      draw_status_text (((ksi_window) stat) -> wid, info, i);
	    }
	}
    }

  return ksi_unspec;
}


static struct Ksi_Prim_Def defs [] =
{
  {"tk-create-status",		ksi_tk_create_status,	KSI_CALL_ARG2, 2},
  {"tk-set-status-pane",	ksi_tk_set_status_pane,	KSI_CALL_ARG3, 3},

  { 0 }
};

void
ksi_tk_init_statbar (void)
{
  char *tmp, *fnt;
  int h1, h2;

  ignore (rcsid);
  ksi_reg_unit (defs);

  tmp = ksi_tk_string_resource ("status.background",
				"Status.Background",
				0);
  if (tmp)
    joke_tk->status_back = ksi_tk_alloc_color_3d (tmp);
  else
    joke_tk->status_back = joke_tk->top_back;

  tmp = ksi_tk_string_resource ("status.foreground",
				"Status.Foreground",
				"black");
  fnt = ksi_tk_string_resource ("status.font",
				"Status.Font",
				"fixed");

  joke_tk->status_font = ksi_tk_alloc_font (tmp, fnt);

  h1 = joke_tk->status_font->fs->ascent + joke_tk->status_font->fs->descent;
  h2 = h1 / 4;
  if (h2 == 0)
    h2 = 1;

  joke_tk->status_sp = h2;
  joke_tk->status_height = 4 + 4 * h2 + h1;
}

 /* End of code */
