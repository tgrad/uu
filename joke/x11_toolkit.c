/*
 * x11_toolkit.c
 * 
 *
 * Copyright (C) 1999-2000, Ivan Demakov.
 *
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose, without fee, and without a written
 * agreement is hereby granted, provided that the above copyright notice
 * and this paragraph and the following two paragraphs appear in all copies.
 * Modifications to this software may be copyrighted by their authors
 * and need not follow the licensing terms described here, provided that
 * the new terms are clearly indicated on the first page of each file where
 * they apply.
 *
 * IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY
 * FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES,
 * INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS
 * DOCUMENTATION, EVEN IF THE AUTHORS HAS BEEN ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIMS ANY WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS
 * ON AN "AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAS NO OBLIGATIONS
 * TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 *
 *
 * Author:        Ivan Demakov <demakov@users.sourceforge.net>
 * Creation date: Fri Apr  9 02:26:44 1999
 * Last Update:   Tue Mar 21 19:31:17 2000
 *
 */

#include "toolkit.h"

static char rcsid[] = "$Id: x11_toolkit.c,v 1.10 2000/04/15 14:03:17 ivan Exp $";


char*
ksi_tk_string_resource (char *resource, char* class, char *def)
{
  XrmValue val;
  const char *app = ksi_app ();
  char *rs, *cl, *tp;

  if (class == 0)
    class = resource;

  if (resource)
    {
      rs = (char*) alloca (strlen (app) + strlen (resource) + 4);
      cl = (char*) alloca (strlen ("Joke") + strlen (class) + 4);
      sprintf (rs, ".%s.%s", app, resource);
      sprintf (cl, ".%s.%s", "Joke", class);

      if (XrmGetResource (joke_rdb, rs, cl, &tp, &val))
	return (char*) val.addr;
    }

  return def;
}

int
ksi_tk_int_resource (char *resource, char* class, int def)
{
  int val;
  char *str = ksi_tk_string_resource (resource, class, 0);
  char *end;

  if (str)
    {
      val = strtol (str, &end, 0);
      if (str != end)
	return val;
    }

  return def;
}

int
ksi_tk_bool_resource (char *resource, char* class, int def)
{
  char *str = ksi_tk_string_resource (resource, class, 0);

  if (str)
    {
      if (ksi_strcasecmp (str, "on") == 0
	  || ksi_strcasecmp (str, "true") == 0
	  || ksi_strcasecmp (str, "yes") == 0)
	return 1;

      if (ksi_strcasecmp (str, "off") == 0
	  || ksi_strcasecmp (str, "false") == 0
	  || ksi_strcasecmp (str, "no") == 0)
	return 0;
    }

  return def;
}


int
ksi_tk_int_arg (ksi_obj args, ksi_obj key, char *res, char* cls, int def)
{
  ksi_obj val;

  val = ksi_get_arg (key, args, 0);
  if (KSI_EINT_P (val))
    return KSI_SINT_COD (val);

  return (res ? ksi_tk_int_resource (res, cls, def) : def);
}

char*
ksi_tk_string_arg (ksi_obj args, ksi_obj key, char *res, char *cls, char *def)
{
  ksi_obj val;

  val = ksi_get_arg (key, args, 0);
  if (val != ksi_unspec)
    return (char*) ksi_obj2name (val);

  return (res ? ksi_tk_string_resource (res, cls, def) : def);
}

int
ksi_tk_rect_arg (ksi_obj args, ksi_obj key, char* res, char *cls,
		 int *x1, int *y1, int *x2, int *y2)
{
  ksi_obj val;
  int tmp;

  val = ksi_get_arg (key, args, 0);
  if (KSI_VEC_P (val) && KSI_VEC_LEN (val) == 4)
    {
      *x1 = ksi_num2int (KSI_VEC_REF (val, 0));
      *y1 = ksi_num2int (KSI_VEC_REF (val, 1));
      *x2 = ksi_num2int (KSI_VEC_REF (val, 2));
      *y2 = ksi_num2int (KSI_VEC_REF (val, 3));

      if (*x1 > *x2)
	tmp = *x1, *x1 = *x2, *x2 = tmp;
      if (*y1 > *y2)
	tmp = *y1, *y1 = *y2, *y2 = tmp;

      return 1;
    }

  return 0;
}

ksi_pixel
ksi_tk_pixel_arg (ksi_obj args, ksi_obj key,
		  char *res, char *cls, ksi_pixel def)
{
  char *name = ksi_tk_string_arg (args, key, res, cls, 0);

  return (name ? ksi_tk_alloc_color (name) : def);
}


static ksi_color
get_light (int c)
{
  int tmp1, tmp2;

  tmp1 = (14 * (int) c) / 10;
  if (tmp1 > MAX_COLOR)
    tmp1 = MAX_COLOR;

  tmp2 = (MAX_COLOR + (int) c) / 2;

  return (ksi_color) (tmp1 > tmp2 ? tmp1 : tmp2);
}


tk_color_3d
ksi_tk_alloc_color_3d (const char *name)
{
  XGCValues	val;
  tk_color_3d	color;
  ksi_color	r = 0, g = 0, b = 0;
  ksi_pixel	back = -1, light = -1, dark = -1;

  if (ksi_parse_color (joke_tk->cmap, name, &r, &g, &b))
    {
      back = ksi_alloc_pixel (joke_tk->cmap, r, g, b);

      dark = ksi_alloc_pixel (joke_tk->cmap,
			      (60 * r) / 100,
			      (60 * g) / 100,
			      (60 * b) / 100);

      light = ksi_alloc_pixel (joke_tk->cmap,
			       get_light (r),
			       get_light (g),
			       get_light (b));
    }

  if (back == (ksi_pixel) -1)
    back = BlackPixel (joke_dpy, joke_tk->screen);

  if (dark == (ksi_pixel) -1)
    dark = BlackPixel (joke_dpy, joke_tk->screen);

  if (light == (ksi_pixel) -1)
    light = WhitePixel (joke_dpy, joke_tk->screen);

  color = (tk_color_3d) ksi_malloc)data (sizeof *color);
  color->red = r;
  color->green = g;
  color->blue = b;
  color->back = back;
  color->light = light;
  color->dark = dark;

  val.foreground = back;
  color->back_gc = XCreateGC (joke_dpy, joke_tk->root, GCForeground, &val);

  val.foreground = light;
  color->light_gc = XCreateGC (joke_dpy, joke_tk->root, GCForeground, &val);

  val.foreground = dark;
  color->dark_gc = XCreateGC (joke_dpy, joke_tk->root, GCForeground, &val);

  return color;
}

ksi_pixel
ksi_tk_alloc_color (const char *name)
{
  ksi_color	r = 0, g = 0, b = 0;
  ksi_pixel	color = -1;

  if (ksi_parse_color (joke_tk->cmap, name, &r, &g, &b))
    color = ksi_alloc_pixel (joke_tk->cmap, r, g, b);

  if (color == (ksi_pixel) -1)
    color = BlackPixel (joke_dpy, joke_tk->screen);

  return color;
}

tk_font
ksi_tk_alloc_font (const char *color, const char *name)
{
  XGCValues	val;
  tk_font	font;
  ksi_color	r = 0, g = 0, b = 0;
  ksi_pixel	pixel = (ksi_pixel) -1;
  XFontStruct	*fs;

  if (ksi_parse_color (joke_tk->cmap, color, &r, &g, &b))
    pixel = ksi_alloc_pixel (joke_tk->cmap, r, g, b);
  if (pixel == (ksi_pixel) -1)
    pixel = WhitePixel (joke_dpy, joke_tk->screen);

  fs = XLoadQueryFont (joke_dpy, name);
  if (fs == 0)
    fs = XLoadQueryFont (joke_dpy, "*");
  if (fs == 0)
    ksi_error ("Cannot load any font");

  font = (tk_font) ksi_malloc)data (sizeof *font);
  font->red = r;
  font->green = g;
  font->blue = b;
  font->pixel = pixel;
  font->fs = fs;

  val.foreground = pixel;
  val.font = fs->fid;
  font->gc = XCreateGC (joke_dpy, joke_tk->root, GCForeground | GCFont, &val);

  return font;
}


void
ksi_joke_init_x11_toolkit (void)
{
  char *tmp;
  ignore (rcsid);

  tmp = XResourceManagerString (joke_dpy);
  if (tmp)
    XrmCombineDatabase (XrmGetStringDatabase (tmp), &joke_rdb, False);
  else
    {
      tmp = ksi_expand_file_name ("~/.Xdefaults");
      XrmCombineFileDatabase (tmp, &joke_rdb, False);
    }

  joke_tk->screen = DefaultScreen (joke_dpy);
  joke_tk->root   = RootWindow (joke_dpy, joke_tk->screen);
  joke_tk->visual = DefaultVisual (joke_dpy, joke_tk->screen);
  joke_tk->cmap   = ksi_get_window_colormap (joke_tk->root);

  joke_tk->atom_wm_protocols = XInternAtom (joke_dpy, "WM_PROTOCOLS", False);
  joke_tk->atom_wm_delete_window = XInternAtom (joke_dpy, "WM_DELETE_WINDOW",
						False);

  joke_tk->sym_wm_delete_window = ksi_str02sym ("wm_delete_window");
  joke_tk->sym_wm_protocols = ksi_str02sym ("wm_protocols");

  ksi_tk_init_border ();
  ksi_tk_init_events ();
  ksi_tk_init_topwin ();
  ksi_tk_init_statbar ();
  ksi_tk_init_scrollbar ();
  ksi_tk_init_canvas ();
}

void
ksi_joke_term_x11_toolkit (void)
{
}

 /* End of code */
