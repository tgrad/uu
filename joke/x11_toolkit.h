/* -*-mode:C++-*- */
/*
 * x11_toolkit.h
 * 
 *
 * Copyright (C) 1999-2000, Ivan Demakov.
 *
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose, without fee, and without a written
 * agreement is hereby granted, provided that the above copyright notice
 * and this paragraph and the following two paragraphs appear in all copies.
 * Modifications to this software may be copyrighted by their authors
 * and need not follow the licensing terms described here, provided that
 * the new terms are clearly indicated on the first page of each file where
 * they apply.
 *
 * IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY
 * FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES,
 * INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS
 * DOCUMENTATION, EVEN IF THE AUTHORS HAS BEEN ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIMS ANY WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS
 * ON AN "AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAS NO OBLIGATIONS
 * TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 *
 *
 * Author:        Ivan Demakov <demakov@users.sourceforge.net>
 * Creation date: Fri Apr  9 02:51:42 1999
 * Last Update:   Tue Mar 21 19:31:32 2000
 *
 * 
 * $Id: x11_toolkit.h,v 1.13 2000/04/15 14:03:17 ivan Exp $
 *
 */

#ifndef X11_TOOLKIT_H
#define X11_TOOLKIT_H


typedef struct Ksi_TopInfo
{
  WindowInfo	base;
  ksi_window	menu;
  ksi_window	status;
  ksi_window	main;
  ksi_obj	top;
  ksi_obj	bottom;
  ksi_obj	left;
  ksi_obj	right;
} TopInfo, *top_info;


typedef struct Ksi_StatusPane
{
  int		x, w, fixed;
  const char	*str;
  ksi_obj	proc;
} StatusPane, *status_pane;

typedef struct Ksi_StatusInfo
{
  WindowInfo	base;
  int		num_panes;
  status_pane	panes;
  GC		draw_gc;
} StatusInfo, *status_info;


typedef struct Ksi_ScrollInfo
{
  WindowInfo	base;
  double	beg, end;
  int		move_beg, move_cur;
  int		active_part;
  unsigned	vertical : 1;
  unsigned	updated : 1;
} ScrollInfo, *scroll_info;


typedef struct Ksi_CanvasInfo
{
  WindowInfo	base;
  ksi_window	vert_scroll;
  ksi_window	horz_scroll;
  int		draw_x1, draw_y1, draw_x2, draw_y2;
  int		work_x1, work_y1, work_x2, work_y2;
  int		view_off_x, view_off_y;
  double	zoom_k;
  int		line_step_x, line_step_y, page_step_x, page_step_y;
  ksi_pixel	background;
  Drawable	pixmap;
  int		pix_w, pix_h;
  int		pix_x1, pix_y1, pix_x2, pix_y2;
  GC		pix_gc, draw_gc;
  unsigned	offscreen_draw : 1;
} CanvasInfo, *canvas_info;



/* x11_toolkit.c */

void
ksi_joke_init_x11_toolkit (void);

void
ksi_joke_term_x11_toolkit (void);

char*
ksi_tk_string_resource (char *resource, char *class, char *def);

int
ksi_tk_int_resource (char *resource, char *class, int def);

int
ksi_tk_bool_resource (char *resource, char *class, int def);

int
ksi_tk_int_arg (ksi_obj args, ksi_obj key, char *res, char *cls, int def);

char*
ksi_tk_string_arg (ksi_obj args, ksi_obj key, char *res, char *cls, char *def);

int
ksi_tk_rect_arg (ksi_obj args, ksi_obj key, char* res, char *cls,
		 int *x1, int *y1, int *x2, int *y2);

ksi_pixel
ksi_tk_pixel_arg (ksi_obj args, ksi_obj key,
		  char *res, char *cls, ksi_pixel def);

ksi_pixel
ksi_tk_alloc_color (const char *name);

tk_color_3d
ksi_tk_alloc_color_3d (const char *color);

tk_font
ksi_tk_alloc_font (const char *color, const char *font);


/* x11_colormap.c */

void
ksi_init_colormap (void);

int
ksi_parse_color (ksi_colormap cmap, const char* name,
		 ksi_color* r, ksi_color* g, ksi_color* b);

ksi_pixel
ksi_alloc_pixel (ksi_colormap dc, ksi_color r, ksi_color g, ksi_color b);

void
ksi_free_pixel (ksi_colormap dc, ksi_pixel pixel);

ksi_colormap
ksi_get_window_colormap (Window wnd);


/* x11_border.c */

void
ksi_tk_init_border (void);

void
ksi_tk_draw_3d_rectangle (Drawable d, tk_color_3d border,
			  int x, int y, int w, int h, int bw, int relief);

void
ksi_tk_fill_3d_rectangle (Drawable d, tk_color_3d border,
			  int x, int y, int w, int h, int bw, int relief);

void
ksi_tk_draw_3d_polygon (Drawable d, tk_color_3d color,
			XPoint *pnt, int num, int bw, int relief);

void
ksi_tk_fill_3d_polygon (Drawable d, tk_color_3d color,
			XPoint *pnt, int num, int bw, int relief);


/* x11_events.c */

void
ksi_tk_init_events (void);


/* x11_topwin.c */

void
ksi_tk_init_topwin (void);

ksi_window
ksi_tk_create_top (ksi_obj args);

ksi_obj
ksi_tk_set_top_name (ksi_window wnd, ksi_obj name);

ksi_obj
ksi_tk_set_top_icon_name (ksi_window wnd, ksi_obj name);

ksi_obj
ksi_tk_set_top_size (ksi_window wnd, ksi_obj w, ksi_obj h);

ksi_obj
ksi_tk_set_top_menu (ksi_window top, ksi_window menu);

ksi_obj
ksi_tk_set_top_status (ksi_window top, ksi_window stat);

ksi_obj
ksi_tk_map_top (ksi_obj wnd);

ksi_obj
ksi_tk_unmap_top (ksi_obj wnd);


/* x11_statbar.c */

void
ksi_tk_init_statbar (void);

ksi_window
ksi_tk_create_status (ksi_window top, ksi_obj args);


/* x11_scroll.c */

void
ksi_tk_init_scrollbar (void);

ksi_window
ksi_tk_create_scrollbar (ksi_window top, ksi_obj args);

ksi_obj
ksi_tk_set_scrolbar_value (ksi_obj wnd, ksi_obj beg, ksi_obj end);


/* x11_canvas.c */

void
ksi_tk_init_canvas (void);

ksi_window
ksi_tk_create_canvas (ksi_window top, ksi_obj args);

ksi_obj
ksi_tk_get_canvas_draw_area (ksi_obj wnd);

ksi_obj
ksi_tk_get_canvas_work_area (ksi_obj wnd);

ksi_obj
ksi_tk_set_canvas_work_area (ksi_obj wnd, ksi_obj v);

ksi_obj
ksi_tk_get_canvas_view_off (ksi_obj wnd);

ksi_obj
ksi_tk_set_canvas_view_off (ksi_obj wnd, ksi_obj x, ksi_obj y);

ksi_obj
ksi_tk_get_canvas_view_zoom (ksi_obj wnd);

ksi_obj
ksi_tk_set_canvas_view_zoom (ksi_obj wnd, ksi_obj k);

ksi_obj
ksi_tk_get_canvas_view_area (ksi_obj wnd);

ksi_obj
ksi_tk_get_canvas_dc (ksi_obj wnd);

ksi_obj
ksi_tk_canvas_work_x (ksi_obj wnd, ksi_obj num);

ksi_obj
ksi_tk_canvas_work_y (ksi_obj wnd, ksi_obj num);

ksi_obj
ksi_tk_canvas_draw_x (ksi_obj wnd, ksi_obj num);

ksi_obj
ksi_tk_canvas_draw_y (ksi_obj wnd, ksi_obj num);


#endif

 /* End of file */
