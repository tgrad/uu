/*
 * x11_topwin.c
 * top level windows
 *
 * Copyright (C) 1999-2000, Ivan Demakov.
 *
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose, without fee, and without a written
 * agreement is hereby granted, provided that the above copyright notice
 * and this paragraph and the following two paragraphs appear in all copies.
 * Modifications to this software may be copyrighted by their authors
 * and need not follow the licensing terms described here, provided that
 * the new terms are clearly indicated on the first page of each file where
 * they apply.
 *
 * IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY
 * FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES,
 * INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS
 * DOCUMENTATION, EVEN IF THE AUTHORS HAS BEEN ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIMS ANY WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS
 * ON AN "AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAS NO OBLIGATIONS
 * TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 *
 *
 * Author:        Ivan Demakov <demakov@users.sourceforge.net>
 * Creation date: Mon Apr  5 16:11:11 1999
 * Last Update:   Tue Mar 21 19:32:07 2000
 *
 */

#include "toolkit.h"

static char rcsid[] = "$Id: x11_topwin.c,v 1.6 2000/04/15 14:03:17 ivan Exp $";


static void
reorder_childs (ksi_window wnd)
{
  int x, y, w, h;
  top_info info = (top_info) wnd->tk_data;
  ksi_window menu = info->menu;
  ksi_window stat = info->status;
  ksi_obj top = info->top;
  ksi_obj bottom = info->bottom;
  ksi_obj left = info->left;
  ksi_obj right = info->right;

  x = 0;
  y = 0;
  w = info->base.w;
  h = info->base.h;

  if (menu)
    {
      XMoveResizeWindow (joke_dpy, menu->wid,
			 x, y, w, menu->tk_data->h);

      y += menu->tk_data->h;
      h -= menu->tk_data->h;
    }

  if (stat)
    {
      XMoveResizeWindow (joke_dpy, stat->wid,
			 x, y + h - stat->tk_data->h,
			 w, stat->tk_data->h);

      h -= stat->tk_data->h;
    }

  while (top != ksi_nil)
    {
      ksi_window curw = (ksi_window) KSI_CAR (top);

      XMoveResizeWindow (joke_dpy, curw->wid, x, y, w, curw->tk_data->h);
      y += curw->tk_data->h;
      h -= curw->tk_data->h;

      top = KSI_CDR (top);
    }

  while (bottom != ksi_nil)
    {
      ksi_window curw = (ksi_window) KSI_CAR (bottom);

      XMoveResizeWindow (joke_dpy, curw->wid,
			 x, y + h - curw->tk_data->h,
			 w, curw->tk_data->h);

      h -= curw->tk_data->h;

      bottom = KSI_CDR (bottom);
    }

  if (h <= 0)
    h = 1;

  while (left != ksi_nil)
    {
      ksi_window curw = (ksi_window) KSI_CAR (left);

      XMoveResizeWindow (joke_dpy, curw->wid, x, y, curw->tk_data->w, h);
      x += curw->tk_data->w;
      w -= curw->tk_data->w;

      left = KSI_CDR (left);
    }

  while (right != ksi_nil)
    {
      ksi_window curw = (ksi_window) KSI_CAR (right);

      XMoveResizeWindow (joke_dpy, curw->wid,
			 x + w - curw->tk_data->w, y,
			 curw->tk_data->w, h);

      w -= curw->tk_data->w;

      right = KSI_CDR (right);
    }

  if (w <= 0)
    w = 1;

  if (info->main)
    {
      XMoveResizeWindow (joke_dpy, info->main->wid, x, y, w, h);
    }
}


static void
top_window_event_proc (ksi_window wnd, XEvent *xe, void *data)
{
  ksi_obj ek;
  int destroy = 0;

  switch (xe->type)
    {
    case MapNotify:
      wnd->tk_data->mapped = 1;
      break;

    case UnmapNotify:
      wnd->tk_data->mapped = 0;
      break;

    case ConfigureNotify:
      wnd->tk_data->x = xe->xconfigure.x;
      wnd->tk_data->y = xe->xconfigure.y;
      wnd->tk_data->w = xe->xconfigure.width;
      wnd->tk_data->h = xe->xconfigure.height;
      if (wnd->tk_data->mapped)
	reorder_childs (wnd);
      break;

    case ClientMessage:
      if (xe->xclient.message_type == joke_tk->atom_wm_protocols)
	{
	  if (xe->xclient.data.l[0] == joke_tk->atom_wm_delete_window)
	    destroy = 1;
	}
    }

  ek = ksi_make_x11_event (xe);
  if (ek != ksi_false)
    ksi_apply_window_event ((ksi_obj) wnd, ek);

  if (destroy)
    XDestroyWindow (joke_dpy, wnd->wid);
}


ksi_window
ksi_tk_create_top (ksi_obj args)
{
  Window win_id;
  XSetWindowAttributes attr;
  XWMHints *hints;
  XSizeHints *size;
  XClassHint *cls;
  Atom protocols[1];
  int x, y, h, w, minw, minh, maxw, maxh, iconic;
  const char *name;
  ksi_window wnd;
  top_info info;

  if (!args)
    args = ksi_nil;

  w = ksi_tk_int_arg (args, joke_tk->key_width,
		      "topLevel.width", "TopLevel.Width", 100);

  h = ksi_tk_int_arg (args, joke_tk->key_height,
		      "topLevel.height", "TopLevel.Height", 100);

  minw = ksi_tk_int_arg (args, joke_tk->key_min_width,
			 "topLevel.minWidth", "TopLevel.MinWidth", 0);

  minh = ksi_tk_int_arg (args, joke_tk->key_min_height,
			 "topLevel.minHeight", "TopLevel.MinHeight", 0);

  maxw = ksi_tk_int_arg (args, joke_tk->key_max_width,
			 "topLevel.maxWidth", "TopLevel.MaxWidth", 0);

  maxh = ksi_tk_int_arg (args, joke_tk->key_max_height,
			 "topLevel.maxHeight", "TopLevel.MaxHeight", 0);

  name = ksi_tk_string_arg (args, joke_tk->key_name,
			    "topLevel.name", "TopLevel.Name", "Top Level");

  iconic = ksi_tk_bool_resource ("topLevel.iconic", "TopLevel.Iconic", 0);

  attr.background_pixel = joke_tk->top_back->back;
  attr.event_mask = StructureNotifyMask | FocusChangeMask;

  x = 0;
  y = 0;
  if (w <= 0) w = 1;
  if (h <= 0) h = 1;

  win_id = XCreateWindow (joke_dpy, joke_tk->root,
			  x, y, w, h,
			  0,
			  CopyFromParent,
			  InputOutput,
			  joke_tk->visual,
			  CWBackPixel | CWEventMask,
			  &attr);

  if (!win_id)
    return (ksi_window) ksi_false;

  XStoreName (joke_dpy, win_id, name);

  hints = XAllocWMHints ();
  hints->flags = InputHint | StateHint;
  hints->input = True;
  hints->initial_state = iconic ? IconicState : NormalState;
  XSetWMHints (joke_dpy, win_id, hints);
  XFree (hints);

  size = XAllocSizeHints ();
  size->flags = 0;
  if (minw > 0 && minh > 0)
    {
      size->flags |= PMinSize;
      size->min_width  = minw;
      size->min_height = minh;
    }
  if (maxw > 0 && maxh > 0)
    {
      size->flags |= PMaxSize;
      size->max_width  = maxw;
      size->max_height = maxh;
    }
  XSetWMNormalHints (joke_dpy, win_id, size);
  XFree (size);

  cls = XAllocClassHint ();
  cls->res_name = (char*) ksi_app ();
  cls->res_class = "Joke";
  XSetClassHint (joke_dpy, win_id, cls);
  XFree (cls);

  protocols[0] = joke_tk->atom_wm_delete_window;
  XSetWMProtocols (joke_dpy, win_id, protocols, 1);

  info = (top_info) ksi_malloc (sizeof *info);
  /* memset (info, 0, sizeof *info); */
  info->base.proc	= top_window_event_proc;
  info->base.type	= TOPLEVEL_WINDOW;
  info->base.superwin	= 1;
  info->base.x		= x;
  info->base.y		= y;
  info->base.w		= w;
  info->base.h		= h;
  info->top		= ksi_nil;
  info->bottom		= ksi_nil;
  info->right		= ksi_nil;
  info->left		= ksi_nil;
  info->right		= ksi_nil;

  wnd = ksi_append_window (joke_wintab, win_id);
  wnd->tk_data = (ksi_window_info) info;
  return wnd;
}


ksi_obj
ksi_tk_set_top_name (ksi_window wnd, ksi_obj name)
{
  KSI_CHECK ((ksi_obj) wnd, WIN_P (wnd),
	     "tk-set-top-name: invalid window in arg1");

  XStoreName (joke_dpy, wnd->wid, ksi_obj2name (name));
  return ksi_unspec;
}

ksi_obj
ksi_tk_set_top_icon_name (ksi_window wnd, ksi_obj name)
{
  KSI_CHECK ((ksi_obj) wnd, TOP_P (wnd),
	     "tk-set-top-icon-name: invalid top window in arg1");

  XSetIconName (joke_dpy, wnd->wid, ksi_obj2name (name));
  return ksi_unspec;
}

ksi_obj
ksi_tk_set_top_size (ksi_window wnd, ksi_obj w, ksi_obj h)
{
  int ww, hh;

  KSI_CHECK ((ksi_obj) wnd, TOP_P (wnd),
	     "tk-set-top-size: invalid top window in arg1");
  KSI_CHECK (w, KSI_EINT_P (w), "tk-set-top-size: invalid width in arg2");
  KSI_CHECK (h, KSI_EINT_P (h), "tk-set-top-size: invalid height in arg3");

  ww = KSI_SINT_COD (w);
  hh = KSI_SINT_COD (h);
  if (ww <= 0) ww = 1;
  if (hh <= 0) hh = 1;

  XResizeWindow (joke_dpy, wnd->wid, ww, hh);
  return ksi_unspec;
}

ksi_obj
ksi_tk_set_top_menu (ksi_window top, ksi_window menu)
{
  top_info info;

  KSI_CHECK ((ksi_obj) top, TOP_P (top),
	     "tk-set-top-menu: invalid top window in arg1");
  KSI_CHECK ((ksi_obj) menu, MENUBAR_P (menu),
	     "tk-set-top-menu: invalid menubar window in arg2");

  info = (top_info) top->tk_data;
  if (info->menu)
    XUnmapWindow (joke_dpy, info->menu->wid);

  info->menu = menu;
  if (top->tk_data->mapped)
    reorder_childs (top);

  XMapRaised (joke_dpy, menu->wid);
  if (info->status)
    XRaiseWindow (joke_dpy, info->status->wid);

  return ksi_unspec;
}

ksi_obj
ksi_tk_set_top_status (ksi_window top, ksi_window stat)
{
  top_info info;

  KSI_CHECK ((ksi_obj) top, TOP_P (top),
	     "tk-set-top-status: invalid top window in arg1");
  KSI_CHECK ((ksi_obj) stat, STATUSBAR_P (stat),
	     "tk-set-top-status: invalid statusbar window in arg2");

  info = (top_info) top->tk_data;

  if (info->status)
    XUnmapWindow (joke_dpy, info->status->wid);

  info->status = stat;
  if (top->tk_data->mapped)
    reorder_childs (top);

  XMapRaised (joke_dpy, stat->wid);

  return ksi_unspec;
}

ksi_obj
ksi_tk_set_top_main (ksi_window top, ksi_window wnd)
{
  top_info info;

  KSI_CHECK ((ksi_obj) top, TOP_P (top),
	     "tk-set-top-main: invalid top window in arg1");
  KSI_CHECK ((ksi_obj) wnd, WIN_P (wnd),
	     "tk-set-top-main: invalid window in arg2");

  info = (top_info) top->tk_data;

  if (info->main)
    XUnmapWindow (joke_dpy, info->main->wid);

  info->main = wnd;
  if (wnd->tk_data->mapped)
    reorder_childs (top);

  XMapWindow (joke_dpy, wnd->wid);
  XLowerWindow (joke_dpy, wnd->wid);

  return ksi_unspec;
}

ksi_obj
ksi_tk_set_top_left (ksi_window top, ksi_obj lst)
{
  top_info info;
  ksi_obj cur;

  KSI_CHECK ((ksi_obj) top, TOP_P (top),
	     "tk-set-top-left: invalid top window in arg1");

  for (cur = lst; cur != ksi_nil; cur = KSI_CDR (cur))
    {
      KSI_CHECK (lst, KSI_PAIR_P (cur),
		 "tk-set-top-left: invalid list in arg2");
      KSI_CHECK (KSI_CAR (cur), WIN_P (KSI_CAR (cur)),
		 "tk-set-top-left: invalid window");
    }

  info = (top_info) top->tk_data;

  for (cur = info->left; cur != ksi_nil; cur = KSI_CDR (cur))
    XUnmapWindow (joke_dpy, ((ksi_window) KSI_CAR (cur)) -> wid);

  info->left = lst;
  if (top->tk_data->mapped)
    reorder_childs (top);

  for (cur = lst; cur != ksi_nil; cur = KSI_CDR (cur))
    XMapWindow (joke_dpy,  ((ksi_window) KSI_CAR (cur)) -> wid);

  if (info->menu)
    XRaiseWindow (joke_dpy, info->menu->wid);
  if (info->status)
    XRaiseWindow (joke_dpy, info->status->wid);

  return ksi_unspec;
}

ksi_obj
ksi_tk_set_top_right (ksi_window top, ksi_obj lst)
{
  top_info info;
  ksi_obj cur;

  KSI_CHECK ((ksi_obj) top, TOP_P (top),
	     "tk-set-top-rigth: invalid top window in arg1");

  for (cur = lst; cur != ksi_nil; cur = KSI_CDR (cur))
    {
      KSI_CHECK (lst, KSI_PAIR_P (cur),
		 "tk-set-top-right: invalid list in arg2");
      KSI_CHECK (KSI_CAR (cur), WIN_P (KSI_CAR (cur)),
		 "tk-set-top-right: invalid window");
    }

  info = (top_info) top->tk_data;

  for (cur = info->right; cur != ksi_nil; cur = KSI_CDR (cur))
    XUnmapWindow (joke_dpy, ((ksi_window) KSI_CAR (cur)) -> wid);

  info->right = lst;
  if (top->tk_data->mapped)
    reorder_childs (top);

  for (cur = lst; cur != ksi_nil; cur = KSI_CDR (cur))
    XMapWindow (joke_dpy,  ((ksi_window) KSI_CAR (cur)) -> wid);

  if (info->menu)
    XRaiseWindow (joke_dpy, info->menu->wid);
  if (info->status)
    XRaiseWindow (joke_dpy, info->status->wid);

  return ksi_unspec;
}

ksi_obj
ksi_tk_set_top_top (ksi_window top, ksi_obj lst)
{
  top_info info;
  ksi_obj cur;

  KSI_CHECK ((ksi_obj) top, TOP_P (top),
	     "tk-set-top-top: invalid top window in arg1");

  for (cur = lst; cur != ksi_nil; cur = KSI_CDR (cur))
    {
      KSI_CHECK (lst, KSI_PAIR_P (cur),
		 "tk-set-top-top: invalid list in arg2");
      KSI_CHECK (KSI_CAR (cur), WIN_P (KSI_CAR (cur)),
		 "tk-set-top-top: invalid window");
    }

  info = (top_info) top->tk_data;

  for (cur = info->top; cur != ksi_nil; cur = KSI_CDR (cur))
    XUnmapWindow (joke_dpy, ((ksi_window) KSI_CAR (cur)) -> wid);

  info->top = lst;
  if (top->tk_data->mapped)
    reorder_childs (top);

  for (cur = lst; cur != ksi_nil; cur = KSI_CDR (cur))
    XMapWindow (joke_dpy,  ((ksi_window) KSI_CAR (cur)) -> wid);

  if (info->menu)
    XRaiseWindow (joke_dpy, info->menu->wid);
  if (info->status)
    XRaiseWindow (joke_dpy, info->status->wid);

  return ksi_unspec;
}

ksi_obj
ksi_tk_set_top_bottom (ksi_window top, ksi_obj lst)
{
  top_info info;
  ksi_obj cur;

  KSI_CHECK ((ksi_obj) top, TOP_P (top),
	     "tk-set-top-bottom: invalid top window in arg1");

  for (cur = lst; cur != ksi_nil; cur = KSI_CDR (cur))
    {
      KSI_CHECK (lst, KSI_PAIR_P (cur),
		 "tk-set-top-bottom: invalid list in arg2");
      KSI_CHECK (KSI_CAR (cur), WIN_P (KSI_CAR (cur)),
		 "tk-set-top-bottom: invalid window");
    }

  info = (top_info) top->tk_data;

  for (cur = info->bottom; cur != ksi_nil; cur = KSI_CDR (cur))
    XUnmapWindow (joke_dpy, ((ksi_window) KSI_CAR (cur)) -> wid);

  info->bottom = lst;
  if (top->tk_data->mapped)
    reorder_childs (top);

  for (cur = lst; cur != ksi_nil; cur = KSI_CDR (cur))
    XMapWindow (joke_dpy,  ((ksi_window) KSI_CAR (cur)) -> wid);

  if (info->menu)
    XRaiseWindow (joke_dpy, info->menu->wid);
  if (info->status)
    XRaiseWindow (joke_dpy, info->status->wid);

  return ksi_unspec;
}

ksi_obj
ksi_tk_map_top (ksi_obj wnd)
{
  KSI_CHECK (wnd, TOP_P (wnd), "tk-map-top: invalid top window in arg1");

  if (!((ksi_window) wnd)->tk_data->mapped)
    {
      reorder_childs ((ksi_window) wnd);
      XMapWindow (joke_dpy, ((ksi_window) wnd) -> wid);
    }

  return ksi_unspec;
}

ksi_obj
ksi_tk_unmap_top (ksi_obj wnd)
{
  KSI_CHECK (wnd, TOP_P (wnd), "tk-unmap-top: invalid top window in arg1");

  if (((ksi_window) wnd)->tk_data->mapped)
    {
      XUnmapWindow (joke_dpy, ((ksi_window) wnd) -> wid);
    }

  return ksi_unspec;
}


static struct Ksi_Prim_Def defs [] =
{
  {"tk-create-top",		ksi_tk_create_top,	KSI_CALL_ARG1, 0},
  {"tk-set-top-name",		ksi_tk_set_top_name,	KSI_CALL_ARG2, 2},
  {"tk-set-top-icon-name",	ksi_tk_set_top_icon_name,KSI_CALL_ARG2, 2},
  {"tk-set-top-size",		ksi_tk_set_top_size,	KSI_CALL_ARG3, 3},
  {"tk-set-top-menu",		ksi_tk_set_top_menu,	KSI_CALL_ARG2, 2},
  {"tk-set-top-status",		ksi_tk_set_top_status,	KSI_CALL_ARG2, 2},
  {"tk-set-top-main",		ksi_tk_set_top_main,	KSI_CALL_ARG2, 2},
  {"tk-set-top-left",		ksi_tk_set_top_left,	KSI_CALL_ARG2, 2},
  {"tk-set-top-right",		ksi_tk_set_top_right,	KSI_CALL_ARG2, 2},
  {"tk-set-top-top",		ksi_tk_set_top_top,	KSI_CALL_ARG2, 2},
  {"tk-set-top-bottom",		ksi_tk_set_top_bottom,	KSI_CALL_ARG2, 2},

  { "tk-map-top",		ksi_tk_map_top,		KSI_CALL_ARG1, 1 },
  { "tk-unmap-top",		ksi_tk_unmap_top,	KSI_CALL_ARG1, 1 },

  { 0 }
};

void
ksi_tk_init_topwin ()
{
  char *tmp;

  ignore (rcsid);
  ksi_reg_unit (defs);

  tmp = ksi_tk_string_resource ("topLevel.background",
				"TopLevel.Background",
				"#bfbfbf");

  joke_tk->top_back = ksi_tk_alloc_color_3d (tmp);
}


 /* End of code */
