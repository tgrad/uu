# Install script for directory: C:/Users/ivan/Source/uu/pcre-8.40

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "C:/Users/ivan/Source/uu")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY OPTIONAL FILES "C:/Users/ivan/Source/uu/pcre-build/Debug/pcred.lib")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY OPTIONAL FILES "C:/Users/ivan/Source/uu/pcre-build/Release/pcre.lib")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Mm][Ii][Nn][Ss][Ii][Zz][Ee][Rr][Ee][Ll])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY OPTIONAL FILES "C:/Users/ivan/Source/uu/pcre-build/MinSizeRel/pcre.lib")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY OPTIONAL FILES "C:/Users/ivan/Source/uu/pcre-build/RelWithDebInfo/pcre.lib")
  endif()
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE SHARED_LIBRARY FILES "C:/Users/ivan/Source/uu/pcre-build/Debug/pcred.dll")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE SHARED_LIBRARY FILES "C:/Users/ivan/Source/uu/pcre-build/Release/pcre.dll")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Mm][Ii][Nn][Ss][Ii][Zz][Ee][Rr][Ee][Ll])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE SHARED_LIBRARY FILES "C:/Users/ivan/Source/uu/pcre-build/MinSizeRel/pcre.dll")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE SHARED_LIBRARY FILES "C:/Users/ivan/Source/uu/pcre-build/RelWithDebInfo/pcre.dll")
  endif()
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY OPTIONAL FILES "C:/Users/ivan/Source/uu/pcre-build/Debug/pcreposixd.lib")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY OPTIONAL FILES "C:/Users/ivan/Source/uu/pcre-build/Release/pcreposix.lib")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Mm][Ii][Nn][Ss][Ii][Zz][Ee][Rr][Ee][Ll])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY OPTIONAL FILES "C:/Users/ivan/Source/uu/pcre-build/MinSizeRel/pcreposix.lib")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY OPTIONAL FILES "C:/Users/ivan/Source/uu/pcre-build/RelWithDebInfo/pcreposix.lib")
  endif()
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE SHARED_LIBRARY FILES "C:/Users/ivan/Source/uu/pcre-build/Debug/pcreposixd.dll")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE SHARED_LIBRARY FILES "C:/Users/ivan/Source/uu/pcre-build/Release/pcreposix.dll")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Mm][Ii][Nn][Ss][Ii][Zz][Ee][Rr][Ee][Ll])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE SHARED_LIBRARY FILES "C:/Users/ivan/Source/uu/pcre-build/MinSizeRel/pcreposix.dll")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE SHARED_LIBRARY FILES "C:/Users/ivan/Source/uu/pcre-build/RelWithDebInfo/pcreposix.dll")
  endif()
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY OPTIONAL FILES "C:/Users/ivan/Source/uu/pcre-build/Debug/pcrecppd.lib")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY OPTIONAL FILES "C:/Users/ivan/Source/uu/pcre-build/Release/pcrecpp.lib")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Mm][Ii][Nn][Ss][Ii][Zz][Ee][Rr][Ee][Ll])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY OPTIONAL FILES "C:/Users/ivan/Source/uu/pcre-build/MinSizeRel/pcrecpp.lib")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY OPTIONAL FILES "C:/Users/ivan/Source/uu/pcre-build/RelWithDebInfo/pcrecpp.lib")
  endif()
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE SHARED_LIBRARY FILES "C:/Users/ivan/Source/uu/pcre-build/Debug/pcrecppd.dll")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE SHARED_LIBRARY FILES "C:/Users/ivan/Source/uu/pcre-build/Release/pcrecpp.dll")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Mm][Ii][Nn][Ss][Ii][Zz][Ee][Rr][Ee][Ll])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE SHARED_LIBRARY FILES "C:/Users/ivan/Source/uu/pcre-build/MinSizeRel/pcrecpp.dll")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE SHARED_LIBRARY FILES "C:/Users/ivan/Source/uu/pcre-build/RelWithDebInfo/pcrecpp.dll")
  endif()
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE FILES "C:/Users/ivan/Source/uu/pcre-build/Debug/pcregrep.exe")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE FILES "C:/Users/ivan/Source/uu/pcre-build/Release/pcregrep.exe")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Mm][Ii][Nn][Ss][Ii][Zz][Ee][Rr][Ee][Ll])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE FILES "C:/Users/ivan/Source/uu/pcre-build/MinSizeRel/pcregrep.exe")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE FILES "C:/Users/ivan/Source/uu/pcre-build/RelWithDebInfo/pcregrep.exe")
  endif()
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE FILES "C:/Users/ivan/Source/uu/pcre-build/Debug/pcretest.exe")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE FILES "C:/Users/ivan/Source/uu/pcre-build/Release/pcretest.exe")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Mm][Ii][Nn][Ss][Ii][Zz][Ee][Rr][Ee][Ll])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE FILES "C:/Users/ivan/Source/uu/pcre-build/MinSizeRel/pcretest.exe")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE FILES "C:/Users/ivan/Source/uu/pcre-build/RelWithDebInfo/pcretest.exe")
  endif()
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE FILES "C:/Users/ivan/Source/uu/pcre-build/Debug/pcrecpp_unittest.exe")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE FILES "C:/Users/ivan/Source/uu/pcre-build/Release/pcrecpp_unittest.exe")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Mm][Ii][Nn][Ss][Ii][Zz][Ee][Rr][Ee][Ll])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE FILES "C:/Users/ivan/Source/uu/pcre-build/MinSizeRel/pcrecpp_unittest.exe")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE FILES "C:/Users/ivan/Source/uu/pcre-build/RelWithDebInfo/pcrecpp_unittest.exe")
  endif()
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE FILES "C:/Users/ivan/Source/uu/pcre-build/Debug/pcre_scanner_unittest.exe")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE FILES "C:/Users/ivan/Source/uu/pcre-build/Release/pcre_scanner_unittest.exe")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Mm][Ii][Nn][Ss][Ii][Zz][Ee][Rr][Ee][Ll])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE FILES "C:/Users/ivan/Source/uu/pcre-build/MinSizeRel/pcre_scanner_unittest.exe")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE FILES "C:/Users/ivan/Source/uu/pcre-build/RelWithDebInfo/pcre_scanner_unittest.exe")
  endif()
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE FILES "C:/Users/ivan/Source/uu/pcre-build/Debug/pcre_stringpiece_unittest.exe")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE FILES "C:/Users/ivan/Source/uu/pcre-build/Release/pcre_stringpiece_unittest.exe")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Mm][Ii][Nn][Ss][Ii][Zz][Ee][Rr][Ee][Ll])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE FILES "C:/Users/ivan/Source/uu/pcre-build/MinSizeRel/pcre_stringpiece_unittest.exe")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE FILES "C:/Users/ivan/Source/uu/pcre-build/RelWithDebInfo/pcre_stringpiece_unittest.exe")
  endif()
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include" TYPE FILE FILES
    "C:/Users/ivan/Source/uu/pcre-build/pcre.h"
    "C:/Users/ivan/Source/uu/pcre-8.40/pcreposix.h"
    )
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include" TYPE FILE FILES
    "C:/Users/ivan/Source/uu/pcre-8.40/pcrecpp.h"
    "C:/Users/ivan/Source/uu/pcre-8.40/pcre_scanner.h"
    "C:/Users/ivan/Source/uu/pcre-build/pcrecpparg.h"
    "C:/Users/ivan/Source/uu/pcre-build/pcre_stringpiece.h"
    )
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/man/man1" TYPE FILE FILES
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/pcre-config.1"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/pcregrep.1"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/pcretest.1"
    )
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/man/man3" TYPE FILE FILES
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/pcre.3"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/pcre16.3"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/pcre32.3"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/pcre_assign_jit_stack.3"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/pcre_compile.3"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/pcre_compile2.3"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/pcre_config.3"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/pcre_copy_named_substring.3"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/pcre_copy_substring.3"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/pcre_dfa_exec.3"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/pcre_exec.3"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/pcre_free_study.3"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/pcre_free_substring.3"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/pcre_free_substring_list.3"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/pcre_fullinfo.3"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/pcre_get_named_substring.3"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/pcre_get_stringnumber.3"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/pcre_get_stringtable_entries.3"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/pcre_get_substring.3"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/pcre_get_substring_list.3"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/pcre_jit_exec.3"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/pcre_jit_stack_alloc.3"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/pcre_jit_stack_free.3"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/pcre_maketables.3"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/pcre_pattern_to_host_byte_order.3"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/pcre_refcount.3"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/pcre_study.3"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/pcre_utf16_to_host_byte_order.3"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/pcre_utf32_to_host_byte_order.3"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/pcre_version.3"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/pcreapi.3"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/pcrebuild.3"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/pcrecallout.3"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/pcrecompat.3"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/pcrecpp.3"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/pcredemo.3"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/pcrejit.3"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/pcrelimits.3"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/pcrematching.3"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/pcrepartial.3"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/pcrepattern.3"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/pcreperform.3"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/pcreposix.3"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/pcreprecompile.3"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/pcresample.3"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/pcrestack.3"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/pcresyntax.3"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/pcreunicode.3"
    )
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/doc/pcre/html" TYPE FILE FILES
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/html/index.html"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/html/pcre-config.html"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/html/pcre.html"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/html/pcre16.html"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/html/pcre32.html"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/html/pcre_assign_jit_stack.html"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/html/pcre_compile.html"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/html/pcre_compile2.html"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/html/pcre_config.html"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/html/pcre_copy_named_substring.html"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/html/pcre_copy_substring.html"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/html/pcre_dfa_exec.html"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/html/pcre_exec.html"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/html/pcre_free_study.html"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/html/pcre_free_substring.html"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/html/pcre_free_substring_list.html"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/html/pcre_fullinfo.html"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/html/pcre_get_named_substring.html"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/html/pcre_get_stringnumber.html"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/html/pcre_get_stringtable_entries.html"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/html/pcre_get_substring.html"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/html/pcre_get_substring_list.html"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/html/pcre_jit_exec.html"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/html/pcre_jit_stack_alloc.html"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/html/pcre_jit_stack_free.html"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/html/pcre_maketables.html"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/html/pcre_pattern_to_host_byte_order.html"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/html/pcre_refcount.html"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/html/pcre_study.html"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/html/pcre_utf16_to_host_byte_order.html"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/html/pcre_utf32_to_host_byte_order.html"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/html/pcre_version.html"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/html/pcreapi.html"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/html/pcrebuild.html"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/html/pcrecallout.html"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/html/pcrecompat.html"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/html/pcrecpp.html"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/html/pcredemo.html"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/html/pcregrep.html"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/html/pcrejit.html"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/html/pcrelimits.html"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/html/pcrematching.html"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/html/pcrepartial.html"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/html/pcrepattern.html"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/html/pcreperform.html"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/html/pcreposix.html"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/html/pcreprecompile.html"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/html/pcresample.html"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/html/pcrestack.html"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/html/pcresyntax.html"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/html/pcretest.html"
    "C:/Users/ivan/Source/uu/pcre-8.40/doc/html/pcreunicode.html"
    )
endif()

if(CMAKE_INSTALL_COMPONENT)
  set(CMAKE_INSTALL_MANIFEST "install_manifest_${CMAKE_INSTALL_COMPONENT}.txt")
else()
  set(CMAKE_INSTALL_MANIFEST "install_manifest.txt")
endif()

string(REPLACE ";" "\n" CMAKE_INSTALL_MANIFEST_CONTENT
       "${CMAKE_INSTALL_MANIFEST_FILES}")
file(WRITE "C:/Users/ivan/Source/uu/pcre-build/${CMAKE_INSTALL_MANIFEST}"
     "${CMAKE_INSTALL_MANIFEST_CONTENT}")
