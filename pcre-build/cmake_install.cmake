# Install script for directory: U:/uu/pcre-8.40

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "U:/uu")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY OPTIONAL FILES "U:/uu/pcre-build/Release/pcre.lib")
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE SHARED_LIBRARY FILES "U:/uu/pcre-build/Release/pcre.dll")
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY OPTIONAL FILES "U:/uu/pcre-build/Release/pcreposix.lib")
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE SHARED_LIBRARY FILES "U:/uu/pcre-build/Release/pcreposix.dll")
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY OPTIONAL FILES "U:/uu/pcre-build/Release/pcrecpp.lib")
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE SHARED_LIBRARY FILES "U:/uu/pcre-build/Release/pcrecpp.dll")
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE FILES "U:/uu/pcre-build/Release/pcregrep.exe")
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE FILES "U:/uu/pcre-build/Release/pcretest.exe")
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE FILES "U:/uu/pcre-build/Release/pcrecpp_unittest.exe")
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE FILES "U:/uu/pcre-build/Release/pcre_scanner_unittest.exe")
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE FILES "U:/uu/pcre-build/Release/pcre_stringpiece_unittest.exe")
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include" TYPE FILE FILES
    "U:/uu/pcre-build/pcre.h"
    "U:/uu/pcre-8.40/pcreposix.h"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include" TYPE FILE FILES
    "U:/uu/pcre-8.40/pcrecpp.h"
    "U:/uu/pcre-8.40/pcre_scanner.h"
    "U:/uu/pcre-build/pcrecpparg.h"
    "U:/uu/pcre-build/pcre_stringpiece.h"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/man/man1" TYPE FILE FILES
    "U:/uu/pcre-8.40/doc/pcre-config.1"
    "U:/uu/pcre-8.40/doc/pcregrep.1"
    "U:/uu/pcre-8.40/doc/pcretest.1"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/man/man3" TYPE FILE FILES
    "U:/uu/pcre-8.40/doc/pcre.3"
    "U:/uu/pcre-8.40/doc/pcre16.3"
    "U:/uu/pcre-8.40/doc/pcre32.3"
    "U:/uu/pcre-8.40/doc/pcre_assign_jit_stack.3"
    "U:/uu/pcre-8.40/doc/pcre_compile.3"
    "U:/uu/pcre-8.40/doc/pcre_compile2.3"
    "U:/uu/pcre-8.40/doc/pcre_config.3"
    "U:/uu/pcre-8.40/doc/pcre_copy_named_substring.3"
    "U:/uu/pcre-8.40/doc/pcre_copy_substring.3"
    "U:/uu/pcre-8.40/doc/pcre_dfa_exec.3"
    "U:/uu/pcre-8.40/doc/pcre_exec.3"
    "U:/uu/pcre-8.40/doc/pcre_free_study.3"
    "U:/uu/pcre-8.40/doc/pcre_free_substring.3"
    "U:/uu/pcre-8.40/doc/pcre_free_substring_list.3"
    "U:/uu/pcre-8.40/doc/pcre_fullinfo.3"
    "U:/uu/pcre-8.40/doc/pcre_get_named_substring.3"
    "U:/uu/pcre-8.40/doc/pcre_get_stringnumber.3"
    "U:/uu/pcre-8.40/doc/pcre_get_stringtable_entries.3"
    "U:/uu/pcre-8.40/doc/pcre_get_substring.3"
    "U:/uu/pcre-8.40/doc/pcre_get_substring_list.3"
    "U:/uu/pcre-8.40/doc/pcre_jit_exec.3"
    "U:/uu/pcre-8.40/doc/pcre_jit_stack_alloc.3"
    "U:/uu/pcre-8.40/doc/pcre_jit_stack_free.3"
    "U:/uu/pcre-8.40/doc/pcre_maketables.3"
    "U:/uu/pcre-8.40/doc/pcre_pattern_to_host_byte_order.3"
    "U:/uu/pcre-8.40/doc/pcre_refcount.3"
    "U:/uu/pcre-8.40/doc/pcre_study.3"
    "U:/uu/pcre-8.40/doc/pcre_utf16_to_host_byte_order.3"
    "U:/uu/pcre-8.40/doc/pcre_utf32_to_host_byte_order.3"
    "U:/uu/pcre-8.40/doc/pcre_version.3"
    "U:/uu/pcre-8.40/doc/pcreapi.3"
    "U:/uu/pcre-8.40/doc/pcrebuild.3"
    "U:/uu/pcre-8.40/doc/pcrecallout.3"
    "U:/uu/pcre-8.40/doc/pcrecompat.3"
    "U:/uu/pcre-8.40/doc/pcrecpp.3"
    "U:/uu/pcre-8.40/doc/pcredemo.3"
    "U:/uu/pcre-8.40/doc/pcrejit.3"
    "U:/uu/pcre-8.40/doc/pcrelimits.3"
    "U:/uu/pcre-8.40/doc/pcrematching.3"
    "U:/uu/pcre-8.40/doc/pcrepartial.3"
    "U:/uu/pcre-8.40/doc/pcrepattern.3"
    "U:/uu/pcre-8.40/doc/pcreperform.3"
    "U:/uu/pcre-8.40/doc/pcreposix.3"
    "U:/uu/pcre-8.40/doc/pcreprecompile.3"
    "U:/uu/pcre-8.40/doc/pcresample.3"
    "U:/uu/pcre-8.40/doc/pcrestack.3"
    "U:/uu/pcre-8.40/doc/pcresyntax.3"
    "U:/uu/pcre-8.40/doc/pcreunicode.3"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/doc/pcre/html" TYPE FILE FILES
    "U:/uu/pcre-8.40/doc/html/index.html"
    "U:/uu/pcre-8.40/doc/html/pcre-config.html"
    "U:/uu/pcre-8.40/doc/html/pcre.html"
    "U:/uu/pcre-8.40/doc/html/pcre16.html"
    "U:/uu/pcre-8.40/doc/html/pcre32.html"
    "U:/uu/pcre-8.40/doc/html/pcre_assign_jit_stack.html"
    "U:/uu/pcre-8.40/doc/html/pcre_compile.html"
    "U:/uu/pcre-8.40/doc/html/pcre_compile2.html"
    "U:/uu/pcre-8.40/doc/html/pcre_config.html"
    "U:/uu/pcre-8.40/doc/html/pcre_copy_named_substring.html"
    "U:/uu/pcre-8.40/doc/html/pcre_copy_substring.html"
    "U:/uu/pcre-8.40/doc/html/pcre_dfa_exec.html"
    "U:/uu/pcre-8.40/doc/html/pcre_exec.html"
    "U:/uu/pcre-8.40/doc/html/pcre_free_study.html"
    "U:/uu/pcre-8.40/doc/html/pcre_free_substring.html"
    "U:/uu/pcre-8.40/doc/html/pcre_free_substring_list.html"
    "U:/uu/pcre-8.40/doc/html/pcre_fullinfo.html"
    "U:/uu/pcre-8.40/doc/html/pcre_get_named_substring.html"
    "U:/uu/pcre-8.40/doc/html/pcre_get_stringnumber.html"
    "U:/uu/pcre-8.40/doc/html/pcre_get_stringtable_entries.html"
    "U:/uu/pcre-8.40/doc/html/pcre_get_substring.html"
    "U:/uu/pcre-8.40/doc/html/pcre_get_substring_list.html"
    "U:/uu/pcre-8.40/doc/html/pcre_jit_exec.html"
    "U:/uu/pcre-8.40/doc/html/pcre_jit_stack_alloc.html"
    "U:/uu/pcre-8.40/doc/html/pcre_jit_stack_free.html"
    "U:/uu/pcre-8.40/doc/html/pcre_maketables.html"
    "U:/uu/pcre-8.40/doc/html/pcre_pattern_to_host_byte_order.html"
    "U:/uu/pcre-8.40/doc/html/pcre_refcount.html"
    "U:/uu/pcre-8.40/doc/html/pcre_study.html"
    "U:/uu/pcre-8.40/doc/html/pcre_utf16_to_host_byte_order.html"
    "U:/uu/pcre-8.40/doc/html/pcre_utf32_to_host_byte_order.html"
    "U:/uu/pcre-8.40/doc/html/pcre_version.html"
    "U:/uu/pcre-8.40/doc/html/pcreapi.html"
    "U:/uu/pcre-8.40/doc/html/pcrebuild.html"
    "U:/uu/pcre-8.40/doc/html/pcrecallout.html"
    "U:/uu/pcre-8.40/doc/html/pcrecompat.html"
    "U:/uu/pcre-8.40/doc/html/pcrecpp.html"
    "U:/uu/pcre-8.40/doc/html/pcredemo.html"
    "U:/uu/pcre-8.40/doc/html/pcregrep.html"
    "U:/uu/pcre-8.40/doc/html/pcrejit.html"
    "U:/uu/pcre-8.40/doc/html/pcrelimits.html"
    "U:/uu/pcre-8.40/doc/html/pcrematching.html"
    "U:/uu/pcre-8.40/doc/html/pcrepartial.html"
    "U:/uu/pcre-8.40/doc/html/pcrepattern.html"
    "U:/uu/pcre-8.40/doc/html/pcreperform.html"
    "U:/uu/pcre-8.40/doc/html/pcreposix.html"
    "U:/uu/pcre-8.40/doc/html/pcreprecompile.html"
    "U:/uu/pcre-8.40/doc/html/pcresample.html"
    "U:/uu/pcre-8.40/doc/html/pcrestack.html"
    "U:/uu/pcre-8.40/doc/html/pcresyntax.html"
    "U:/uu/pcre-8.40/doc/html/pcretest.html"
    "U:/uu/pcre-8.40/doc/html/pcreunicode.html"
    )
endif()

if(CMAKE_INSTALL_COMPONENT)
  set(CMAKE_INSTALL_MANIFEST "install_manifest_${CMAKE_INSTALL_COMPONENT}.txt")
else()
  set(CMAKE_INSTALL_MANIFEST "install_manifest.txt")
endif()

string(REPLACE ";" "\n" CMAKE_INSTALL_MANIFEST_CONTENT
       "${CMAKE_INSTALL_MANIFEST_FILES}")
file(WRITE "U:/uu/pcre-build/${CMAKE_INSTALL_MANIFEST}"
     "${CMAKE_INSTALL_MANIFEST_CONTENT}")
