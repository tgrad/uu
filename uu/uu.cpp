
#include "uu.h"

#include <QEventLoop>
#include <QAbstractEventDispatcher>
#include <QWidget>
#include <QMessageBox>

#include <maksi/maksi.h>
#include <maksi/engine.h>
#include <maksi/maksi_ksi.h>


namespace uu {

QtApp *QtApp::qtApp = 0;

CWinApp *QtApp::mfc_app = 0;
char **QtApp::mfc_argv;
int QtApp::mfc_argc;

QFile *QtApp::m_logfile = 0;
QTextStream *QtApp::m_log = 0;

Q_GLOBAL_STATIC(QMfcAppEventFilter, qmfcEventFilter);


static maksi_t m_maksi = 0;
static ksi_env m_topenv = 0;


maksi_t maksi()
{
    return m_maksi;
}


ksi_env ksi_topenv()
{
    return m_topenv;
}


QMfcAppEventFilter::QMfcAppEventFilter() : QAbstractNativeEventFilter()
{
}

bool QMfcAppEventFilter::nativeEventFilter(const QByteArray &, void *message, long *result)
{
    return QtApp::qtApp->winEventFilter((MSG*)message, result);
}


QtApp::QtApp(CWinApp *mfcApp, int &argc, char **argv)
    : QApplication(argc, argv), idleCount(0), doIdle(false)
{
    QAbstractEventDispatcher::instance()->installNativeEventFilter(qmfcEventFilter());
    setQuitOnLastWindowClosed(false);
}

QtApp::~QtApp()
{
    mfc_app = 0;
    for (int a = 0; a < mfc_argc; ++a) {
        char *arg = mfc_argv[a];
        delete[] arg;
    }
    delete[]mfc_argv;
    mfc_argv = 0;

    if (m_maksi) {
	maksi_freeInstance(m_maksi);
	m_maksi = 0;
	m_topenv = 0;
    }
    if (m_log) {
	delete m_log;
	m_log = 0;
    }
    if (m_logfile) {
	delete m_logfile;
	m_logfile = 0;
    }
}

void QtApp::message(int lev, const QString &msg)
{
    if (m_log) {
	switch (lev) {
	case maksi::MESSAGE_ALARM:
	    (*m_log) << "ALARM   " << msg << endl;
	    break;
	case maksi::MESSAGE_ERROR:
	    (*m_log) << "ERROR   " << msg << endl;
	    break;
	case maksi::MESSAGE_WARN:
	    (*m_log) << "WARNING " << msg << endl;
	    break;
	case maksi::MESSAGE_NOTE:
	    (*m_log) << "NOTICE  " << msg << endl;
	    break;
	case maksi::MESSAGE_INFO:
	    (*m_log) << "INFO    " << msg << endl;
	    break;
	case maksi::MESSAGE_DEBUG:
	    (*m_log) << "DEBUG   " << msg << endl;
	    break;
	case maksi::MESSAGE_DEBUG_CORE:
	    (*m_log) << "DEBUG*  " << msg << endl;
	    break;
	default:
	    break;
	}
	m_log->flush();
    }
    if (lev <= maksi::MESSAGE_WARN) {
        QMessageBox box;
	box.setText(msg);
	box.setInformativeText("Do you want to continue?");
	box.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
	box.setDefaultButton(QMessageBox::Yes);
        if (lev <= maksi::MESSAGE_ERROR) {
            box.setIcon(QMessageBox::Critical);
        } else if (lev <= maksi::MESSAGE_WARN) {
            box.setIcon(QMessageBox::Warning);
        } else {
            box.setIcon(QMessageBox::Information);
        }
	int ret = box.exec();
	if (ret == QMessageBox::No) {
	    qApp->exit(1);
	}
    }
}

QtApp *QtApp::create(CWinApp *mfcApp, const char *logfile, const char *initfile)
{
    if (!mfcApp) {
	return 0;
    }
    mfc_app = mfcApp;

#if defined(UNICODE)
    QString exeName((QChar*)mfc_app->m_pszExeName, wcslen(mfc_app->m_pszExeName));
    QString cmdLine((QChar*)mfc_app->m_lpCmdLine, wcslen(mfc_app->m_lpCmdLine));
#else
    QString exeName = QString::fromLocal8Bit(mfc_app->m_pszExeName);
    QString cmdLine = QString::fromLocal8Bit(mfc_app->m_lpCmdLine);
#endif
    QStringList arglist = QString(exeName + " " + cmdLine).split(' ');

    mfc_argc = arglist.count();
    mfc_argv = new char*[mfc_argc + 1];
    int a;
    for (a = 0; a < mfc_argc; ++a) {
	QString arg = arglist[a];
	mfc_argv[a] = new char[arg.length() + 1];
	qstrcpy(mfc_argv[a], arg.toLocal8Bit().data());
    }
    mfc_argv[a] = 0;

    qtApp = new QtApp(mfcApp, mfc_argc, mfc_argv);

    if (logfile) {
	QString name = QString::fromLocal8Bit(logfile);
	name.replace(QChar('\\'), QChar('/'));
	m_logfile = new QFile(name);
	if (m_logfile->open(QIODevice::WriteOnly |QIODevice::Truncate | QIODevice::Text)) {
	    m_log = new QTextStream(m_logfile);
	}
    }

    m_maksi = maksi_newInstance("maksi");
    maksi_setMessageHandler(m_maksi, message);
    maksi_loadConfig(m_maksi, QString::fromLocal8Bit(initfile));

    maksi::Engine *me = m_maksi->engine(QString::fromLocal8Bit("ksi"));
    if (me) {
	maksi::EngineInternal *eni = me->getInternal();
	if (eni) {
	    m_topenv = ((maksi::Ksi_MaksiEngineInternal *) eni)->env();
	}
    }

    return qtApp;
}

int QtApp::run()
{
    int result = qtApp->exec();
    return result;
}

bool QtApp::winEventFilter(MSG *msg, long *result)
{
    static bool recursion = false;
    if (recursion)
        return false;
    recursion = true;

    QWidget *widget = QWidget::find((WId)msg->hwnd);
    HWND toplevel = 0;
    if (widget) {
        HWND parent = (HWND)widget->winId();
        while(parent) {
            toplevel = parent;
            parent = GetParent(parent);
        }
        HMENU menu = toplevel ? GetMenu(toplevel) : 0;
        if (menu && GetFocus() == msg->hwnd) {
            if (msg->message == WM_SYSKEYUP && msg->wParam == VK_MENU) {
                // activate menubar on Alt-up and move focus away
                SetFocus(toplevel);
                SendMessage(toplevel, msg->message, msg->wParam, msg->lParam);
                widget->setFocus();
                recursion = false;
                return true;
            } else if (msg->message == WM_SYSKEYDOWN && msg->wParam != VK_MENU) {
                SendMessage(toplevel, msg->message, msg->wParam, msg->lParam);
                SendMessage(toplevel, WM_SYSKEYUP, VK_MENU, msg->lParam);
                recursion = false;
                return true;
            }
        }
    } else if (mfc_app) {
        MSG tmp;
        while (doIdle && !PeekMessage(&tmp, 0, 0, 0, PM_NOREMOVE)) {
            if (!mfc_app->OnIdle(idleCount++))
                doIdle = false;
        }
        if (mfc_app->IsIdleMessage(msg)) {
            doIdle = true;
            idleCount = 0;
        }
    }
    if (mfc_app && mfc_app->PreTranslateMessage(msg)) {
        recursion = false;
	return true;
    }

    recursion = false;
    return false;
}


};
