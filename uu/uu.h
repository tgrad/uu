#pragma once

#include <QApplication>
#include <QAbstractNativeEventFilter>
#include <QFile>
#include <QTextStream>

#include <afxwin.h>

#include <maksi/iface.h>

#include <ksi/ksi_env.h>

#pragma comment(lib, "maksi")
#pragma comment(lib, "Qt5Core")
#pragma comment(lib, "Qt5Gui")
#pragma comment(lib, "Qt5Widgets")


namespace uu {


class QMfcAppEventFilter : public QAbstractNativeEventFilter
{
public:
    QMfcAppEventFilter();
    bool nativeEventFilter(const QByteArray &eventType, void *message, long *result);
};


class QtApp : public QApplication
{
    QtApp(CWinApp *mfcApp, int &argc, char **argv);

public:
    ~QtApp();

    static QtApp *create(CWinApp *mfcApp, const char *logfile, const char *initfile);
    static int run();

    bool winEventFilter(MSG *msg, long *result);

    static QtApp *qtApp;

private:
    static void message(int lev, const QString &msg);

    static CWinApp *mfc_app;
    static char ** mfc_argv;
    static int mfc_argc;

    static QFile *m_logfile;
    static QTextStream *m_log;

    int idleCount;
    bool doIdle;
};


maksi_t maksi();

ksi_env ksi_topenv();


};
